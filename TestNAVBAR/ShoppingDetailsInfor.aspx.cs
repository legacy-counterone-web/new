﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR
{
    public partial class ShoppingDetailsInfor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ReportDocument cryRpt = new ReportDocument();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;


            DataTable dt = new DataTable();
            string SQL = "select * from vw_shoppingdetails " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            //string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }


            string reportPath = Server.MapPath("~/aspnet_client/shoppingdetails.rpt");
            cryRpt.Load(reportPath);
            cryRpt.SetDataSource(dt);
            string ServerName = "";
            string DatabaseName = "";
            string UserID = "";
            string password = "";

            ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
            DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
            UserID = System.Configuration.ConfigurationManager.AppSettings["UserID"];
            password = System.Configuration.ConfigurationManager.AppSettings["password"];



            crConnectionInfo.ServerName = ServerName;
            crConnectionInfo.DatabaseName = DatabaseName;
            crConnectionInfo.UserID = UserID;
            crConnectionInfo.Password = password;

            CrTables = cryRpt.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }
            
            ShoppingDetailsViewer.ReportSource = cryRpt;
            ShoppingDetailsViewer.Zoom(75); 
            ShoppingDetailsViewer.HasPageNavigationButtons = true;
            ShoppingDetailsViewer.RefreshReport();

        }
    }
}