﻿
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public partial class UnpaidLoansInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "select * FROM   vw_UnpaidLoan  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument UnpaidLoans = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/UnpaidLoansData.rpt");
            UnpaidLoans.Load(reportPath);
            UnpaidLoans.SetDataSource(dt);
            UnpaidLoansViewer.ReportSource = UnpaidLoans;
            UnpaidLoansViewer.Zoom(75);
            UnpaidLoansViewer.HasToggleGroupTreeButton = false;
            UnpaidLoansViewer.HasPageNavigationButtons = true;
            UnpaidLoansViewer.HasRefreshButton = true;
            UnpaidLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            UnpaidLoansViewer.RefreshReport();

        }
    }
}
//using CrystalDecisions.CrystalReports.Engine;
//using System;
//using System.Configuration;
//using System.Data;
//using System.Data.SqlClient;
//using System.Text.RegularExpressions;

//namespace TestNAVBAR.Models
//{
//    public partial class UnpaidLoansInfo : System.Web.UI.Page
//    {
//        protected void Page_Load(object sender, EventArgs e)
//        {
//            string dateRange_ = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
//            string _DateFrom = dateRange_.Replace("(", string.Empty).Replace(")", string.Empty);
//            string[] _Begin = Regex.Split(_DateFrom, "AND");
//            string[] _datefrom = Regex.Split(_Begin[0], ">=");
//            string datefrom = _datefrom[1].Replace("'", string.Empty);
//            string[] _dateto = Regex.Split(_Begin[1], "<=");
//            string dateto = _dateto[1].Replace("'", string.Empty);
//            DataTable dt;

//           // DataTable dt = new DataTable();
//            string SQL = "select * FROM   vw_UnpaidLoan  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
//            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
//            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
//            using (SqlConnection conn = new SqlConnection(sConstr))
//            {
//                using (SqlCommand comm = new SqlCommand(SQL, conn))
//                {
//                    conn.Open();
//                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
//                    {
//                        dt = new DataTable("tbl");
//                        da.Fill(dt);
//                    }
//                }
//            }

//            ReportDocument UnpaidLoans = new ReportDocument();
//            string reportPath = Server.MapPath("~/aspnet_client/UnpaidLoansData.rpt");
//            UnpaidLoans.Load(reportPath);
//            UnpaidLoans.SetDataSource(dt);
//            UnpaidLoans.SetParameterValue("mdate", "From: " + datefrom + "\n    To: " + dateto); // Add multiple parameter as you want
//            UnpaidLoans.SetParameterValue("yuser", System.Web.HttpContext.Current.Session["username"].ToString()); // Add multiple parameter as you want
//            UnpaidLoansViewer.ReportSource = UnpaidLoans;
//            //UnpaidLoansViewer.Zoom(75);
//            //UnpaidLoansViewer.HasToggleGroupTreeButton = false;
//            //UnpaidLoansViewer.HasPageNavigationButtons = true;
//            //UnpaidLoansViewer.HasRefreshButton = true;  
//            //UnpaidLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
//            //UnpaidLoansViewer.RefreshReport(); 

//        }
//    }
//}