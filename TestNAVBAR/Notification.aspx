﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="notification.aspx.cs" Inherits="TestNAVBAR.Controllers.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="notification" runat="server">
    <div>
        <script>
var numberOfEmails_original= 0;
Sys.Application;
app.add_init(applicationInitHandler);
function applicationInitHandler(sender, args) 
{
  InboxService.GetLatestNumberOfEmails(OnCurrentNumberOfEmailsReady);
}
function OnCurrentNumberOfEmailsReady(result, userContext, methodName) 
{
  numberOfEmails_original= result;
  // Start Checking
  StartChecking();
}
function StartChecking() 
{
  InboxService.GetLatestNumberOfEmails(OnLastestNumberOfEmailsReady);
}
function OnLastestNumberOfEmailsReady(result, userContext, methodName)
{
  var numberOfEmails_new = result;
  if (numberOfEmails_new > numberOfEmails_original)
  {
    ShowPopup();
    $get("modalBody").innerHTML = numberOfEmails_new - numberOfEmails_original;
    // Update the count here
    numberOfEmails_original = numberOfEmails_new;
  }
  // Start checking again
  window.setTimeout(StartChecking, 10000);
}
            </script>

         </div>
    </form>
</body>
</html>
