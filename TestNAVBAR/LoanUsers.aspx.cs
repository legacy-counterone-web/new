﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Web.Mvc;
using System.Diagnostics;

namespace TestNAVBAR
{
    public partial class LoanUsers : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ReportDocument LoanUserss = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/LoanUsers.rpt");
            LoanUserss.Load(reportPath);
          
            LoanUsersInfoo.ReportSource = LoanUserss;
            LoanUsersInfoo.HasToggleGroupTreeButton =false;
            LoanUsersInfoo.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            LoanUsersInfoo.HasRefreshButton = true;
            LoanUsersInfoo.RefreshReport();
            LoanUsersInfoo.HasPageNavigationButtons = true;


        }
    }
}