﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartiallyPaidLoansInfo.aspx.cs" Inherits="TestNAVBAR.Models.PartiallyPaidLoansInfo" %>
    

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="formPapd" runat="server">
    <div>
         <CR:CrystalReportViewer ID="PartiallyPaidLoansViewer" runat="server" AutoDataBind="true" PageZoomFactor="125" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" />              
    </div>
    </form>
</body>
</html>
