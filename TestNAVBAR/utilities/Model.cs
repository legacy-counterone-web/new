﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.Utilities
{
    public class Model
    {
        public static string SafeSqlLiteral(string inputSQL)
        {
            if (String.IsNullOrEmpty(inputSQL) != true)
            {
                return inputSQL.Replace("'", "''");
            }
            else
            {

                return inputSQL;
            }
        }

        public static string InsertString(string table, Dictionary<string, string> collection)
        {
            string sql = "";
            string Value_original = "";

            if (collection.Count > 0)
            {
                string keys = "";
                string values = "";
                foreach (var item in collection)
                {
                    keys += item.Key + ",";
                    Value_original = SafeSqlLiteral(item.Value);
                    values += "'" + Value_original + "',";
                }
                keys = keys.Remove(keys.Length - 1);
                values = values.Remove(values.Length - 1);
                sql = "insert into " + table + " (" + keys + ") values(" + values + ")";
            }

            return sql;
        }

        public static string UpdateString(string table, Dictionary<string, string> collection, Dictionary<string, string> wherecollection)
        {
            string sql = "";
            string Value_original = "";

            if (collection.Count > 0)
            {
                string keys = "";
                string values = "";

                sql = "update " + table + " set ";

                foreach (var item in collection)
                {
                    keys = item.Key + "=";
                    Value_original = SafeSqlLiteral(item.Value);
                    if (string.IsNullOrEmpty(Value_original) != true && Value_original[0].Equals('@'))
                    {
                        values = "" + Value_original + ",";
                    }
                    else
                    {
                        values = "'" + Value_original + "',";
                    }
                    sql += keys + values;
                }

                sql = sql.Remove(sql.Length - 1);

                sql += " where ";

                foreach (var item in wherecollection)
                {
                    keys = item.Key + "=";
                    Value_original = SafeSqlLiteral(item.Value);
                    values = "'" + Value_original + "' and ";
                    sql += keys + values;
                }
                sql = sql.Remove(sql.Length - 5);
            }

            return sql;
        }

        public static string DeleteString(string table, Dictionary<string, string> wherecollection)
        {
            string sql = "";

            if (wherecollection.Count > 0)
            {
                string keys = "";
                string values = "";
                sql = "delete from " + table + " ";

                sql += " where ";

                foreach (var item in wherecollection)
                {
                    keys = item.Key + "=";
                    values = "'" + item.Value + "' and ";
                    sql += keys + values;
                }
                sql = sql.Remove(sql.Length - 5);
            }

            return sql;
        }

        public static string Likes(Dictionary<string, string> filterscollection)
        {
            string sql = " where ";
            string keys;
            string values;
            foreach (var item in filterscollection)
            {
                if (string.IsNullOrEmpty(item.Value)) continue;

                keys = item.Key + " like ";
                values = "'%" + item.Value + "%' and ";
                sql += keys + values;
            }
            sql = sql.Remove(sql.Length - 5);

            return sql;
        }


        public static string Equal(Dictionary<string, string> filterscollection)
        {
            string sql = " where ";
            string keys;
            string values;
            foreach (var item in filterscollection)
            {
                if (string.IsNullOrEmpty(item.Value)) continue;

                keys = item.Key + " = ";
                values = "'" + item.Value + "' and ";
                sql += keys + values;
            }
            sql = sql.Remove(sql.Length - 5);

            return sql;
        }

        public static bool isNumeric(string value)
        {
            try
            {
                decimal t = decimal.Parse(value);
                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}
