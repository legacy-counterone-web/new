﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Security.Cryptography;

namespace TestNAVBAR.Utilities
{
    public class Users
    {
        // Fields
        private BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        

        // Methods
        public bool AuthenticateUser(string username, string password, string txtUsertype, string txtbranch, ref string strUserName_r, ref string strFullName_r, ref string strBranchCode_r, ref string strBranchName_r, ref Dictionary<string,string> userrights, ref string strBranchCode2_r, ref string strUsertype_r, ref string strLastLogin, ref string strStatus)
        {
            //Bank Requested we avoid filling in Usertype na Branch -Henri Okode 29th April 2011
           // string strSql = "SELECT * FROM [Users] WHERE Active='True' and UserName = '" + username + "' and USERTYPE='" + txtUsertype + "' and branch_Code='" + txtbranch + "'";
            string strSql = "SELECT * FROM [Users] WHERE Active='True' and UserName = '" + username + "'";
            SqlDataReader objDR = Blogic.RunQueryReturnDataReader(strSql);
                    
            using (objDR)
            {
                if (objDR!=null && objDR.HasRows)
                {
                    objDR.Read();

                    //get the user rights
                    // canManageCustomers, canManageAccounts, canManageUsers, 
                    // canManagePasswords, canSendPasswords,canManageCustInfo 
                    //  canManageParameters, canManageCharges, canManageReports,canManageAccountOpening
                    userrights["canManageCustomers"] = objDR["canManageCustomers"].ToString();
                    userrights["canManageCustInfo"] = objDR["canManageCustInfo"].ToString();
                    userrights["canManageAccOpening"] = objDR["canManageAccOpening"].ToString();
                    userrights["canManageAccounts"] = objDR["canManageAccounts"].ToString();
                    userrights["canManageUsers"] = objDR["canManageUsers"].ToString();
                    userrights["canManagePasswords"] = objDR["canManagePasswords"].ToString();
                    userrights["canSendPasswords"] = objDR["canSendPasswords"].ToString();
                    userrights["canManageParameters"] = objDR["canManageParameters"].ToString();
                    userrights["canManageCharges"] = objDR["canManageCharges"].ToString();
                    userrights["canManageReports"] = objDR["canManageReports"].ToString();
                   // userrights["canmanageAdmin"] = objDR["canmanageAdmin"].ToString();

                    userrights["canApproveUsers"] = objDR["canApproveUsers"].ToString();
                    userrights["canNgwazi"] = objDR["canNgwazi"].ToString();
                    userrights["canManageATM"] = objDR["canManageATM"].ToString();
                    userrights["canManageBranches"] = objDR["canManageBranches"].ToString();
                    userrights["canManageEmails"] = objDR["canManageEmails"].ToString();
                    userrights["canManageAgents"] = objDR["canManageAgents"].ToString();


                    //locked?
                    strStatus = objDR["locked"].ToString();

                    if (string.IsNullOrEmpty(strStatus) != true && strStatus.ToLower().Equals("true"))
                    {
                        strStatus = "locked";
                        return false;
                    }

                    //expired?
                    string lastpwdchange = objDR["lastpasswordchange"].ToString();
                    int maxpwdage = int.Parse(Blogic.RunStringReturnStringValue("select itemvalue from generalparams where itemname='MaxPasswordAge'"));
                    DateTime lastpwdchangedt = DateTime.Parse("jan 1, 1970");
                    try
                    {
                        lastpwdchangedt = DateTime.Parse(lastpwdchange);
                    }
                    catch
                    {
                    }

                    //set last login
                    strLastLogin = objDR["LastLogin"].ToString();
                    if (string.IsNullOrEmpty(strLastLogin))
                    {
                        strLastLogin = "First Login";
                    }

                    //set expiry date
                    string strExpiry = "";
                    strExpiry = lastpwdchangedt.AddDays(maxpwdage).ToString("dd-MMM-yyyy");

                    if ((DateTime.Now - lastpwdchangedt).Days > maxpwdage)
                    {
                        strStatus = "expired";
                        //Bank Requested we avoid filling in Usertype na Branch -Henri Okode 29th April 2011
                        //Blogic.RunNonQuery(" update Users set expired='1',lastlogin='" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt") + "' " +
                        //" WHERE UserName = '" + username + "' and USERTYPE='" + txtUsertype + "' and branch_Code='" + txtbranch + "'");

                        Blogic.RunNonQuery(" update Users set expired='1',lastlogin='" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt") + "' " +
                        " WHERE UserName = '" + username + "'");
                   
                        return false;
                    }
                    
                    bool BlnPassword;
                    password = BusinessLogic.Utilities.Functions.Encrypter(password);
                    if (string.Compare(password, objDR["password"].ToString(), false) == 0)
                    {
                        BlnPassword = true;                        
                        strUserName_r = objDR["UserName"].ToString();
                        strFullName_r = objDR["FullName"].ToString();
                        strBranchCode_r = objDR["branch_Code"].ToString();
                        strBranchName_r = objDR["branch_Name"].ToString();
                        strBranchCode2_r = objDR["branch_Code"].ToString();
                        strUsertype_r = objDR["usertype"].ToString();

                        //update, set trials=0
                        //Bank Requested we avoid filling in Usertype na Branch -Henri Okode 29th April 2011
                        //Blogic.RunNonQuery(" update Users set trials='0',lastlogin='" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt") + "' " +
                        //" WHERE UserName = '" + username + "' and USERTYPE='" + txtUsertype + "' and branch_Code='" + txtbranch + "'");

                        Blogic.RunNonQuery(" update Users set trials='0',lastlogin='" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt") + "' " +
                        " WHERE UserName = '" + username + "'");
                    }
                    else
                    {
                        //check on locking and lock if necessary
                        //Bank Requested we avoid filling in Usertype na Branch -Henri Okode 29th April 2011
                       // string updatesql =
                       //" declare @trials int; select @trials=trials+1 from Users " +
                       //" WHERE UserName = '" + username + "' and USERTYPE='" + txtUsertype + "' and branch_Code='" + txtbranch + "'; " +
                       //" declare @status nvarchar(20); " +
                       //" declare @maxloginfailures int; " +
                       //" select @maxloginfailures=itemvalue from generalparams where itemname='MaxLoginFailures'; " +
                       //" if @trials > @maxloginfailures " +
                       //" begin  " +
                       //" set @status='1' " +
                       //" end " +
                       //" else set @status='0'; " +
                       //" update Users set trials=trials+1,locked=@status,lastlogin='" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt") + "' " +
                       //" WHERE UserName = '" + username + "' and USERTYPE='" + txtUsertype + "' and branch_Code='" + txtbranch + "'; select @status;";


                        string updatesql =
                      " declare @trials int; select @trials=trials+1 from Users " +
                      " WHERE UserName = '" + username + "'; " +
                      " declare @status nvarchar(20); " +
                      " declare @maxloginfailures int; " +
                      " select @maxloginfailures=itemvalue from generalparams where itemname='MaxLoginFailures'; " +
                      " if @trials > @maxloginfailures " +
                      " begin  " +
                      " set @status='1' " +
                      " end " +
                      " else set @status='0'; " +
                      " update Users set trials=trials+1,locked=@status,lastlogin='" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt") + "' " +
                      " WHERE UserName = '" + username + "'; select @status;";

                        strStatus = Blogic.RunStringReturnStringValue(updatesql);

                        if(strStatus.Equals("1"))
                        {
                            strStatus = "locked";
                        }

                        BlnPassword = false;
                    }
                    if (BlnPassword)
                    {
                        return true;
                    }
                }                
                return false;
            }            
        }
        public  bool SessionRights(string txtUsertype, ref Dictionary<string, string> userRoles)
        {
           
            
            

        string strSql = "select *from tbUsers  where  UserName='" + txtUsertype.Trim()  + "'";
            SqlDataReader objDR =   Blogic.RunQueryReturnDataReader(strSql);

            using (objDR)
            {
                if (objDR != null && objDR.HasRows)
                {
                    objDR.Read();
                    userRoles["clientregister"] = objDR["clientregister"].ToString();
                    userRoles["parameters"] = objDR["parameters"].ToString();
                    userRoles["usermanagement"] = objDR["usermanagement"].ToString();
                    userRoles["accounts"] = objDR["accounts"].ToString();
                    userRoles["userlevel"] = objDR["userlevel"].ToString();
                    userRoles["Editapprovals"] = objDR["Editapprovals"].ToString();
                    userRoles["Approvals"] = objDR["Approvals"].ToString();
                    userRoles["Savings"] = objDR["Savings"].ToString();
                    userRoles["floatmngt"] = objDR["floatmngt"].ToString();
                }
                return true;
            }
        }

        public static bool ManageAuditTrail(string strUsername, string strUserType, string strBranch, string strAction, string strModule, string IP, string Host_name )
        {
            bool flag = false;
            try
            {
                if (strUsername == "")
                {

                    strUsername = System.Web.HttpContext.Current.Session["username"].ToString() ;
                }

                BusinessLogic.DataConnections auditlogic = new BusinessLogic.DataConnections();
                Dictionary<string, string> data = new Dictionary<string, string>();
                
                data["strUserName"] = strUsername; 
                data["strUsertype"] = strUserType;
                data["strBranch"] = strBranch;
                data["strAction"] = strAction;
                data["strModule"] = strModule;
                data["dtDate"] = DateTime.Now.ToString("yyyy-MM-dd");
                data["dtTime"] = DateTime.Now.ToString("hh:mm:ss tt");
                data["IP_Address"] = IP;
                data["Host_name"] = Host_name;
                string strUpdateSaveString = BusinessLogic.Utilities.Model.InsertString("tbAdminAuditTrail", data);
                flag = auditlogic.RunNonQuery(strUpdateSaveString);
            }
            catch (Exception exception1)
            {

            }
            return flag;
        }

        public static byte Asc(char src)
        {
            return (System.Text.Encoding.GetEncoding("iso-8859-1").GetBytes(src + "")[0]);
        }

        public static string EncyptString(string strPasswordString)
        {
            // Encyption Algorithm...
            //int s = 0;
            //Int64 sCount = 0;
            //strPasswordString = strPasswordString + "!Eclectic%IsThe BomB%Limited!??hehehe";
            //for (s = 0; s <= strPasswordString.Length - 1; s++)
            //{
            //    sCount = sCount + Asc((strPasswordString.Substring(s, 1))[0]);
            //}
            //sCount = sCount * 111;
            //return sCount.ToString();
            try
            {
                System.Security.Cryptography.SHA256Managed sha2 = new SHA256Managed();
                byte[] hash = sha2.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(strPasswordString));
                return Convert.ToBase64String(hash);
            }
            catch
            {
                return "";
            }
        }



        //public void ManageAuditTrail(string p, string p_2, string p_3, string p_4, string p_5, string p_6)
        //{
        //    throw new NotImplementedException();
        //}

        //public void ManageAuditTrail(string p, string p_2, string p_3, string p_4, string p_5)
        //{
        //    throw new NotImplementedException();
        //}
    }



}
