﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.VisualBasic;
using System.Diagnostics;
//using Microsoft.Reporting;
//using Microsoft.Reporting.WinForms;
using MySecurity;

namespace BusinessLogic.Utilities
{


    public class Functions
    {
        private static BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
        static MySecurity.CryptoFactory cryptographyfactory = new MySecurity.CryptoFactory();        
       
        private static string _smstitle = "";
        public static string SMSTitle
        {
            get
            {
                if (string.IsNullOrEmpty(_smstitle))
                {
                    _smstitle = blogic.RunStringReturnStringValue("Select ITEMVALUE from GENERALPARAMS where ITEMNAME='SMSTITLE'");
                }
                return _smstitle;
            }
        }

        private static string _ibTitle = "";
        public static string IBTitle
        {
            get
            {
                if (string.IsNullOrEmpty(_ibTitle))
                {
                    _ibTitle = blogic.RunStringReturnStringValue("Select ITEMVALUE from GENERALPARAMS where ITEMNAME='INTERNETTITLE'");
                }
                return _ibTitle;
            }
        }

        public static string Encrypter(string str)
        {
            MySecurity.ICrypto cryptographer = cryptographyfactory.MakeCryptographer();

            return cryptographer.Encrypt(str);
        }

        public static string DeCrypter(string str)
        {
            MySecurity.ICrypto cryptographer = cryptographyfactory.MakeCryptographer();

            return cryptographer.Decrypt(str);
        }


       


        public static String GetCustomerInfo(string txnid, string oper, string AccountNumber)
        {
            string sql = "";
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> where = new Dictionary<string, string>();
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            string Balance = "";
            string CustomerDetails = "";
            string Responsecode = "";
            String Response = "";
            string Narration = "";
            string AvailableBal = "";
            string LedgerBal = "";
            int strlength = 0;
            //Response="DATE_TIME|SEQ_NR|TRAN_TYPE|ACC_ID1|TRAN_AMOUNT|CURR_CODE|~20100504000000|000000| NBS2|(MWK9,500.00)|000000000000|454~20100504000000|000001|NBS347|MWK80.00|000000000000|454~20100504000000|000002|NBS849|MWK19,033.33|000000000000|454~20100504000000|000003| NBS2|(MWK18,250.00)|000000000000|454~20100504000000|000004| NBS2|(MWK1,000.00)|000000000000|454~20100504000000|000005|NBS347|(MWK80.00)|000000000000|454~20100504000000|000006|NBS433|MWK55.00|000000000000|454~20100504000000|000007|NBS912|(MWK100.00)|000000000000|454~20100504000000|000008|NBS433|(MWK55.00)|000000000000|454~20100504000000|000009|NBS912|(MWK100.00)|000000000000|454~";
            //AND updated='False'' AND Updated = '1'
            string processed = blogic.RunStringReturnStringValue("Select processed from tbIBTransactions where txnid='" + txnid + "' order by id asc");
            if (processed == "True")
            {
                // its processed so go on else continue waiting
                string str = "Select * from tbIBTransactions where txnid='" + txnid + "'";
                SqlDataReader reader = blogic.RunQueryReturnDataReader(str);
                using (reader)
                {
                    if (reader != null && reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Responsecode = reader["Responsecode"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["Responsecode"];
                            //Balance = reader["Field54"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["Field54"];
                            CustomerDetails = reader["Field48"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["Field48"];
                            where["TxnID"] = (string)reader["txnid"];
                        }

                        //Now display the balances if its a bal Enquiry

                        if (Responsecode == "000")
                        {
                            Narration = "";
                            // Successful Txns so update the balance and send emails if need be
                            switch (oper)
                            {
                                case "CUST":
                                    Response = CustomerDetails;
                                    break;
                            }
                        }
                        else
                        {

                            data["Updated"] = "True";
                            Narration = "Service Unavailable- Please register manually";
                            sql = BusinessLogic.Utilities.Model.UpdateString("tbIBTransactions", data, where);
                            blogic.RunNonQuery(sql);
                            //Insufficient Funds

                        }
                    }
                }
                //reader.Close();
                //reader.Dispose();
            }
            return Response;
        }

    }
}

