﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public partial class Balalncesheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string reportpathx = "";
            reportpathx = System.Configuration.ConfigurationManager.AppSettings["reportpath"];

            ReportDocument cryRpt = new ReportDocument();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;


            string absolutepath = reportpathx + "B_sheet.rpt";

            string reportPath = Server.MapPath(absolutepath);
            cryRpt.Load(reportPath);


            string ServerName = "";
            string DatabaseName = "";
            string UserID = "";
            string password = "";

            ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
            DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
            UserID = System.Configuration.ConfigurationManager.AppSettings["UserID"];
            password = System.Configuration.ConfigurationManager.AppSettings["password"];



            crConnectionInfo.ServerName = ServerName;
            crConnectionInfo.DatabaseName = DatabaseName;
            crConnectionInfo.UserID = UserID;
            crConnectionInfo.Password = password;

            CrTables = cryRpt.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }
            BSviewer.Zoom(75);
            BSviewer.ReportSource = cryRpt;
            BSviewer.BestFitPage = true;
            BSviewer.HasRefreshButton = true;
            BSviewer.HasPageNavigationButtons = true;
            BSviewer.RefreshReport();

        }
    }
}
