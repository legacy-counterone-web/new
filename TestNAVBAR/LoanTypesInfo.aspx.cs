﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public partial class LoanTypesInfo : System.Web.UI.Page
    {       
        protected void Page_Load(object sender, EventArgs e)
        {

            DataTable dt = new DataTable();
            string SQL = "select * from LoanApplications WHERE " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument LoanTypes = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/LoanTypesData.rpt");
            LoanTypes.Load(reportPath);
            LoanTypes.SetDataSource(dt);
            LoanTypesViewer.ReportSource = LoanTypes;
            LoanTypesViewer.HasRefreshButton = true;
            LoanTypesViewer.RefreshReport();
            LoanTypesViewer.HasPageNavigationButtons = true;
        }
    }
}