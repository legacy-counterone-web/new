﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.Controllers
{
    public partial class CallLogsInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "select * from CallLogs WHERE " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument CallLogs = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/CallLogsData.rpt");
            CallLogs.Load(reportPath);
            CallLogs.SetDataSource(dt);
            CallLogsViewer.ReportSource = CallLogs;
            CallLogsViewer.HasToggleGroupTreeButton = false;
            CallLogsViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
        }
    }
}