﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.aspnet_client
{
    public partial class RejectedByHrLoansInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             DataTable dt = new DataTable();
            string SQL = "select * from LoanApplications a,Employers b WHERE a.acceptrejectflag='2' and b.ID=a.employerid";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument RejectedByHrLoans = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/RejectedByHrLoansData.rpt");
            RejectedByHrLoans.Load(reportPath);
            RejectedByHrLoans.SetDataSource(dt);
            RejectedByHrLoansViewer.ReportSource = RejectedByHrLoans;
            RejectedByHrLoansViewer.HasToggleGroupTreeButton = false;
            RejectedByHrLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;

        }
    }
}