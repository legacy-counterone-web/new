﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestNAVBAR.Reports
{
    public partial class members : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/members.rdlc");
                saccomembers dsCustomers = GetData("select top 20 * from tblloanusers");
                ReportDataSource datasource = new ReportDataSource("members", dsCustomers.Tables[0]);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }

        }
        private saccomembers GetData(string query)
        {
            // string connectionString = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
            // string conString = ConfigurationManager.ConnectionStrings["constr"].ConnectionString;
            string conString = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
            SqlCommand cmd = new SqlCommand(query);
            using (SqlConnection con = new SqlConnection(conString))
            {
                using (SqlDataAdapter sda = new SqlDataAdapter())
                {
                    cmd.Connection = con;

                    sda.SelectCommand = cmd;
                    using (saccomembers dsCustomers = new saccomembers())
                    {
                        sda.Fill(dsCustomers, "DataTable1");
                        return dsCustomers;
                    }
                }
            }
        }
    }
}