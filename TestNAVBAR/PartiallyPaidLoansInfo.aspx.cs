﻿

using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TestNAVBAR.Models
{
    public partial class PartiallyPaidLoansInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "select *  from  vw_partiallyPaidLoan  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument PartiallyPaidLoans = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/PartiallyPaidLoansData.rpt");
            PartiallyPaidLoans.Load(reportPath);
            PartiallyPaidLoans.SetDataSource(dt);
            PartiallyPaidLoansViewer.ReportSource = PartiallyPaidLoans;
            PartiallyPaidLoansViewer.HasToggleGroupTreeButton = false;
            PartiallyPaidLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            PartiallyPaidLoansViewer.HasRefreshButton = true;
            PartiallyPaidLoansViewer.HasPageNavigationButtons = true;
            PartiallyPaidLoansViewer.RefreshReport();



        }
    }
}

//using System.Configuration;
//using System.Data;
//using System.Data.SqlClient;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
//using System;
//using System.Text.RegularExpressions;

//namespace TestNAVBAR.Models
//{
//    public partial class PartiallyPaidLoansInfo : System.Web.UI.Page
//    {       
//        ReportDocument rd = new ReportDocument ();
//        protected void Page_Load(object sender, EventArgs e)
//        {

//            string dateRange_ = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
//            string _DateFrom = dateRange_.Replace("(", string.Empty).Replace(")", string.Empty);
//            string[] _Begin = Regex.Split(_DateFrom, "AND");
//            string[] _datefrom = Regex.Split(_Begin[0], ">=");
//            string datefrom = _datefrom[1].Replace("'", string.Empty);
//            string[] _dateto = Regex.Split(_Begin[1], "<=");
//            string dateto = _dateto[1].Replace("'", string.Empty);
//            DataTable dt;
//            String SQL = "select distinct *from vw_partiallyPaidLoan ";
//            //" +dateRange_ + "

//            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
//            using (SqlConnection conn = new SqlConnection(sConstr))
//            {
//                using (SqlCommand comm = new SqlCommand(SQL, conn))
//                {
//                    conn.Open();
//                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
//                    {
//                        dt = new DataTable("tbl");
//                        da.Fill(dt);
//                    }
//                }
//            }

//            ReportDocument rd = new ReportDocument();
//            string reportPath = Server.MapPath("~/aspnet_client/PartiallyPaidLoansData.rpt");
//            rd.Load(reportPath);
//            rd.SetDataSource(dt);
//            //rd.SetParameterValue("partially", "From: " + datefrom + "\n    To: " + dateto); // Add multiple parameter as you want
//            //rd.SetParameterValue("user1", System.Web.HttpContext.Current.Session["username"].ToString()); // Add multiple parameter as you want
//            partiallyPaidLoansViewer.ReportSource = rd;
//            partiallyPaidLoansViewer.HasToggleGroupTreeButton = false;
//            partiallyPaidLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
//            partiallyPaidLoansViewer.HasRefreshButton = true;
//            partiallyPaidLoansViewer.HasPageNavigationButtons = true;
//            partiallyPaidLoansViewer.RefreshReport();
//        }
//    }
//}
////using CrystalDecisions.CrystalReports.Engine;
////using System;
////using System.Configuration;
////using System.Data;
////using System.Data.SqlClient;
////using System.Text.RegularExpressions;


////namespace TestNAVBAR.Models
////{
////    public partial class PartiallyPaidLoansInfo : System.Web.UI.Page
////    {
////        ReportDocument Report = new ReportDocument();
////        protected void Page_Load(object sender, EventArgs e)
////        {

////            string dateRange_ = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
////            string _DateFrom = dateRange_.Replace("(", string.Empty).Replace(")", string.Empty);
////            string[] _Begin = Regex.Split(_DateFrom, "AND");
////            string[] _datefrom = Regex.Split(_Begin[0], ">=");
////            string datefrom = _datefrom[1].Replace("'", string.Empty);
////            string[] _dateto = Regex.Split(_Begin[1], "<=");
////            string dateto = _dateto[1].Replace("'", string.Empty);
////            DataTable dt;
////            String SQL = "select distinct *from vw_PartiallyPaidLoan " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";

////            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
////            using (SqlConnection conn = new SqlConnection(sConstr))
////            {
////                using (SqlCommand comm = new SqlCommand(SQL, conn))
////                {
////                    conn.Open();
////                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
////                    {
////                        dt = new DataTable("tbl");
////                        da.Fill(dt);
////                    }
////                }
////            }

////            ReportDocument rd = new ReportDocument();
////            string reportPath = Server.MapPath("~/aspnet_client/PartiallyPaidLoansData.rpt");
////            rd.Load(reportPath);
////            rd.SetDataSource(dt);
////            rd.SetParameterValue("partially", "From: " + datefrom + "\n    To: " + dateto); // Add multiple parameter as you want
////            rd.SetParameterValue("muser", System.Web.HttpContext.Current.Session["username"].ToString()); // Add multiple parameter as you want
////            //rd.SetParameterValue("", "date from " + datefrom + " date to" + dateto); // Add multiple parameter as you want
////            partiallyPaidLoansViewer.ReportSource = rd;

////        }
////    }
////}

//////            string dateRange_ = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
//////            string _DateFrom = dateRange_.Replace("(", string.Empty).Replace(")", string.Empty);
//////            string[] _Begin = Regex.Split(_DateFrom, "AND");
//////            string[] _datefrom = Regex.Split(_Begin[0], ">=");
//////            string datefrom = _datefrom[1].Replace("'", string.Empty);
//////            string[] _dateto = Regex.Split(_Begin[1], "<=");
//////            string dateto = _dateto[1].Replace("'", string.Empty);
//////            //DataTable dt;
//////            //protected void Page_Load(object sender, EventArgs e)
//////            //{
//////                DataTable dt = new DataTable();
//////            string SQL = "select *  from  vw_partiallyPaidLoan  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
//////            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
//////            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
//////            using (SqlConnection conn = new SqlConnection(sConstr))
//////            {
//////                using (SqlCommand comm = new SqlCommand(SQL, conn))
//////                {
//////                    conn.Open();
//////                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
//////                    {
//////                        dt = new DataTable("tbl");
//////                        da.Fill(dt);
//////                    }
//////                }
//////            }

//////            ReportDocument partiallyPaidLoans = new ReportDocument();
//////            string reportPath = Server.MapPath("~/aspnet_client/PartiallyPaidLoansData.rpt");
//////            partiallyPaidLoans.Load(reportPath);
//////            partiallyPaidLoans.SetDataSource(dt);
//////           // partiallyPaidLoans.SetParameterValue("unpaid", "date from " + datefrom + " date to" + dateto); // Add multiple parameter as you want
//////            //partiallyPaidLoans.SetParameterValue("unpaid", "demabio");
//////            partiallyPaidLoansViewer.ReportSource = partiallyPaidLoans;
//////            partiallyPaidLoansViewer.HasToggleGroupTreeButton = false;
//////            partiallyPaidLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
//////            partiallyPaidLoansViewer.HasRefreshButton = true;
//////            partiallyPaidLoansViewer.HasPageNavigationButtons = true;
//////            partiallyPaidLoansViewer.RefreshReport();



//////        }
//////    }
//////}