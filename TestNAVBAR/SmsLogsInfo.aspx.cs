﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;


namespace TestNAVBAR.Views.SMS
{
    public partial class SmsLogsInfo : System.Web.UI.Page
    {
        ReportDocument Report = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();                    
            string SQL = "select * from smslogs WHERE " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument SmsLogs = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/SmsLogsData.rpt");
            SmsLogs.Load(reportPath);
            SmsLogs.SetDataSource(dt);
            SmsLogsViewer.ReportSource = SmsLogs;
            SmsLogsViewer.Zoom(75);
            SmsLogsViewer.HasToggleGroupTreeButton = false;
            SmsLogsViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
        }
    }
}