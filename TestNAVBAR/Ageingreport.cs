﻿

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public partial class Ageingreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "SELECT firstname, lastname, phoneno, _0to30, _31to40, _41to50, _51to60, _61to90, _91to120, _121to150, _151to180, _above180 FROM VW_AGING";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }



            string reportpathx = "";
            reportpathx = System.Configuration.ConfigurationManager.AppSettings["reportpath"];

            ReportDocument cryRpt = new ReportDocument();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;


            string absolutepath = reportpathx + "Ageingreport.rpt";

            string reportPath = Server.MapPath(absolutepath);
            cryRpt.Load(reportPath);


            string ServerName = "";
            string DatabaseName = "";
            string UserID = "";
            string password = "";

            ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
            DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
            UserID = System.Configuration.ConfigurationManager.AppSettings["UserID"];
            password = System.Configuration.ConfigurationManager.AppSettings["password"];



            crConnectionInfo.ServerName = ServerName;
            crConnectionInfo.DatabaseName = DatabaseName;
            crConnectionInfo.UserID = UserID;
            crConnectionInfo.Password = password;
            CrTables = cryRpt.Database.Tables;

            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }


            cryRpt.Load(reportPath);



            vwage.ReportSource = cryRpt;
            vwage.Zoom(75);
            vwage.RefreshReport();
            vwage.HasToggleGroupTreeButton = false;
            vwage.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            vwage.HasRefreshButton = true;
            vwage.HasPageNavigationButtons = true;
            vwage.AllowedExportFormats = (int)(ViewerExportFormats.ExcelFormat | ViewerExportFormats.PdfFormat);





        }
    }
}
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
//using System;
//using System.Configuration;
//using System.Data;
//using System.Data.SqlClient;
//using System.Text.RegularExpressions;

//namespace TestNAVBAR.Models
//{
//    public partial class Ageingreport : System.Web.UI.Page
//    {

//             protected void Page_Load(object sender, EventArgs e)
//        {


//            String gunia_ = "kimani";
//            DataTable dt = new DataTable();
//            string SQL = "select '" + gunia_ + "' as loggedUser, * from  LoanApplications WHERE LoanApproved='1' and LoanDisbursed='1' and vetted='YES'and loancode='L0003'";
//            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
//            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
//            using (SqlConnection conn = new SqlConnection(sConstr))
//            {
//                using (SqlCommand comm = new SqlCommand(SQL, conn))
//                {
//                    conn.Open();
//                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
//                    {
//                        dt = new DataTable("tbl");
//                        da.Fill(dt);
//                    }
//                }
//            }

//            string reportpathx = "";
//            reportpathx = System.Configuration.ConfigurationManager.AppSettings["reportpath"];

//            ReportDocument cryRpt = new ReportDocument();
//            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
//            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
//            ConnectionInfo crConnectionInfo = new ConnectionInfo();
//            Tables CrTables;


//            string absolutepath = reportpathx + "Ageingreport.rpt";

//            string reportPath = Server.MapPath(absolutepath);
//            cryRpt.Load(reportPath);


//            string ServerName = "";
//            string DatabaseName = "";
//            string UserID = "";
//            string password = "";

//            ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
//            DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
//            UserID = System.Configuration.ConfigurationManager.AppSettings["UserID"];
//            password = System.Configuration.ConfigurationManager.AppSettings["password"];



//            crConnectionInfo.ServerName = ServerName;
//            crConnectionInfo.DatabaseName = DatabaseName;
//            crConnectionInfo.UserID = UserID;
//            crConnectionInfo.Password = password;
//            CrTables = cryRpt.Database.Tables;

//            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
//            {
//                crtableLogoninfo = CrTable.LogOnInfo;
//                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
//                CrTable.ApplyLogOnInfo(crtableLogoninfo);
//            }


//            cryRpt.Load(reportPath);

//            //cryRpt.SetParameterValue("PrintedBy", "Demabio");

//            vwage.ReportSource = cryRpt;
//            cryRpt.SetParameterValue("PrintedBy", "Demabio");
//            vwage.Zoom(75);
//            vwage.RefreshReport();
//            vwage.HasToggleGroupTreeButton = false;
//            vwage.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
//            vwage.HasRefreshButton = true;
//            vwage.HasPageNavigationButtons = true;
//            vwage.AllowedExportFormats = (int)(ViewerExportFormats.ExcelFormat | ViewerExportFormats.PdfFormat);





//        }
//    }
//}