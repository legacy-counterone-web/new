﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoanUsers.aspx.cs" Inherits="TestNAVBAR.LoanUsers" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" 
    Namespace="CrystalDecisions.Web" 
    TagPrefix="CR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="formloanusers" runat="server">
    <div>
             <CR:CrystalReportViewer ID="LoanUsersInfoo" 
                 runat="server" AutoDataBind="true" 
                 PageZoomFactor="125" 
                 EnableDatabaseLogonPrompt="False" 
                 EnableParameterPrompt="False" />          

    </div>
    </form>
</body>
</html>

