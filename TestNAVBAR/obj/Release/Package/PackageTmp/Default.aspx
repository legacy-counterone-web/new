<%@ Page Language="C#"  AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Inbox Notifier</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
            <Services>
                <asp:ServiceReference Path="~/InboxService.asmx" />
            </Services>
        </asp:ScriptManager>
        
        <script type="text/javascript">
            var numberOfEmails_original= 0;
            
            var app = Sys.Application;
            app.add_init(applicationInitHandler);
            
            function applicationInitHandler(sender, args) {
                InboxService.GetLatestNumberOfEmails(OnCurrentNumberOfEmailsReady);
		    }
		    function OnCurrentNumberOfEmailsReady(result, userContext, methodName) {
                numberOfEmails_original= result;

                // Start Checking
                StartChecking();
            }

		    function StartChecking() {
		        InboxService.GetLatestNumberOfEmails(OnLastestNumberOfEmailsReady);
		    }
            function OnLastestNumberOfEmailsReady(result, userContext, methodName) {
                var numberOfEmails_new= result;
                if (numberOfEmails_new > numberOfEmails_original) {
                    ShowPopup();
                    $get("modalBody").innerHTML= numberOfEmails_new - numberOfEmails_original;
                    
                    // Update the count here
                    numberOfEmails_original= numberOfEmails_new;
                }

                // Start checking again
                window.setTimeout(StartChecking, 5000);
            }
            function ShowPopup() {
                $get("UpdateProgress1").style.visibility= "visible";
                $get("UpdateProgress1").style.display= "block";
            }
            function HidePopup() {
                $get("UpdateProgress1").style.visibility= "hidden";
                $get("UpdateProgress1").style.display= "none";
            }
        </script>
        <h1>Inbox Notifier</h1>
        <p>
            The notifier will fire every 1 minute. You can configure it to run at any interval
            you want.</p>
        <p>
            Enjoy the new Messenger-Like Notifier!!</p>

    <asp:UpdateProgress DynamicLayout="False" ID="UpdateProgress1" runat="server">
    <ProgressTemplate>
        <div id="modal" class="modal">
            <div class="modalTop">
                <div class="modalTitle">My Inbox</div>
                <span style="CURSOR: hand" onclick="javascript:HidePopup();">
	                <img alt="Hide Popup" src="App_Themes/Default/images/close_vista.gif" border="0" />
	            </span>
	        </div>
            <div class="modalBody">
                You received <strong><span id="modalBody"></span></strong>&nbsp; Email(s).
            </div>
        </div>
    </ProgressTemplate>
    </asp:UpdateProgress>
    
<%--    <cc1:AnimationExtender 
        ID="AnimationExtender1" runat="server"
        TargetControlID="btnHidden">
        <Animations>
            <OnLoad>
                 <Sequence>
                        <FadeIn Duration="5" Fps="20" AnimationTarget="UpdateProgress1" />
                 </Sequence>
            </OnLoad>
        </Animations>            
    </cc1:AnimationExtender>--%>

    </form>
</body>
</html>