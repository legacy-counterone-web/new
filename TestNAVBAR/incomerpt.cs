﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public partial class incomerpt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "select * from  LoanApplications WHERE LoanApproved='1' and LoanDisbursed='1' and vetted='YES'and loancode='L0003'";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument incomeviewerx = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/incomerpt.rpt");
            incomeviewerx.Load(reportPath);
            // TBviewerx.SetDataSource(dt);
            Incomeviewer.ReportSource = incomeviewerx;
            Incomeviewer.HasToggleGroupTreeButton = false;
            Incomeviewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;

        }
    }
}