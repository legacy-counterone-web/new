﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TestNAVBAR.Models;

namespace TestNAVBAR.Repository
{
    public class TransactionRepo
    {
        SqlConnection con;
        //To Handle connection related activities    
        private void connection()
        {
            // string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            // string constr = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
            string constr = ConfigurationSettings.AppSettings["ApplicationServices"];
            con = new SqlConnection(constr);
        }
        public List<SaccoDeposits> GetAllDeposits()
        {
            try
            {
                connection();
                con.Open();
                IList<SaccoDeposits> EmpList = SqlMapper.Query<SaccoDeposits>(
                                  con, "Getdeposits").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public void AddDeposits(SaccoDeposits objMember)
        //{

        //    try
        //    {

        //        DynamicParameters ObjParm = new DynamicParameters();
        //        DateTime dates = DateTime.Now;
        //        ObjParm.Add("@Accountno", objMember.Accountno);
        //        ObjParm.Add("@Surname", objMember.Surname);
        //        ObjParm.Add("@OtherNames", objMember.OtherNames);
        //        ObjParm.Add("@Amount", objMember.Amount);
        //        ObjParm.Add("@Balance", objMember.Balance);
        //        ObjParm.Add("@Phone", objMember.Phone);
        //        ObjParm.Add("@Depositdate", objMember.Depositdate=dates.ToString());
        //        connection();
        //        con.Open();
        //        con.Execute("AddDeposits", ObjParm, commandType: CommandType.StoredProcedure);
        //        con.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public void AddDeposits(SaccoDeposits objMember)
        {

            try
            {

                DynamicParameters ObjParm = new DynamicParameters();
                DateTime dates = DateTime.Now;
                ObjParm.Add("@Accountno", objMember.Accountno);
                ObjParm.Add("@Surname", objMember.Surname);
                ObjParm.Add("@OtherNames", objMember.OtherNames);
                ObjParm.Add("@Amount", objMember.Amount);
                ObjParm.Add("@Balance", objMember.Balance);
                ObjParm.Add("@Phone", objMember.Phone);
                ObjParm.Add("@Depositdate", objMember.Depositdate = dates.ToString());
                connection();
                con.Open();
                con.Execute("AddDeposits", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateDeposits(SaccoDeposits objMember)
        {
            try
            {


                DynamicParameters ObjParam = new DynamicParameters();
                DateTime dates = DateTime.Now;
                ObjParam.Add("@Transid", objMember.Transid);
                ObjParam.Add("@Accountno", objMember.Accountno);
                ObjParam.Add("@Surname", objMember.Surname);
                ObjParam.Add("@OtherNames", objMember.OtherNames);
                ObjParam.Add("@Amount", objMember.Amount);
                ObjParam.Add("@Balance", objMember.Balance);
                ObjParam.Add("@Phone", objMember.Phone);
                ObjParam.Add("@Depositdate", objMember.Depositdate = dates.ToString());
                connection();
                con.Open();
                con.Execute("UpdateDepositDetails", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool DeleteDeposits(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Transid", Id);
                connection();
                con.Open();
                con.Execute("DeleteDepositsById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }
        }
        public List<SaccoWithdrawals> GetAllWithdrawals()
        {
            try
            {
                connection();
                con.Open();
                IList<SaccoWithdrawals> EmpList = SqlMapper.Query<SaccoWithdrawals>(
                                  con, "Getwithdrawals").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void AddDWithdrawals(SaccoWithdrawals objMember)
        {

            try
            {
               
                DynamicParameters ObjParm = new DynamicParameters();
                DateTime dates = DateTime.Now;
                ObjParm.Add("@Accountno", objMember.Accountno);
                ObjParm.Add("@Surname", objMember.Surname);
                ObjParm.Add("@OtherNames", objMember.OtherNames);
                ObjParm.Add("@Amount", objMember.Amount);
                ObjParm.Add("@Balance", objMember.Balance);
                ObjParm.Add("@Phone", objMember.Phone);
                ObjParm.Add("@Depositdate", objMember.Depositdate= dates.ToString());
                connection();
                con.Open();
                con.Execute("AddWithdrawals", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateWithdrawals(SaccoWithdrawals objMember)
        {
            try
            {


                DynamicParameters ObjParam = new DynamicParameters();
                DateTime dates = DateTime.Now;
                ObjParam.Add("@Transid", objMember.Transid);
                ObjParam.Add("@Accountno", objMember.Accountno);
                ObjParam.Add("@Surname", objMember.Surname);
                ObjParam.Add("@OtherNames", objMember.OtherNames);
                ObjParam.Add("@Amount", objMember.Amount);
                ObjParam.Add("@Balance", objMember.Balance);
                ObjParam.Add("@Phone", objMember.Phone);
                ObjParam.Add("@Depositdate", objMember.Depositdate = dates.ToString());
                connection();
                con.Open();
                con.Execute("UpdateWithdrawalsDetails", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool DeleteWithdrawals(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Transid", Id);
                connection();
                con.Open();
                con.Execute("DeleteWithdrawalsById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }
        }
        public List<SaccoTransfers> GetAllTransfers()
        {
            try
            {
                connection();
                con.Open();
                IList<SaccoTransfers> EmpList = SqlMapper.Query<SaccoTransfers>(
                                  con, "GetTransfers").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void AddDTransfers(SaccoTransfers objMember)
        {

            try
            {

                DynamicParameters ObjParm = new DynamicParameters();
                ObjParm.Add("@SenderAccountno", objMember.Accountno);
                ObjParm.Add("@SenderSurname", objMember.DSurname);
                ObjParm.Add("@SenderOtherNames", objMember.DOtherNames);
                ObjParm.Add("@SenderPhone", objMember.DPhone);
                ObjParm.Add("@ReceipientAccountno", objMember.Accountno1);
                ObjParm.Add("@ReceipientSurname", objMember.WSurname);
                ObjParm.Add("@ReceipientOtherNames", objMember.WOtherNames);
                ObjParm.Add("@ReceipientPhone", objMember.WPhone);
                ObjParm.Add("@TransferAmount", objMember.Amount);

                connection();
                con.Open();
                con.Execute("AddTransfers", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UpdateTransfers(SaccoTransfers objMember)
        {
            try
            {


                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@Transerid", objMember.Transerid);
                ObjParam.Add("@SenderAccountno", objMember.Accountno);
                ObjParam.Add("@SenderSurname", objMember.DSurname);
                ObjParam.Add("@SenderOtherNames", objMember.DOtherNames);
                ObjParam.Add("@SenderPhone", objMember.DPhone);
                ObjParam.Add("@ReceipientAccountno", objMember.Accountno1);
                ObjParam.Add("@ReceipientSurname", objMember.WSurname);
                ObjParam.Add("@ReceipientOtherNames", objMember.WOtherNames);
                ObjParam.Add("@ReceipientPhone", objMember.WPhone);
                ObjParam.Add("@TransferAmount", objMember.Amount);

                connection();
                con.Open();
                con.Execute("UpdateWTransfersDetails", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool DeleteTransfers(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Transerid", Id);
                connection();
                con.Open();
                con.Execute("DeleteTranfersById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }
        }
    }
}