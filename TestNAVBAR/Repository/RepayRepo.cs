﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TestNAVBAR.Models;

namespace TestNAVBAR.Repository
{
    public class RepayRepo
    {
        SqlConnection con;
        //To Handle connection related activities    
        private void connection()
        {
            // string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            // string constr = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
            string constr = ConfigurationSettings.AppSettings["ApplicationServices"];
            con = new SqlConnection(constr);
        }
        public List<Repay> GetAllRepays()
        {
            try
            {
                connection();
                con.Open();
                IList<Repay> EmpList = SqlMapper.Query<Repay>(
                                  con, "Getcorrectepayments").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Safcontribution> GetAllMembersContribution()
        {
            try
            {
                connection();
                con.Open();
                IList<Safcontribution> MembersContributionList = SqlMapper.Query<Safcontribution>(
                                  con, "GetMembersContribution ").ToList();
                con.Close();
                return MembersContributionList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
       
       
        //To view employee details     

        //To Update Employee details    
        public void Updatepayment(Repay objMember)
        {
            try
            {
                var name = HttpContext.Current.Session["username"];
                //string _userssession = HttpContext.//Session["username"].ToString();
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                if (objMember.rby != name.ToString())
                {
                    ObjParam.Add("@Rid", objMember.Rid);
                    //ObjParam.Add("@lid", objMember.lid);
                    ObjParam.Add("@loaf", objMember.loaf);
                    ObjParam.Add("@pdate", objMember.pdate);
                    ObjParam.Add("@tcode", objMember.tcode);
                    ObjParam.Add("@balo", objMember.balo);
                    ObjParam.Add("@bdate", objMember.bdate);
                    ObjParam.Add("@pcore", objMember.pcore);
                    ObjParam.Add("@epayments", objMember.epayments);
                    ObjParam.Add("@tamount", objMember.tamount);
                    ObjParam.Add("@rby", name);
                    //ObjParam.Add("@Openingshares", objMember.Openingshares);
                    ObjParam.Add("@uright", 1);
                    ObjParam.Add("@aright", 1);
                    ObjParam.Add("@aby", objMember.rby);
                    //ObjParam.Add("@useright", 1);
                    //ObjParam.Add("@adminright", 1);

                    connection();
                    con.Open();
                    con.Execute("Updatepayments", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                }
                else
                {
                    ObjParam.Add("@Rids", objMember.Rid);
                    //ObjParam.Add("@lid", objMember.lid);
                    ObjParam.Add("@loafs", objMember.loaf);
                    ObjParam.Add("@pdates", objMember.pdate);
                    ObjParam.Add("@tcodes", objMember.tcode);
                    ObjParam.Add("@balos", objMember.balo);
                    ObjParam.Add("@bdates", objMember.bdate);
                    ObjParam.Add("@pcores", objMember.pcore);
                    ObjParam.Add("@epayments", objMember.epayments);
                    ObjParam.Add("@tamounts", objMember.tamount);
                    ObjParam.Add("@rbys", name);
                    //ObjParam.Add("@Openingshares", objMember.Openingshares);
                    ObjParam.Add("@urights", 1);
                    ObjParam.Add("@arights", 0);
                    ObjParam.Add("@abys", objMember.aby);
                    //ObjParam.Add("@useright", 1);
                    //ObjParam.Add("@adminright", 1);

                    connection();
                    con.Open();
                    con.Execute("Updatepaymentss", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                }
                    
                //// DateTime dates = DateTime.Now;
                // DynamicParameters ObjParams = new DynamicParameters();
                // ObjParams.Add("@Userid", objMember.Userid);
                // ObjParams.Add("@Accountno", objMember.Accountno);
                // ObjParams.Add("@Surname", objMember.Surname);
                // ObjParams.Add("@OtherNames", objMember.OtherNames);
                // ObjParams.Add("@Email", objMember.Email);
                // ObjParams.Add("@DateJoined", objMember.DateJoined);
                // ObjParams.Add("@Phone", objMember.Phone);
                // ObjParams.Add("@Idno", objMember.Idno);
                // ObjParams.Add("@Membershipfee", objMember.Membershipfee);
                // ObjParams.Add("@sharecontibution", objMember.sharecontibution);
                // // ObjParam.Add("@Openingshares", objMember.Openingshares);
                // ObjParams.Add("@LoanLimit", objMember.LoanLimit);
                // ObjParams.Add("@Totalshares", objMember.Totalshares);
                // ObjParams.Add("@Membership", 1);
                // ObjParams.Add("@useright", 1);
                // ObjParams.Add("@adminright", 0);

                // connection();
                // cons.Open();
                // cons.Execute("AddNewMemberDetailsapproval", ObjParams, commandType: CommandType.StoredProcedure);
                // cons.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void rejectpayment(Repay objMember)
        {
            try
            {

                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@Rid", objMember.Rid);
                //ObjParam.Add("@lid", objMember.lid);
                //ObjParam.Add("@loaf", objMember.loaf);
                //ObjParam.Add("@pdate", objMember.pdate);
                //ObjParam.Add("@tcode", objMember.tcode);
                //ObjParam.Add("@balo", objMember.balo);
                //ObjParam.Add("@bdate", objMember.bdate);
                //ObjParam.Add("@pcore", objMember.pcore);
                //ObjParam.Add("@epayments", objMember.epayments);
                //ObjParam.Add("@tamount", objMember.tamount);
                //ObjParam.Add("@rby", objMember.rby);
                ////ObjParam.Add("@Openingshares", objMember.Openingshares);
                //ObjParam.Add("@uright", 0);
                //ObjParam.Add("@aright", 0);
                //ObjParam.Add("@aby", objMember.aby);
                //ObjParam.Add("@useright", 1);
                //ObjParam.Add("@adminright", 1);

                connection();
                con.Open();
                con.Execute("rejectrepayment", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
                //// DateTime dates = DateTime.Now;
                // DynamicParameters ObjParams = new DynamicParameters();
                // ObjParams.Add("@Userid", objMember.Userid);
                // ObjParams.Add("@Accountno", objMember.Accountno);
                // ObjParams.Add("@Surname", objMember.Surname);
                // ObjParams.Add("@OtherNames", objMember.OtherNames);
                // ObjParams.Add("@Email", objMember.Email);
                // ObjParams.Add("@DateJoined", objMember.DateJoined);
                // ObjParams.Add("@Phone", objMember.Phone);
                // ObjParams.Add("@Idno", objMember.Idno);
                // ObjParams.Add("@Membershipfee", objMember.Membershipfee);
                // ObjParams.Add("@sharecontibution", objMember.sharecontibution);
                // // ObjParam.Add("@Openingshares", objMember.Openingshares);
                // ObjParams.Add("@LoanLimit", objMember.LoanLimit);
                // ObjParams.Add("@Totalshares", objMember.Totalshares);
                // ObjParams.Add("@Membership", 1);
                // ObjParams.Add("@useright", 1);
                // ObjParams.Add("@adminright", 0);

                // connection();
                // cons.Open();
                // cons.Execute("AddNewMemberDetailsapproval", ObjParams, commandType: CommandType.StoredProcedure);
                // cons.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        //To delete Employee details    
        public bool DeleteMember(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Userid", Id);
                connection();
                con.Open();
                con.Execute("DeleteMemberById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }
        }
        public bool DeleteMemberContibution(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Userid", Id);
                connection();
                con.Open();
                con.Execute("DeleteMemberContributionById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }
        }
    }
}