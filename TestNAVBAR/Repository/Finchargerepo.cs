﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TestNAVBAR.Models;

namespace TestNAVBAR.Repository
{
    public class Finchargerepo
    {
        SqlConnection con;

        //To Handle connection related activities    
        private void connection()
        {
            // string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            // string constr = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
            string constr = ConfigurationSettings.AppSettings["ApplicationServices"];
            con = new SqlConnection(constr);
        }
        public List<Fincharges> GetAllDailyinterest()
        {
            try
            {
                connection();
                con.Open();
                IList<Fincharges> EmpList = SqlMapper.Query<Fincharges>(
                                  con, "Loandailyinterest").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Fincharges> GetAllmMonthly()
        {
            try
            {
                connection();
                con.Open();
                IList<Fincharges> EmpList = SqlMapper.Query<Fincharges>(
                                  con, "Loanmonthlyinterest").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Fincharges> GetAlllistMonthly()
        {
            try
            {
                connection();
                con.Open();
                IList<Fincharges> EmpList = SqlMapper.Query<Fincharges>(
                                  con, "Monthlyinterest").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Fincharges> GetWeeklylist()
        {
            try
            {
                connection();
                con.Open();
                IList<Fincharges> EmpList = SqlMapper.Query<Fincharges>(
                                  con, "Weeklylistinterest").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Fincharges> GetAllDailylist()
        {
            try
            {
                connection();
                con.Open();
                IList<Fincharges> EmpList = SqlMapper.Query<Fincharges>(
                                  con, "Dailyinterest").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Fincharges> GetIpay()
        {
            try
            {
                connection();
                con.Open();
                IList<Fincharges> EmpList = SqlMapper.Query<Fincharges>(
                                  con, "Monthlyinterest").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void Addcharge(Fincharges objMember)
        {
            try
            {
                string name = HttpContext.Current.Session["username"].ToString();
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@month", objMember.loandisbursedate);
                ObjParam.Add("@principle", objMember.principle);
                ObjParam.Add("@adminfee", objMember.adminfee);
                ObjParam.Add("@year", objMember.years);
                ObjParam.Add("@paid", "0");
                ObjParam.Add("@name", name);




                connection();
                con.Open();
                con.Execute("spchargesapproval", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
               
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}