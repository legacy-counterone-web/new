﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TestNAVBAR.Models;

namespace TestNAVBAR.Repository
{
    public class SaccoAdminRepo
    {
        SqlConnection con;
        SqlConnection cons;
        //To Handle connection related activities    
        private void connection()
        {
            // string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            // string constr = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
            string constr = ConfigurationSettings.AppSettings["ApplicationServices"];
            con = new SqlConnection(constr);
            string constrs = ConfigurationSettings.AppSettings["ApplicationServices"];
            cons = new SqlConnection(constrs);
        }
        public List<SafrikaMembers> GetAllMember()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetMembersforapproval ").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> GetAllDeleteMember()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetDeleteMembersforapproval").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Safcontribution> GetAllMembersContribution()
        {
            try
            {
                connection();
                con.Open();
                IList<Safcontribution> MembersContributionList = SqlMapper.Query<Safcontribution>(
                                  con, "GetMemberscontributionforapproval").ToList();
                con.Close();
                return MembersContributionList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void AddMember(SafrikaMembers objMember)
        {

            try
            {


                var name = HttpContext.Current.Session["username"];

                DynamicParameters ObjParm = new DynamicParameters();
                DateTime dates = DateTime.Now;
                // ObjParm.Add("@Accountno", objMember.Accountno);
                ObjParm.Add("@Surname", objMember.Surname);
                ObjParm.Add("@OtherNames", objMember.OtherNames);
                ObjParm.Add("@Email", objMember.Email);
                ObjParm.Add("@DateJoined", objMember.DateJoined);
                ObjParm.Add("@mpesaref", objMember.mpesaref);
                ObjParm.Add("@Phone", objMember.Phone);
                ObjParm.Add("@Idno", objMember.Idno);
                ObjParm.Add("@Membershipfee", objMember.Membershipfee);
                ObjParm.Add("@sharecontibution", objMember.sharecontibution);
                ObjParm.Add("@LoanLimit", objMember.LoanLimit);
                ObjParm.Add("@Totalshares", objMember.Totalshares);
                ObjParm.Add("@Membership", 1);
                ObjParm.Add("@automatic", objMember.automatic);
                ObjParm.Add("@Airautomatic", objMember.Airautomatic);
                ObjParm.Add("@AirTimeLimit", objMember.AirTimeLimit);
                connection();
                con.Open();
                con.Execute("AddNewMemberDetails", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddMembers(SafrikaMembers objMember)
        {

            try
            {


                var name = HttpContext.Current.Session["username"];

                DynamicParameters ObjParm = new DynamicParameters();
                DateTime dates = DateTime.Now;
                // ObjParm.Add("@Accountno", objMember.Accountno);
                ObjParm.Add("@Surname", objMember.Surname);
                ObjParm.Add("@OtherNames", objMember.OtherNames);
                ObjParm.Add("@Email", objMember.Email);
                ObjParm.Add("@DateJoined", objMember.DateJoined);
                ObjParm.Add("@mpesaref", objMember.mpesaref);
                ObjParm.Add("@Phone", objMember.Phone);
                ObjParm.Add("@Idno", objMember.Idno);
                ObjParm.Add("@Membershipfee", objMember.Membershipfee);
                ObjParm.Add("@sharecontibution", objMember.sharecontibution);
                ObjParm.Add("@LoanLimit", objMember.LoanLimit);
                ObjParm.Add("@Totalshares", objMember.Totalshares);
                ObjParm.Add("@Membership", 1);
                ObjParm.Add("@automatic", objMember.automatic);
                connection();
                con.Open();
                con.Execute("AddNewMemberDetails", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddMemberContribution(Safcontribution objMember)
        {

            try
            {
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParm = new DynamicParameters();
                ObjParm.Add("@Accountno", objMember.Accountno);
                ObjParm.Add("@Membershipfee", objMember.Membershipfee);
                ObjParm.Add("@mpesaref", objMember.mpesaref);
               // ObjParm.Add("@phone", objMember.Phone);
                ObjParm.Add("@sharecontibution", objMember.sharecontibution);
                ObjParm.Add("@LoanLimit", objMember.LoanLimit);
                ObjParm.Add("@Totalshares", objMember.Totalshares);
                ObjParm.Add("@Months", objMember.Months);
                ObjParm.Add("@Monthlycontribution", objMember.Monthlycontribution);
                ObjParm.Add("@useright", 1);
                ObjParm.Add("@adminright", 0);
                connection();
                con.Open();

                con.Execute("AddNewMemberContributions", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //To view employee details     

        //To Update Employee details    
        public void UpdateMember(SafrikaMembers objMember)
        {
            try
            {
                var name = HttpContext.Current.Session["username"];
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                if(objMember.actionedby != name.ToString())
                {
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    ObjParam.Add("@Surname", objMember.Surname);
                    ObjParam.Add("@OtherNames", objMember.OtherNames);
                    ObjParam.Add("@Email", objMember.Email);
                    ObjParam.Add("@DateJoined", objMember.DateJoined);
                    ObjParam.Add("@Phone", objMember.Phone);
                    ObjParam.Add("@Idno", objMember.Idno);
                    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                    ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Membership", 1);
                    //ObjParam.Add("@useright", 1);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedby", name);
                    ObjParam.Add("@adminright", 1);
                    ObjParam.Add("@automatic", objMember.automatic);
                    ObjParam.Add("@Airautomatic", objMember.Airautomatic);
                    ObjParam.Add("@AirTimeLimit", objMember.AirTimeLimit);
                    connection();
                    con.Open();
                    con.Execute("UpdateMemberDetailsapproval", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                }
                   else
                {
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    ObjParam.Add("@Surname", objMember.Surname);
                    ObjParam.Add("@OtherNames", objMember.OtherNames);
                    ObjParam.Add("@Email", objMember.Email);
                    ObjParam.Add("@DateJoined", objMember.DateJoined);
                    ObjParam.Add("@Phone", objMember.Phone);
                    ObjParam.Add("@Idno", objMember.Idno);
                    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                    ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Membership", 1);
                    //ObjParam.Add("@useright", 1);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedby", name);
                    ObjParam.Add("@adminright", 1);
                    ObjParam.Add("@automatics", objMember.automatic);

                    connection();
                    con.Open();
                    con.Execute("UpdateMemberDetailsapproval", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                } 
               
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void DelMember(SafrikaMembers objMember)
        {
            try
            {
                var name = HttpContext.Current.Session["username"];
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                if (objMember.actionedby != name.ToString())
                {
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    ObjParam.Add("@Surname", objMember.Surname);
                    ObjParam.Add("@OtherNames", objMember.OtherNames);
                    ObjParam.Add("@Email", objMember.Email);
                    ObjParam.Add("@DateJoined", objMember.DateJoined);
                    ObjParam.Add("@Phone", objMember.Phone);
                    ObjParam.Add("@Idno", objMember.Idno);
                    //ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                   // ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                   // ObjParam.Add("@mpesaref", objMember.mpesaref);
                   // ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                   // ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Membership", 1);
                    //ObjParam.Add("@useright", 1);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedby", name);
                    ObjParam.Add("@adminright", 1);
                    ObjParam.Add("@automatic", objMember.automatic);
                    ObjParam.Add("@Airautomatic", objMember.Airautomatic);
                    ObjParam.Add("@AirTimeLimit", objMember.AirTimeLimit);
                    ObjParam.Add("@Dels", objMember.Dels);
                    connection();
                    con.Open();
                    con.Execute("DeleteMemberDetailsapproval", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                }
                else
                {
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    ObjParam.Add("@Surname", objMember.Surname);
                    ObjParam.Add("@OtherNames", objMember.OtherNames);
                    ObjParam.Add("@Email", objMember.Email);
                    ObjParam.Add("@DateJoined", objMember.DateJoined);
                    ObjParam.Add("@Phone", objMember.Phone);
                    ObjParam.Add("@Idno", objMember.Idno);
                    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                    ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Membership", 1);
                    //ObjParam.Add("@useright", 1);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedby", name);
                    ObjParam.Add("@adminright", 1);
                    ObjParam.Add("@automatics", objMember.automatic);
                    ObjParam.Add("@Dels", objMember.Dels);
                    connection();
                    con.Open();
                    con.Execute("DeleteMemberDetailsapproval", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
        public void UpdateMembers(SafrikaMembers objMember)
        {
            try
            {
                var name = HttpContext.Current.Session["username"];
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();

                // ObjParam.Add("@Userid", objMember.Userid);
                ObjParam.Add("@Accountno", objMember.Accountno);
                ObjParam.Add("@Surname", objMember.Surname);
                ObjParam.Add("@OtherNames", objMember.OtherNames);
                ObjParam.Add("@Email", objMember.Email);
                ObjParam.Add("@DateJoined", objMember.DateJoined);
                ObjParam.Add("@Phone", objMember.Phone);
                ObjParam.Add("@Idno", objMember.Idno);
                ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                ObjParam.Add("@mpesaref", objMember.mpesaref);
                ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                ObjParam.Add("@Totalshares", objMember.Totalshares);
                ObjParam.Add("@Membership", 1);
                //  ObjParam.Add("@useright", 1);
                ObjParam.Add("@adminright", 1);
                ObjParam.Add("@actionedby", objMember.actionedby);
                ObjParam.Add("@Rectifiedby", name);
                connection();
                con.Open();
                con.Execute("UpdateMember", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();


                //// DateTime dates = DateTime.Now;
                // DynamicParameters ObjParams = new DynamicParameters();
                // ObjParams.Add("@Userid", objMember.Userid);
                // ObjParams.Add("@Accountno", objMember.Accountno);
                // ObjParams.Add("@Surname", objMember.Surname);
                // ObjParams.Add("@OtherNames", objMember.OtherNames);
                // ObjParams.Add("@Email", objMember.Email);
                // ObjParams.Add("@DateJoined", objMember.DateJoined);
                // ObjParams.Add("@Phone", objMember.Phone);
                // ObjParams.Add("@Idno", objMember.Idno);
                // ObjParams.Add("@Membershipfee", objMember.Membershipfee);
                // ObjParams.Add("@sharecontibution", objMember.sharecontibution);
                // // ObjParam.Add("@Openingshares", objMember.Openingshares);
                // ObjParams.Add("@LoanLimit", objMember.LoanLimit);
                // ObjParams.Add("@Totalshares", objMember.Totalshares);
                // ObjParams.Add("@Membership", 1);
                // ObjParams.Add("@useright", 1);
                // ObjParams.Add("@adminright", 0);

                // connection();
                // cons.Open();
                // cons.Execute("AddNewMemberDetailsapproval", ObjParams, commandType: CommandType.StoredProcedure);
                // cons.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void rejectMember(SafrikaMembers objMember)
        {
            try
            {

                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@Userid", objMember.Userid);
                ObjParam.Add("@Accountno", objMember.Accountno);
                //ObjParam.Add("@Surname", objMember.Surname);
                //ObjParam.Add("@OtherNames", objMember.OtherNames);
                //ObjParam.Add("@Email", objMember.Email);
                //ObjParam.Add("@DateJoined", objMember.DateJoined);
                //ObjParam.Add("@Phone", objMember.Phone);
                //ObjParam.Add("@Idno", objMember.Idno);
                //ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                //ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                ////ObjParam.Add("@Openingshares", objMember.Openingshares);
                //ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                //ObjParam.Add("@Totalshares", objMember.Totalshares);
                ObjParam.Add("@Membership", 0);
                ObjParam.Add("@useright", 0);
                ObjParam.Add("@adminright", 0);

                connection();
                con.Open();
                con.Execute("rejectmember", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();

            }
            catch (Exception)
            {
                throw;
            }
        }
        public void UpdateMemberContribution(Safcontribution objMember)
        {
            try
            {
                DateTime dates = DateTime.Now;
                var name = HttpContext.Current.Session["username"];
                DynamicParameters ObjParam = new DynamicParameters();
                if(objMember.actionedby!=name.ToString())
                {
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    // ObjParam.Add("@Openingshares", objMember.Openingshares);
                    //ObjParam.Add("@phone", objMember.Phone);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                    ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Months", objMember.Months);
                    ObjParam.Add("@Monthlycontribution", objMember.Monthlycontribution);
                    ObjParam.Add("@useright", 1);
                    ObjParam.Add("@adminright", 1);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedby", name);
                    connection();
                    con.Open();
                    con.Execute("UpdateMemberContributionsApproval", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                }
                else 
                {
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    // ObjParam.Add("@Openingshares", objMember.Openingshares);
                    //ObjParam.Add("@phone", objMember.Phone);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                    ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Months", objMember.Months);
                    ObjParam.Add("@Monthlycontribution", objMember.Monthlycontribution);
                    ObjParam.Add("@useright", 1);
                    ObjParam.Add("@adminright", 1);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedbys", name);
                    connection();
                    con.Open();
                    con.Execute("UpdateMemberContributionsApproval", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                }


            }
            catch (Exception)
            {
                throw;
            }
        }
        public void UpdateMemberContributions(Safcontribution objMember)
        {
            try
            {
                DateTime dates = DateTime.Now;
                var name = HttpContext.Current.Session["username"];
                DynamicParameters ObjParam = new DynamicParameters();
                ////if (name.ToString() == objMember.actionedby.ToString())
                ////{
                ////    ObjParam.Add("@Userid", objMember.Userid);
                ////    ObjParam.Add("@Accountno", objMember.Accountno);
                ////    //ObjParam.Add("@Phone", objMember.Phone);
                ////    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                ////    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                ////    ObjParam.Add("@mpesaref", objMember.mpesaref);
                ////    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                ////    ObjParam.Add("@Totalshares", objMember.Totalshares);
                ////    ObjParam.Add("@Months", objMember.Months);
                ////    ObjParam.Add("@Monthlycontribution", objMember.Monthlycontribution);
                ////    ObjParam.Add("@useright", 1);
                ////    ObjParam.Add("@adminright", 0);
                ////    ObjParam.Add("@actionedby", objMember.actionedby);
                ////    ObjParam.Add("@Rectifiedby", name);
                ////    connection();
                ////    con.Open();
                ////    con.Execute("UpdatesContributions", ObjParam, commandType: CommandType.StoredProcedure);
                ////    con.Close();
                ////}
               
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    //ObjParam.Add("@Phone", objMember.Phone);
                    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                    ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Months", objMember.Months);
                    ObjParam.Add("@Monthlycontribution", objMember.Monthlycontribution);
                    ObjParam.Add("@useright", 1);
                    ObjParam.Add("@adminright", 0);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedby", name);
                    connection();
                    con.Open();
                    con.Execute("UpdatesContributions", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
               
                
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void rejectMemberContribution(Safcontribution objMember)
        {
            try
            {
                DateTime dates = DateTime.Now;

                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@Userid", objMember.Userid);
                //ObjParam.Add("@Accountno", objMember.Accountno);
                //ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                //ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                //// ObjParam.Add("@Openingshares", objMember.Openingshares);
                //ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                //ObjParam.Add("@Totalshares", objMember.Totalshares);
                //ObjParam.Add("@Months", objMember.Months);
                //ObjParam.Add("@Monthlycontribution", objMember.Monthlycontribution);
                ObjParam.Add("@useright", 0);
               // ObjParam.Add("@adminright", 1);
                connection();
                con.Open();
                con.Execute("rejectcontribution", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        //To delete Employee details    
        public bool DeleteMember(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Userid", Id);
                connection();
                con.Open();
                con.Execute("DeleteMemberById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }
        }
        public bool DeleteMemberContibution(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Userid", Id);
                connection();
                con.Open();
                con.Execute("DeleteMemberContributionById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }
        }
    }
}