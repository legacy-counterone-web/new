﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using TestNAVBAR.Models;

namespace TestNAVBAR.Repository
{
    public class Loanrepo
    {
        SqlConnection con;
        
        //To Handle connection related activities    
        private void connection()
        {
            // string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            // string constr = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
            string constr = ConfigurationSettings.AppSettings["ApplicationServices"];
            con = new SqlConnection(constr);
        }
        public List<Loandisbursed> GetAllDisbursed()
        {
            try
            {
                connection();
                con.Open();
                IList<Loandisbursed> EmpList = SqlMapper.Query<Loandisbursed>(
                                  con, "Loansdisbursdbymonth").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Gender> GetAllGenders()
        {
            
            try
            {
                connection();
                con.Open();
                IList<Gender> EmpList = SqlMapper.Query<Gender>(
                                  con, "Loanusersbygender").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Payments> GetAllRepayments()
        {
            try
            {
                connection();
                con.Open();
                IList<Payments> EmpList = SqlMapper.Query<Payments>(
                                  con, "Loanrepaymentsbymonth").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Monthlypar> Getpar()
        {
            try
            {
                connection();
                con.Open();
                IList<Monthlypar> EmpList = SqlMapper.Query<Monthlypar>(
                                  con, "Monthlypar").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Monthlyforecasting> Getforecasting()
        {
            try
            {
                connection();
                con.Open();
                IList<Monthlyforecasting> EmpList = SqlMapper.Query<Monthlyforecasting>(
                                  con, "Monthlyforecasting").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Productdistribution> Getprod()
        {
            try
            {
                connection();
                con.Open();
                IList<Productdistribution> EmpList = SqlMapper.Query<Productdistribution>(
                                  con, "Productdisbursement").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void Addtrans(testtransaction objMember)
        {

            try
            {


           

                DynamicParameters ObjParm = new DynamicParameters();
                DateTime dates = DateTime.Now;
                // ObjParm.Add("@Accountno", objMember.Accountno);
                
                ObjParm.Add("@Phone", objMember.Phone);
                ObjParm.Add("@Amount", objMember.Amount);
               
               

                connection();
                con.Open();
                con.Execute("spheadech", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Saccoloantypes> Getloanstypes()
        {
            try
            {
                connection();
                con.Open();
                IList<Saccoloantypes> EmpList = SqlMapper.Query<Saccoloantypes>(
                                  con, "sploantypes").ToList();
                con.Close();
                return EmpList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}