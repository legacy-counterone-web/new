﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Data.Entity;
using System.Web;
using TestNAVBAR.Models;
using System.Collections.Specialized;
using System.Net;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace TestNAVBAR.Repository
{
    public class SaccoRepo
    {
        public string post_url = "https://counterone.net:8181/counterone/counteronetest/newpin.php";
        SqlConnection con;
        SqlConnection cons;
        //To Handle connection related activities    
        private void connection()
        {
            // string constr = ConfigurationManager.ConnectionStrings["SqlConn"].ToString();
            // string constr = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
            string constr = ConfigurationSettings.AppSettings["ApplicationServices"];
            con = new SqlConnection(constr);
            string constrs = ConfigurationSettings.AppSettings["ApplicationServices"];
            cons = new SqlConnection(constrs);
        }
        public List<SafrikaMembers> GetAllMember()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetMembers").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> Getnewpin()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "Getmemberspins").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Images> GetImages()
        {
            try
            {
                connection();
                con.Open();
                IList<Images> CommissionList = SqlMapper.Query<Images>(
                                  con, "Getimages").ToList();
                con.Close();
                return CommissionList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Images> GetTB()
        {
            try
            {
                connection();
                con.Open();
                IList<Images> CommissionList = SqlMapper.Query<Images>(
                                  con, "GetTB").ToList();
                con.Close();
                return CommissionList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SafrikaMembers> GetThirdPartypending()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "Getpending").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> Getmemberspin()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "Getmemberspins").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> GetRejectedMembers()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetrejectedMembers").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> GetAllThirdParty()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetTpMembers").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SafrikaMembers> GetAllpendingMember()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> MembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetpendingMembers").ToList();
                con.Close();
                return MembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> Getussdpin()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> SaccoMembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "Getussdpins").ToList();
                con.Close();
                return SaccoMembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> Getloanstodisburse()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> MembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "getLoantodisburse").ToList();
                con.Close();
                return MembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> GetAllpending()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> MembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "Getpending").ToList();
                con.Close();
                return MembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> GetAllSalvonadvance()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> MembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetAllSalvonadvance").ToList();
                con.Close();
                return MembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> GetAllAutoMembers()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> MembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetautogMembers").ToList();
                con.Close();
                return MembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<SafrikaMembers> GetAllAirTimeAutoMembers()
        {
            try
            {
                connection();
                con.Open();
                IList<SafrikaMembers> MembersList = SqlMapper.Query<SafrikaMembers>(
                                  con, "GetAirtimeautoMembers").ToList();
                con.Close();
                return MembersList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Safcontribution> GetAllMembersContribution()
        {
            try
            {
                connection();
                con.Open();
                IList<Safcontribution> MembersContributionList = SqlMapper.Query<Safcontribution>(
                                  con, "GetMembersContribution ").ToList();
                con.Close();
                return MembersContributionList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<commission> GetAllcommission()
        {
            try
            {
                connection();
                con.Open();
                IList<commission> CommissionList = SqlMapper.Query<commission>(
                                  con, "Getcommission").ToList();
                con.Close();
                return CommissionList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Deposits> GetAllDeposits()
        {
            try
            {
                connection();
                con.Open();
                IList<Deposits> depositList = SqlMapper.Query<Deposits>(
                                  con, "Getdeposit").ToList();
                con.Close();
                return depositList.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void Createussdpin(SafrikaMembers objMember)
        {

            try
            {


                var name = HttpContext.Current.Session["username"];

                DynamicParameters ObjParm = new DynamicParameters();
                string postData = "phone=" + objMember.Phone;
                var responses = DataRequestToServer(postData, post_url);
                // dynamic json = JsonConvert.DeserializeObject(responses);

                string messagetoadmin = objMember.Surname + "  Kindly wait for your Pin";
                SendSms(messagetoadmin, objMember.Phone);





                connection();
                con.Open();
                //con.Execute("", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void deleteussdpin(SafrikaMembers objMember)
        {

            try
            {


                var name = HttpContext.Current.Session["username"];

                DynamicParameters ObjParm = new DynamicParameters();
                DateTime dates = DateTime.Now;


                ObjParm.Add("@Userid", objMember.Userid);


                connection();
                con.Open();
                con.Execute("Deleteussd", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddMember(SafrikaMembers objMember)
        {

            try
            {


                var name = HttpContext.Current.Session["username"];

                DynamicParameters ObjParm = new DynamicParameters();
                DateTime dates = DateTime.Now;
                string messagetoadmin = objMember.Surname +" "+ "Your now registered as Counterone member";
                SendSms(messagetoadmin, objMember.Phone);
                // ObjParm.Add("@Accountno", objMember.Accountno);
                ObjParm.Add("@Surname", objMember.Surname);
                ObjParm.Add("@OtherNames", objMember.OtherNames);
                ObjParm.Add("@Email", objMember.Email);
                ObjParm.Add("@DateJoined", objMember.DateJoined);
                ObjParm.Add("@Phone", objMember.Phone);
                ObjParm.Add("@mpesaref", objMember.mpesaref);
                ObjParm.Add("@Idno", objMember.Idno);
                ObjParm.Add("@Membershipfee", objMember.Membershipfee);
                ObjParm.Add("@sharecontibution", objMember.sharecontibution);
                ObjParm.Add("@LoanLimit", objMember.LoanLimit);
                ObjParm.Add("@Totalshares", objMember.Totalshares);
                ObjParm.Add("@actionedby", name);
                ObjParm.Add("@Rectifiedby", name);
                ObjParm.Add("@Membership", 0);
                ObjParm.Add("@automatic", objMember.automatic);
                ObjParm.Add("@Airautomatic", objMember.Airautomatic);
                ObjParm.Add("@AirTimeLimit", objMember.AirTimeLimit);

                connection();
                con.Open();
                con.Execute("AddNewMemberDetails", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();
                
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Loantodisburse(SafrikaMembers objMember)
        {

            try
            {


                var name = HttpContext.Current.Session["username"];

                DynamicParameters ObjParm = new DynamicParameters();
                DateTime dates = DateTime.Now;
                string messagetoadmin = objMember.Surname + " " + "Request sent waiting for Approval";
                //SendSms(messagetoadmin, objMember.Phone);
                ObjParm.Add("@Accountno", objMember.Accountno);
                ObjParm.Add("@Phone", objMember.Phone);
                ObjParm.Add("@Idno", objMember.Idno);
                ObjParm.Add("@loanamount", objMember.Amount);

                connection();
                con.Open();
                con.Execute("Loantodisburse", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string DataRequestToServer(string postData, string post_url)
        {
            try
            {
                //System.Net.ServicePointManager.Expect100Continue = false;   
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(post_url);
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] data = encoding.GetBytes(postData);
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                string responses = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return responses;

            }
            catch (Exception e)
            {
                return e.Message;
            }

        }

        public void Createpin(SafrikaMembers objMember)
        {

            try
            {


                var name = HttpContext.Current.Session["username"];

                DynamicParameters ObjParm = new DynamicParameters();
                

                if (objMember.pin==0)
                {
                    string postData = "phone=" + objMember.Phone;
                    var responses = DataRequestToServer(postData, post_url);
                   // dynamic json = JsonConvert.DeserializeObject(responses);
                   
                    //string messagetoadmin = objMember.Surname + "  Kindly wait for your Pin";
                    //    SendSms(messagetoadmin, objMember.Phone);
                   
                }
                else
                {
                    ObjParm.Add("@Accountno", objMember.Accountno);
                    ObjParm.Add("@Phone", objMember.Phone);
                    ObjParm.Add("@Idno", objMember.Idno);
                    ObjParm.Add("@loanamount", objMember.Amount);

                }


                connection();
                con.Open();
                con.Execute("Loantodisburse", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddMemberContribution(Safcontribution objMember)
        {

            try
            {
                var name = HttpContext.Current.Session["username"];
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParm = new DynamicParameters();
                ObjParm.Add("@Accountno", objMember.Accountno);
                ObjParm.Add("@Membershipfee", objMember.Membershipfee);
                ObjParm.Add("@sharecontibution", objMember.sharecontibution);
                ObjParm.Add("@mpesaref", objMember.mpesaref);
              //  ObjParm.Add("@phone", objMember.Phone);
                ObjParm.Add("@LoanLimit", objMember.LoanLimit);
                ObjParm.Add("@Totalshares", objMember.Totalshares);
                ObjParm.Add("@Months", objMember.Months);
                ObjParm.Add("@Monthlycontribution", objMember.Monthlycontribution);
                ObjParm.Add("@useright", 1);
                ObjParm.Add("@adminright", 0);
                ObjParm.Add("@Usersid", 0);
                ObjParm.Add("@actionedby", name);
                ObjParm.Add("@Rectifiedby", name);
                connection();
                con.Open();
               
                con.Execute("AddNewMemberContributions", ObjParm, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //To view employee details     

        //To Update Employee details    
        public void UpdateMember(SafrikaMembers objMember)
        {
            try
            {
                var name = HttpContext.Current.Session["username"];

                DateTime dates = DateTime.Now;
                    DynamicParameters ObjParam = new DynamicParameters();
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    ObjParam.Add("@Surname", objMember.Surname);
                    ObjParam.Add("@OtherNames", objMember.OtherNames);
                    ObjParam.Add("@Email", objMember.Email);
                    ObjParam.Add("@DateJoined", objMember.DateJoined);
                    ObjParam.Add("@Phone", objMember.Phone);
                    ObjParam.Add("@Idno", objMember.Idno);
                    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                    ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Membership", 1);
                    ObjParam.Add("@useright", 0);
                    ObjParam.Add("@adminright", 0);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedby", name);
                    ObjParam.Add("@automatic", objMember.automatic);

                    ObjParam.Add("@Airautomatic", objMember.Airautomatic);
                    ObjParam.Add("@AirTimeLimit", objMember.AirTimeLimit);


                    connection();
                    con.Open();
                    con.Execute("UpdateMemberDetails", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void UpdateUpdateTP(SafrikaMembers objMember)
        {
            try
            {

                var name = HttpContext.Current.Session["username"];
               
                    DateTime dates = DateTime.Now;
                    DynamicParameters ObjParam = new DynamicParameters();
                    ObjParam.Add("@Userid", objMember.Userid);
                    ObjParam.Add("@Accountno", objMember.Accountno);
                    ObjParam.Add("@Surname", objMember.Surname);
                    ObjParam.Add("@OtherNames", objMember.OtherNames);
                    ObjParam.Add("@Email", objMember.Email);
                    ObjParam.Add("@DateJoined", objMember.DateJoined);
                    ObjParam.Add("@Phone", objMember.Phone);
                    ObjParam.Add("@Idno", objMember.Idno);
                    ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                    ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                    ObjParam.Add("@mpesaref", objMember.mpesaref);
                    ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                    ObjParam.Add("@Totalshares", objMember.Totalshares);
                    ObjParam.Add("@Membership", 1);
                    ObjParam.Add("@useright", 0);
                    ObjParam.Add("@adminright", 0);
                    ObjParam.Add("@actionedby", objMember.actionedby);
                    ObjParam.Add("@Rectifiedby", name);
                    ObjParam.Add("@automatic", objMember.automatic);

                    ObjParam.Add("@Airautomatic", objMember.Airautomatic);
                    ObjParam.Add("@AirTimeLimit", objMember.AirTimeLimit);


                    connection();
                    con.Open();
                    con.Execute("UpdateTPDetails", ObjParam, commandType: CommandType.StoredProcedure);
                    con.Close();
                
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void RejectpendingMember(SafrikaMembers objMember)
        {
            try
            {
                var name = HttpContext.Current.Session["username"];
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@Userid", objMember.Userid);
                ObjParam.Add("@useright", '2');
                ObjParam.Add("@adminright", '2');

                connection();
                con.Open();
                con.Execute("RejectMemberDetails", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
                //// DateTime dates = DateTime.Now;
                // DynamicParameters ObjParams = new DynamicParameters();
                // ObjParams.Add("@Userid", objMember.Userid);
                // ObjParams.Add("@Accountno", objMember.Accountno);
                // ObjParams.Add("@Surname", objMember.Surname);
                // ObjParams.Add("@OtherNames", objMember.OtherNames);
                // ObjParams.Add("@Email", objMember.Email);
                // ObjParams.Add("@DateJoined", objMember.DateJoined);
                // ObjParams.Add("@Phone", objMember.Phone);
                // ObjParams.Add("@Idno", objMember.Idno);
                // ObjParams.Add("@Membershipfee", objMember.Membershipfee);
                // ObjParams.Add("@sharecontibution", objMember.sharecontibution);
                // // ObjParam.Add("@Openingshares", objMember.Openingshares);
                // ObjParams.Add("@LoanLimit", objMember.LoanLimit);
                // ObjParams.Add("@Totalshares", objMember.Totalshares);
                // ObjParams.Add("@Membership", 1);
                // ObjParams.Add("@useright", 1);
                // ObjParams.Add("@adminright", 0);

                // connection();
                // cons.Open();
                // cons.Execute("AddNewMemberDetailsapproval", ObjParams, commandType: CommandType.StoredProcedure);
                // cons.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void RejectTPMember(SafrikaMembers objMember)
        {
            try
            {
                var name = HttpContext.Current.Session["username"];
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@Userid", objMember.Userid);
                ObjParam.Add("@useright", 0);
                ObjParam.Add("@adminright", 0);

                connection();
                con.Open();
                con.Execute("RejectTPMember", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
                //// DateTime dates = DateTime.Now;
                // DynamicParameters ObjParams = new DynamicParameters();
                // ObjParams.Add("@Userid", objMember.Userid);
                // ObjParams.Add("@Accountno", objMember.Accountno);
                // ObjParams.Add("@Surname", objMember.Surname);
                // ObjParams.Add("@OtherNames", objMember.OtherNames);
                // ObjParams.Add("@Email", objMember.Email);
                // ObjParams.Add("@DateJoined", objMember.DateJoined);
                // ObjParams.Add("@Phone", objMember.Phone);
                // ObjParams.Add("@Idno", objMember.Idno);
                // ObjParams.Add("@Membershipfee", objMember.Membershipfee);
                // ObjParams.Add("@sharecontibution", objMember.sharecontibution);
                // // ObjParam.Add("@Openingshares", objMember.Openingshares);
                // ObjParams.Add("@LoanLimit", objMember.LoanLimit);
                // ObjParams.Add("@Totalshares", objMember.Totalshares);
                // ObjParams.Add("@Membership", 1);
                // ObjParams.Add("@useright", 1);
                // ObjParams.Add("@adminright", 0);

                // connection();
                // cons.Open();
                // cons.Execute("AddNewMemberDetailsapproval", ObjParams, commandType: CommandType.StoredProcedure);
                // cons.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DelMember(SafrikaMembers objMember)
        {
            try
            {
                var name = HttpContext.Current.Session["username"];
                DateTime dates = DateTime.Now;
                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@Userid", objMember.Userid);
                ObjParam.Add("@Accountno", objMember.Accountno);
                ObjParam.Add("@Surname", objMember.Surname);
                ObjParam.Add("@OtherNames", objMember.OtherNames);
                ObjParam.Add("@Email", objMember.Email);
                ObjParam.Add("@DateJoined", objMember.DateJoined);
                ObjParam.Add("@Phone", objMember.Phone);
                ObjParam.Add("@Idno", objMember.Idno);
                ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                ObjParam.Add("@mpesaref", objMember.mpesaref);
                ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                ObjParam.Add("@Totalshares", objMember.Totalshares);
                ObjParam.Add("@Membership", 1);
                ObjParam.Add("@useright", 1);
                ObjParam.Add("@adminright", 1);
                ObjParam.Add("@actionedby", objMember.actionedby);
                ObjParam.Add("@Rectifiedby", name);
                ObjParam.Add("@automatic", objMember.automatic);

                ObjParam.Add("@Airautomatic", objMember.Airautomatic);
                ObjParam.Add("@AirTimeLimit", objMember.AirTimeLimit);
                ObjParam.Add("@Dels", objMember.Dels);

                connection();
                con.Open();
                con.Execute("DelMemberDetails", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
               
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void UpdateMemberContribution(Safcontribution objMember)
        {
            try
            {
                DateTime dates = DateTime.Now;
                var name = HttpContext.Current.Session["username"];
                DynamicParameters ObjParam = new DynamicParameters();
                ObjParam.Add("@Userid", objMember.Userid);
                ObjParam.Add("@Accountno", objMember.Accountno);
                //ObjParam.Add("@Phone", objMember.Phone);
                ObjParam.Add("@Membershipfee", objMember.Membershipfee);
                ObjParam.Add("@sharecontibution", objMember.sharecontibution);
                 ObjParam.Add("@mpesaref", objMember.mpesaref);
                ObjParam.Add("@LoanLimit", objMember.LoanLimit);
                ObjParam.Add("@Totalshares", objMember.Totalshares);
                ObjParam.Add("@Months", objMember.Months);
                ObjParam.Add("@Monthlycontribution", objMember.Monthlycontribution);
                ObjParam.Add("@useright", 1);
                ObjParam.Add("@adminright", 0);
                ObjParam.Add("@actionedby", objMember.actionedby);
                ObjParam.Add("@Rectifiedby", name);
                connection();
                con.Open();
                con.Execute("UpdateMemberContributions", ObjParam, commandType: CommandType.StoredProcedure);
                con.Close();
            }
            catch (Exception)
            {
                throw;
            }
        }
        //To delete Employee details    
        public bool DeleteMember(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Userid", Id);
                connection();
                con.Open();
                con.Execute("DeleteMemberById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }
        }
        public bool DeleteMemberContibution(int Id)
        {
            try
            {
                DynamicParameters param = new DynamicParameters();
                param.Add("@Userid", Id);
                connection();
                con.Open();
                con.Execute("DeleteMemberContributionById", param, commandType: CommandType.StoredProcedure);
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                //Log error as per your need     
                throw ex;
            }

        }
        private string SendSms(string message, string recipient)
        {
            string username = "counterone";
            string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
            string status = "";

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {

                // Thats it, hit send and we'll take care of the rest
                string from = "counterone";
                dynamic results = gateway.sendMessage(recipient, message, from);
                foreach (dynamic result in results)
                {
                    status = (string)result["status"];

                }
            }
            catch (AfricasTalkingGatewayException e)
            {

                Console.WriteLine("Encountered an error: " + e.Message);

            }
            return status;
        }
    }
}