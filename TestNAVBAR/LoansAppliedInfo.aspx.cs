﻿

using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Text.RegularExpressions;

namespace TestNAVBAR
{
    public partial class LoansAppliedInfo : System.Web.UI.Page
    {
        ReportDocument Report = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {

            string dateRange_ = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
            string _DateFrom = dateRange_.Replace("(", string.Empty).Replace(")", string.Empty);
            string[] _Begin = Regex.Split(_DateFrom, "AND");
            string[] _datefrom = Regex.Split(_Begin[0], ">=");
            string datefrom = _datefrom[1].Replace("'", string.Empty);
            string[] _dateto = Regex.Split(_Begin[1], "<=");
            string dateto = _dateto[1].Replace("'", string.Empty);
            DataTable dt;
            String SQL = "select distinct *from loanapplications " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";

            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument rd = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/LoansAppliedData.rpt");
            rd.Load(reportPath);
            rd.SetDataSource(dt);
            rd.SetParameterValue("mdate", "From: " + datefrom + "\n    To: " + dateto); // Add multiple parameter as you want
            rd.SetParameterValue("muser", System.Web.HttpContext.Current.Session["username"].ToString()); // Add multiple parameter as you want
            LoansAppliedViewer.ReportSource = rd;

        }
    }
}



//using System.Configuration;
//using System.Data;
//using System.Data.SqlClient;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
//using System;


//namespace TestNAVBAR
//{
//    public partial class LoansAppliedInfo : System.Web.UI.Page
//    {
//        ReportDocument Report = new ReportDocument();
//        protected void Page_Load(object sender, EventArgs e)
//        {
//            DataTable dt = new DataTable();
//            string SQL = "select * from vw_LoansApplied  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
//            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
//            //string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
//            using (SqlConnection conn = new SqlConnection(sConstr))
//            {
//                using (SqlCommand comm = new SqlCommand(SQL, conn))
//                {
//                    conn.Open();
//                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
//                    {
//                        dt = new DataTable("tbl");
//                        da.Fill(dt);
//                    }
//                }
//            }


//            string reportpathx = "";
//            reportpathx = System.Configuration.ConfigurationManager.AppSettings["reportpath"];

//            ReportDocument cryRpt = new ReportDocument();
//            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
//            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
//            ConnectionInfo crConnectionInfo = new ConnectionInfo();
//            Tables CrTables;


//            string absolutepath = reportpathx + "LoansAppliedData.rpt";

//            string reportPath = Server.MapPath(absolutepath);
//            cryRpt.Load(reportPath);


//            string ServerName = "";
//            string DatabaseName = "";
//            string UserID = "";
//            string password = "";

//            ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
//            DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
//            UserID = System.Configuration.ConfigurationManager.AppSettings["UserID"];
//            password = System.Configuration.ConfigurationManager.AppSettings["password"];



//            crConnectionInfo.ServerName = ServerName;
//            crConnectionInfo.DatabaseName = DatabaseName;
//            crConnectionInfo.UserID = UserID;
//            crConnectionInfo.Password = password;

//            CrTables = cryRpt.Database.Tables;
//            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
//            {
//                crtableLogoninfo = CrTable.LogOnInfo;
//                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
//                CrTable.ApplyLogOnInfo(crtableLogoninfo);
//            }


//            cryRpt.Load(reportPath);
//            cryRpt.SetDataSource(dt);
//            cryRpt.SetParameterValue("currentuser", "Test");
//            LoansAppliedViewer.ReportSource = cryRpt;
//            LoansAppliedViewer.HasRefreshButton = true;
//            LoansAppliedViewer.HasPageNavigationButtons = true;
//            LoansAppliedViewer.RefreshReport();
//        }
//    }
//}