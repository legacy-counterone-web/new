﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestNAVBAR.Controllers
{
    public partial class Notification : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
     
    }
      
        public int GetLatestNumberOfEmails()
        {
            int numberOfEmails = 0;

            using (SqlConnection conn = new SqlConnection
              (WebConfigurationManager.ConnectionStrings[0].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("GetLatestNumberOfEmails", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();
                    numberOfEmails = (int)cmd.ExecuteScalar();
                }
            }

            return numberOfEmails;
        }
    }
}