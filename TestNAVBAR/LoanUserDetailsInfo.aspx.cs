﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Text.RegularExpressions;

namespace TestNAVBAR
{
    public partial class LoanUserDetailsInfo : System.Web.UI.Page
    {
        ReportDocument Report = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            string dateRange_ = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
            string _DateFrom = dateRange_.Replace("(", string.Empty).Replace(")", string.Empty);
            string[] _Begin  = Regex.Split(_DateFrom, "AND");
            string[] _datefrom = Regex.Split(_Begin[0], ">=");
            string datefrom = _datefrom[1].Replace("'",string.Empty);
            string[] _dateto = Regex.Split(_Begin[1], "<=");
            string dateto = _dateto[1].Replace("'", string.Empty);
            DataTable dt;
            String SQL = "select distinct *from LoanUsers WHERE " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";

            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument rd = new ReportDocument();            
            string reportPath = Server.MapPath("~/aspnet_client/LoanUsersData.rpt");
            rd.Load(reportPath);
            rd.SetDataSource(dt);
          //  rd.SetParameterValue("mdate", "date from "+datefrom+" date to"+dateto); // Add multiple parameter as you want
            rd.SetParameterValue("mdate", "From: " + datefrom + "\n    To: " + dateto); // Add multiple parameter as you want
            rd.SetParameterValue("muser", System.Web.HttpContext.Current.Session["username"].ToString()); // Add multiple parameter as you want
            // rd.SetParameterValue("mdate", System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString()); // Add multiple parameter as you want
            //rd.SetParameterValue("printuser", _currentuser); // Add multiple parameter as you want
            LoanUsersViewer.ReportSource = rd;

        }
    }
}