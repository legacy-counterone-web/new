﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TestNAVBAR.Models
{
    public partial class SalaryAdvancePendingReviewInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "select * from LoanApplications a,Employers b WHERE a.loanapproved='0' and a.loandisbursed='0' and a.vetted='YES' and a.sentforreview='1' and a.Employerid=b.id ";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument SalaryAdvancePendingReview = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/SalaryAdvancePendingReviewData.rpt");
            SalaryAdvancePendingReview.Load(reportPath);
            SalaryAdvancePendingReview.SetDataSource(dt);
            SalaryAdvancePendingReviewViewer.ReportSource = SalaryAdvancePendingReview;
            SalaryAdvancePendingReviewViewer.HasToggleGroupTreeButton = false;
            SalaryAdvancePendingReviewViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
        }
    }
}