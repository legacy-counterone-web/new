﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestNAVBAR
{
    public partial class Incomestatement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string reportpathx = "";
            reportpathx = System.Configuration.ConfigurationManager.AppSettings["reportpath"];

            ReportDocument cryRpt = new ReportDocument();
            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;


            string absolutepath = reportpathx + "Incomestatement.rpt";

            string reportPath = Server.MapPath(absolutepath);
            cryRpt.Load(reportPath);


            string ServerName = "";
            string DatabaseName = "";
            string UserID = "";
            string password = "";

            ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
            DatabaseName = System.Configuration.ConfigurationManager.AppSettings["DatabaseName"];
            UserID = System.Configuration.ConfigurationManager.AppSettings["UserID"];
            password = System.Configuration.ConfigurationManager.AppSettings["password"];



            crConnectionInfo.ServerName = ServerName;
            crConnectionInfo.DatabaseName = DatabaseName;
            crConnectionInfo.UserID = UserID;
            crConnectionInfo.Password = password;

            CrTables = cryRpt.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }
            ISviewer.Zoom(75);
            ISviewer.ReportSource = cryRpt;
            ISviewer.BestFitPage = true;
            ISviewer.HasRefreshButton = true;
            ISviewer.HasPageNavigationButtons = true;
            ISviewer.RefreshReport();
        }
    }
}