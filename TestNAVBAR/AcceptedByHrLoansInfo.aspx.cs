﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public partial class AcceptedByHrLoansInfo : System.Web.UI.Page
    {
        ReportDocument Report = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "select * from LoanApplications a,Employers b WHERE a.acceptrejectflag='1' and b.ID=a.employerid ";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument AcceptedByHrLoans = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/AcceptedByHrLoansData.rpt");
            AcceptedByHrLoans.Load(reportPath);
            AcceptedByHrLoans.SetDataSource(dt);
            AcceptedByHrLoansViewer.ReportSource = AcceptedByHrLoans;
            AcceptedByHrLoansViewer.HasToggleGroupTreeButton = false;
            AcceptedByHrLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            AcceptedByHrLoansViewer.HasPageNavigationButtons = true;

        }
    }
}