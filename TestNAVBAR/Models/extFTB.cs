﻿namespace TestNAVBAR.Models
{
    public class extFTB
    {

        public extFTA extfsDetails;
        public string extFromAccount { get; set; }
        public string extToAccount { get; set; }
        public double extAmount { get; set; }
        public string extDescription { get; set; }
    }
}