﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class Deposits
    {
        public string Transactionref { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Idno { get; set; }
        public string Deposit { get; set; }
        public string Withdrawals { get; set; }
        public string Balance { get; set; }
        public string Phone { get; set; }
        public string Types { get; set; }
    }
}