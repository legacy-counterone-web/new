﻿using System;

namespace TestNAVBAR.Models
{
    class TestPersonalData
    {
        public int ID { get; set; }
        public string StationName { get; set; }
        public DateTime Date { get; set; }
        public Double Amount { get; set; }
        public Double TaxBonus { get; set; }
        public String Comment { get; set; }
    }
}
