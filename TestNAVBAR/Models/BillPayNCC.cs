﻿namespace TestNAVBAR.Models
{
    public class BillPayNCC
    {

        public string bpDebitncc { get; set; }
        public string bpCreditncc { get; set; }
        public string bpCarRegNoncc { get; set; }
        public string bpAmountncc { get; set; }
    }
}