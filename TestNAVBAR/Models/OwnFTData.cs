﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public class OwnFTData
    {

        BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();

        public OwnFTA ProcessownftQueryItem()
        {
            OwnFTA internalownftlistquery = new OwnFTA();
            internalownftlistquery.ownFtFlag = "1";
            internalownftlistquery.Action = "Select";
            internalownftlistquery.ownAccountNo = "1000000000001234";
            internalownftlistquery.ownAccountNo2 = "1000000000002093";
            internalownftlistquery.ownAccounttype = "Savings-Senior";
            internalownftlistquery.ownCurrency = "USD";
            internalownftlistquery.ownLedgerBal = 3400.54;
            internalownftlistquery.ownAvailBal = 3200.42;
            internalownftlistquery.ownAccountStatus = "Active";
            return internalownftlistquery;

        }

        public OwnFTB ProcessownFT()
        {

            OwnFTB internalftpostlist = new OwnFTB();
            internalftpostlist.ownFromAccount = "1000000000001234";
            internalftpostlist.ownToAccount = "1000000000002093";
            internalftpostlist.ownAmount = 210;
            internalftpostlist.ownDescription = "Garage Service Fee";

            return internalftpostlist;

        }


        public List<Dictionary<string, object>> InternalFTSelectionList
        {
            get
            {


                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 1000 [SenderEmailAddress] ,[Password],[SMTPServerName]  ,[Domain]  FROM [BRIDGE].[dbo].[EmailSetUp]";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);

                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["acAccountNumber"] = rd["acAccountNumber"];
                            entry["acAccountType"] = rd["acAccountType"];
                            entry["acCurrency"] = rd["acCurrency"];
                            entry["acActualBalance"] = rd["acActualBalance"];
                            entry["acAvailableBalance"] = rd["acAvailableBalance"];
                            //entry["Active"] = rd["Active"];
                            entry["Active"] = rd["Active"].ToString().ToLower().Equals("true") ? "Active" : "unauthorised";
                            data.Add(entry);
                        }

                    }
                }
                rd.Close();
                rd.Dispose();
                return data;
            }
        }
    }
}