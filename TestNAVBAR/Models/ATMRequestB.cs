﻿namespace TestNAVBAR.Models
{
    public class ATMRequestB
    {
        public ATMRequestA AtmDetails;
        public string bpFromAccount { get; set; }
        public string bpRemarks { get; set; }
    }
}