﻿namespace TestNAVBAR.Models
{
    public class StopChequeRequestB
    {

        public StopChequeRequestA stopchqDetails;
        public string bpFromAccount { get; set; }
        public string chequenumber { get; set; }
        public string BlockReason { get; set; }
    }
}