﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace TestNAVBAR.Models
{
    public class Billspayment
    {
        string IncomingOFSString = "";
        string t24IPAddress = "";
        int t24BankingPort = 0;

        public string SendToMpesalistner(string StringToMpesa)
        {
            string _mpesaresponse;
            try
            {
                //POST TO URL KIMANI WILL GIVE
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    System.Collections.Specialized.NameValueCollection nvc = new System.Collections.Specialized.NameValueCollection()
                  {
                     {"StringToMpesa",StringToMpesa}
                  };
                    byte[] responseBytes = client.UploadValues("https://counterone.net:8181/counterone/counteronetest/payable.php", nvc);
                    _mpesaresponse = System.Text.Encoding.ASCII.GetString(responseBytes);
                }
            }
            catch (Exception ex)
            {
                _mpesaresponse = "90001-Error posting to ";
            }
            return _mpesaresponse;
        }

        public string PHPRequest(string strURL, string strRequest)
        {
            HttpWebResponse objHttpWebResponse = null;
            UTF8Encoding encoding;
            string strResponse = "";

            HttpWebRequest objHttpWebRequest;
            objHttpWebRequest = (HttpWebRequest)WebRequest.Create(strURL);
            objHttpWebRequest.ContentType = "application/x-www-form-urlencoded";
            objHttpWebRequest.PreAuthenticate = true;

            objHttpWebRequest.Method = "POST";

            //Prepare the request stream
            if (strRequest != null && strRequest != string.Empty)
            {
                encoding = new UTF8Encoding();
                Stream objStream = objHttpWebRequest.GetRequestStream();
                Byte[] Buffer = encoding.GetBytes(strRequest);
                // Post the request
                objStream.Write(Buffer, 0, Buffer.Length);
                objStream.Close();
            }
            objHttpWebResponse = (HttpWebResponse)objHttpWebRequest.GetResponse();
            encoding = new UTF8Encoding();
            StreamReader objStreamReader =
                new StreamReader(objHttpWebResponse.GetResponseStream(), encoding);
            strResponse = objStreamReader.ReadToEnd();
            objHttpWebResponse.Close();
            objHttpWebRequest = null;
            return strResponse;
        }
        public string PHPRequestvs2(string strURL, string amount)
        {
            string pagesource = "";
            using (WebClient client = new WebClient())
            {
                NameValueCollection postData = new NameValueCollection()
    {
            { "amount", amount }  //order: {"parameter name", "parameter value"}
           
    };

                System.Net.ServicePointManager.ServerCertificateValidationCallback +=
    delegate (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
        return true; // **** Always accept
    };

                // client.UploadValues returns page's source as byte array (byte[])
                // so it must be transformed into a string
                pagesource = Encoding.UTF8.GetString(client.UploadValues(strURL, postData));


            }

            return pagesource;
        }
        public string cpayment(string strURL, string amount, string phone, string mpesaref, string loanreference, string disbursedate, string custname)
        {
            // transamount,mpesano,mppesaref,loanref,ddate,custname 
            string pagesource = "";
            using (WebClient client = new WebClient())
            {
                NameValueCollection postData = new NameValueCollection()
    {
            { "transamount",amount },  //order: {"parameter name", "parameter value"}
            { "loanref",loanreference },
             { "mpesaref",mpesaref },
              { "ddate",disbursedate },
               { "custname",custname },
            { "mpesano", phone }
    };

                System.Net.ServicePointManager.ServerCertificateValidationCallback +=
    delegate (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
        return true; // **** Always accept
    };

                // client.UploadValues returns page's source as byte array (byte[])
                // so it must be transformed into a string
                pagesource = Encoding.UTF8.GetString(client.UploadValues(strURL, postData));


            }

            return pagesource;
        }


        public string SendEmail(string fromAddress, string toAddress, string MsgID, string fromPassword, string subject, string EmailBody)
        {
            string emailResponse = "";
            MailMessage mail = new MailMessage();
            SmtpClient SmtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress(fromAddress.ToString());
            mail.To.Add(toAddress.ToString());
            mail.Subject = "OTP Password";
            mail.Body = EmailBody.ToString();
            //System.Net.Mail.Attachment attachment;
            //attachment = new System.Net.Mail.Attachment("c:/textfile.txt");
            //mail.Attachments.Add(attachment);

            SmtpServer.Port = 587;
            SmtpServer.Credentials = new System.Net.NetworkCredential(fromAddress.ToString(), fromPassword.ToString());
            SmtpServer.EnableSsl = true;

            SmtpServer.Send(mail);
            return emailResponse;
        }
        public String inFTTransaction(String tag, String InOFS, String _MsgID)
        {

            return null;
        }
    }
}