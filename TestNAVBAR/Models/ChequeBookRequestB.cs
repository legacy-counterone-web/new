﻿namespace TestNAVBAR.Models
{
    public class ChequeBookRequestB
    {

        public ChequeBookRequestA chequeDetails;
        public string bpFromCHQAccount { get; set; }
        public string bpNumberOfPages { get; set; }
    }
}