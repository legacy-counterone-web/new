﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public class extFTData
    {

        BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();

        public extFTA ProcessextftQueryItem()
        {
            extFTA extftlistquery = new extFTA();
            extftlistquery.extFtFlag = "1";
            extftlistquery.extAction = "Select";
            extftlistquery.extAccountNo = "1000000000001234";
            extftlistquery.extAccountNo2 = "1000000000002093";
            extftlistquery.extAccounttype = "Savings-Senior";
            extftlistquery.extCurrency = "USD";
            extftlistquery.extLedgerBal = 3400.54;
            extftlistquery.extAvailBal = 3200.42;
            extftlistquery.extAccountStatus = "Active";
            return extftlistquery;

        }

        public extFTB ProcessextFT()
        {

            extFTB extftpostlist = new extFTB();
            extftpostlist.extFromAccount = "1000000000001234";
            extftpostlist.extToAccount = "1000000000002093";
            extftpostlist.extAmount = 210;
            extftpostlist.extDescription = "Garage Service Fee";

            return extftpostlist;

        }


        public List<Dictionary<string, object>> InternalFTSelectionList
        {
            get
            {


                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 1000 [SenderEmailAddress] ,[Password],[SMTPServerName]  ,[Domain]  FROM [BRIDGE].[dbo].[EmailSetUp]";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);

                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["acAccountNumber"] = rd["acAccountNumber"];
                            entry["acAccountType"] = rd["acAccountType"];
                            entry["acCurrency"] = rd["acCurrency"];
                            entry["acActualBalance"] = rd["acActualBalance"];
                            entry["acAvailableBalance"] = rd["acAvailableBalance"];
                            //entry["Active"] = rd["Active"];
                            entry["Active"] = rd["Active"].ToString().ToLower().Equals("true") ? "Active" : "unauthorised";
                            data.Add(entry);
                        }

                    }
                }
                rd.Close();
                rd.Dispose();
                return data;
            }
        }
    }
}