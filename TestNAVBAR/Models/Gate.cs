﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Web.Mvc;
using System.Collections.Generic;

namespace TestNAVBAR.Models
{
    public class Gate
    {
        public string PreprationRequired { get; set; }
        public List<CheckBoxes> lstPreprationRequired { get; set; }
        public string[] CategoryIds { get; set; }
    }

    public class CheckBoxes
    {
        public int ID { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
        public bool Checked { get; set; }
    }
}

 