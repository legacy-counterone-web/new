﻿namespace TestNAVBAR.Models
{
    public class BillPayA
    {
        public string bpAction { get; set; }
        public string bpAccountNo { get; set; }
        public string bpAccountNo2 { get; set; }
        public string bpAccounttype { get; set; }
        public string bpCurrency { get; set; }
        public double bpLedgerBal { get; set; }
        public double bpAvailBal { get; set; }
        public string bpFtFlag { get; set; }
        public string bpAccountStatus { get; set; }
        public string bpSelectedAccount {get;set;}
    }

}