﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class commission
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string idno { get; set; }
        public string loantype { get; set; }
        public string transactedamount { get; set; }
        public string commissionrate { get; set; }
        public string totatalcommission { get; set; }
        public string transacteddate { get; set; }
    }
}