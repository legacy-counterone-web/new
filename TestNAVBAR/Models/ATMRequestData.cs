﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public class ATMRequestData
    {
        ATMRequestB bpftpostlist = new ATMRequestB();
        BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
        public ATMRequestA ProcessbpQueryItem()
        {
            ATMRequestA bpftlistquery = new ATMRequestA();
            bpftlistquery.bpFtFlag = "1";
            bpftlistquery.bpAction = "Select";
            bpftlistquery.bpAccountNo = "1000000000001234";
            bpftlistquery.bpAccountNo2 = "1000000000002093";
            bpftlistquery.bpAccounttype = "Savings-Senior";
            bpftlistquery.bpCurrency = "USD";
            bpftlistquery.bpLedgerBal = 3400.54;
            bpftlistquery.bpAvailBal = 3200.42;
            bpftlistquery.bpAccountStatus = "Active";
            return bpftlistquery;

        }

        public SelectList ChequeBookPages
        {
            get
            {
                List<object> data = new List<object>();

                data.Add(new { Name = "-select one-", Value = "" });
                data.Add(new { Name = "25", Value = "25" });
                data.Add(new { Name = "50", Value = "50" });
                data.Add(new { Name = "100", Value = "100" });

                return new SelectList(data, "Value", "Name", bpftpostlist.bpRemarks);
            }
        }







        public ATMRequestB ProcessSelectUtility()
        {

            bpftpostlist.bpFromAccount = "1000000000001234";
            bpftpostlist.bpRemarks = "50";

            return bpftpostlist;

        }

        public List<Dictionary<string, object>> InternalFTSelectionList
        {
            get
            {


                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 1000 [SenderEmailAddress] ,[Password],[SMTPServerName]  ,[Domain]  FROM [BRIDGE].[dbo].[EmailSetUp]";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);

                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["acAccountNumber"] = rd["acAccountNumber"];
                            entry["acAccountType"] = rd["acAccountType"];
                            entry["acCurrency"] = rd["acCurrency"];
                            entry["acActualBalance"] = rd["acActualBalance"];
                            entry["acAvailableBalance"] = rd["acAvailableBalance"];
                            //entry["Active"] = rd["Active"];
                            entry["Active"] = rd["Active"].ToString().ToLower().Equals("true") ? "Active" : "unauthorised";
                            data.Add(entry);
                        }

                    }
                }
                rd.Close();
                rd.Dispose();
                return data;
            }
        }
    }
}