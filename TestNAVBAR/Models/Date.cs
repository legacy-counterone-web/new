﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TestNAVBAR.Models
{
    public class Date
    {
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
    }
}