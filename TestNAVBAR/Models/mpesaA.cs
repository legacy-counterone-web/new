﻿namespace TestNAVBAR.Models
{
    public class mpesaA
    {
        public string mpesaAction { get; set; }
        public string mpesaAccountNo { get; set; }
        public string mpesaAccountNo2 { get; set; }
        public string mpesaAccounttype { get; set; }
        public string mpesaCurrency { get; set; }
        public double mpesaLedgerBal { get; set; }
        public double mpesaAvailBal { get; set; }
        public string mpesaFtFlag { get; set; }
        public string mpesaAccountStatus { get; set; }
        public string mpesaSelectedAccount { get; set; }
    }
}