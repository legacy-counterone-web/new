﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class Repay
    {
        public int Rid { get; set; }
        public string lid { get; set; }
        [DisplayName("Loan Ref")]
        public string loaf { get; set; }
        [DisplayName("Payment Date")]
        public string pdate { get; set; }
        [DisplayName("Transaction Code")]
        public string tcode { get; set; }
        [DisplayName("Balance")]
        public string balo { get; set; }
        [DisplayName("Borrow Date")]
        public string bdate { get; set; }
        [DisplayName("Payment Score")]
        public string pcore { get; set; }
        [DisplayName("Expected Payment")]
        public string epayments { get; set; }
        [DisplayName("Transaction Amount")]
        public string tamount { get; set; }
        [DisplayName("Rectified by")]
        public string rby { get; set; }
        [DisplayName("User")]
        public string uright { get; set; }
        [DisplayName("Admin")]
        public string aright { get; set; }
        [DisplayName("Actioned by")]
        public string aby { get; set; }
    }
}