﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class images
    {
        public class Datum
        {
            public int Id { get; set; }
            public string Image { get; set; }
            public string phone { get; set; }
        }

        public class RootObject
        {
            public object ContentEncoding { get; set; }
            public object ContentType { get; set; }
            public List<Datum> Data { get; set; }
            public int JsonRequestBehavior { get; set; }
            public object MaxJsonLength { get; set; }
            public object RecursionLimit { get; set; }
        }
    }
}