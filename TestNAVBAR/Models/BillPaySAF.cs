﻿namespace TestNAVBAR.Models
{
    public class BillPaySAF
    {

        public string bpDebitsaf { get; set; }
        public string bpCreditsaf { get; set; }
        public string bpAccountNoSaf { get; set; }
        public string bpAmountSaf { get; set; }
    }
}