﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class Fincharges
    {
        [DisplayName("Month")]
        public string loandisbursedate { get; set; }
        [DisplayName("Total Disbursed")]
        public string principle { get; set; }
        [DisplayName("Total Interest")]
        public string adminfee { get; set; }
        [DisplayName("Year")]
        public string years { get; set; }
        public string paystatus { get; set; }
        public string phone { get; set; }
        public string month { get; set; }
        public string day { get; set; }
        public int charid { get; set; }
    }
}