﻿namespace TestNAVBAR.Models
{
    public class BillPayKPLC
    {
        public string bpDebitKPLC { get; set; }
        public string bpCreditKPLC { get; set; }
        public string bpMetreNoKPLC { get; set; }
        public string bpAmountKPLC { get; set; }


    }
}