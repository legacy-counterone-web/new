﻿namespace TestNAVBAR.Models
{


    public class BillPayDSTV
    {
        public string bpDebitDTSV { get; set; }
        public string bpCreditDSTV { get; set; }
        public string bpAmountDSTV { get; set; }
        public string bpPackageDSTV { get; set; }

    }

}