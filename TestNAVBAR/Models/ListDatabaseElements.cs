﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.ComponentModel.DataAnnotations;
using TestNAVBAR.Controllers;
using System.Globalization;
using System.Web;

namespace TestNAVBAR.Models
{

    public class ListDatabaseElements
    {
        BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
        private Utilities.Users ulogic = new Utilities.Users();
        private object myparam;

        public string selectedsaving
        {
            get;
            set;
        }
        public string selectedDuration
        {
            get;
            set;
        }
        public string selectedEmployer
        {
            get;
            set;
        }
        public string selectedRoles
        {
            get;
            set;
        }
        public string selectedCountry
        {
            get;
            set;
        }
        public string selectedloancodetypes
        {
            get;
            set;
        }
        public string selectedduration
        {
            get;
            set;
        }
        public string selectedloancode
        {
            get;
            set;
        }
        public string selectedloantype
        {
            get;
            set;
        }


        public string selectedReports
        {
            get;
            set;
        }
        public string selectedLoanType
        {
            get;
            set;
        }
        public string selectedAR
        {
            get;
            set;
        }
        public string selectedLoanCodes
        {
            get;
            set;
        }
        public string selectedAccountType
        {
            get;
            set;
        }
        public string selectedSupermarket
        {
            get;
            set;
        }

        public string selectedsupermarketlist2
        {
            get;
            set;
        }

        public string LoanUsers360 { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime SomeTimeDate { get; set; }

        [Display(Name = "Start date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime startdate { get; set; }


        [Display(Name = "End date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime enddate { get; set; }






        public string SelectedToAccount
        {
            get;
            set;
        }
        public SelectList AccountTypeList
        {
            get
            {
                List<object> data = new List<object>();

                data.Add(new { AccountType = "Assets" });
                data.Add(new { AccountType = "Liabilities" });
                data.Add(new { AccountType = "Income" });
                data.Add(new { AccountType = "Expenses" });


                // data.Add("");
                return new SelectList(data, "AccountType", "AccountType", selectedAccountType);

            }
        }

        public SelectList EmployerList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select CompanyName +'-'+ CONVERT(varchar(10), ID) companyname from Employers";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { companyname = rd["companyname"], companycode = rd["companyname"] });
                        }
                    }
                    return new SelectList(data, "companyname", "companyname", selectedEmployer);
                }
            }
        }
        public SelectList CountryList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select Countryname +'-'+ CONVERT(varchar(10), countrycode) Countryname from tblcountry";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { Countryname = rd["Countryname"], companycode = rd["Countryname"] });
                        }
                    }
                    return new SelectList(data, "Countryname", "Countryname", selectedCountry);
                }
            }
        }
        public SelectList RolesList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select Rolename +'-'+ CONVERT(varchar(10), roleid) Rolename from tblroles";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { Rolename = rd["Rolename"], companycode = rd["Rolename"] });
                        }
                    }
                    return new SelectList(data, "Rolename", "Rolename", selectedRoles);
                }
            }
        }
        public SelectList LoantypeList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select loancode +'-'+ loantype loancode from tbloancodes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { loancode = rd["loancode"], loantype = rd["loancode"] });
                        }
                    }
                    return new SelectList(data, "loancode", "loancode", selectedloancodetypes);
                }
            }
        }
        public SelectList DurationList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select Duration +'-'+ Days Duration  from tblloanschedule";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { Duration = rd["Duration"], Days = rd["Duration"] });
                        }
                    }
                    return new SelectList(data, "Duration", "Duration", selectedduration);
                }
            }
        }
        public SelectList LoanTypeList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select loantype +'-'+ CONVERT(varchar(10), loantypeid) loantype from loancodes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { loantype = rd["loantype"], loancode = rd["loantype"] });
                        }
                    }
                    return new SelectList(data, "loantype", "loantype", selectedLoanType);
                }
            }
        }
        public SelectList savingList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select Typecode +'-'+ Types Typecode from tblsavingtypes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { Typecode = rd["Typecode"], loancode = rd["Typecode"] });
                        }
                    }
                    return new SelectList(data, "Typecode", "Typecode", selectedsaving);
                }
            }
        }
        public SelectList SavingDurationList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select Duration +'-'+ CONVERT(varchar(10), Days) Duration from tblsavingschedule";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { Duration = rd["Duration"], loancode = rd["Duration"] });
                        }
                    }
                    return new SelectList(data, "Duration", "Duration", selectedDuration);
                }
            }
        }
        public List<Dictionary<string, object>> Audittrail
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select strusername,strusertype,strAction,dtDate,dtTime    from dbo.tbAdminAuditTrail order by auditID desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["strusername"] = rd["strusername"];
                            entry["strusertype"] = rd["strusertype"];
                            entry["strAction"] = rd["strAction"].ToString();
                            entry["dtDate"] = rd["dtDate"];
                            entry["dtTime"] = rd["dtTime"];



                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> AudittrailTime
        {
            get
            {
                string sql = "";
                string testss = "";
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                sql = "select strusername,strusertype,strAction,dtDate,dtTime    from dbo.tbAdminAuditTrail " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "order by auditID desc ";//order by dtDate desc
                testss = sql;
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["strusername"] = rd["strusername"];
                            entry["strusertype"] = rd["strusertype"];
                            entry["strAction"] = rd["strAction"].ToString();
                            entry["dtDate"] = rd["dtDate"];
                            entry["dtTime"] = rd["dtTime"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoanSchedule
        {

            get
            {
                //Request.Url.AbsoluteUri
                Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                //System.Web.HttpContext.Current.Session.[""];
                string URLs = myUri.AbsolutePath.ToString();
                string[] recordID = URLs.Split('/');
                string IDyetu = recordID[4];
                //HttpContext.Current.Session["userid"] = IDyetu;
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                //string test = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
                //string MuserID = HttpContext.Current.Session["userid"].ToString();

                string sql = " select a.* from loanschedule  a,loanapplications b where a.loanreference=b.loanref and b.loanid='" + IDyetu + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["entryid"] = rd["entryid"];
                            entry["amounttopay"] = rd["amounttopay"];
                            entry["phone"] = rd["phone"];
                            entry["dateofpayment"] = rd["dateofpayment"].ToString().Replace("12:00:00 AM", "");
                            entry["loanreference"] = rd["loanreference"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> TopupFloataccountstatement
        {

            get
            {
                Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                string URLs = myUri.AbsolutePath.ToString();
                string[] recordID = URLs.Split('/');
                string IDyetu = recordID[4];
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from tblfloatstatement where code='" + IDyetu + "' order by id DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["description"] = rd["description"];
                            entry["Amount"] = rd["Amount"];
                            entry["Balance"] = rd["Balance"];
                            entry["Date"] = rd["Date"];
                            entry["account"] = rd["account"];
                            entry["type"] = rd["type"];
                            entry["code"] = rd["code"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> ShoppingDetailsDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select shoppingdate,supermarketname,description,customerphone,shoppingtype FROM ShoppingDetails " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["supermarketname"] = rd["supermarketname"];
                            entry["description"] = rd["description"];
                            entry["shoppingdate"] = rd["shoppingdate"].ToString().Replace("12:00:00 AM", "");
                            entry["customerphone"] = rd["customerphone"];
                            entry["shoppingtype"] = rd["shoppingtype"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ShoppingDetails
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select TOP 800 shoppingdate,supermarketname,description,customerphone,shoppingtype FROM ShoppingDetails " + " ORDER BY ID DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["supermarketname"] = rd["supermarketname"];
                            entry["description"] = rd["description"];
                            entry["shoppingdate"] = rd["shoppingdate"].ToString().Replace("12:00:00 AM", "");
                            entry["customerphone"] = rd["customerphone"];
                            entry["shoppingtype"] = rd["shoppingtype"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoanTypesDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanCode,LoanType FROM LoanApplications WHERE loanamount>'0' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loancode"] = rd["loancode"];
                            entry["loantype"] = rd["loantype"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public SelectList supermarketEmployerList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select supermarketcode  +'||' + supermarketbranch +'|| '+ supermarketname as res from Supermarket";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { companyname = rd["res"], companycode = rd["res"] });
                        }
                    }
                    return new SelectList(data, "companyname", "companyname", selectedEmployer);
                }
            }
        }


        public SelectList newsupermarketlist
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select supermarketcode from Supermarket order by supermarketname";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { companyname = rd["supermarketcode"], companycode = rd["supermarketcode"] });
                        }
                    }
                    return new SelectList(data, "supermarketcode", "supermarketcode", selectedEmployer);
                }
            }
        }
        public SelectList outlets
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select distinct(supermarketname)  as description from supermarket  order by supermarketname ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { description = rd["description"], reportcode = rd["description"] });
                        }
                    }
                    return new SelectList(data, "description", "description", selectedEmployer);
                }
            }
        }


        public SelectList ReportsList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select CONVERT(varchar(10), id)+'-'+ description description from Reports";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { description = rd["description"], reportcode = rd["description"] });
                        }
                    }
                    return new SelectList(data, "description", "description", selectedEmployer);
                }
            }
        }

        public SelectList vendor
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select distinct(vendor)  as description from dbo.ShoppingDetails";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { description = rd["description"], reportcode = rd["description"] });
                        }
                    }
                    return new SelectList(data, "description", "description", selectedEmployer);
                }
            }
        }



        public SelectList vendor_stocklist
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select distinct(vendorname)  as description from tb_vendors  order by vendorname ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { description = rd["description"], reportcode = rd["description"] });
                        }
                    }
                    return new SelectList(data, "description", "description", selectedEmployer);
                }
            }
        }


        public SelectList shopping
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select distinct(shoppingtype)  as description from dbo.ShoppingDetails";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { description = rd["description"], reportcode = rd["description"] });
                        }
                    }
                    return new SelectList(data, "description", "description", selectedEmployer);
                }
            }
        }

        public SelectList accountcode
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "Select AccountNo +'-'+ ItemName as AccountNo from ChartOfAccounts where selectcode is not NULL";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { AccountNo = rd["AccountNo"], reportcode = rd["AccountNo"] });
                        }
                    }
                    return new SelectList(data, "AccountNo", "AccountNo", selectedEmployer);
                }
            }
        }

        public SelectList LoansList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select loanname from LoanTypes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { loanname = rd["loanname"], loancode = rd["loanname"] });
                        }
                    }
                    return new SelectList(data, "loanname", "loanname", selectedLoanType);
                }
            }
        }

        public List<Dictionary<string, object>> supermarketlist
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select supermarketcode  +'||' + supermarketbranch +'|| '+ supermarketname as results from Supermarket order by supermarketname";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);

                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)

                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            //data.Add(new { supermarkets = rd["results"], supermarketlist = rd["results"] });
                            entry["results"] = rd["results"];
                            data.Add(entry);
                        }
                    }
                    return data;
                }
            }
        }

        public List<Dictionary<string, object>> supermarketlist2
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select ID as recordid supermarketcode  +'||' + supermarketbranch +'|| '+ supermarketname as resultsx from Supermarket order by supermarketname";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["recordid"] = rd["recordid"];
                            entry["resultsx"] = rd["resultsx"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public SelectList LoanCodesList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select loantype from LoanCodes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { loantype = rd["loantype"], loancode = rd["loantype"] });
                        }
                    }
                    return new SelectList(data, "loantype", "loantype", selectedLoanCodes);
                }
            }
        }
        public SelectList ARList
        {
            get
            {
                List<object> data = new List<object>();

                data.Add(new { acceptrejectflag = "Accept" });
                data.Add(new { acceptrejectflag = "Reject" });
                // data.Add("");
                return new SelectList(data, "acceptrejectflag", "acceptrejectflag", selectedAR);

            }
        }

        public List<Dictionary<string, object>> RegisteredUsers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select * from Users WHERE Approved=0 or Approved is NULL";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["userid"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["gender"] = rd["gender"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["idorpp"] = rd["idorpp"];
                            entry["vrno"] = rd["vrno"];
                            entry["datecreated"] = rd["datecreated"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }



        //public List<Dictionary<string, object>>Tools
        //{
        //    get
        //    {
        //        List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
        //        string sql = " select interestrate,adminfee,principle,loantype* from loancodes";
        //        SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
        //        using (rd)
        //        {
        //            if (rd != null && rd.HasRows)
        //            {
        //                while (rd.Read())
        //                {
        //                    Dictionary<string, object> entry = new Dictionary<string, object>();

        //                    entry["adminfee"] = rd["adminfee"];
        //                    entry["principle"] = rd["principle"];
        //                    entry["interestfee"] = rd["interestfee"];
        //                    entry["loantype"] = rd["loantype"];

        //                    data.Add(entry);
        //                }
        //            }
        //        }
        //        return data;
        //    }
        //}


        public List<Dictionary<string, object>> RegisteredChurch
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select channelid,customerNumber,shortname,EmailAddress,Phonenumber,Sector,Target,Industry,RegDate from customers WHERE CustomerGroup='CHURCHES-C00006' and (Approved=0 or Approved is NULL)";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["channelid"] = rd["channelid"];
                            entry["customernumber"] = rd["customernumber"];
                            entry["shortname"] = rd["shortname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["sector"] = rd["sector"];
                            entry["target"] = rd["target"];
                            entry["industry"] = rd["industry"];
                            entry["regdate"] = rd["regdate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> penaltyapproval
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  actionedby, LoanId,BorrowerPhone Phone,(select firstname+' '+lastname from LoanUsers where phonenumber=Borrowerphone)as name,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanDisbursed='1' and loanapproved='1' and Penaltystatus='0' and loancode NOT LIKE 'L003' ORDER BY LoanDisbursedate DESC ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["name"] = rd["name"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["actionedby"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> mpesamessages
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 800 mpesa.transactionid,mpesa.datetime,mpesa.transactiontype, mpesa.status, mpesa.debited,mpesa.credited,mpesa.nationalid from mpesa order by nationalid desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["transactionid"] = rd["transactionid"];
                            entry["datetime"] = rd["datetime"];
                            entry["transactiontype"] = rd["transactiontype"];
                            entry["status"] = rd["status"];
                            entry["debited"] = rd["debited"];
                            entry["credited"] = rd["credited"];
                            entry["nationalid"] = rd["nationalid"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ViewTrailbalance
        {
            get
            {
                string sql = "";
                string testss = "";
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                sql = "select accounttype,accountcode,AccountName,Dates,Amountcr,Amountdr    from dbo.Temp_TB3  ";//order by dtDate desc
                testss = sql;
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["accounttype"] = rd["accounttype"];
                            entry["accountcode"] = rd["accountcode"];
                            entry["AccountName"] = rd["AccountName"].ToString();
                            entry["Dates"] = rd["Dates"];
                            entry["Amountcr"] = rd["Amountcr"];
                            entry["Amountdr"] = rd["Amountdr"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ApprovedChurch
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select channelid,customerNumber,shortname,EmailAddress,Phonenumber,Sector,Target,Industry,RegDate from customers WHERE CustomerGroup='CHURCHES-C00006' and Approved=1 and (accountsgenerated is NULL or accountsgenerated=0)";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["channelid"] = rd["channelid"];
                            entry["customernumber"] = rd["customernumber"];
                            entry["shortname"] = rd["shortname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["sector"] = rd["sector"];
                            entry["target"] = rd["target"];
                            entry["industry"] = rd["industry"];
                            entry["regdate"] = rd["regdate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LinkApprovedChurch
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select channelid,customerNumber,shortname,EmailAddress,Phonenumber,Sector,Target,Industry,RegDate from customers WHERE CustomerGroup = 'CHURCHES-C00006' and Approved = 1 AND accountsgenerated = 1 AND (allaccountslinked is NULL or allaccountslinked=0)";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["channelid"] = rd["channelid"];
                            entry["customernumber"] = rd["customernumber"];
                            entry["shortname"] = rd["shortname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["sector"] = rd["sector"];
                            entry["target"] = rd["target"];
                            entry["industry"] = rd["industry"];
                            entry["regdate"] = rd["regdate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> PasswordGenerateList
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select channelid,customerNumber,shortname,EmailAddress,Phonenumber,Sector,Target,Industry,RegDate from customers WHERE  Approved = 1 AND accountsgenerated = 1 AND (PasswordGenerated is NULL or PasswordGenerated=0) AND (PasswordSend is NULL or PasswordSend=0)";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["channelid"] = rd["channelid"];
                            entry["customernumber"] = rd["customernumber"];
                            entry["shortname"] = rd["shortname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["sector"] = rd["sector"];
                            entry["target"] = rd["target"];
                            entry["industry"] = rd["industry"];
                            entry["regdate"] = rd["regdate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> PasswordSendList
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select channelid,customerNumber,shortname,EmailAddress,Phonenumber,Sector,Target,Industry,RegDate from customers WHERE  Approved = 1 AND accountsgenerated = 1 AND PasswordGenerated=1 AND (PasswordSend is NULL or PasswordSend=0)";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["channelid"] = rd["channelid"];
                            entry["customernumber"] = rd["customernumber"];
                            entry["shortname"] = rd["shortname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["sector"] = rd["sector"];
                            entry["target"] = rd["target"];
                            entry["industry"] = rd["industry"];
                            entry["regdate"] = rd["regdate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }



        public List<Dictionary<string, object>> DeLinkApprovedChurch
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select channelid,customerNumber,shortname,EmailAddress,Phonenumber,Sector,Target,Industry,RegDate from customers WHERE CustomerGroup = 'CHURCHES-C00006' and Approved = 1 AND accountsgenerated = 1 AND allaccountslinked=1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["channelid"] = rd["channelid"];
                            entry["customernumber"] = rd["customernumber"];
                            entry["shortname"] = rd["shortname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["sector"] = rd["sector"];
                            entry["target"] = rd["target"];
                            entry["industry"] = rd["industry"];
                            entry["regdate"] = rd["regdate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> RegisteredChurchAdmins
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,fullnames,CreateBy,createDate,Email1,Email2,Mobile1,Mobile2,stationcode,StationBranchCode,BankBranchCode FROM tbUsers WHERE (approved=0 or approved is NULL) AND Section='C' AND Admin='Y'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["createby"] = rd["CreateBy"];
                            entry["createdate"] = rd["CreateDate"];
                            entry["email1"] = rd["Email1"];
                            entry["email2"] = rd["Email2"];
                            entry["mobile1"] = rd["Mobile1"];
                            entry["mobile2"] = rd["Mobile2"];
                            entry["stationcode"] = rd["stationcode"];
                            entry["StationBranchCode"] = rd["StationBranchCode"];
                            entry["bankbranchcode"] = rd["BankBranchCode"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LinkCustomerAccounts
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select * from CustomerAccounts WHERE CustomerNo='" + System.Web.HttpContext.Current.Session["CustomerID"].ToString() + "' AND (Linked is NULL or Linked=0) ORDER BY accountid ASC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["accountid"] = rd["accountid"];
                            entry["accountno"] = rd["accountno"];
                            entry["customerno"] = rd["customerno"];
                            entry["product"] = rd["product"];
                            entry["currency"] = rd["currency"];
                            entry["category"] = rd["category"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> DeLinkCustomerAccounts
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select * from CustomerAccounts WHERE CustomerNo='" + System.Web.HttpContext.Current.Session["CustomerID"].ToString() + "' AND Linked=1 ORDER BY accountid ASC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["accountid"] = rd["accountid"];
                            entry["accountno"] = rd["accountno"];
                            entry["customerno"] = rd["customerno"];
                            entry["product"] = rd["product"];
                            entry["currency"] = rd["currency"];
                            entry["category"] = rd["category"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> RegisteredVendorAdmins
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,fullnames,CreateBy,createDate,Email1,Email2,Mobile1,Mobile2,stationcode,StationBranchCode,BankBranchCode FROM tbUsers WHERE (approved=0 or approved is NULL) AND Section='V' AND Admin='Y'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["createby"] = rd["CreateBy"];
                            entry["createdate"] = rd["CreateDate"];
                            entry["email1"] = rd["Email1"];
                            entry["email2"] = rd["Email2"];
                            entry["mobile1"] = rd["Mobile1"];
                            entry["mobile2"] = rd["Mobile2"];
                            entry["stationcode"] = rd["stationcode"];
                            entry["StationBranchCode"] = rd["StationBranchCode"];
                            entry["bankbranchcode"] = rd["BankBranchCode"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> RegisteredChurchUsers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,fullnames,CreateBy, createDate,Email1,Email2,Mobile1,approved,Mobile2 FROM tbUsers";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["createby"] = rd["CreateBy"];
                            entry["createdate"] = rd["CreateDate"];
                            entry["email1"] = rd["Email1"];
                            entry["email2"] = rd["Email2"];
                            entry["mobile1"] = rd["Mobile1"];
                            entry["mobile2"] = rd["Mobile2"];
                            if (rd["approved"].ToString() == "True") { entry["approved"] = "True"; } else { entry["approved"] = "False"; }


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public SelectList PaymentmethodList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select Name loancode from tblpaymentmethods";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { loancode = rd["loancode"], loantype = rd["loancode"] });
                        }
                    }
                    return new SelectList(data, "loancode", "loancode", selectedloancodetypes);
                }
            }
        }
        public SelectList Dr_Accounts
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "Select AccountNo +'-'+ ItemName AccountNo from ChartOfAccounts where selectcode is not NULL";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { AccountNo = rd["AccountNo"], reportcode = rd["AccountNo"] });
                        }
                    }
                    return new SelectList(data, "AccountNo", "AccountNo", selectedEmployer);
                }
            }
        }
        public SelectList Cr_Accounts
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "Select AccountNo +'-'+ ItemName AccountNo from ChartOfAccounts where selectcode is not NULL";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { AccountNo = rd["AccountNo"], reportcode = rd["AccountNo"] });
                        }
                    }
                    return new SelectList(data, "AccountNo", "AccountNo", selectedEmployer);
                }
            }
        }
        public SelectList DebtorsList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select  Accountno +'-'+ Creditorname loancode from tblDebtors where approved='1'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { loancode = rd["loancode"], loantype = rd["loancode"] });
                        }
                    }
                    return new SelectList(data, "loancode", "loancode", selectedloancodetypes);
                }
            }
        }
        public SelectList CreditorsList
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "select Accountno +'-'+ Creditorname loancode from tblcreditors where approved='1'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { loancode = rd["loancode"], loantype = rd["loancode"] });
                        }
                    }
                    return new SelectList(data, "loancode", "loancode", selectedloancodetypes);
                }
            }
        }
        public List<Dictionary<string, object>> DebtorsInvoice_Payment
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Name,CreditAccount,DebitAccount,Vourcherno,Amount,Disbursesource,TransDate FROM tblinvoicepayments where approved='0' and types='1'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Name"] = rd["Name"];
                            entry["DebitAccount"] = rd["DebitAccount"];
                            entry["CreditAccount"] = rd["CreditAccount"];
                            entry["Vourcherno"] = rd["Vourcherno"];
                            entry["Amount"] = rd["Amount"];

                            entry["Disbursesource"] = rd["Disbursesource"];
                            entry["TransDate"] = rd["TransDate"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> CreditorsInvoice_Payment
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Name,CreditAccount,DebitAccount,Vourcherno,Amount,Disbursesource,TransDate FROM tblinvoicepayments where approved='0' and types='2'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Name"] = rd["Name"];
                            entry["DebitAccount"] = rd["DebitAccount"];
                            entry["CreditAccount"] = rd["CreditAccount"];
                            entry["Vourcherno"] = rd["Vourcherno"];
                            entry["Amount"] = rd["Amount"];

                            entry["Disbursesource"] = rd["Disbursesource"];
                            entry["TransDate"] = rd["TransDate"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Creditors_purchaseaccount
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorsname,Purchaseaccount,TotalInvoiceamount,Vatamount,Vats,Totalvat,Invoiceno,Vat,Invoiceamount,Createdby,Approvedby,Duedate,Datecreated,Createdby FROM tblpurchaseaccount where approved='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorsname"] = rd["Creditorsname"];
                            entry["Purchaseaccount"] = rd["Purchaseaccount"];
                            entry["Vats"] = rd["Vats"];
                            entry["Invoiceno"] = rd["Invoiceno"];
                            entry["Vat"] = rd["Vat"];

                            entry["TotalInvoiceamount"] = rd["TotalInvoiceamount"];
                            entry["Vatamount"] = rd["Totalvat"];
                            entry["Invoiceamount"] = rd["Invoiceamount"];
                            entry["Createdby"] = rd["Createdby"];
                            entry["Approvedby"] = rd["Approvedby"];
                            entry["Duedate"] = rd["Duedate"];
                            entry["Datecreated"] = rd["Datecreated"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Creditors_approvepurchaseaccounts
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorsname,Purchaseaccount,Vats, Invoiceno,Vat,Invoiceamount,Createdby,Approvedby,Duedate,Datecreated FROM tblpurchaseaccount where approved='1'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorsname"] = rd["Creditorsname"];
                            entry["Purchaseaccount"] = rd["Purchaseaccount"];
                            entry["Vats"] = rd["Vats"];
                            entry["Invoiceno"] = rd["Invoiceno"];
                            entry["Vat"] = rd["Vat"];
                            entry["Invoiceamount"] = rd["Invoiceamount"];
                            entry["Createdby"] = rd["Createdby"];
                            entry["Approvedby"] = rd["Approvedby"];
                            entry["Duedate"] = rd["Duedate"];
                            entry["Datecreated"] = rd["Datecreated"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Creditors_Rejectedpurchaseaccounts
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorsname,Purchaseaccount,Vats, Invoiceno,Vat,Invoiceamount,Createdby,Approvedby,Duedate,Datecreated FROM tblpurchaseaccount where approved='2'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorsname"] = rd["Creditorsname"];
                            entry["Purchaseaccount"] = rd["Purchaseaccount"];
                            entry["Vats"] = rd["Vats"];
                            entry["Invoiceno"] = rd["Invoiceno"];
                            entry["Vat"] = rd["Vat"];
                            entry["Invoiceamount"] = rd["Invoiceamount"];
                            entry["Createdby"] = rd["Createdby"];
                            entry["Approvedby"] = rd["Approvedby"];
                            entry["Duedate"] = rd["Duedate"];
                            entry["Datecreated"] = rd["Datecreated"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public SelectList accountcode2
        {
            get
            {
                List<object> data = new List<object>();
                string sql = "Select AccountNo +'-'+ ItemName AccountNo from ChartOfAccounts where selectcode is not NULL and typecode='3'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                //data.Add(new { companyname = "SALVON AFRICA LTD-0", companycode = "0" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { AccountNo = rd["AccountNo"], reportcode = rd["AccountNo"] });
                        }
                    }
                    return new SelectList(data, "AccountNo", "AccountNo", selectedEmployer);
                }
            }
        }
        public List<Dictionary<string, object>> Creditors
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorname,Mobileno,Email, Address,Town,Contactperson,Datecreated,CreateBy FROM tblcreditors where approved='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorname"] = rd["Creditorname"];
                            entry["Mobileno"] = rd["Mobileno"];
                            entry["Email"] = rd["Email"];
                            entry["Address"] = rd["Address"];
                            entry["Town"] = rd["Town"];
                            entry["Contactperson"] = rd["Contactperson"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["CreateBy"] = rd["CreateBy"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ApprovedCreditors
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorname,Mobileno,Email, Address,Town,Contactperson,Datecreated,CreateBy FROM tblcreditors where approved='1'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorname"] = rd["Creditorname"];
                            entry["Mobileno"] = rd["Mobileno"];
                            entry["Email"] = rd["Email"];
                            entry["Address"] = rd["Address"];
                            entry["Town"] = rd["Town"];
                            entry["Contactperson"] = rd["Contactperson"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["CreateBy"] = rd["CreateBy"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> RejectedCreditors
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorname,Mobileno,Email, Address,Town,Contactperson,Datecreated,CreateBy FROM tblcreditors where approved='2'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorname"] = rd["Creditorname"];
                            entry["Mobileno"] = rd["Mobileno"];
                            entry["Email"] = rd["Email"];
                            entry["Address"] = rd["Address"];
                            entry["Town"] = rd["Town"];
                            entry["Contactperson"] = rd["Contactperson"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["CreateBy"] = rd["CreateBy"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Debtors
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorname,Mobileno,Email, Address,Town,Contactperson,Datecreated,CreateBy FROM tblDebtors where approved='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorname"] = rd["Creditorname"];
                            entry["Mobileno"] = rd["Mobileno"];
                            entry["Email"] = rd["Email"];
                            entry["Address"] = rd["Address"];
                            entry["Town"] = rd["Town"];
                            entry["Contactperson"] = rd["Contactperson"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["CreateBy"] = rd["CreateBy"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ApprovedDebtors
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorname,Mobileno,Email, Address,Town,Contactperson,Datecreated,CreateBy FROM tblDebtors where approved='1'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorname"] = rd["Creditorname"];
                            entry["Mobileno"] = rd["Mobileno"];
                            entry["Email"] = rd["Email"];
                            entry["Address"] = rd["Address"];
                            entry["Town"] = rd["Town"];
                            entry["Contactperson"] = rd["Contactperson"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["CreateBy"] = rd["CreateBy"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> RejectedDebtors
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Creditorname,Mobileno,Email, Address,Town,Contactperson,Datecreated,CreateBy FROM tblDebtors where approved='2'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Creditorname"] = rd["Creditorname"];
                            entry["Mobileno"] = rd["Mobileno"];
                            entry["Email"] = rd["Email"];
                            entry["Address"] = rd["Address"];
                            entry["Town"] = rd["Town"];
                            entry["Contactperson"] = rd["Contactperson"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["CreateBy"] = rd["CreateBy"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Debtors_salesaccount
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Totalvat,TotalInvoiceamount,Debtorsname,Salesaccount,Vats, Invoiceno,Vat,Invoiceamount,Createdby,Approvedby,Duedate,Datecreated FROM tblsalesaccount where approved='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Debtorsname"] = rd["Debtorsname"];
                            entry["Salesaccount"] = rd["Salesaccount"];
                            entry["Vats"] = rd["Vats"];
                            entry["Invoiceno"] = rd["Invoiceno"];
                            entry["Vat"] = rd["Vat"];
                            entry["Invoiceamount"] = rd["Invoiceamount"];
                            entry["CreatedBy"] = rd["Createdby"];
                            entry["Approvedby"] = rd["Approvedby"];
                            entry["Duedate"] = rd["Duedate"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["TotalInvoiceamount"] = rd["TotalInvoiceamount"];
                            entry["Vatamount"] = rd["Totalvat"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Debtors_approved_salesaccounts
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,TotalInvoiceamount,Totalvat,Debtorsname,Salesaccount,Vats, Invoiceno,Vat,Invoiceamount,Createdby,Approvedby,Duedate,Datecreated FROM tblsalesaccount where approved='1'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Debtorsname"] = rd["Debtorsname"];
                            entry["Salesaccount"] = rd["Salesaccount"];
                            entry["Vats"] = rd["Vats"];
                            entry["Invoiceno"] = rd["Invoiceno"];
                            entry["Vat"] = rd["Vat"];
                            entry["Invoiceamount"] = rd["Invoiceamount"];
                            entry["CreatedBy"] = rd["Createdby"];
                            entry["Approvedby"] = rd["Approvedby"];
                            entry["Duedate"] = rd["Duedate"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["TotalInvoiceamount"] = rd["TotalInvoiceamount"];
                            entry["Vatamount"] = rd["Totalvat"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Debtors_Rejected_salesaccounts
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,TotalInvoiceamount,Totalvat,Debtorsname,Salesaccount,Vats, Invoiceno,Vat,Invoiceamount,Createdby,Approvedby,Duedate,Datecreated FROM tblsalesaccount where approved='2'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Debtorsname"] = rd["Debtorsname"];
                            entry["Salesaccount"] = rd["Salesaccount"];
                            entry["Vats"] = rd["Vats"];
                            entry["Invoiceno"] = rd["Invoiceno"];
                            entry["Vat"] = rd["Vat"];
                            entry["Invoiceamount"] = rd["Invoiceamount"];
                            entry["Createdby"] = rd["Createdby"];
                            entry["Approvedby"] = rd["Approvedby"];
                            entry["Duedate"] = rd["Duedate"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["TotalInvoiceamount"] = rd["TotalInvoiceamount"];
                            entry["Vatamount"] = rd["Totalvat"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> paycharges
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,name,principle, adminfee,years,months FROM tblcarhesapproval where Paid='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["name"] = rd["name"];
                            entry["principle"] = rd["principle"];
                            entry["adminfee"] = rd["adminfee"];
                            entry["years"] = rd["years"];
                            entry["months"] = rd["months"];



                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }



        public List<Dictionary<string, object>> RegisteredChurchofficerdates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,Firstname,Lastname,Email,phonenumber1,phonenumber2,Salary,idno,occupation,station,Town,Country FROM tblthirdparty ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["Firstname"] = rd["Firstname"];
                            entry["Lastname"] = rd["Lastname"];
                            entry["Email"] = rd["Email"];
                            entry["phonenumber1"] = rd["phonenumber1"];
                            entry["phonenumber2"] = rd["phonenumber2"];
                            entry["Salary"] = rd["Salary"];
                            entry["idno"] = rd["idno"];
                            entry["occupation"] = rd["occupation"];
                            entry["station"] = rd["station"];
                            entry["Town"] = rd["Town"];
                            entry["Country"] = rd["Country"];
                            // if (rd["approved"].ToString() == "True") { entry["approved"] = "True"; } else { entry["approved"] = "False"; }
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> PayChannelsList
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select * from dbo.tbPayChannels WHERE Active=0";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["channelid"] = rd["channelid"];
                            entry["channelname"] = rd["channelname"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> RegisteredUsersApproved
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,fullnames,CreateBy,createDate,Email1,Email2,Mobile1,Mobile2  FROM tbUsers WHERE approved=1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["createby"] = rd["CreateBy"];
                            entry["createdate"] = rd["CreateDate"];
                            entry["email1"] = rd["Email1"];
                            entry["email2"] = rd["Email2"];
                            entry["mobile1"] = rd["Mobile1"];
                            entry["mobile2"] = rd["Mobile2"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> CallLogs
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "";

                //if (System.Web.HttpContext.Current.Session["_queryoption"].ToString()== "allfilterdata")
                //{
                //    sql = "select calllogid,phonenumber,callDate,Duration,CallType,CustomerPhone,DateLogged from CallLogs " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                //}
                //else
                //{
                //    sql = "select top 20 calllogid,phonenumber,callDate,Duration,CallType,CustomerPhone,DateLogged from CallLogs Order By calllogid DESC";
                //}                 
                sql = "select DISTINCT top 800 calllogid,phonenumber,callDate,Duration,CallType,CustomerPhone,DateLogged from CallLogs Order By calllogid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["calllogid"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["calldate"] = rd["calldate"];
                            entry["duration"] = rd["duration"];
                            entry["calltype"] = rd["calltype"];
                            entry["customerphone"] = rd["customerphone"];
                            entry["datelogged"] = rd["datelogged"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Lu
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "";

                //if (System.Web.HttpContext.Current.Session["_queryoption"].ToString()== "allfilterdata")
                //{
                //    sql = "select calllogid,phonenumber,callDate,Duration,CallType,CustomerPhone,DateLogged from CallLogs " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                //}
                //else
                //{
                //    sql = "select top 20 calllogid,phonenumber,callDate,Duration,CallType,CustomerPhone,DateLogged from CallLogs Order By calllogid DESC";
                //}                 
                sql = "select  DISTINCT top 800 calllogid,phonenumber,callDate,Duration,CallType,CustomerPhone,DateLogged from CallLogs Order By calllogid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["calllogid"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["calldate"] = rd["calldate"];
                            entry["duration"] = rd["duration"];
                            entry["calltype"] = rd["calltype"];
                            entry["customerphone"] = rd["customerphone"];
                            entry["datelogged"] = rd["datelogged"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> CallLogDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select DISTINCT TOP 800 calllogid,phonenumber,callDate,Duration,CallType,CustomerPhone,DateLogged from CallLogs " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["calllogid"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["calldate"] = rd["calldate"];
                            entry["duration"] = rd["duration"];
                            entry["calltype"] = rd["calltype"];
                            entry["customerphone"] = rd["customerphone"];
                            entry["datelogged"] = rd["datelogged"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> SMSLogs
        {
            get
            {


                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  TOP 800 SMSLogId,PhoneNumber Recipient,Sender,Message from SMSLogs where DATEDIFF(day,transdate,getdate()) between 0 and 30  ORDER BY smslogid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["smslogid"];
                            entry["recipient"] = rd["recipient"];
                            entry["sender"] = rd["sender"];
                            entry["message"] = rd["message"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> SMSLogsportal
        {
            get
            {


                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select TOP 800 message,senttime,sender,customerphone from customerenquiries_ALL order by messageid desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["message"] = rd["message"];
                            entry["senttime"] = rd["senttime"];
                            entry["sender"] = rd["sender"];
                            entry["customerphone"] = rd["customerphone"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> SMSLogsportalDates
        {
            get
            {


                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  message,senttime,sender,customerphone from customerenquiries_ALL " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["message"] = rd["message"];
                            entry["senttime"] = rd["senttime"];
                            entry["sender"] = rd["sender"];
                            entry["customerphone"] = rd["customerphone"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> SMSLogsDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select TOP 800 SMSLogId,PhoneNumber Recipient,Sender,Message from SMSLogs  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["smslogid"];
                            entry["recipient"] = rd["recipient"];
                            entry["sender"] = rd["sender"];
                            entry["message"] = rd["message"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoanUsers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select TOP 800 iprscheckdescription,UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink,idno,datecreated from LoanUsers ORDER BY userid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["middlename"] = rd["middlename"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["gender"] = rd["gender"];
                            entry["idno"] = rd["idno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];




                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> PaymentHistory
        {
            get
            {

                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "";
                string Hump = "";
                string mpesareff = "";
                string lloan_ID = "";

                if (HttpContext.Current.Session["loanid"] != null)
                {
                    lloan_ID = HttpContext.Current.Session["loanid"].ToString();

                    if (HttpContext.Current.Session["mpesareff"] == null && HttpContext.Current.Session["loanref"] == null)
                    {
                        sql = "select DISTINCT a.firstname+' '+a.lastname as CustomerName,a.phonenumber,b.paymentid,b.paymentdate,b.transactioncode,b.transamount,b.loanref,b.transactioncode from loanusers a,paymenthistory b INNER JOIN loanapplications c   on b.loanref = c.loanref where a.phonenumber = c.borrowerphone and c.loanid='" + lloan_ID + "'";
                    }
                    else
                    {
                        mpesareff = HttpContext.Current.Session["mpesareff"].ToString().Trim();
                        //mpesareff = HttpContext.Current.Session["mpesareff"].ToString().Trim();
                        Hump = HttpContext.Current.Session["loanref"].ToString().Trim();

                        if (Hump.Length > 0 && mpesareff.Length > 0)
                        {
                            sql = "select DISTINCT a.firstname+' '+a.lastname as CustomerName,a.phonenumber,b.paymentid,b.paymentdate,b.transactioncode,b.transamount,b.loanref,b.transactioncode from loanusers a,paymenthistory b INNER JOIN loanapplications c   on b.loanref = c.loanref where a.phonenumber = c.borrowerphone and c.loanref = '" + Hump + "' and b.transactioncode = '" + mpesareff + "'and  c.loanid='" + lloan_ID + "'";
                        }
                        else
                        {
                            sql = "select DISTINCT a.firstname+' '+a.lastname as CustomerName,a.phonenumber,b.paymentid,b.paymentdate,b.transactioncode,b.transamount,b.loanref,b.transactioncode from loanusers a,paymenthistory b INNER JOIN loanapplications c   on b.loanref = c.loanref where a.phonenumber = c.borrowerphone and c.loanid='" + lloan_ID + "'";

                        }

                    }

                    SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                    using (rd)
                    {
                        if (rd != null && rd.HasRows)
                        {
                            while (rd.Read())
                            {
                                Dictionary<string, object> entry = new Dictionary<string, object>();
                                entry["id"] = rd["paymentid"];
                                entry["customername"] = rd["CustomerName"];
                                entry["paymentdate"] = rd["paymentdate"];
                                entry["transactioncode"] = rd["transactioncode"];
                                entry["transamount"] = rd["transamount"];
                                entry["loanreference"] = rd["loanref"];
                                entry["phone"] = rd["phonenumber"];
                                entry["transid"] = rd["transactioncode"];
                                data.Add(entry);
                            }
                        }
                    }
                }
                return data;
            }

        }
        public List<Dictionary<string, object>> BlackListed
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select TOP 800 iprscheckdescription,UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink,idno,datecreated from LoanUsers where blacklist='1' ORDER BY userid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["middlename"] = rd["middlename"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["gender"] = rd["gender"];
                            entry["idno"] = rd["idno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];




                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> BlackListedOne
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select iprscheckdescription,UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink,idno,datecreated from LoanUsers" + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["middlename"] = rd["middlename"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["gender"] = rd["gender"];
                            entry["idno"] = rd["idno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];




                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoanUsersWList
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select iprscheckdescription,UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink,idno,datecreated from LoanUsers  where blacklist='1' ORDER BY userid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["middlename"] = rd["middlename"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["gender"] = rd["gender"];
                            entry["idno"] = rd["idno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoanUsersDates
        {
            get
            {

                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select iprscheckdescription,idno,datecreated,UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink from LoanUSers " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["middlename"] = rd["middlename"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["gender"] = rd["gender"];
                            entry["idno"] = rd["idno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoanUsersDatesBlist
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select  iprscheckdescription,idno,datecreated,UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink from LoanUSers " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["middlename"] = rd["middlename"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["gender"] = rd["gender"];
                            entry["idno"] = rd["idno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoanUsersBlist
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select  iprscheckdescription,idno,datecreated,UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink from LoanUSers " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["middlename"] = rd["middlename"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["gender"] = rd["gender"];
                            entry["idno"] = rd["idno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoanUsersDatesWlist
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select blacklist, iprscheckdescription,idno,datecreated,UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink from LoanUSers " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " where blacklist='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["middlename"] = rd["middlename"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["gender"] = rd["gender"];
                            entry["blacklist"] = rd["blacklist"];
                            entry["idno"] = rd["idno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoanUsers360x(string phone)
        {

            {
                Dictionary<string, object> entry = new Dictionary<string, object>();
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink from LoanUSers where PhoneNumber = '" + phone.ToString() + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {

                            entry["id"] = rd["userid"].ToString();
                            entry["firstname"] = rd["firstname"].ToString();
                            entry["middlename"] = rd["middlename"].ToString();
                            entry["lastname"] = rd["lastname"].ToString();
                            entry["emailaddress"] = rd["emailaddress"].ToString();
                            entry["phonenumber"] = rd["phonenumber"].ToString();
                            entry["dateofbirth"] = rd["dateofbirth"].ToString();
                            entry["gender"] = rd["gender"].ToString();
                            data.Add(entry);
                        }
                    }
                    return data;
                }
            }
        }

        public List<Dictionary<string, object>> LoanUsers360xshopping(string phone)
        {

            {
                Dictionary<string, object> entry = new Dictionary<string, object>();
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink from LoanUSers where PhoneNumber = '" + phone.ToString() + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);



                sql = "select  count(borrowerphone) as results from dbo.loanapplications where loancode='L0003' and borrowerphone = " + phone.ToString() + "";
                rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            entry["shoppingloan"] = rd["results"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoanUserscredit(string phone)
        {

            {
                Dictionary<string, object> entry = new Dictionary<string, object>();
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "EXEC creditScore '" + phone.ToString() + "'";
                blogic.RunNonQuery(sql);

                System.Threading.Thread.Sleep(500);


                sql = "select  * from dbo.loanapplications where  borrowerphone = '" + phone.ToString() + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);



                rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {


                            entry["paymentscore"] = rd["paymentscore"];
                            entry["mpesascore"] = rd["mpesascore"];
                            entry["Avaragescore"] = 0;
                            entry["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
                            entry["deliquencystatuscode"] = rd["deliquencystatuscode"];
                            entry["crbcreditscore"] = rd["crbcreditscore"];
                            entry["iprscheckdescription"] = rd["iprscheckdescription"];

                            data.Add(entry);




                        }
                    }
                }
                return data;
            }
        }



        public List<Dictionary<string, object>> customeridno(string phone)
        {

            {
                Dictionary<string, object> entry = new Dictionary<string, object>();
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "";


                sql = "select  idno from dbo.loanusers where phonenumber= '" + phone.ToString() + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);



                rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {


                            entry["id"] = rd["idno"];

                            data.Add(entry);




                        }
                    }
                }
                return data;
            }
        }
        //}
        //public List<Dictionary<string, object>> TopupFloataccountstatement
        //{

        //    {
        //        //Request.Url.AbsoluteUri
        //        Uri myUri = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
        //        //System.Web.HttpContext.Current.Session.[""];
        //        string URLs = myUri.AbsolutePath.ToString();
        //        string[] recordID = URLs.Split('/');
        //        string IDyetu = recordID[4];

        //        Dictionary<string, object> entry = new Dictionary<string, object>();
        //        List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
        //        string sql = "";
        //        sql = "select  * from tblfloatstatement where code='" + IDyetu + "' order by id DESC";
        //        SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
        //        rd = blogic.RunQueryReturnDataReader(sql);
        //        using (rd)
        //        {
        //            if (rd != null && rd.HasRows)
        //            {
        //                if (rd.Read())
        //                {                       

        //                entry["id"] = rd["idno"];
        //                entry["description"] = rd["description"];
        //                entry["Amount"] = rd["Amount"];
        //                entry["Balance"] = rd["Balance"];
        //                entry["Date"] = rd["Date"];
        //                entry["account"] = rd["account"];
        //                entry["type"] = rd["type"];
        //                entry["code"] = rd["code"];

        //                    data.Add(entry);
        //                }
        //            }
        //        }
        //        return data;
        //    }
        //}


        public List<Dictionary<string, object>> smscount(string phone)
        {

            {
                Dictionary<string, object> entry = new Dictionary<string, object>();
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select count(*) total,sum(case when sender = 'KCB' then 1 else 0 end) KCB,sum(case when sender = 'EQUITY' then 1 else 0 end) EQUITY from smslogs where phonenumber= '" + phone.ToString() + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {


                            entry["KCB"] = rd["KCB"];
                            entry["EQUITY"] = rd["EQUITY"];


                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoanUsers360xsummary(string phone)
        {

            {
                Dictionary<string, object> entry = new Dictionary<string, object>();
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "Select UserId,FirstName,MiddleName,LastName,EmailAddress,PhoneNumber,DateOfBirth,Gender,FacebookLink,googleLink from LoanUSers where PhoneNumber = '" + phone.ToString() + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);



                sql = "select  count(borrowerphone) as results from dbo.loanapplications where   LoanApproved='0' and LoanDisbursed='0' and Vetted='NO' and VettingStatus='PENDINGREVIEW' and borrowerphone = '" + phone.ToString() + "'";
                rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            entry["shoppingloan"] = rd["results"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> pendingmembersapproval
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select a.Userid, a.Accountno, a.Surname, a.OtherNames, a.Email, a.Airtimelimit, a.DateJoined, a.Phone, a.Idno,a.Membershipfee,a.sharecontibution,a.Totalshares,a.LoanLimit,a.Mpesaref as mpesaref,a.actionedby as Rectifiedby,a.Rectifiedby as actionedby,a.automatic,CASE WHEN a.Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN a.useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN a.adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusersapproval a inner join tblloanusers b on b.phone=a.phone  where a.adminright = 0 and a.useright = 1 and b.floatclient=2 and a.Tp = 0";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["adminright"] = rd["adminright"];
                            entry["useright"] = rd["useright"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        
        public List<Dictionary<string, object>> Ptpendingmembersapproval
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select a.Userid as Userid, a.Accountno as Accountno, a.Surname as Surname, a.OtherNames as OtherNames, a.Email as Email,a.Airtimelimit as Airtimelimit, a.DateJoined as DateJoined, a.Phone as Phone, a.Idno as Idno,a.Membershipfee as Membershipfee,a.sharecontibution as sharecontibution,a.Totalshares as Totalshares,a.LoanLimit as LoanLimit,b.Tp as Tp,a.Mpesaref as mpesaref,a.actionedby as Rectifiedby,a.Rectifiedby as actionedby,a.automatic as automatic,CASE WHEN a.Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN a.useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN a.adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusersapproval a INNER JOIN tblloanusers b on a.Phone = b.phone WHERE a.adminright = 0 and a.useright = 1 and b.floatclient = 2 and b.Tp = 1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["adminright"] = rd["adminright"];
                            entry["useright"] = rd["useright"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ModifyLoanUser
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select Userid,Accountno,Surname,OtherNames,Email,DateJoined,Phone,Idno,Membershipfee,sharecontibution,Totalshares,LoanLimit,Mpesaref as mpesaref,actionedby,Rectifiedby,automatic,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers  where adminright=1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["Phone"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            //entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Membershipfee"] = rd["Membershipfee"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["Rectifiedby"] = rd["Rectifiedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> members
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select Userid,Accountno,Surname,OtherNames,Email,DateJoined,Phone,Idno,Membershipfee,sharecontibution,Totalshares,LoanLimit,CASE WHEN AirTimeLimit IS NOT NULL THEN AirTimeLimit ELSE '0' end as AirTimeLimit,Mpesaref as mpesaref,actionedby,automatic,Rectifiedby,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers  where Membership='1' and floatclient=2 and reject is null  or reject=0 ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["adminright"] = rd["adminright"];
                            entry["useright"] = rd["useright"];
                            entry["Rectifiedby"] = rd["Rectifiedby"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

       

        public List<Dictionary<string, object>> membersTP
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select Userid,Accountno,Surname,OtherNames,Email,DateJoined,Phone,Idno,Membershipfee,sharecontibution,floatclient,Totalshares,LoanLimit,AirTimeLimit,Mpesaref as mpesaref,actionedby,automatic,Rectifiedby,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers  where Membership=1 and Tp='1' and floatclient=2 ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["adminright"] = rd["adminright"];
                            entry["useright"] = rd["useright"];
                            entry["Rectifiedby"] = rd["Rectifiedby"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> ussdpin
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select tblloanusers.Userid,tblloanusers.Accountno,tblloanusers.Surname,tblloanusers.OtherNames,tblloanusers.Email,tblloanusers.DateJoined,tblloanusers.Phone,tblloanusers.Idno,tblloanusers.Membershipfee,tblloanusers.sharecontibution,tblloanusers.Totalshares,tblloanusers.LoanLimit,tblloanusers.AirTimeLimit,tblloanusers.Mpesaref as mpesaref,tblloanusers.actionedby,tblloanusers.automatic,tblloanusers.Rectifiedby,CASE WHEN tblloanusers.Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN tblloanusers.useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN tblloanusers.adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers LEFT JOIN LoanUsers t2 ON t2.idno = tblloanusers.idno WHERE t2.idno IS NULL and tblloanusers.Membership = 1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["adminright"] = rd["adminright"];
                            entry["useright"] = rd["useright"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> TPmembers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select Userid,Accountno,Surname,OtherNames,Email,DateJoined,Phone,Idno,Membershipfee,sharecontibution,Totalshares,LoanLimit,AirTimeLimit,Mpesaref as mpesaref,actionedby,automatic,Rectifiedby,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers  where Membership=1 and Tp='1'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["adminright"] = rd["adminright"];
                            entry["useright"] = rd["useright"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> pendingfladmins
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT tblthirdparty.id,Datecreated,Amount,Depositamount,Withdrawalamount,Principle,username,Firstname,Lastname,Email,phonenumber1,phonenumber2,Salary,idno,occupation,station,Town,Country FROM tblthirdparty tblthirdparty inner join tbllite tbllite on tblthirdparty.code=tbllite.code where occupation='Float Client'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["Firstname"] = rd["Firstname"] +" "+rd["Lastname"];
                            entry["Lastname"] = rd["Lastname"];
                            entry["Email"] = rd["Email"];
                            entry["phonenumber1"] = rd["phonenumber1"];
                            entry["phonenumber2"] = rd["phonenumber2"];
                            entry["Salary"] = rd["Salary"];
                            entry["idno"] = rd["idno"];
                            entry["occupation"] = rd["occupation"];
                            entry["station"] = rd["station"];
                            entry["Town"] = rd["Town"];
                            entry["Country"] = rd["Country"];
                            entry["Principle"] = rd["Principle"];
                            entry["Withdrawalamount"] = rd["Withdrawalamount"];
                            entry["Depositamount"] = rd["Depositamount"];
                            entry["Amount"] = rd["Amount"];
                            entry["Datecreated"] = rd["Datecreated"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        

        public List<Dictionary<string, object>> floataccount
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT * FROM tbllite";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Name"] = rd["Name"];
                            entry["Code"] = rd["Code"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["modifiedby"] = rd["modifiedby"];
                            entry["approvedby"] = rd["approvedby"];
                            entry["Amount"] = rd["Amount"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> pendingfloataccount
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT * FROM tbllite where approval=1 and approved=2";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Name"] = rd["Name"];
                            entry["Code"] = rd["Code"];
                            entry["Datecreated"] = rd["Datecreated"];
                            entry["modifiedby"] = rd["modifiedby"];
                            entry["approvedby"] = rd["approvedby"];
                            entry["Amount"] = rd["Depositamount"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        

        public List<Dictionary<string, object>> allFloatstatements
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT * FROM tblfloatstatement";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["description"] = rd["description"];
                            entry["Amount"] = rd["Amount"];
                            entry["Balance"] = rd["Balance"];
                            entry["Date"] = rd["Date"];
                            entry["account"] = rd["account"];
                            entry["type"] = rd["type"];
                            entry["code"] = rd["code"];
                            entry["TransDetails"] = rd["TransDetails"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        

        public List<Dictionary<string, object>> approvedfladmins
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,Firstname,Lastname,Email,phonenumber1,phonenumber2,Salary,idno,occupation,station,Town,Country,Actionedby FROM tblthirdparty where Approved=1 and occupation='Float Client'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["Firstname"] = rd["Firstname"];
                            entry["Lastname"] = rd["Lastname"];
                            entry["Email"] = rd["Email"];
                            entry["phonenumber1"] = rd["phonenumber1"];
                            entry["phonenumber2"] = rd["phonenumber2"];
                            entry["Salary"] = rd["Salary"];
                            entry["idno"] = rd["idno"];
                            entry["occupation"] = rd["occupation"];
                            entry["station"] = rd["station"];
                            entry["Town"] = rd["Town"];
                            entry["Country"] = rd["Country"];
                            entry["Actionedby"] = rd["Actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        

        public List<Dictionary<string, object>> rejectedfladmins
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,Firstname,Lastname,Actionedby,Email,phonenumber1,phonenumber2,Salary,idno,occupation,station,Town,Country FROM tblthirdparty where Approved=2 and occupation='Float Client'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["Firstname"] = rd["Firstname"];
                            entry["Lastname"] = rd["Lastname"];
                            entry["Email"] = rd["Email"];
                            entry["phonenumber1"] = rd["phonenumber1"];
                            entry["phonenumber2"] = rd["phonenumber2"];
                            entry["Salary"] = rd["Salary"];
                            entry["idno"] = rd["idno"];
                            entry["occupation"] = rd["occupation"];
                            entry["station"] = rd["station"];
                            entry["Town"] = rd["Town"];
                            entry["Country"] = rd["Country"];
                            entry["Actionedby"] = rd["Actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> TPnewussdmembers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select * from tbldatamain where platform=0 and approved=0 ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                           // entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Email"] = rd["email"];
                            entry["Loanlimit"] = rd["loanlimit"];
                            entry["Lastname"] = rd["lastname"];
                            entry["Firstname"] = rd["firstname"];
                            //entry["Airtimelimit"] = rd["Airtimelimit"];
                            //entry["DateJoined"] = rd["DateJoined"];
                            //entry["Totalshares"] = rd["Totalshares"];
                            //entry["sharecontibution"] = rd["sharecontibution"];
                            //entry["adminright"] = rd["adminright"];
                            //entry["useright"] = rd["useright"];
                            //entry["Accountno"] = rd["Accountno"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> floats
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select id,Name,Amount,Code from tblfloat where approved='1' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Name"] = rd["Name"];
                            entry["Code"] = rd["Code"];
                            entry["Amount"] = rd["Amount"];
                            
                            

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> floatsapproval
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select id,Name,Amount,Code from tblfloat where approval='0' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Name"] = rd["Name"];
                            entry["Code"] = rd["Code"];
                            entry["Amount"] = rd["Amount"];



                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> paymentrectifactions
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select id,phone,phone1,mpesaref,Amount,Loanamount,Dates,Actionedby,Loanamount from tblreconciliation where Exist='1' and Transuccess='0' and useright='0' and adminright='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["phone"] = rd["phone"];
                            entry["phone1"] = rd["phone1"];
                            entry["Amount"] = rd["Amount"];
                            entry["Dates"] = rd["Dates"];
                            entry["mpesaref"] = rd["mpesaref"];
                            entry["Actionedby"] = rd["Actionedby"];
                            entry["Loanamount"] = rd["Loanamount"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> paymentrectifactionsapproval
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select id,phone,phone1,mpesaref,Amount,Loanamount,Dates,Actionedby,Loanamount from tblreconciliation where Exist='1' and Transuccess='0' and useright='1' and adminright='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["phone"] = rd["phone"];
                            entry["phone1"] = rd["phone1"];
                            entry["Amount"] = rd["Amount"];
                            entry["Dates"] = rd["Dates"];
                            entry["mpesaref"] = rd["mpesaref"];
                            entry["Loanamount"] = rd["Loanamount"];
                            entry["Actionedby"] = rd["Actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> paymentsfoundapproval
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select id,phonenumber,phonenumber2,responseid,Amount,Actionedby,date from STK3 where  useright='1' and adminright='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["phone"] = rd["phonenumber"];
                            entry["phone1"] = rd["phonenumber2"];
                            entry["Amount"] = rd["Amount"];
                            entry["Dates"] = rd["date"];
                            entry["mpesaref"] = rd["responseid"];
                            
                            entry["Actionedby"] = rd["Actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> scams
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select id,name,phoneno,borrowdate,loanammount,loanbalance,loandisbursedate from tblscams";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["name"] = rd["name"];
                            entry["phoneno"] = rd["phoneno"];
                            entry["borrowdate"] = rd["borrowdate"];
                            entry["loanammount"] = rd["loanammount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"];
                            

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Rejectedmembers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select Userid,Accountno,Surname,OtherNames,Email,DateJoined,Phone,Idno,Membershipfee,sharecontibution,Totalshares,LoanLimit,CASE WHEN AirTimeLimit IS NOT NULL THEN AirTimeLimit ELSE '0' end as AirTimeLimit,Mpesaref as mpesaref,actionedby,automatic,Rectifiedby,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers  where  adminright=0 and useright=0 and reject='1' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["adminright"] = rd["adminright"];
                            entry["useright"] = rd["useright"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> TPpendingmembers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select Userid,Accountno,floatclient,Surname,OtherNames,tblloanusers.Email,DateJoined,tblloanusers.Phone,tblloanusers.Idno,Membershipfee,sharecontibution,Totalshares,tblloanusers.LoanLimit,AirTimeLimit,Mpesaref as mpesaref,actionedby,Rectifiedby,occupation,description,location,coordinates,date,status,username,employer,company,employmentyear,salary,hr,automatic,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers JOIN tbldatamain  ON [tblloanusers].Phone = tbldatamain.Phone where Membership=0 and  floatclient=2 order by Userid desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            // entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            //entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Membershipfee"] = rd["Membershipfee"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        
        public List<Dictionary<string, object>> Floatpendingmembers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select tbldatamain.hr,tbldatamain.username,c.Code,c.occupation,Userid,Accountno,Surname,OtherNames,tblloanusers.Email,DateJoined,tblloanusers.Phone,tblloanusers.Idno,Membershipfee,sharecontibution,Totalshares,tblloanusers.LoanLimit,AirTimeLimit,Mpesaref as mpesaref,tblloanusers.actionedby,Rectifiedby,tbldatamain.occupation,description,location,coordinates,date,status,tbldatamain.username,employer,company,employmentyear,tbldatamain.salary,hr,automatic,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers JOIN tbldatamain  ON[tblloanusers].Phone = tbldatamain.Phone inner join tblthirdparty c on c.Code = tbldatamain.hr where Membership = 0 and adminright = 0 and c.Code = tbldatamain.hr and c.occupation = 'Float Client' order by Userid desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["username"] = rd["username"];
                            entry["hr"] = rd["hr"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            // entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            //entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Membershipfee"] = rd["Membershipfee"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        
        public List<Dictionary<string, object>> Floatapprovedmembers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select b.hr,b.username,c.Code,c.occupation,Userid,Accountno,Surname,OtherNames,a.Email,a.floatclient,DateJoined,a.Phone,a.Idno,Membershipfee,sharecontibution,Totalshares,a.LoanLimit,AirTimeLimit,a.approved,Mpesaref as mpesaref,a.actionedby,Rectifiedby,b.occupation,description,location,coordinates,date,status,b.username,company,employmentyear,b.salary,hr,automatic,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers a inner join tbldatamain b ON a.Phone = b.Phone inner join tblthirdparty c on c.Code = b.hr where Membership = '1' and floatclient = 1 and useright = 1 and a.approved = 1 and adminright = 1 and reject is null  or reject = 0 order by Userid desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["username"] = rd["username"];
                            // entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["hr"] = rd["hr"];
                            //entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Membershipfee"] = rd["Membershipfee"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> pendingmembers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select TOP 500 Userid,tblloanusers.Accountno as Accountno,tblloanusers.Surname as Surname,tblloanusers.OtherNames as OtherNames,tblloanusers.Email as Email,tblloanusers.DateJoined as DateJoined,tblloanusers.Phone as Phone,tblloanusers.Idno as Idno,tblloanusers.Membershipfee as Membershipfee,tblloanusers.sharecontibution,tblloanusers.Totalshares,CASE WHEN tblloanusers.LoanLimit IS NOT NULL THEN  tblloanusers.LoanLimit ELSE '0' END AS loanlimit,CASE WHEN tblloanusers.AirTimeLimit IS NOT NULL THEN tblloanusers.AirTimeLimit ELSE '0' end as AirTimeLimit,tblloanusers.Mpesaref as mpesaref,tblloanusers.actionedby,tblloanusers.Rectifiedby,automatic,CASE WHEN Membership = 1 THEN 'Approved' ELSE 'Pending' end as Membership,CASE WHEN useright = 1 THEN 'Approved' ELSE 'Pending'end as useright,CASE WHEN adminright = 1 THEN 'Approved' ELSE 'Pending' END as adminright from tblloanusers  WHERE Phone not in(select phone from tbldatamain where tbldatamain.phone=tblloanusers.phone) and membership = 0 and useright=0 and adminright=0 and floatclient=2 and Reject is null or reject='0' order by userid desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["Userid"];
                            entry["mobileno"] = rd["Phone"];
                            entry["idno"] = rd["idno"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Email"] = rd["Email"];
                            entry["Loanlimit"] = rd["Loanlimit"];
                            entry["Lastname"] = rd["Surname"];
                            entry["Firstname"] = rd["OtherNames"];
                            entry["Airtimelimit"] = rd["Airtimelimit"];
                            entry["DateJoined"] = rd["DateJoined"];

                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];
                            entry["adminright"] = rd["adminright"];
                            entry["useright"] = rd["useright"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public string getpayables()
        {

            {
                string res = "";
                string sql = "SELECT  ROUND(commissions, 0) results from tblBillings WHERE Paid='0' and Months=month(getDate())-1 AND Years=year(getDate())";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string getpayablerate()
        {

            {
                string res = "";
                string sql = "SELECT  Rates as results from tblBillings WHERE Paid='0' and Months=month(getDate())-1 AND Years=year(getDate())";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string getdisbursed()
        {

            {
                string res = "";
                string sql = "SELECT  Disbursedamount as results from tblBillings WHERE Paid='0' and Months=month(getDate())-1 AND Years=year(getDate())";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public List<Dictionary<string, object>> View_PAR
        {
            get
            {
                string sql = "";
                string testss = "";
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                sql = "SELECT * FROM New_ageingSummary";
                testss = sql;
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["Name"] = rd["Name"];
                            entry["Principle"] = rd["Totalloan"];
                            entry["interestamount"] = rd["interestamount"];
                            entry["Loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["Disbursementdate"] = rd["Disbursementdate"];
                            entry["expectedcleardate"] = rd["expectedcleardate"];
                            entry["phoneno"] = rd["phoneno"];
                            entry["_0to7"] = rd["_0to7"];
                            entry["_8to14"] = rd["_8to14"];
                            entry["_15to21"] = rd["_15to21"];
                            entry["_22to28"] = rd["_22to28"];
                            entry["_29to35"] = rd["_29to35"];
                            entry["_36to42"] = rd["_36to42"];
                            entry["_43to49"] = rd["_43to49"];
                            entry["_50to56"] = rd["_50to56"];
                            entry["_57to63"] = rd["_57to63"];
                            entry["_64to70"] = rd["_64to70"];
                            entry["_71to91"] = rd["_71to91"];
                            entry["_92to112"] = rd["_92to112"];
                            entry["_113to133"] = rd["_113to133"];
                            entry["_134to154"] = rd["_113to133"];
                            entry["_155to175"] = rd["_155to175"];
                            entry["_above175"] = rd["_above175"];
                            entry["_above64"] = rd["_above64"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public string getrights(string username)
        {

            {
                string res = "";
                string sql = "select top 1 CONVERT(varchar(10),automatic) + '|' + CONVERT(varchar(10), Airautomatic)  as results from tblloanusersapproval where adminright=0 and Phone='" + username + "' order by id desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }
                return res;
            }
        }
        public string getcredits(string idno)
        {

            {
                string res = "";
                string sql = "select cast(isnull(sum(cast (Debit as decimal(9,0)) ),0)as varchar(50))+'|'+cast(isnull(sum(cast (credit as decimal(9,0))),0) as varchar(50))+'|'+isnull(cast((sum(cast (Debit as decimal(9,0)))+sum(cast (credit as decimal(9,0))))/2 as varchar(50)),0)  as results from tblmpesatransactions where idno='" + idno + "' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }
                return res;
            }
        }
        public List<Dictionary<string, object>> getmpesa(string idno)
        {

            {
                

                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select id, Name,Debit,Credit from tblmpesatransactions where  idno='" + idno + "' order by Name desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Name"] = rd["Name"];
                            entry["Debit"] = rd["Debit"];
                            entry["Credit"] = rd["Credit"];
                            

                            
                            data.Add(entry);


                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> GetLoanRefsDisbursed
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "";
                string smiu = "";
                if (HttpContext.Current.Session["msimu"] != null)
                {
                    smiu = HttpContext.Current.Session["msimu"].ToString();
                    sql = "select Top 300 actionedby, LoanId,Loanref,MPESATransactionREF,BorrowerPhone Phone,(select firstname+' '+lastname  from LoanUsers where phonenumber=Borrowerphone)as name,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanDisbursed='1' and loanapproved='1' and loancode NOT LIKE 'L003' and loancleared='0' and borrowerphone='" + smiu + "' ORDER BY LoanDisbursedate DESC ";

                }
                else
                {
                    sql = "select Top 300 actionedby, LoanId,Loanref,MPESATransactionREF,BorrowerPhone Phone,(select firstname+' '+lastname  from LoanUsers where phonenumber=Borrowerphone)as name,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanDisbursed='1' and loanapproved='1' and loancode NOT LIKE 'L003' and loancleared='0' ORDER BY LoanDisbursedate DESC ";

                }


                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["customername"] = rd["name"];
                            entry["phonenumber"] = rd["phone"];
                            entry["mpesaref"] = rd["MPESATransactionREF"];
                            entry["loanref"] = rd["loanref"];
                            entry["borrowdate"] = Convert.ToDateTime(rd["borrowdate"]).ToString("yyyy-MM-dd"); //rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["disbursedate"] = Convert.ToDateTime(rd["loandisbursedate"]).ToString("yyyy-MM-dd"); //rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["actionedby"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> fLoansDisbursed
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  actionedby, LoanId,BorrowerPhone Phone,(select floatclient from tblloanusers where phone=Borrowerphone)as floatclient,(select firstname+' '+lastname from LoanUsers where phonenumber=Borrowerphone)as name,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanDisbursed='1' and loanapproved='1' and floatclient=1 and loancode NOT LIKE 'L003' ORDER BY LoanDisbursedate DESC ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["name"] = rd["name"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["actionedby"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoansDisbursed
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  actionedby, LoanId,BorrowerPhone Phone,(select firstname+' '+lastname from LoanUsers where phonenumber=Borrowerphone)as name,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,floatclient,ApproverName FROM LoanApplications WHERE LoanDisbursed='1' and loanapproved='1'  and floatclient is null and loancode NOT LIKE 'L003' ORDER BY LoanDisbursedate DESC ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["name"] = rd["name"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["actionedby"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoansDisbursedapproval
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  actionedby, LoanId,BorrowerPhone Phone,(select firstname+' '+lastname from LoanUsers where phonenumber=Borrowerphone)as name,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanDisbursed='1' and loanapproved='1' and Reversalstatus='0' and loancode NOT LIKE 'L003' ORDER BY LoanDisbursedate DESC ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["name"] = rd["name"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["actionedby"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoansDisbursedDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  actionedby,LoanId,BorrowerPhone Phone,(select firstname+' '+lastname from LoanUsers where phonenumber=Borrowerphone)as name,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " and LoanDisbursed='1'  ORDER BY LoanDisbursedate DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["name"] = rd["name"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["actionedby"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoansForReview
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanApproved='0' and LoanDisbursed='0' and Vetted='NO' and VettingStatus='PENDINGREVIEW'  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> LoansForReview360(string phoneno)
        {

            {
                string sqls = "Exec creditScore'" + phoneno + "'";
                blogic.RunNonQuery(sqls);

                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select firstname+ ' ' +middlename  as names,LoanId,BorrowerPhone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName,paymentscore FROM         dbo.LoanUsers INNER JOIN dbo.loanapplications ON dbo.LoanUsers.phonenumber = dbo.loanapplications.borrowerphone WHERE LoanApproved = '0' and LoanDisbursed = '0' and Vetted = 'NO' and VettingStatus = 'PENDINGREVIEW' and BorrowerPhone=" + phoneno + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["names"] = rd["names"];
                            entry["phonenumber"] = rd["BorrowerPhone"];
                            entry["borrowdate"] = rd["borrowdate"];
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"];
                            entry["approvername"] = rd["approvername"];

                            if (rd["paymentscore"].ToString() == "")
                            {
                                entry["paymentscore"] = "0";
                            }

                            else

                            {
                                entry["paymentscore"] = rd["paymentscore"];
                            }

                            data.Add(entry);


                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoansForReviewDashboard()
        {


            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "exec dashboardsummary";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            string res = rd["results"].ToString();
                            res.Split('|');

                            entry["appliedloans"] = rd["results"];
                            //entry["loanspendingreview"] = res[1];
                            //entry["loansdisuburesd"] = res[2];
                            //entry["loansrejected"] = res[3];
                            //entry["shoppingsummary"] = res[4];
                            //entry["salaryadvancesummary"] = res[5]; 

                            data.Add(entry);
                        }
                    }
                }

                return data;
            }
        }

        public string LoansForReview360xdashboard()
        {

            {
                string results = "";
                string sql = "exec dashboardsummary";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            results = rd["results"].ToString();

                        }
                    }
                }
                return results;
            }
        }

        public string Loanscreditscore(string phonenumber)
        {

            {
                string results = "";
                string sql = "exec creditScore'" + phonenumber + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            results = rd["results"].ToString();

                        }
                    }
                }
                return results;
            }
        }



        public List<Dictionary<string, object>> LoansForReview360x(string phoneno)
        {

            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select count(borrowerphone) as results from loanapplications WHERE LoanApproved = '0' and LoanDisbursed = '0' and Vetted = 'NO' and VettingStatus = 'PENDINGREVIEW' and BorrowerPhone='" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["results"] = rd["results"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> clearedloans(string phoneno)
        {

            {
                Dictionary<string, object> entry = new Dictionary<string, object>();
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select count(borrowerphone) as results from loanapplications where loancleared='1' and loandisbursed='1' and  loancode='L0002' and borrowerphone='" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {


                            entry["shopping"] = rd["results"];
                            //data.Add(entry);
                        }
                    }
                }


                sql = "select count(borrowerphone) as results from loanapplications where loancleared='1' and loandisbursed='1' and  loancode='L0001' and borrowerphone='" + phoneno + "'";
                rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {


                            entry["cash"] = rd["results"];
                            //data.Add(entry);
                        }
                    }
                }


                if (entry.Count > 0)
                {
                    data.Add(entry);
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Loanstatements(string loanid)
        {

            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select * from paymenthistory  where loanref='" + loanid + "' order by paymentid ASC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["paymentdate"] = rd["paymentdate"];
                            entry["transactioncode"] = rd["loanref"];
                            entry["transamount"] = rd["transamount"];
                            entry["balance"] = rd["balance"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public string SMS360x(string phoneno)
        {

            {
                string res = "";
                string sql = "exec smssummary '" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }
                return res;
            }
        }

        //SELECT sum(amounttransacted)/COUNT(*)/datediff(day, dateadd(month, -1, GETDATE()),GETDATE()) from smslogs where phonenumber='254720207273' and  sender='MPESA' and transdate > DATEADD(month, -1, GETDATE())




        public string getrights()

        {

            string strUsername = "";
            //string m_username = "";

            if (null != HttpContext.Current && null != HttpContext.Current.Session && null != HttpContext.Current.Session["username"])
            {
                MyGlobalVariables.username = System.Web.HttpContext.Current.Session["username"].ToString();
                strUsername = MyGlobalVariables.username;
                Boolean status = ulogic.SessionRights(strUsername, ref MyGlobalVariables.userrights);
                MyGlobalVariables.userrightsx = MyGlobalVariables.userrights;
            }
            else
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session.Abandon();
                HttpContext.Current.Response.Redirect("~/Account/Login", true);
            }

            return "";
        }

        public string mpesascore(string phoneno)
        {

            {
                string res = "";
                string sql = "select (sum(amounttransacted) /30) as results from SMSlogs WHERE phonenumber='" + phoneno + "' and sender='MPESA' AND DATEDIFF(day,transdate,Getdate())<=30";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }
                return res;
            }
        }

        public string getloanexcessamt(string phoneno)
        {

            {
                string res = "";
                string sql = "select sum(excessamount) as results from dbo.tboverpaidloans where phonenumber='" + phoneno + "' and ispaidback=0";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string getlocation(string phoneno)
        {

            {
                string res = "";
                string sql = "select location from dbo.tbldatamain where phone='" + phoneno + "' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["location"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string getoccuption(string phoneno)
        {

            {
                string res = "";
                string sql = "select occupation from dbo.tbldatamain where phone='" + phoneno + "' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["occupation"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string getphoto(string phoneno)
        {

            {
                string res = "";
                string sql = "select photo from dbo.tbldatamain where phone='" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["photo"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string getdescripton(string phoneno)
        {

            {
                string res = "";
                string sql = "select description from dbo.tbldatamain where phone='" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["description"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string getcoordinates(string phoneno)
        {

            {
                string res = "";
                string sql = "select coordinates from dbo.tbldatamain where phone='" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["coordinates"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string gethr(string phoneno)
        {

            {
                string res = "";
                string sql = "select description from dbo.tblloan where phone='" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["description"].ToString();

                        }
                    }
                }

                if (res == "") { res = "0"; }

                return res;

            }
        }
        public string getid(string phoneno)
        {

            {
                string res = "";
                string sql = "select  idno from dbo.loanusers where phonenumber='" + phoneno.ToString() + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);



                rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {


                            res = rd["idno"].ToString();

                        }
                    }
                }
                return res;
            }
        }


        public string inquiries()
        {

            {
                string res = "";
                string sql = "SELECT count(messageid) as cnt from customerenquiries where responded='0'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);



                rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        if (rd.Read())
                        {


                            res = rd["cnt"].ToString();

                        }
                    }
                }
                return res;
            }
        }


        public string getpaymentscore(string phoneno)
        {

            {
                string res = "";
                string sql = "select paymentscore  from loanapplications  where borrowerphone ='" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["paymentscore"].ToString();

                        }
                    }
                }
                return res;
            }
        }


        public string datascrub(string phoneno)
        {

            {
                string res = "";
                string sql = "select paymentscore  from loanapplications  where borrowerphone ='" + phoneno + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["paymentscore"].ToString();

                        }
                    }
                }
                return res;
            }
        }

        public string getuserights(string username)
        {

            {
                string res = "";
                string sql = " select userlevel + '|' + CONVERT(varchar(10), clientregister)  + '|'+ CONVERT(varchar(10), parameters)  + '|'+ CONVERT(varchar(10), usermanagement) + '|' + CONVERT(varchar(10), Approvals) + '|' + CONVERT(varchar(10), Editapprovals)+ '|' + CONVERT(varchar(10), Savings)+ '|' + CONVERT(varchar(10), accounts)+ '|' + CONVERT(varchar(10), floatmngt)  as results from tbUsers where ID='" + username + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }
                return res;
            }
        }

        public string getofficerrights(string username)
        {

            {
                string res = "";
                string sql = " select CONVERT(varchar(10),Salary) + '|' + CONVERT(varchar(10),Commission) as results from tblthirdparty where ID='" + username + "'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {


                            res = rd["results"].ToString();

                        }
                    }
                }
                return res;
            }
        }



        public string getsupermarketcode()
        {

            {
                string SCODE = "";
                Random generator = new Random();
                SCODE = generator.Next(0, 1000000).ToString("D6");
                return "S" + SCODE;
            }
        }
        public List<Dictionary<string, object>> ReviewedLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                // string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanApproved='0' and LoanDisbursed='0' and Vetted='YES' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                string sql = "select TOP 300 a.actionedby , a.LoanId,a.BorrowerPhone Phone,a.mpesascore as mpesaavg,b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a.LoanApproved='1' and a.LoanDisbursed='1' and a.Vetted='YES' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["firstname"] = rd["firstname"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["mpesaavg"] = rd["mpesaavg"];
                            entry["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
                            entry["deliquencystatuscode"] = rd["deliquencystatuscode"];
                            entry["paymentscore"] = rd["paymentscore"];
                            entry["principle"] = rd["principle"];
                            entry["loantype"] = rd["loantype"];
                            entry["actionedby"] = rd["actionedby"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ReviewedLoansDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                // string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanApproved='0' and LoanDisbursed='0' and Vetted='YES' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria";
                string sql = "select  a.actionedby , a.LoanId,a.BorrowerPhone Phone,a.mpesascore as mpesaavg,b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a.LoanApproved='1' and a.LoanDisbursed='1' and a.Vetted='YES' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " ORDER BY loanid DESC";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["firstname"] = rd["firstname"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["mpesaavg"] = rd["mpesaavg"];
                            entry["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
                            entry["deliquencystatuscode"] = rd["deliquencystatuscode"];
                            entry["paymentscore"] = rd["paymentscore"];
                            entry["principle"] = rd["principle"];
                            entry["loantype"] = rd["loantype"];
                            entry["actionedby"] = rd["actionedby"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Reports
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select * FROM Reports ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["description"] = rd["description"];
                            entry["reportpath"] = rd["reportpath"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        //public List<Dictionary<string, object>> ReportsDates
        //{
        //    get
        //    {
        //        List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
        //        string sql = "select * FROM Reports WHERE + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
        //        SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
        //        using (rd)
        //        {
        //            if (rd != null && rd.HasRows)
        //            {
        //                while (rd.Read())
        //                {
        //                    Dictionary<string, object> entry = new Dictionary<string, object>();
        //                    entry["id"] = rd["loanid"];
        //                    entry["phonenumber"] = rd["phone"];
        //                    entry["borrowdate"] = rd["borrowdate"];
        //                    entry["loanamount"] = rd["loanamount"];
        //                    entry["loantype"] = rd["loantype"];
        //                    entry["loancode"] = rd["loancode"];

        //                    data.Add(entry);
        //                }
        //            }
        //        }
        //        return data;
        //    }
        //}
        public List<Dictionary<string, object>> PendingReviewLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "";
                string hapaa = "";
                //string mpesaavg = "select(sum(amounttransacted)) / COUNT(*) as TranAmount from smslogs where phonenumber = '+phonenumber+'";
                sql = "select  a.loancode,a.LoanId,a.applicationdate,a.loanref,a.BorrowerPhone Phone, a.mpesascore mpesaavg,b.FirstName,a.BorrowDate date,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.interestamount,a.floatclient,a.loanamount,a.rateapplied,a.repayperiod,a.adminfee,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a.Loandisbursed = '0'and loanapproved='0' and vetted='NO' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                hapaa = sql;
                // string sql = "select distinct a.LoanId,a.loanref,a.BorrowerPhone Phone, b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.loanref,a.rateapplied,a.repayperiod,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a. LoanAmount>'0' a.Loandisbursed = '0' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["loancode"] = rd["loancode"];
                            entry["loanref"] = rd["loanref"];
                            entry["phonenumber"] = rd["phone"];
                            entry["firstname"] = rd["firstname"];
                            //entry["borrowdate"] = rd["applicationdate"].ToString().Replace("12:00:00 AM", "");
                            entry["borrowdate"] = rd["date"];
                            entry["mpesaavg"] = rd["mpesaavg"];
                            entry["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
                            entry["deliquencystatuscode"] = rd["deliquencystatuscode"];
                            entry["paymentscore"] = rd["paymentscore"];
                            entry["principle"] = rd["principle"];
                            entry["interestamount"] = rd["interestamount"];
                            entry["loanamount"] = rd["loanamount"];
                            entry["rateapplied"] = rd["rateapplied"];
                            entry["repayperiod"] = rd["repayperiod"];
                            entry["adminfee"] = rd["adminfee"];
                            entry["loantype"] = rd["loantype"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> PendingReviewLoansFL
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "";
                string hapaa = "";
                //string mpesaavg = "select(sum(amounttransacted)) / COUNT(*) as TranAmount from smslogs where phonenumber = '+phonenumber+'";
                sql = "select  a.loancode,a.LoanId,a.applicationdate,a.loanref,a.BorrowerPhone Phone, a.mpesascore mpesaavg,b.FirstName,a.BorrowDate date,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.floatclient,a.principle,a.interestamount,a.loanamount,a.rateapplied,a.repayperiod,a.adminfee,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a.Loandisbursed = '0'and loanapproved='0' and vetted='NO' and a.borroweruniqueid = b.uniqueid and a.floatclient=1 and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                hapaa = sql;
                // string sql = "select distinct a.LoanId,a.loanref,a.BorrowerPhone Phone, b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.loanref,a.rateapplied,a.repayperiod,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a. LoanAmount>'0' a.Loandisbursed = '0' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["loancode"] = rd["loancode"];
                            entry["loanref"] = rd["loanref"];
                            entry["phonenumber"] = rd["phone"];
                            entry["firstname"] = rd["firstname"];
                            //entry["borrowdate"] = rd["applicationdate"].ToString().Replace("12:00:00 AM", "");
                            entry["borrowdate"] = rd["date"];
                            entry["mpesaavg"] = rd["mpesaavg"];
                            entry["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
                            entry["deliquencystatuscode"] = rd["deliquencystatuscode"];
                            entry["paymentscore"] = rd["paymentscore"];
                            entry["principle"] = rd["principle"];
                            entry["interestamount"] = rd["interestamount"];
                            entry["loanamount"] = rd["loanamount"];
                            entry["rateapplied"] = rd["rateapplied"];
                            entry["repayperiod"] = rd["repayperiod"];
                            entry["adminfee"] = rd["adminfee"];
                            entry["loantype"] = rd["loantype"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> overpaidloansFL
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

                //string mpesaavg = "select(sum(amounttransacted)) / COUNT(*) as TranAmount from smslogs where phonenumber = '+phonenumber+'";
                string sql = "select  a.phonenumber,a.loanref,a.excessamount,a.paymentdate,a.loanamount,b.floatclient,a.LoanType,a.dateloancleared,a.firstname + ' ' + a.lastname as names from vw_overpaid a inner join loanapplications b on a.phonenumber=b.borrowerphone where b.floatclient=1";

                // string sql = "select distinct a.LoanId,a.loanref,a.BorrowerPhone Phone, b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.loanref,a.rateapplied,a.repayperiod,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a. LoanAmount>'0' a.Loandisbursed = '0' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["loanref"] = rd["loanref"];
                            entry["excessamount"] = rd["excessamount"];
                            entry["paymentdate"] = rd["paymentdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["LoanType"] = rd["LoanType"];
                            entry["names"] = rd["names"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        
        public List<Dictionary<string, object>> overpaidloans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

                //string mpesaavg = "select(sum(amounttransacted)) / COUNT(*) as TranAmount from smslogs where phonenumber = '+phonenumber+'";
                string sql = "select  a.phonenumber,a.loanref,a.excessamount,a.paymentdate,a.loanamount,b.floatclient,a.LoanType,a.dateloancleared,a.firstname + ' ' + a.lastname as names from vw_overpaid a inner join loanapplications b on a.phonenumber=b.borrowerphone where b.floatclient is null";

                // string sql = "select distinct a.LoanId,a.loanref,a.BorrowerPhone Phone, b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.loanref,a.rateapplied,a.repayperiod,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a. LoanAmount>'0' a.Loandisbursed = '0' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["loanref"] = rd["loanref"];
                            entry["excessamount"] = rd["excessamount"];
                            entry["paymentdate"] = rd["paymentdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["LoanType"] = rd["LoanType"];
                            entry["names"] = rd["names"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> overpaidloansdates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();

                //string mpesaavg = "select(sum(amounttransacted)) / COUNT(*) as TranAmount from smslogs where phonenumber = '+phonenumber+'";
                string sql = "select  phonenumber,loanref,excessamount,paymentdate,loanamount,LoanType,dateloancleared,firstname + ' ' + lastname as names from vw_overpaid" + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";

                // string sql = "select distinct a.LoanId,a.loanref,a.BorrowerPhone Phone, b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.loanref,a.rateapplied,a.repayperiod,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a. LoanAmount>'0' a.Loandisbursed = '0' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["loanref"] = rd["loanref"];
                            entry["excessamount"] = rd["excessamount"];
                            entry["paymentdate"] = rd["paymentdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["LoanType"] = rd["LoanType"];
                            entry["names"] = rd["names"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> AppliedLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  loanref,LoanId,BorrowerPhone Phone,BorrowDate date,applicationdate,LoanAmount,LoanType, LoanCode FROM LoanApplications  WHERE LoanAmount>'0' and loanapproved=0 and loandisbursed not in ('1','2')and loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["date"];

                            //entry["borrowdate"] = rd["applicationdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];
                            entry["loanref"] = rd["loanref"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> SaccoMembers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select Phone,Accountno,Surname,OtherNames,Idno,Email, DateJoined,Membershipfee,Openingshares,Totalshares,sharecontibution FROM tblloanusers ORDER BY Userid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            //  entry["id"] = rd["Userid"];
                            entry["Phone"] = rd["Phone"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Surname"] = rd["Surname"];
                            entry["OtherNames"] = rd["OtherNames"];
                            entry["Idno"] = rd["Idno"];
                            entry["Email"] = rd["Email"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Membershipfee"] = rd["Membershipfee"];
                            entry["Openingshares"] = rd["Openingshares"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];


                            data.Add(entry);
                        }
                    }
                    return data;
                }
            }
        }


        public List<Dictionary<string, object>> AppliedLoansstatements
        {
            get
            {
                string[] formats = { "dd/MM/yyyy" };

                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select Top 800  loanref,LoanId,BorrowerPhone Phone,BorrowDate date,applicationdate,LoanAmount,LoanType, LoanCode FROM LoanApplications  WHERE LoanAmount>'0'  and loandisbursed ='1' and loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];


                            // entry["borrowdate"] = rd["applicationdate"].ToString().Replace("12:00:00 AM","") ;
                            entry["borrowdate"] = rd["date"];
                            entry["loanamount"] = rd["loanamount"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];
                            entry["loanref"] = rd["loanref"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }





        public List<Dictionary<string, object>> redeemdiscount
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select customerphone,  sum(ROUND(CAST(DiscountPrice AS decimal(18,2)), 2)) as discount from ShoppingDetails   where (discountredeemed=0 or discountredeemed is null)  group by customerphone";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["phonenumber"] = rd["customerphone"];


                            entry["totals"] = rd["discount"].ToString();

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> redeemdiscountdates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select customerphone,  sum(ROUND(CAST(DiscountPrice AS decimal(18,2)), 2)) as discount from ShoppingDetails  where  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "  and (discountredeemed=0 or discountredeemed is null)   group by customerphone";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["phonenumber"] = rd["customerphone"];


                            entry["totals"] = rd["discount"].ToString();

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> AppliedLoans_index
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select Top 20 LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanType, LoanCode FROM LoanApplications  WHERE LoanAmount>'0' and loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> capturedattendants
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select Top 20 selectedemployer,tillnumber,supermarketattendant,attendantphone,CreateBy,CreateDate FROM SupermarketAttendant order by id DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["selectedemployer"] = rd["selectedemployer"];
                            entry["tillnumber"] = rd["tillnumber"];
                            entry["supermarketattendant"] = rd["supermarketattendant"];
                            entry["attendantphone"] = rd["attendantphone"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> AppliedLoansstatementsDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  loanref,LoanId,BorrowerPhone Phone,BorrowDate,applicationdate,LoanAmount,LoanType, LoanCode FROM LoanApplications  WHERE LoanAmount>'0' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " and loandisbursed ='1' and loancode NOT LIKE 'L0003' ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];
                            entry["loanref"] = rd["loanref"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> AppliedLoansDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select loanref,LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanType, LoanCode FROM LoanApplications WHERE LoanAmount>'0' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];
                            entry["loanref"] = rd["loanref"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Saccouser
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select Phone,Accountno,Surname,OtherNames,Idno,Email, DateJoined,Membershipfee,Openingshares,Totalshares,sharecontibution FROM tblloanusers WHERE Membershipfee>'0' ORDER BY Userid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            //  entry["id"] = rd["Userid"];
                            entry["Phone"] = rd["Phone"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Surname"] = rd["Surname"];
                            entry["OtherNames"] = rd["OtherNames"];
                            entry["Idno"] = rd["Idno"];
                            entry["Email"] = rd["Email"];
                            entry["DateJoined"] = rd["DateJoined"];
                            entry["Membershipfee"] = rd["Membershipfee"];
                            entry["Openingshares"] = rd["Openingshares"];
                            entry["Totalshares"] = rd["Totalshares"];
                            entry["sharecontibution"] = rd["sharecontibution"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Rejectedloans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                // string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanApproved='0' and LoanDisbursed='0' and Vetted='YES' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                string sql = "select TOP 800 a.actionedby, a.LoanId,a.BorrowerPhone Phone, a.mpesascore as mpesaavg,b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a.LoanApproved='0' and a.LoanDisbursed='2' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["firstname"] = rd["firstname"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["mpesaavg"] = rd["mpesaavg"];
                            entry["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
                            entry["deliquencystatuscode"] = rd["deliquencystatuscode"];
                            entry["paymentscore"] = rd["paymentscore"];
                            entry["principle"] = rd["principle"];
                            entry["loantype"] = rd["loantype"];
                            entry["actionedby"] = rd["actionedby"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> RejectedloansDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                // string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanApproved='0' and LoanDisbursed='0' and Vetted='YES' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                string sql = "select  a.actionedby, a.LoanId,a.BorrowerPhone Phone, a.mpesascore as mpesaavg,b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a.LoanApproved='0' and a.LoanDisbursed='0' and a.Vetted='YES' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["firstname"] = rd["firstname"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["mpesaavg"] = rd["mpesaavg"];
                            entry["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
                            entry["deliquencystatuscode"] = rd["deliquencystatuscode"];
                            entry["paymentscore"] = rd["paymentscore"];
                            entry["principle"] = rd["principle"];
                            entry["loantype"] = rd["loantype"];
                            entry["actionedby"] = rd["actionedby"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> Rejectedloanslist
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                // string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanApproved='0' and LoanDisbursed='0' and Vetted='YES' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                string sql = "select  a.LoanId,a.BorrowerPhone Phone, (select(sum(amounttransacted)) / COUNT(*) as TranAmount from smslogs where phonenumber = a.borrowerphone)as mpesaavg,b.FirstName,a.BorrowDate,b.deliquencystatusdescription,b.deliquencystatuscode,a.paymentscore,a.principle,a.LoanType FROM LoanApplications a, LoanUsers b WHERE a.LoanApproved='0' and a.LoanDisbursed='0' and a.Vetted='YES' and a.borroweruniqueid = b.uniqueid and a.loancode NOT LIKE 'L0003' ORDER BY loanid DESC";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["firstname"] = rd["firstname"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["mpesaavg"] = rd["mpesaavg"];
                            entry["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
                            entry["deliquencystatuscode"] = rd["deliquencystatuscode"];
                            entry["paymentscore"] = rd["paymentscore"];
                            entry["principle"] = rd["principle"];
                            entry["loantype"] = rd["loantype"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoanTypes
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select top 20 LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanCode,LoanType FROM LoanApplications ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loancode"] = rd["loancode"];
                            entry["loantype"] = rd["loantype"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        //public List<Dictionary<string, object>> LoanTypesDates
        //{
        //    get
        //    {
        //        List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
        //        string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanCode,LoanType FROM LoanApplications WHERE loanamount>'0' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
        //        SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
        //        using (rd)
        //        {
        //            if (rd != null && rd.HasRows)
        //            {
        //                while (rd.Read())
        //                {
        //                    Dictionary<string, object> entry = new Dictionary<string, object>();
        //                    entry["id"] = rd["loanid"];
        //                    entry["phonenumber"] = rd["phone"];
        //                    entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
        //                    entry["loanamount"] = rd["loanamount"];
        //                    entry["loanbalance"] = rd["loanbalance"];
        //                    entry["loancode"] = rd["loancode"];
        //                    entry["loantype"] = rd["loantype"];
        //                    data.Add(entry);
        //                }
        //            }
        //        }
        //        return data;
        //    }
        //}

        public List<Dictionary<string, object>> AppliedAdvances
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string emps = System.Web.HttpContext.Current.Session["selectedemployer"].ToString();
                String emps1 = emps.Replace("AND EmployerID", "AND a.EmployerID");

                string sql = "select a.LoanId,a.BorrowerPhone Phone, b.CompanyName employername,a.BorrowDate,a.LoanAmount FROM LoanApplications a,Employers b WHERE a.LoanApproved='0' and a.LoanDisbursed='0' and a.acceptrejectflag='0' and a.loancode='L0003' and a.sentforreview='0' and a.Employerid=b.id  " + emps1 + "";
                //string sql = " select a.loanid,a.borrowerphone phone,b.companyname employername,a.borrowdate,a.loanamount from loanapplications a,Employers b WHERE a.Employerid=b.id and a.LoanApproved=0 and a.LoanDisbursed=0 and a.acceptrejectflag=0 and a.loantype='SALARYADVANCE' and a.sentforreview=0";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["employername"] = rd["employername"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> LoansPendingReview
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string emps = System.Web.HttpContext.Current.Session["selectedemployer"].ToString();

                String emps1 = emps.Replace("AND EmployerID", "AND a.EmployerID");

                string sql = "select a.LoanId,a.BorrowerPhone Phone,b.CompanyName employername,a.BorrowDate date,a.LoanAmount FROM LoanApplications a,Employers b WHERE a.loanapproved='0' and a.loandisbursed='0' and a.vetted='YES' and a.sentforreview='1' and a.Employerid=b.id " + emps1 + "";
                //string sql = "select a.LoanId,a.BorrowerPhone Phone,b.CompanyName employername,a.BorrowDate,a.LoanAmount FROM LoanApplications a,Employers b WHERE a.loanapproved=0 and a.loandisbursed=0 and a.vetted='YES' and a.sentforreview=1 and a.Employerid=b.id";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            //u had gven it an alias by the name Phone so it returns the column name as phone instead of borrowerphone.try again
                            entry["borrowerphone"] = rd["phone"];
                            entry["employername"] = rd["employername"];
                            entry["borrowdate"] = rd["date"];
                            // entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> AcceptedByHRLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string emps = System.Web.HttpContext.Current.Session["selectedemployer"].ToString();

                String emps1 = emps.Replace("AND EmployerID", "AND a.EmployerID");

                string sql = "select a.LoanId,a.BorrowerPhone Phone,b.companyname EmployerName,a.BorrowDate,a.LoanAmount,a.NetAmount FROM LoanApplications a,Employers b WHERE a.acceptrejectflag='1' and b.ID=a.employerid " + emps1 + "";
                // string sql = "select a.LoanId,a.BorrowerPhone Phone,b.companyname EmployerName,a.BorrowDate,a.LoanAmount,a.NetAmount FROM LoanApplications a,Employers b WHERE a.acceptrejectflag=1 and b.ID=a.employerid";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            //u had gven it an alias by the name Phone so it returns the column name as phone instead of borrowerphone.try again
                            entry["borrowerphone"] = rd["phone"];
                            entry["employername"] = rd["employername"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["netamount"] = rd["netamount"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> RejectedByHRLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string emps = System.Web.HttpContext.Current.Session["selectedemployer"].ToString();

                String emps1 = emps.Replace("AND EmployerID", "AND a.EmployerID");

                string sql = "select a.LoanId,a.BorrowerPhone Phone,b.companyname EmployerName,a.BorrowDate,a.LoanAmount,a.NetAmount,a.Hrcomments FROM LoanApplications a,Employers b WHERE a.acceptrejectflag='2' and b.ID=a.employerid " + emps1 + "";

                //string sql = "select LoanId,BorrowerPhone Phone,EmployerName,Borrowdate,LoanAmount,NetAmount FROM LoanApplications WHERE acceptrejectflag=0";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            //u had gven it an alias by the name Phone so it returns the column name as phone instead of borrowerphone.try again
                            entry["borrowerphone"] = rd["phone"];
                            entry["employername"] = rd["employername"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["netamount"] = rd["netamount"];
                            entry["Hrcomments"] = rd["Hrcomments"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> DisbursedSalaryAdvance
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string emps = System.Web.HttpContext.Current.Session["selectedemployer"].ToString();
                String emps1 = emps.Replace("AND EmployerID", "AND a.EmployerID");
                // string sql = "select LoanId,BorrowerPhone Phone,EmployerName,LoanAmount,NetAmount,BaseAmount,ApproverName FROM LoanApplications WHERE LoanApproved='1' and LoanDisbursed='1' and vetted='YES'and loancode='L0003'";
                string sql = "select a.LoanId,a.BorrowerPhone Phone, b.CompanyName Employername,a.LoanAmount,a.NetAmount,a.BaseAmount,a.ApproverName FROM LoanApplications a,Employers b WHERE a.LoanApproved='1' and a.LoanDisbursed='1' and a.vetted='YES' and a.loancode='L0003' and a.Employerid=b.id  " + emps1 + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["employername"] = rd["employername"];
                            entry["loanamount"] = rd["loanamount"];
                            entry["netamount"] = rd["netamount"];
                            entry["baseamount"] = rd["baseamount"];
                            entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }




        public List<Dictionary<string, object>> UnpaidLoansfl
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 800 loanapplications.borrowerphone as phone ,loanapplications.loanid,loanapplications.borrowdate ,loanapplications.floatclient,loanapplications.loandisbursedate,loanapplications.loanid,loanapplications.borrowerphone,loanapplications.loanamount,loanapplications.loanbalance,loantype FROM  dbo.loanapplications  where (loanapplications.loancleared is NULL or loanapplications.loancleared='0') and loanapplications.loandisbursed='1' and loanapplications.floatclient=1   and loanref not in (select loanref from paymenthistory)   ORDER BY loanapplications.loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            //entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        

        public List<Dictionary<string, object>> UnpaidLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 800 loanapplications.borrowerphone as phone ,loanapplications.loanid,loanapplications.borrowdate ,loanapplications.floatclient,loanapplications.loandisbursedate,loanapplications.loanid,loanapplications.borrowerphone,loanapplications.loanamount,loanapplications.loanbalance,loantype FROM  dbo.loanapplications  where (loanapplications.loancleared is NULL or loanapplications.loancleared='0') and loanapplications.loandisbursed='1'   and loanref not in (select loanref from paymenthistory) and floatclient is null   ORDER BY loanapplications.loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            //entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> UnpaidLoansDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanBalance > '0' and LoanBalance=LoanAmount and loandisbursed='1' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> PaidLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                //string sql = "select a.LoanId,a.BorrowerPhone Phone,b.PaymentDate,a.LoanAmount,a.LoanBalance,a.ApproverName FROM LoanApplications a, LoanRepayment b WHERE a.LoanBalance <= '0' and a.loandisbursed='1' and a.loanbalance=b.balance and a.loanamount=b.borrowamount and a.loanref=b.loanref ORDER BY a.loanid DESC";

                string sql = "select TOP 800 * from loanapplications where loancleared='1'   and loandisbursed='1' and floatclient is null ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["borrowerphone"];
                            entry["cleardate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loantype"] = rd["loantype"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> PaidLoansFL
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                //string sql = "select a.LoanId,a.BorrowerPhone Phone,b.PaymentDate,a.LoanAmount,a.LoanBalance,a.ApproverName FROM LoanApplications a, LoanRepayment b WHERE a.LoanBalance <= '0' and a.loandisbursed='1' and a.loanbalance=b.balance and a.loanamount=b.borrowamount and a.loanref=b.loanref ORDER BY a.loanid DESC";

                string sql = "select TOP 800 * from loanapplications where loancleared='1'   and loandisbursed='1' and floatclient=1 ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["borrowerphone"];
                            entry["cleardate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loantype"] = rd["loantype"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> PaidLoansDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select * from loanapplications where loandisbursed='1' and loancleared='1' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "ORDER BY loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["borrowerphone"];
                            entry["cleardate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loantype"] = rd["loantype"];
                            //entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> OverDueLoansFL
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select loanref,actionedby,LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,floatclient,(select firstname+' '+lastname from LoanUsers where phonenumber=Borrowerphone)as name,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanBalance > '0'and loandisbursed='1' and loancleared='0'  and GETDATE()>expectedcleardate and floatclient=1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["loanref"] = rd["loanref"];
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["name"] = rd["name"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["approvername"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> OverDueLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select loanref,actionedby,LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,(select firstname+' '+lastname from LoanUsers where phonenumber=Borrowerphone)as name,LoanBalance,LoanDisbursedate,ApproverName,floatclient  FROM LoanApplications WHERE LoanBalance > '0'and loandisbursed='1' and loancleared='0' and floatclient is null and GETDATE()>expectedcleardate";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["loanref"] = rd["loanref"];
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["name"] = rd["name"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["approvername"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> OverDueInstalment
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select TOP 800 a.phone as customerphone,(c.firstname+' '+c.middlename+' '+c.lastname)as customername,a.entryid as paymentno,a.loanreference,a.amounttopay as installmentamount,a.dateofpayment as expectedpaydate,b.loanamount as borrowedAmount,b.loanbalance as totalbalance from loanschedule a ,loanapplications b,LoanUsers c where dateofpayment < convert(varchar(10),GETDATE(),121) and a.loanreference=b.loanref and CAST(b.loanbalance as DECIMAL(18,2)) >0 and b.loandisbursed='1' and a.phone=c.phonenumber";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            //customerphone customername loanreference paymentno  installmentamount expectedpaydate  borrowedamount  totalbalance
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["customername"] = rd["customername"];
                            entry["customerphone"] = rd["customerphone"];
                            entry["loanref"] = rd["loanreference"];
                            entry["paymentno"] = rd["paymentno"];
                            entry["installmentamount"] = rd["installmentamount"];
                            entry["expectedpaydate"] = rd["expectedpaydate"];
                            entry["borrowedamount"] = rd["borrowedamount"];
                            entry["totalbalance"] = rd["totalbalance"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> OverDueLoansDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select loanref,actionedby,LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE LoanBalance > '0'and loandisbursed='1' and loancleared='0'  and GETDATE()> expectedcleardate " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["loanref"] = rd["loanref"];
                            entry["phonenumber"] = rd["phone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            entry["approvername"] = rd["actionedby"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> PartiallyPaidLoansFL
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 800 (loanapplications.loanid),loanapplications.borrowerphone,loanapplications.borrowdate ,loanapplications.loandisbursedate,loanapplications.loanid,loanapplications.floatclient,loanapplications.borrowerphone,loanapplications.loanamount,loanapplications.loanbalance,loantype FROM         dbo.paymenthistory INNER JOIN dbo.loanapplications ON dbo.paymenthistory.loanref = dbo.loanapplications.loanref where (loanapplications.loancleared is NULL or loanapplications.loancleared='0') and loanapplications.loandisbursed='1' and loanapplications.floatclient=1 ORDER BY loanapplications.loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["borrowerphone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", ""); ;
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", ""); ;
                            //entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

     
        public List<Dictionary<string, object>> PartiallyPaidLoans
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 800 (loanapplications.loanid),loanapplications.borrowerphone,loanapplications.floatclient ,loanapplications.borrowdate ,loanapplications.loandisbursedate,loanapplications.loanid,loanapplications.borrowerphone,loanapplications.loanamount,loanapplications.loanbalance,loantype FROM         dbo.paymenthistory INNER JOIN dbo.loanapplications ON dbo.paymenthistory.loanref = dbo.loanapplications.loanref where (loanapplications.loancleared is NULL or loanapplications.loancleared='0') and loanapplications.loandisbursed='1' and floatclient is null ORDER BY loanapplications.loanid DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["borrowerphone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", ""); ;
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", ""); ;
                            //entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> PartiallyPaidLoansDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT distinct (loanapplications.loanid),loanapplications.borrowdate,loanapplications.borrowerphone ,loanapplications.borrowdate ,loanapplications.loandisbursedate,loanapplications.loanid,loanapplications.borrowerphone,loanapplications.loanamount,loanapplications.loanbalance,loantype FROM   dbo.paymenthistory INNER JOIN dbo.loanapplications ON dbo.paymenthistory.loanref = dbo.loanapplications.loanref where (loanapplications.loancleared is NULL or loanapplications.loancleared='0') and loanapplications.loandisbursed='1'  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " ORDER BY loanapplications.loanid DESC";
                //string sql = "select LoanId,BorrowerPhone Phone,BorrowDate,LoanAmount,LoanBalance,LoanDisbursedate,ApproverName FROM LoanApplications WHERE loanbalance>'0' AND loanbalance!=loanamount and loandisbursed='1' " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["loanid"];
                            entry["phonenumber"] = rd["borrowerphone"];
                            entry["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            entry["loanamount"] = rd["loanamount"];
                            entry["loanbalance"] = rd["loanbalance"];
                            entry["loandisbursedate"] = rd["loandisbursedate"].ToString().Replace("12:00:00 AM", "");
                            //entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }



        public List<Dictionary<string, object>> RegisteredVendorAdminsApproved
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,fullnames,CreateBy,createDate,Email1,Email2,Mobile1,Mobile2,stationcode,StationBranchCode,BankBranchCode FROM tbUsers WHERE approved=1 and Section='V'";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["createby"] = rd["CreateBy"];
                            entry["createdate"] = rd["CreateDate"];
                            entry["email1"] = rd["Email1"];
                            entry["email2"] = rd["Email2"];
                            entry["mobile1"] = rd["Mobile1"];
                            entry["mobile2"] = rd["Mobile2"];
                            entry["stationcode"] = rd["stationcode"];
                            entry["StationBranchCode"] = rd["StationBranchCode"];
                            entry["bankbranchcode"] = rd["BankBranchCode"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Balancesheet_Statement
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT * FROM  Temp_Balancesheet " + System.Web.HttpContext.Current.Session["code"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["accounttype"] = rd["accounttype"];
                            entry["accountcode"] = rd["accountcode"];
                            entry["AccountName"] = rd["AccountName"];

                            entry["Amountcr"] = rd["Amountcr"];
                            entry["Amountdr"] = rd["Amountdr"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Incom_Statement
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT * FROM  Temp_Incomestatement " + System.Web.HttpContext.Current.Session["code"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["accounttype"] = rd["accounttype"];
                            entry["accountcode"] = rd["accountcode"];
                            entry["AccountName"] = rd["AccountName"];

                            entry["Amountcr"] = rd["Amountcr"];
                            entry["Amountdr"] = rd["Amountdr"];


                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Ledgers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT Temp_TBLeders1.AccountName as Na,(select sum(Amountcr)  from Temp_TBLeders1 ) as cr,(select sum(Amountdr)  from Temp_TBLeders1 ) as dr,SUM(isnull(cast(Amountdr as decimal(18,2)),0) - isnull(cast(Amountcr as decimal(18,2)),0)) OVER (ORDER BY id  ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) as Op,* FROM  Temp_TBLeders1 " + System.Web.HttpContext.Current.Session["code"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["accounttype"] = rd["accounttype"];
                            entry["accountcode"] = rd["accountcode"];

                            string code = rd["accountcode"].ToString();
                            entry["AccountName"] = rd["AccountName"];
                            entry["Dates"] = rd["Dates"];
                            entry["Amountcr"] = rd["Amountcr"];
                            entry["Amountdr"] = rd["Amountdr"];
                            entry["openingbalcr"] = rd["Op"];
                            entry["mpesaref"] = rd["mpesaref"];
                            entry["loanrepaymentdate"] = rd["loanrepaymentdate"];
                            entry["borrowerphone"] = rd["borrowerphone"];
                            entry["loandisbursedate"] = rd["Dates"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["Narration"] = rd["Narration"];
                            double cr = Convert.ToDouble(rd["Amountcr"]);
                            double dr = Convert.ToDouble(rd["Amountdr"]);
                            double cr1 = Convert.ToDouble(rd["cr"]);
                            double dr1 = Convert.ToDouble(rd["dr"]);
                            // if (cr==0)
                            //{
                            //entry["openingbalcr"] = dr1 - dr;
                            //}//
                            //else
                            //{
                            //entry["openingbalcr"] = cr1 - cr;
                            //}
                            //entry["borrowerphone"] = rd["borrowerphone"];
                            //entry["approvername"] = rd["approvername"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        //\\\\\\\\\\\\


        public List<Dictionary<string, object>> RegisteredChurchUserApproved
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,fullnames,CreateBy,createDate,Email1,Email2,Mobile1,Mobile2 FROM tbUsers";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["createby"] = rd["CreateBy"];
                            entry["createdate"] = rd["CreateDate"];
                            entry["email1"] = rd["Email1"];
                            entry["email2"] = rd["Email2"];
                            entry["mobile1"] = rd["Mobile1"];
                            entry["mobile2"] = rd["Mobile2"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> RegisteredChurchofficerApproved
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,Firstname,Lastname,Email,phonenumber1,phonenumber2,Salary,idno,occupation,station,Town,Country FROM tblthirdparty";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["Firstname"] = rd["Firstname"];
                            entry["Lastname"] = rd["Lastname"];
                            entry["Email"] = rd["Email"];
                            entry["phonenumber1"] = rd["phonenumber1"];
                            entry["phonenumber2"] = rd["phonenumber2"];
                            entry["Salary"] = rd["Salary"];
                            entry["idno"] = rd["idno"];
                            entry["occupation"] = rd["occupation"];
                            entry["station"] = rd["station"];
                            entry["Town"] = rd["Town"];
                            entry["Country"] = rd["Country"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Deposit
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Accountno,Firstname,Lastname,idno,Mobileno,accounttype,Depositamount FROM tblcurrentaccount";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Accountno"] = rd["Accountno"];
                            entry["Firstname"] = rd["Firstname"];
                            entry["Lastname"] = rd["Lastname"];
                            entry["idno"] = rd["idno"];
                            entry["Mobileno"] = rd["Mobileno"];
                            entry["accounttype"] = rd["accounttype"];
                            entry["Depositamount"] = rd["Depositamount"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Registeredloantype
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,loancode,loantype,Duration,Adminrate,interestrate,commissionrate,Repayperiod FROM tblloantypes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["loancode"] = rd["loancode"];
                            entry["loantype"] = rd["loantype"];
                            entry["Duration"] = rd["Duration"];
                            entry["Adminrate"] = rd["Adminrate"];
                            entry["interestrate"] = rd["interestrate"];
                            entry["commissionrate"] = rd["commissionrate"];
                            entry["Repayperiod"] = rd["Repayperiod"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Registeredsavingtype
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,Savingcode,Savingtype,Duration,adminfee,interest,maturity FROM tblsavings";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["Savingcode"] = rd["Savingcode"];
                            entry["Savingtype"] = rd["Savingtype"];
                            entry["Duration"] = rd["Duration"];
                            entry["adminfee"] = rd["adminfee"];
                            entry["interest"] = rd["interest"];
                            entry["maturity"] = rd["maturity"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Tools
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,fullnames,CreateBy,createDate,Email1,Email2,Mobile1,Mobile2 FROM tbUsers";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["createby"] = rd["CreateBy"];
                            entry["createdate"] = rd["CreateDate"];
                            entry["email1"] = rd["Email1"];
                            entry["email2"] = rd["Email2"];
                            entry["mobile1"] = rd["Mobile1"];
                            entry["mobile2"] = rd["Mobile2"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> RegisteredChurchUserApproveddates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT id,username,fullnames,CreateBy,createDate,Email1,Email2,Mobile1,Mobile2 FROM tbUsers  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["username"] = rd["username"];
                            entry["fullnames"] = rd["fullnames"];
                            entry["createby"] = rd["CreateBy"];
                            entry["createdate"] = rd["CreateDate"];
                            entry["email1"] = rd["Email1"];
                            entry["email2"] = rd["Email2"];
                            entry["mobile1"] = rd["Mobile1"];
                            entry["mobile2"] = rd["Mobile2"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Registeredsupermarkets
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select id, supermarketcode, supermarketbranch , supermarketname  from Supermarket order by supermarketname";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["id"] = rd["id"];
                            entry["supermarketcode"] = rd["supermarketcode"];
                            entry["supermarketbranch"] = rd["supermarketbranch"];
                            entry["supermarketname"] = rd["supermarketname"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> RegisteredChurchModify
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select channelid,customerNumber,shortname,EmailAddress,Phonenumber,Sector,Target,Industry,RegDate from customers WHERE CustomerGroup='CHURCHES-C00006' and accountsgenerated=1 AND allaccountslinked=1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["channelid"] = rd["channelid"];
                            entry["customernumber"] = rd["customernumber"];
                            entry["shortname"] = rd["shortname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["sector"] = rd["sector"];
                            entry["target"] = rd["target"];
                            entry["industry"] = rd["industry"];
                            entry["regdate"] = rd["regdate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> RegisteredEdit
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select * from Stations WHERE Active=1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["stationid"] = rd["stationid"];
                            entry["StationName"] = rd["StationName"];
                            entry["stationcode"] = rd["stationcode"];
                            entry["churchurl"] = rd["url"];
                            entry["stationmanager"] = rd["stationmanager"];
                            entry["managersemail"] = rd["managersemail"];
                            entry["managersmobile"] = rd["managersmobile"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> ApprovedUsers
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = " select * from Users WHERE Approved=1";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["userid"] = rd["userid"];
                            entry["firstname"] = rd["firstname"];
                            entry["lastname"] = rd["lastname"];
                            entry["emailaddress"] = rd["emailaddress"];
                            entry["phonenumber"] = rd["phonenumber"];
                            entry["gender"] = rd["gender"];
                            entry["dateofbirth"] = rd["dateofbirth"];
                            entry["idorpp"] = rd["idorpp"];
                            entry["vrno"] = rd["vrno"];
                            entry["datecreated"] = rd["datecreated"];
                            entry["dateapproved"] = rd["dateapproved"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> product_rates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from Loancodes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["loantypeid"] = rd["loantypeid"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];
                            entry["interestrate"] = rd["interestrate"];
                            entry["adminfee"] = rd["adminfee"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> loan_rates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from tblloantypes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["id"] = rd["id"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];
                            entry["Repayperiod"] = rd["Repayperiod"];
                            entry["Adminrate"] = rd["Adminrate"];
                            entry["interestrate"] = rd["interestrate"];
                            entry["commissionrate"] = rd["commissionrate"];
                            entry["Duration"] = rd["Duration"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Saving_rates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from tblsavings";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["id"] = rd["id"];
                            entry["Savingtype"] = rd["Savingtype"];
                            entry["Savingcode"] = rd["Savingcode"];
                            entry["maturity"] = rd["maturity"];
                            entry["adminfee"] = rd["adminfee"];
                            entry["interest"] = rd["interest"];
                            entry["Duration"] = rd["Duration"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Saving_accounts
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from tblsavingaccount";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["id"] = rd["id"];
                            entry["Savingtype"] = rd["Savingtype"];
                            entry["Savingcode"] = rd["Savingcode"];
                            entry["Maturity"] = rd["Maturity"];
                            entry["Phone"] = rd["Phone"];
                            entry["Status"] = rd["Status"];
                            entry["Idno"] = rd["Idno"];
                            entry["Amount"] = rd["Amount"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> Supermarket
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  distinct (supermarketname),supermarketcode from Supermarket";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["supermarketcode"] = rd["supermarketcode"];
                            entry["supermarketname"] = rd["supermarketname"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> Supermarketdates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  distinct (supermarketname),supermarketcode from Supermarket" + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " ORDER BY supermarketname"; ;
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["supermarketcode"] = rd["supermarketcode"];
                            entry["supermarketname"] = rd["supermarketname"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> vendors
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from tb_vendors order by vendorname";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["id"] = rd["id"];
                            entry["vendorname"] = rd["vendorname"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> vendorsdates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from tb_vendors " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " order by vendorname";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["id"] = rd["id"];
                            entry["vendorname"] = rd["vendorname"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> Supermarket_Stock
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select top(800) ItemNo,Description,Vendor,Contact,Discount      from dbo.StockItems order by StockID  desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["ItemNo"] = rd["ItemNo"];
                            entry["Description"] = rd["Description"];
                            entry["Vendor"] = rd["Vendor"];
                            entry["Contact"] = rd["Contact"];
                            entry["Discount"] = rd["Discount"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> Supermarket_Stockdates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select top(800) ItemNo,Description,Vendor,Contact,Discount      from dbo.StockItems   " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "  order by StockID  desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["ItemNo"] = rd["ItemNo"];
                            entry["Description"] = rd["Description"];
                            entry["Vendor"] = rd["Vendor"];
                            entry["Contact"] = rd["Contact"];
                            entry["Discount"] = rd["Discount"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> Supermarketbranch(string supercode)
        {

            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from Supermarket where supermarketcode='" + supercode + "' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["ID"] = rd["ID"];
                            entry["supermarketname"] = rd["supermarketname"];
                            entry["supermarketcode"] = rd["supermarketcode"];
                            entry["supermarketbranch"] = rd["supermarketbranch"];
                            entry["tillnumber"] = rd["tillnumber"];
                            entry["attendantbame"] = rd["attendantbame"];
                            entry["attendantphone"] = rd["attendantphone"];
                            entry["attendantbame1"] = rd["attendantbame1"];
                            entry["attendantphone1"] = rd["attendantphone1"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> Supermarketbranchdates(string supercode)
        {

            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from Supermarket   " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "  and supermarketcode=  '" + supercode.Trim() + "' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["ID"] = rd["ID"];
                            entry["supermarketname"] = rd["supermarketname"];
                            entry["supermarketcode"] = rd["supermarketcode"];
                            entry["supermarketbranch"] = rd["supermarketbranch"];
                            entry["tillnumber"] = rd["tillnumber"];
                            entry["attendantbame"] = rd["attendantbame"];
                            entry["attendantphone"] = rd["attendantphone"];
                            entry["attendantbame1"] = rd["attendantbame1"];
                            entry["attendantphone1"] = rd["attendantphone1"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> Supermarket_attendands(string supercode)
        {

            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from Supermarket where ID='" + supercode + "' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["ID"] = rd["ID"];
                            entry["attendantbame"] = rd["attendantbame"];
                            entry["attendantphone"] = rd["attendantphone"];
                            entry["attendantbame1"] = rd["attendantbame1"];
                            entry["attendantphone1"] = rd["attendantphone1"];
                            entry["supermarketbranch"] = rd["supermarketbranch"];



                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }


        public List<Dictionary<string, object>> Supermarketattendandsh(string supercode)
        {

            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "select  * from Supermarket where supermarketcode='" + supercode + "' ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["ID"] = rd["ID"];
                            entry["attendantbame"] = rd["attendantbame"];
                            entry["attendantphone"] = rd["attendantphone"];
                            entry["attendantbame1"] = rd["attendantbame1"];
                            entry["attendantphone1"] = rd["attendantphone1"];
                            entry["supermarketbranch"] = rd["supermarketbranch"];

                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoantypesApproval
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT loantypeid,loantype,loancode, interestrate,maxloan,maxperiod,CreateBy,CreateDate  FROM LoanCodes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["loantypeid"] = rd["loantypeid"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];
                            entry["interestrate"] = rd["interestrate"];
                            entry["maxloan"] = rd["maxloan"];
                            entry["maxperiod"] = rd["maxperiod"];
                            entry["CreateBy"] = rd["CreateBy"];
                            entry["CreateDate"] = rd["CreateDate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> LoantypesApproved
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT loantypeid,loantype,loancode, interestrate,maxloan,maxperiod,CreateBy,CreateDate  FROM LoanCodes";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["loantypeid"] = rd["loantypeid"];
                            entry["loantype"] = rd["loantype"];
                            entry["loancode"] = rd["loancode"];
                            entry["interestrate"] = rd["interestrate"];
                            entry["maxloan"] = rd["maxloan"];
                            entry["maxperiod"] = rd["maxperiod"];
                            entry["CreateBy"] = rd["CreateBy"];
                            entry["CreateDate"] = rd["CreateDate"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> CustomersEnquiries
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 800 LoanUsers.firstname,LoanUsers.lastname,customerenquiries.messageid, customerenquiries.customerphone phone, customerenquiries.enquiry,enquirytime from LoanUsers INNER JOIN customerenquiries on LoanUsers.phonenumber = customerenquiries.customerphone where responded='0' order by messageid desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["messageid"] = rd["messageid"];
                            entry["customerphone"] = rd["phone"];
                            entry["firstname"] = rd["firstname"];
                            entry["lastname"] = rd["lastname"];
                            entry["enquiry"] = rd["enquiry"];
                            entry["enquirytime"] = rd["enquirytime"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }

        public List<Dictionary<string, object>> RespondedEnquiries
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT Top 300 messageid, customerphone phone, enquiry,response,enquirytime,responsetime from customerenquiries where responded='1'order by messageid desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["messageid"] = rd["messageid"];
                            entry["customerphone"] = rd["phone"];
                            entry["enquiry"] = rd["enquiry"];
                            entry["response"] = rd["response"];
                            entry["enquirytime"] = rd["enquirytime"];
                            entry["responsetime"] = rd["responsetime"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> RespondedEnquiriesDates
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT  messageid, customerphone phone, enquiry,response,enquirytime,responsetime from customerenquiries " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["messageid"] = rd["messageid"];
                            entry["customerphone"] = rd["phone"];
                            entry["enquiry"] = rd["enquiry"];
                            entry["response"] = rd["response"];
                            entry["enquirytime"] = rd["enquirytime"];
                            entry["responsetime"] = rd["responsetime"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ReminderMessages
        {
            get
            {
                //INSERT INTO LoansDailyAlerts_Records (phone,SMS_message,isSent,LOAN_REF)
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 800 id,phone,SMS_message,datesent,LOAN_REF from LoansDailyAlerts_Records order by datesent desc";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["messageid"] = rd["id"];
                            entry["customerphone"] = rd["phone"];
                            entry["adminmessage"] = rd["SMS_message"];
                            entry["sendtime"] = rd["datesent"];
                            entry["sender"] = rd["LOAN_REF"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ReminderMessagesDates
        {
            get
            //  string sql = "select shoppingdate,supermarketname,description,customerphone,shoppingtype FROM ShoppingDetails " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
            {
                //INSERT INTO LoansDailyAlerts_Records (phone,SMS_message,isSent,LOAN_REF)
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT  id,phone,SMS_message,datesent,LOAN_REF from LoansDailyAlerts_Records WHERE " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " ORDER BY datesent DESC";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["messageid"] = rd["id"];
                            entry["customerphone"] = rd["phone"];
                            entry["adminmessage"] = rd["SMS_message"];
                            entry["sendtime"] = rd["datesent"];
                            entry["sender"] = rd["LOAN_REF"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> ModifyAccounts
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT * from ChartofAccounts";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["accID"] = rd["accID"];
                            entry["itemname"] = rd["itemname"];
                            entry["accountnumber"] = rd["accountno"];
                            entry["accounttype"] = rd["accounttype"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }
        public List<Dictionary<string, object>> StockItems
        {
            get
            {
                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT *from StockItems where " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " ";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["StockID"] = rd["StockID"];
                            entry["itemNo"] = rd["itemNo"];
                            entry["Description"] = rd["Description"];
                            entry["ItemNo"] = rd["ItemNo"];
                            entry["Vendor"] = rd["Vendor"];
                            entry["Contact"] = rd["Contact"];
                            entry["Website"] = rd["Website"];
                            data.Add(entry);
                        }
                    }
                }
                return data;
            }
        }




    }
}