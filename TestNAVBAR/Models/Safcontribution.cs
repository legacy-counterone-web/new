﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class Safcontribution
    {

        public int Userid { get; set; }
        [DisplayName("Account No")]
        public string Accountno { get; set; }

        [DisplayName("Membership Fee")]
        public string Membershipfee { get; set; }
        // public string Openingshares { get; set; }
        [DisplayName("Share Contribution")]
        public string sharecontibution { get; set; }
        [DisplayName("Total Shares")]
        public string Totalshares { get; set; }
        [DisplayName("Loan Limit")]
        public string LoanLimit { get; set; }
        [DisplayName("Month")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string Months { get; set; }
        [DisplayName("Monthly Contribution")]
        public string Monthlycontribution { get; set; }
        public string useright { get; set; }
        public string adminright { get; set; }
        [DisplayName("Mpesa Ref")]
        public string mpesaref { get; set; }
        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "Phone No Required")]
        public string Phone { get; set; }
        public string @actionedby { get; set; }
        public string @Rectifiedby { get; set; }
    }
}