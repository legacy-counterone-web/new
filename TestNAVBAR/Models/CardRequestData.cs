﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public class CardRequestData
    {

        CardRequestB bpftchqlist = new CardRequestB();
        BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
        public CardRequestA ProcessbpQueryItem()
        {
            CardRequestA bpftlistquery = new CardRequestA();
            bpftlistquery.bpFtFlag = "1";
            bpftlistquery.bpAction = "Select";
            bpftlistquery.bpAccountNo = "1000000000001234";
            bpftlistquery.bpAccountNo2 = "1000000000002093";
            bpftlistquery.bpAccounttype = "Savings-Senior";
            bpftlistquery.bpCurrency = "USD";
            bpftlistquery.bpLedgerBal = 3400.54;
            bpftlistquery.bpAvailBal = 3200.42;
            bpftlistquery.bpAccountStatus = "Active";
            return bpftlistquery;

        }

        public SelectList StopCardReason
        {
            get
            {
                List<object> data = new List<object>();

                data.Add(new { Name = "-select one-", Value = "" });
                data.Add(new { Name = "STOLEN", Value = "STOLEN" });
                data.Add(new { Name = "LOST", Value = "LOST" });
                data.Add(new { Name = "DAMAGED", Value = "DAMAGED" });
                data.Add(new { Name = "TRAVEL", Value = "TRAVEL" });

                return new SelectList(data, "Value", "Name", bpftchqlist.BlockReason);
            }
        }

        public string SelectedUtility
        {
            get;
            set;
        }

        public string SelectedPackage
        {
            get;
            set;
        }

        public SelectList Utilities
        {
            get
            {
                List<object> data = new List<object>();
                string sql = " select ID,Utility from BillCompany";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);

                data.Add(new { CompanyCode = "", CompanyName = "-select one-" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { CompanyCode = rd["Utility"], CompanyName = rd["Utility"] });
                        }
                    }
                    return new SelectList(data, "CompanyCode", "CompanyName", SelectedUtility);
                }
            }
        }

        public SelectList DSTVpacks
        {
            get
            {
                List<object> data = new List<object>();
                string sql = " select ID,package from DSTVPackages";
                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);

                data.Add(new { packageCode = "", PackageName = "-select one-" });
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            data.Add(new { packageCode = rd["Package"], PackageName = rd["Package"] });
                        }
                    }
                    return new SelectList(data, "packageCode", "PackageName", SelectedPackage);
                }
            }
        }

        public CardRequestB ProcessSelectUtility()
        {

            bpftchqlist.bpFromAccount = "1000000000001234";
            bpftchqlist.BlockReason = "50";

            return bpftchqlist;

        }

        public List<Dictionary<string, object>> InternalFTSelectionList
        {
            get
            {


                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 1000 [SenderEmailAddress] ,[Password],[SMTPServerName]  ,[Domain]  FROM [BRIDGE].[dbo].[EmailSetUp]";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);

                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();
                            entry["acAccountNumber"] = rd["acAccountNumber"];
                            entry["acAccountType"] = rd["acAccountType"];
                            entry["acCurrency"] = rd["acCurrency"];
                            entry["acActualBalance"] = rd["acActualBalance"];
                            entry["acAvailableBalance"] = rd["acAvailableBalance"];
                            //entry["Active"] = rd["Active"];
                            entry["Active"] = rd["Active"].ToString().ToLower().Equals("true") ? "Active" : "unauthorised";
                            data.Add(entry);
                        }

                    }
                }
                rd.Close();
                rd.Dispose();
                return data;
            }
        }
    }
}