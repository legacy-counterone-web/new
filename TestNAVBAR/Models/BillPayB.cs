﻿namespace TestNAVBAR.Models
{
    public class BillPayB
    {

        public BillPayA bpfsDetails;
        public string bpFromAccount { get; set; }
        public string bpBillUtility { get; set; }

    }
}