﻿namespace TestNAVBAR.Models
{
    public class CardRequestB
    {
        public CardRequestA cardDetails;
        public string bpFromAccount { get; set; }
        public string cardnumber { get; set; }
        public string BlockReason { get; set; }
    }
}