﻿namespace TestNAVBAR.Models
{
    public class MinistatementData
    {

        public mMiniEnquiryA ProcessEnqItem()
        {
            mMiniEnquiryA minilist = new mMiniEnquiryA();
            minilist.MiniFlag = "1";
            minilist.AccountNumber= "1000000000001234";
            return minilist;


        }


        public mMiniEnquiryB ProcessListItem()
        {
            mMiniEnquiryB proceslist = new mMiniEnquiryB();
            proceslist.TransaDate = "22-04-2014";
            proceslist.Particulars = "Salary Payment";
            proceslist.MoneyOut = 1200;
            proceslist.MoneyIn = 0;
            return proceslist;
        }
    }
}