﻿namespace TestNAVBAR.Models
{
    public class PersonalB
    {


        public PersonalA CustDetails;
        public string Account { get; set; }
        public string AccountType { get; set; }
        public string Currency { get; set; }
        public double LedgerBal { get; set; }
        public double AvailBal { get; set; }
        public string Status { get; set; }

   }
}