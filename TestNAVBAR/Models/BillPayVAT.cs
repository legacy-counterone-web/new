﻿namespace TestNAVBAR.Models
{
    public class BillPayVAT
    {
        public string bpDebitvat { get; set; }
        public string bpCreditvat { get; set; }
        public string bpVATNo { get; set; }
        public string bpAmountvat { get; set; }
    }
}