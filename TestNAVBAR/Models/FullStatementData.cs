﻿namespace TestNAVBAR.Models
{
    public class FullStatementData
    {

        public fStatementA ProcessfstQueryItem()
        {
            fStatementA fstlist = new fStatementA();
            fstlist.FstmtFlag = "1";          
            fstlist.StartDate = "22-01-2015";
            fstlist.EndDate = "22-02-2015";
            fstlist.AccountNumber = "1000000000001234";
            return fstlist;


        }


        public fStatementB ProcessfstListItem()
        {
            fStatementB fstDetails = new fStatementB();
            fstDetails.TransDate = "22-04-2014";
            fstDetails.Description = "Salary Payment";
            fstDetails.DR = 1200;
            fstDetails.CR = 0;
            return fstDetails;
        }
    }
}