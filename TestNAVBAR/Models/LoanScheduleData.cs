﻿namespace TestNAVBAR.Models
{
    public class LoanScheduleData
    {
        public LoanScheduleEnquiryA LoanEnquiryCriteria()
        { 
            LoanScheduleEnquiryA LnScheduleCriteria=new LoanScheduleEnquiryA ();
            LnScheduleCriteria.LoanNumber = "LD-902910-2015";
            return LnScheduleCriteria;
        }

        public LoanScheduleEnquiryB LoanEnquiryData()
        {
            LoanScheduleEnquiryB LnSchduleData = new LoanScheduleEnquiryB();
            LnSchduleData.OpeningBalance = 457500;
            LnSchduleData.Principal = 7500;
            LnSchduleData.Interest = 810;
            LnSchduleData.Charges = 50;
            LnSchduleData.ClosingBalance = 450000;
            return LnSchduleData;
        }
    }
}