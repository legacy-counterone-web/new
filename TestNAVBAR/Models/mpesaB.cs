﻿namespace TestNAVBAR.Models
{
    public class mpesaB
    {
        public mpesaA mpesaDetails;
        public string mpesaFromAccount { get; set; }
        public string mpesaToAccount { get; set; }
        public double mpesaAmount { get; set; }
        public string mpesaDescription { get; set; }
    }
}