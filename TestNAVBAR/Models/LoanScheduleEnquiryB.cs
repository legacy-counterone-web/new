﻿namespace TestNAVBAR.Models
{
    public class LoanScheduleEnquiryB
    {

        public LoanScheduleEnquiryA LnDetails;
        public double OpeningBalance { get; set; }
        public double Principal { get; set; }
        public double Interest { get; set; }
        public double Charges { get; set; }
        public double ClosingBalance { get; set; }
    }
}