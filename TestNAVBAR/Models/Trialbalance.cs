﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class Trialbalance
    {
        public string accounttype { get; set; }
        public string accountcode { get; set; }
        public string AccountName { get; set; }
        public string Dates { get; set; }
        public string Amountdr { get; set; }
        public string Amountcr { get; set; }
    }
}