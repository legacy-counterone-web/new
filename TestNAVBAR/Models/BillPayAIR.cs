﻿namespace TestNAVBAR.Models
{
    public class BillPayAIR
    {

        public string bpDebitair { get; set; }
        public string bpCreditair { get; set; }
        public string bpAccountNoair { get; set; }
        public string bpAmountair { get; set; }
    }
}