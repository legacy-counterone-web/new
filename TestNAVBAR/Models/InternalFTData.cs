﻿using System.Collections.Generic;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public class InternalFTData
    {

        BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();

        public InternalFTA ProcessftQueryItem()
        {
            InternalFTA internalftlistquery = new InternalFTA();
            internalftlistquery.internalFtFlag = "1";
            internalftlistquery.Action = "Select";
            internalftlistquery.AccountNo = "1000000000001234";
            internalftlistquery.AccountNo2 = "1000000000002093";
            internalftlistquery.Accounttype = "Savings-Senior";
            internalftlistquery.Currency = "USD";
            internalftlistquery.LedgerBal = 3400.54;
            internalftlistquery.AvailBal = 3200.42;
            internalftlistquery.AccountStatus = "Active";
            return internalftlistquery;

        }

        public InternalFTB ProcessInternalFT()
        {

            InternalFTB internalftpostlist = new InternalFTB();
            internalftpostlist.FromAccount = "1000000000001234";
            internalftpostlist.ToAccount = "1000000000002093";
            internalftpostlist.Amount = 210;
            internalftpostlist.Description = "Garage Service Fee";

            return internalftpostlist;

        }


        public List<Dictionary<string, object>> InternalFTSelectionList
        {
            get
            {


                List<Dictionary<string, object>> data = new List<Dictionary<string, object>>();
                string sql = "SELECT TOP 1000 [SenderEmailAddress] ,[Password],[SMTPServerName]  ,[Domain]  FROM [BRIDGE].[dbo].[EmailSetUp]";

                SqlDataReader rd = blogic.RunQueryReturnDataReader(sql);

                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            Dictionary<string, object> entry = new Dictionary<string, object>();

                            entry["acAccountNumber"] = rd["acAccountNumber"];
                            entry["acAccountType"] = rd["acAccountType"];
                            entry["acCurrency"] = rd["acCurrency"];
                            entry["acActualBalance"] = rd["acActualBalance"];
                            entry["acAvailableBalance"] = rd["acAvailableBalance"];
                            //entry["Active"] = rd["Active"];
                            entry["Active"] = rd["Active"].ToString().ToLower().Equals("true") ? "Active" : "unauthorised";
                            data.Add(entry);
                        }

                    }
                }
                rd.Close();
                rd.Dispose();
                return data;
            }
        }
    }
}