﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class SaccoTransfers
    {
        public int Transerid { get; set; }
        [DisplayName("Depositer Account No")]
        public string Accountno { get; set; }
        [DisplayName("Reciepient Surname")]
        public string WSurname { get; set; }
        [DisplayName("Reciepient Other Names")]
        public string WOtherNames { get; set; }
        [DisplayName("Reciepient Phone")]
        public string WPhone { get; set; }
        [DisplayName("Reciepient Account No")]
        public string Accountno1 { get; set; }
        [DisplayName("Depositer Surname")]
        public string DSurname { get; set; }
        [DisplayName("Depositer Other Names")]
        public string DOtherNames { get; set; }
        [DisplayName("Depositer Phone")]
        public string DPhone { get; set; }
        [DisplayName("Transfer Amount")]
        public string Amount { get; set; }
    }
}