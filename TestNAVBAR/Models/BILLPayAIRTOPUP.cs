﻿namespace TestNAVBAR.Models
{
    public class BILLPayAIRTOPUP
    {
        public string bpDebitairtopup { get; set; }
        public string bpCreditairtopup { get; set; }
        public string bpAmountairtopup { get; set; }
        public string bpNumberairtopup { get; set; }
        public bool IsOwnOther { get; set; }
    }
        
}