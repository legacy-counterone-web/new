﻿namespace TestNAVBAR.Models
{
    public class extFTA
    {
        public string extAction { get; set; }
        public string extAccountNo { get; set; }
        public string extAccountNo2 { get; set; }
        public string extAccounttype { get; set; }
        public string extCurrency { get; set; }
        public double extLedgerBal { get; set; }
        public double extAvailBal { get; set; }
        public string extFtFlag { get; set; }
        public string extAccountStatus { get; set; }
        public string extSelectedAccount {get;set;}
    }
}