﻿namespace TestNAVBAR.Models
{
    public class mMiniEnquiryB
    {
        public mMiniEnquiryA EnqDetails;
        public string TransaDate { get; set; }
        public string Particulars { get; set; }
        public double MoneyOut { get; set; }
        public double MoneyIn { get; set; }

    }
}