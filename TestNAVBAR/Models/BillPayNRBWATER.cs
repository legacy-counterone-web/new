﻿namespace TestNAVBAR.Models
{

    public class BillPayNRBWATER
    {
        public string bpDebitnrbwater { get; set; }
        public string bpCreditnrbwater { get; set; }
        public string bpMetreNonrbwater { get; set; }
        public string bpAmountnrbwater { get; set; }


    }
}