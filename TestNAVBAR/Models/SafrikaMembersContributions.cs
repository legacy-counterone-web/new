﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class SafrikaMembersContributions
    {
        public int Userid { get; set; }
        [DisplayName("Account No")]
        public string Accountno { get; set; }
        [DisplayName("Membership Fee")]

        public string Membershipfee { get; set; }
        [DisplayName("Opening Shares")]
        public string Openingshares { get; set; }
        [DisplayName("Share Contribution")]
        public string sharecontibution { get; set; }
        [DisplayName("Total Shares")]
        public string Totalshares { get; set; }
        [DisplayName("Loan Limit")]
        public string LoanLimit { get; set; }
        [DisplayName("Month")]
        public string Months { get; set; }
        [DisplayName("Monthly Contribution")]
        public string Monthlycontribution { get; set; }
    }
}