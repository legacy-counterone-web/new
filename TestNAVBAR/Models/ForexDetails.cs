﻿using System.Collections.Generic;

namespace TestNAVBAR.Models
{
    public class ForexDetails
    {

        public string Currency { get; set; }
        public double Buy { get; set; }
        public double Sale { get; set; }

        public static List<ForexDetails> GetForexRates()
        {
            List<ForexDetails> ForexDetails = new List<ForexDetails>{
            new ForexDetails   { Currency="USD/JMD", Buy=113.2, Sale=117.9},
            new ForexDetails   { Currency="EUR/JMD", Buy=127.6, Sale=134.5},
             new ForexDetails   { Currency="USD/GBP", Buy=8.12, Sale=12.3}

            };
            return ForexDetails;
        }
    }
}