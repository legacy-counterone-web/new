﻿namespace TestNAVBAR.Models
{
    public class OwnFTA
    {
        public string Action { get; set; }
        public string ownAccountNo { get; set; }
        public string ownAccountNo2 { get; set; }
        public string ownAccounttype { get; set; }
        public string ownCurrency { get; set; }
        public double ownLedgerBal { get; set; }
        public double ownAvailBal { get; set; }
        public string ownFtFlag { get; set; }
        public string ownAccountStatus { get; set; }
        public string ownSelectedAccount {get;set;}
    }
}