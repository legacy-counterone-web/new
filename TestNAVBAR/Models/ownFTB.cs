﻿namespace TestNAVBAR.Models
{
    public class OwnFTB
    {

        public OwnFTA ownfsDetails;
        public string ownFromAccount { get; set; }
        public string ownToAccount { get; set; }
        public double  ownAmount { get; set; }
        public string ownDescription { get; set; }
    }
}