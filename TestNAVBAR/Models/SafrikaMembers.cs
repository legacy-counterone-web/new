﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class SafrikaMembers
    {
        public string file { get; set; }
        public int Userid { get; set; }
        [DisplayName("Account No")]
        //[ReadOnly(true)]
        public string Accountno { get; set; }
        [Required]
        [DisplayName("Surname")]
        public string Surname { get; set; }
        [DisplayName("Other Names")]
        [Required]
        public string OtherNames { get; set; }
        [Required(ErrorMessage = "Field can't be empty")]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
        [DisplayName("Date Joined")]
        [DataType(DataType.Date)]
        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public string DateJoined { get; set; }
        [DisplayName("Phone Number")]
        [Required(ErrorMessage = "Phone No Required")]
        public string Phone { get; set; }
        [DisplayName("ID NO")]
        [Required(ErrorMessage = "ID No required")]
        public string Idno { get; set; }
        [DisplayName("Membership Fee")]
        [Required(ErrorMessage = "Membership Required")]
        public string Membershipfee { get; set; }
        [DisplayName("Share Contribution")]
        [Required(ErrorMessage = "Contribution Required")]
        // public string Openingshares { get; set; }
        public string sharecontibution { get; set; }
        [DisplayName("Total Shares")]
        [Required(ErrorMessage = "Total Share required")]
        public string Totalshares { get; set; }
        [DisplayName("Loan Limit")]
        [Required(ErrorMessage = "Loan Limit Reguired")]
        [DisplayFormat(DataFormatString = "{0}")]
        public string LoanLimit { get; set; }
        [DisplayName("AirTime Limit")]
        public string AirTimeLimit { get; set; }
        public string Membership { get; set; }
        public string useright { get; set; }
        public string adminright { get; set; }
        public int pin { get; set; }
        [DisplayName("Mpesa Ref")]
        public string mpesaref { get; set; }
        [DisplayName("Actioned By")]
        public string actionedby { get; set; }
        [DisplayName("Rectified By")]
        public string Rectifiedby { get; set; }
        [DisplayName("Loan Auto")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "You gotta tick the box!")]
        public bool automatic { get; set; }
        [DisplayName("AirTime Auto")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "You gotta tick the box!")]
        public bool Airautomatic { get; set; }
        [DisplayName("Account")]
        public string Dels { get; set; }
        [DisplayName("Amount")]
        public string Amount { get; set; }

        //Added for ThirdParty
        public int ID { get; set; }
        [DisplayName("Occupation")]
        public string ocuppation { get; set; }
        [DisplayName("Description")]
        public string description { get; set; }
        public string location { get; set; }
        [DisplayName("Coordinates")]
        public string coordinates { get; set; }
        [DisplayName("Photo")]
        public string photo { get; set; }
        public string date { get; set; }
        public int status { get; set; }
        public string username { get; set; }
        public string employer { get; set; }
        [DisplayName("Company")]
        public string company { get; set; }
        [DisplayName("Employment Year")]
        public string employmentyear { get; set; }
        public string salary { get; set; }
        [DisplayName("Company HR")]
        public string hr { get; set; }
        [DisplayName("HR Comment")]
        public string hrcomment { get; set; }

    }
}