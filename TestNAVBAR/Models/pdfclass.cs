﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
namespace TestNAVBAR.Models
{
    public class pdfclass
    {
        [Required(ErrorMessage = "Please select file.")]
        public HttpPostedFileBase PostedFile { get; set; }
        [Required(ErrorMessage = "National/Passport/Alien ID Field must be filled")]
        [Range(minimum: 999999, maximum: 999999999999, ErrorMessage = "Enter correct ID Nimber")]
        [Display(Name = "National/Alien/Passport ID No")]
        public int id { get; set; }
    }
}