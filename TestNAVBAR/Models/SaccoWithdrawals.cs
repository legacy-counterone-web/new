﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace TestNAVBAR.Models
{
    public class SaccoWithdrawals
    {
        public int Transid { get; set; }
        [DisplayName("Account No")]
        public string Accountno { get; set; }
        public string Surname { get; set; }
        [DisplayName("Other Names")]
        public string OtherNames { get; set; }
        [DisplayName("Phone")]
        public string Phone { get; set; }
        [DisplayName("Amount")]
        public int Amount { get; set; }
        [DisplayName("Balance")]
        public int Balance { get; set; }
        public string Depositdate { get; set; }


    }
}