﻿namespace TestNAVBAR.Models
{
    public class InternalFTA
    {
        public string Action { get; set; }
        public string AccountNo { get; set; }
        public string AccountNo2 { get; set; }
        public string Accounttype { get; set; }
        public string Currency { get; set; }
        public double LedgerBal { get; set; }
        public double AvailBal { get; set; }
        public string internalFtFlag { get; set; }
        public string AccountStatus { get; set; }
        public string SelectedAccount {get;set;}
    }
}