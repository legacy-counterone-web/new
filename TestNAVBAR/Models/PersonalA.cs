﻿namespace TestNAVBAR.Models
{
    public class PersonalA
    {
  
        public string AccountName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string PasswordExpiry { get; set; }
        public string LastLogin { get; set; }
        
    }
}