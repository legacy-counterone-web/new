﻿using System.ComponentModel.DataAnnotations;

namespace TestNAVBAR.Models
{
    public class BILLPaySAFTOPUP
    {
        [Required]
        [Display(Name = "[Debit Account]")]
        public string bpDebitsaftopup { get; set; }

        [Required]
        [Display(Name = "[Credit Account]")]
        public string bpCreditsaftopup { get; set; }

        [Required]
        [Display(Name = "[Amount]")]
        public string bpAmountsaftopup { get; set; }


        [Required]
        [Display(Name = "[Number to Topup]")]
        public string bpNumbersaftopup { get; set; }

        public bool IsOwnOther { get; set; }
    }
        
}