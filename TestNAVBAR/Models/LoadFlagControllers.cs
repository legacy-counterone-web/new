﻿namespace TestNAVBAR.Models
{
    public class LoadFlagControllers
    {
        public FlagsViewModel LoadFlags()
        {
            FlagsViewModel flags = new FlagsViewModel();
            flags.LoadMiniFlag = "1";
            return flags;
        }
    }
}