﻿namespace TestNAVBAR.Models
{
    public class fStatementB
    {

        public fStatementA fstDetails;
        public string TransDate { get; set; }
        public string Description { get; set; }
        public double DR { get; set; }
        public double CR { get; set; }
    }
}