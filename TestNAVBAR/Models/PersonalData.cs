﻿namespace TestNAVBAR.Models
{
    public class PersonalData
    {

        public PersonalA  ProfileDetails()
        {


            PersonalA profile = new PersonalA();
            profile.AccountName = "WANJERU J. GITHINJI";
            profile.Address = "3627362 NAIROBI";
            profile.Phone = "+254714565656";
            profile.PasswordExpiry = "30.06-2015";
            profile.LastLogin = "22-02-2015"; 
            return profile;
        }

        public PersonalB AccountList()
        {
            
            PersonalB accountlist = new PersonalB();
            accountlist.Account = "1000000000001234";
            accountlist.AccountType = "SAVINGS-VIP";
            accountlist.Currency = "JMD";
            accountlist.LedgerBal = 30000;
            accountlist.AvailBal = 25000;
            accountlist.Status = "Active";
            return accountlist;
        }
    }
}