﻿namespace TestNAVBAR.Models
{
    public class InternalFTB
    {

        public InternalFTA internalfsDetails;
        public string FromAccount { get; set; }
        public string ToAccount { get; set; }
        public double  Amount { get; set; }
        public string Description { get; set; }
    }
}