﻿
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
namespace TestNAVBAR.Models
{
    public partial class PaidLoansInfo : System.Web.UI.Page
    {
        ReportDocument Report = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "select  *  from vw_PaidLoan  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument PaidLoans = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/PaidLoansData.rpt");
            PaidLoans.Load(reportPath);
            PaidLoans.SetDataSource(dt);
            PaidLoansViewer.ReportSource = PaidLoans;
            PaidLoansViewer.HasToggleGroupTreeButton = false;
            PaidLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            PaidLoansViewer.HasRefreshButton = true;

            PaidLoansViewer.HasPageNavigationButtons = true;
            PaidLoansViewer.RefreshReport();

        }
    }
}
//using System.Configuration;
//using System.Data;
//using System.Data.SqlClient;
//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
//using System;
//using System.Text.RegularExpressions;

//namespace TestNAVBAR.Models
//{
//    public partial class PaidLoansInfo : System.Web.UI.Page
//    {
//        ReportDocument Report = new ReportDocument();
//        protected void Page_Load(object sender, EventArgs e)
//        {

//            string dateRange_ = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
//            string _DateFrom = dateRange_.Replace("(", string.Empty).Replace(")", string.Empty);
//            string[] _Begin = Regex.Split(_DateFrom, "AND");
//            string[] _datefrom = Regex.Split(_Begin[0], ">=");
//            string datefrom = _datefrom[1].Replace("'", string.Empty);
//            string[] _dateto = Regex.Split(_Begin[1], "<=");
//            string dateto = _dateto[1].Replace("'", string.Empty);
//            DataTable dt;
//            String SQL = "select distinct *from vw_PaidLoan " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";

//            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
//            using (SqlConnection conn = new SqlConnection(sConstr))
//            {
//                using (SqlCommand comm = new SqlCommand(SQL, conn))
//                {
//                    conn.Open();
//                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
//                    {
//                        dt = new DataTable("tbl");
//                        da.Fill(dt);
//                    }
//                }
//            }

//            ReportDocument rd = new ReportDocument();
//            string reportPath = Server.MapPath("~/ aspnet_client / PaidLoansData.rpt");
//            rd.Load(reportPath);
//            rd.SetDataSource(dt);
//            rd.SetParameterValue("mdate", "date from " + datefrom + " date to" + dateto); // Add multiple parameter as you want
//            PaidLoansViewer.ReportSource =rd;
//            //PaidLoansViewer.ReportSource = rd;

//        }
//    }
//}
////using CrystalDecisions.CrystalReports.Engine;
////using System;
////using System.Configuration;
////using System.Data;
////using System.Data.SqlClient;
////using System.Text.RegularExpressions;
////namespace TestNAVBAR.Models
////{
////    public partial class PaidLoansInfo : System.Web.UI.Page
////    {
////        ReportDocument Report = new ReportDocument();
////        protected void Page_Load(object sender, EventArgs e)
////        {
////            string dateRange_ = System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString();
////            string _DateFrom = dateRange_.Replace("(", string.Empty).Replace(")", string.Empty);
////            string[] _Begin = Regex.Split(_DateFrom, "AND");
////            string[] _datefrom = Regex.Split(_Begin[0], ">=");
////            string datefrom = _datefrom[1].Replace("'", string.Empty);
////            string[] _dateto = Regex.Split(_Begin[1], "<=");
////            string dateto = _dateto[1].Replace("'", string.Empty);
////            //DataTable dt;
////           DataTable dt = new DataTable();
////            string SQL = "select  *  from vw_PaidLoan  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + "";
////            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
////            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
////            using (SqlConnection conn = new SqlConnection(sConstr))
////            {
////                using (SqlCommand comm = new SqlCommand(SQL, conn))
////                {
////                    conn.Open();
////                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
////                    {
////                        dt = new DataTable("tbl");
////                        da.Fill(dt);
////                    }
////                }
////            }

////            ReportDocument PaidLoans = new ReportDocument();
////            string reportPath = Server.MapPath("~/aspnet_client/PaidLoansData.rpt");
////            PaidLoans.Load(reportPath);
////            PaidLoans.SetDataSource(dt);

////           // PaidLoans.SetParameterValue("paid", "demabio");
////            PaidLoansViewer.ReportSource = PaidLoans;
////            PaidLoansViewer.HasToggleGroupTreeButton = false;
////            PaidLoansViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
////            PaidLoansViewer.HasRefreshButton = true;

////            PaidLoansViewer.HasPageNavigationButtons = true;
////            PaidLoansViewer.RefreshReport();

////        }
////    }
////}