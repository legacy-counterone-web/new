﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class NewreportsController : Controller
    {
        //
        // GET: /Newreports/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Image()

        {
           
            var d = GetData();
            // ViewData["Message"] = GetData().ToString();
            var json = JsonConvert.SerializeObject(d);
            var model = JsonConvert.DeserializeObject<Trialbalance>(json);
            var jsons = JsonConvert.SerializeObject(model);
            ViewData["Message"] = model;
            return View();
        }

        public ActionResult GetData()
        {
            //using (DBModels db = new DBModels())
            //{GetAllLoans
            SaccoRepo MemberRepo = new SaccoRepo();
            //string id = "254771364026";
            //return Json(MemberRepo.Getnewpin(), JsonRequestBehavior.AllowGet);
            // List<csvpayments> empList = db.Employees.ToList<csvpayments>();
            return Json(MemberRepo.GetTB(), JsonRequestBehavior.AllowGet);
            //}

        }
    }
}
