﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class ReportsController: Controller
    {
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();

        public ActionResult Income()
        {
            return View();
        }
        public ActionResult Balance()
        {
            return View();
        }
        public ActionResult Ldgrs()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Aging()
        {
            return View();
        }
        public ActionResult outletsdetails()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoanUsers()
        {
             return View();          
        }
        public ActionResult Trailbalance()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]

        public ActionResult Trialbalance()


        {
            return Redirect("~/TB.rpt"); 
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]

        public ActionResult Reconciliation()


        {
            return Redirect("~/Reconciliation.rpt");
        }
        public ActionResult Balancesheets()


        {
            return Redirect("~/BalanceSheet.rpt");
        }
        public ActionResult Incomestatements()


        {
            return Redirect("~/Incomestatement.rpt");
        }
        public ActionResult ReportLoaderIncomestatement()


        {
            return View();
        }
        public ActionResult ReportLoaderBalancesheet()


        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReportLoader()
        {
            return View();
        }
        //shoppingdetailsReport

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult shoppingdetailsReport()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult stockitemsdetails()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult statementsDetails()
        {
            return View();
        }
        

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult audittraildetails()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReportLoaderAging()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReportLoaderReconciliation()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReportLoaderLedgers()
        {
            return View();
        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReportLoaderTrialbalance()
        {
            return View();
        }



        //stockitemsdetails
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoader(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string TB_startdate = collection["bp.startdate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];
            string _SQLSelectionCriteria = "";
            string ftodate= Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            _startdate = ftodate;

            string ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
            _enddate = ftenddate;

            string PageToLoad = "";
            string[] _strempoyerID = collection["selectedreport"].Split('-');
            string ReportID = _strempoyerID[0].ToString().Trim();
            string ReportName = Blogic.RunStringReturnStringValue("select reportpath +'-'+ tablename from Reports WHERE ID = '" + ReportID + "'");


            //-------Report Name and Report table have to be factored
            string[] _ReportDetails = ReportName.Split('-');
            ReportName = _ReportDetails[0].ToString();
            string ReportTable = _ReportDetails[1].ToString();
            //---------------------------------------------
            String ReportPath = "C:////";
            string FullReportToLoad = ReportPath + "" + ReportName;

            if (ReportName == "LoansAppliedData.rpt")
            {
                PageToLoad = "LoansApplied";

            }
            if (ReportName == "LoansDisbursedData.rpt")
            {
                PageToLoad = "LoansDisbursed";

            }
            if (ReportName == "CallLogsData.rpt")
            {
                PageToLoad = "CallLogs";

            }
            if (ReportName == "SmsLogsData.rpt")
            {
                PageToLoad = "SmsLogs";

            }


            if (ReportName == "LoanUsersData.rpt")
            {
                PageToLoad = "LoanUsers";

            }
            if (ReportName == "PaidLoansData.rpt")
            {
                PageToLoad = "PaidLoans";

            }
            if (ReportName == "UnpaidLoansData.rpt")
            {
                PageToLoad = "UnpaidLoans";

            }
            if (ReportName == "PartiallyPaidLoansData.rpt")
            {
                PageToLoad = "PartiallyPaidLoans";

            }
            if (ReportName == "AppliedSalaryAdvanceData.rpt")
            {
                PageToLoad = "AppliedSalaryAdvance";

            }
            if (ReportName == "SalaryAdvancePendingReviewData.rpt")
            {
                PageToLoad = "SalaryAdvancePendingReview";

            }
            if (ReportName == "AcceptedByHrLoansData.rpt")
            {
                PageToLoad = "AcceptedByHrLoans";

            }
            if (ReportName == "RejectedByHrLoansData.rpt")
            {
                PageToLoad = "RejectedByHrLoans";

            }
            if (ReportName == "DisbursedSalaryAdvanceData.rpt")
            {
                PageToLoad = "DisbursedSalaryAdvance";

            }


           


          

            

           if (ReportName == "BalanceSheet.rpt")
            {
                PageToLoad = "Balancesheet";

            }

            if (ReportName == "incomerpt.rpt")
            {
                PageToLoad = "incomerpt";

            }

            
            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }
            if (errors.Count > 0)
            {
            }
            else
            {

                

                if (ReportName.ToUpper() == "LOANSAPPLIEDDATA.RPT")
                {
                    _SQLSelectionCriteria = null;

                    _SQLSelectionCriteria = "WHERE     (loanamount > '0') and"; 
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria =  _SQLSelectionCriteria  + "  (borrowdate>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                    }

                    if (collection["mpesano"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                    }

                }

                

                if (ReportName.ToUpper() == "LOANSDISBURSEDDATA.RPT" || ReportName.ToUpper() == "DISBURSEDSALARYADVANCEDATA.RPT")
                {
                    _SQLSelectionCriteria = null;
                    _SQLSelectionCriteria = "    WHERE (loandisbursed = '1') and";
                   
                   
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria= _SQLSelectionCriteria +  "  (loandisbursedate>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND loandisbursedate<='" + _enddate + "')";
                    }

                    if (collection["mpesano"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                    }

                }
                if (ReportName.ToUpper() == "LOANTYPEDATA.RPT")
                {
                    _SQLSelectionCriteria = null;
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = "  (borrowdate>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                    }

                    if (collection["selectedLoanCodes"].ToString().Length > 0)
                    {
                       // _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND loantype='" + _selectedLoanCodes + "'";
                    }

                }
                if (ReportName.ToUpper() == "CALLLOGSDATA.RPT")
                {
                    _SQLSelectionCriteria = null;
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = " (CallDate>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND CallDate<='" + _enddate + "')";
                    }

                    if (collection["mpesano"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerPhone='" + _mpesano + "'";
                    }
                }

                if (ReportName.ToUpper() == "SMSLOGSDATA.RPT")
                {
                    _SQLSelectionCriteria = null;
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = " (datelogged>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datelogged<='" + _enddate + "')";
                    }

                    if (collection["mpesano"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
                    }
                }
                if (ReportName.ToUpper() == "LOANUSERSDATA.RPT")
                {
                    _SQLSelectionCriteria = null;
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = " (datecreated>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datecreated<='" + _enddate + "')";
                    }

                    if (collection["mpesano"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
                    }
                }
                if (ReportName.ToUpper() == "PAIDLOANSDATA.RPT")
                {
                    _SQLSelectionCriteria = null;

                    _SQLSelectionCriteria = " WHERE     (loandisbursed = '1') AND (loancleared = '1') and ";
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " (applicationdate>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND applicationdate<='" + _enddate + "')";
                    }
                }

                    if (ReportName.ToUpper() == "UNPAIDLOANSDATA.RPT")
                    {
                        _SQLSelectionCriteria = null;

                        _SQLSelectionCriteria = "WHERE     (loandisbursed = '1') AND (loancleared IS NULL OR loancleared = '0') AND (loanref NOT IN (SELECT     loanref  FROM          dbo.paymenthistory)) AND";


                        if (collection["bp.startdate"].ToString().Length > 0)
                        {
                            _SQLSelectionCriteria = _SQLSelectionCriteria + " (applicationdate>='" + _startdate + "'";
                        }

                        if (collection["bp.enddate"].ToString().Length > 0)
                        {
                            _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND applicationdate<='" + _enddate + "')";
                        }

                        if (collection["mpesano"].ToString().Length > 0)
                        {
                            _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND a.borrowerphone='" + _mpesano + "'";
                        }

                    }
                        if (ReportName.ToUpper() == "PARTIALLYPAIDLOANSDATA.RPT")
                        {
                            _SQLSelectionCriteria = null;

                            _SQLSelectionCriteria = " WHERE     (loandisbursed = '1') AND (loancleared IS NULL OR loancleared = '0') AND ";
                            if (collection["bp.startdate"].ToString().Length > 0)
                            {
                                _SQLSelectionCriteria = _SQLSelectionCriteria + " ( applicationdate>='" + _startdate + "'";
                            }

                            if (collection["bp.enddate"].ToString().Length > 0)
                            {
                                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND  applicationdate<='" + _enddate + "')";
                            }

                            if (collection["mpesano"].ToString().Length > 0)
                            {
                                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND  borrowerphone='" + _mpesano + "'";
                            }

                        }
                if (ReportName.ToUpper() == "APPLIEDSALARYADVANCEDATA.RPT" || ReportName.ToUpper() == "SALARYADVANCEPENDINGREVIEWDATA.RPT" || ReportName.ToUpper() == "ACCEPTEDBYHRLOANSDATA.RPT" || ReportName.ToUpper() == "REJECTEDBYHRLOANSDATA.RPT")
                {
                    _SQLSelectionCriteria = null;
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = " (borrowerdate>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowerdate<='" + _enddate + "')";
                    }

                    if (collection["mpesano"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                    }
                }
                if (ReportName.ToUpper() == "SHOPPINGDETAILSDATA.RPT")
                {
                    _SQLSelectionCriteria = null;
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = " (borrowerdate>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowerdate<='" + _enddate + "')";
                    }

                    if (collection["mpesano"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                    }
                }


                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
           // return Redirect("~/"+ AspPageToLoad + "");
            return View(PageToLoad);
           
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoaderBalancesheet(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";
            string _startdate = collection["bp.startdate"];
            string TB_startdate = collection["bp.startdate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            string ffromdate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
            TB_startdate = ftodate;

            DateTime getdate = DateTime.Now;
            TempData["From"] = _startdate;
            TempData["To"] = _enddate;
            TempData["Date"] = Convert.ToDateTime(getdate).ToString("yyyy-MM-dd");
            TempData["username"] = HttpContext.Session["username"].ToString();//_selectedusers[1].ToString().Trim();
            TempData["Rptname"] = "Balancesheet Statement";
            HttpContext.Session["code"] = "where cast(Dates as date)>='" + _startdate + "' and cast(Dates as date)<='" + _enddate + "'";

            string ReportName = "";

            string results = "";
            string sql = "exec spBalancesheet '" + TB_startdate + "','" + ffromdate + "'";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQuery(sql);
            PageToLoad = "Balancesheets";
            return View("Balance");

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoaderIncomestatement(FormCollection collection)
        {

            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";
            string _startdate = collection["bp.startdate"];
            //string TB_startdate = collection["bp.startdate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string ftoendate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            DateTime getdate = DateTime.Now;
            TempData["From"] = _startdate;
            TempData["To"] = _enddate;
            TempData["Date"] = Convert.ToDateTime(getdate).ToString("yyyy-MM-dd");
            TempData["username"] = HttpContext.Session["username"].ToString();//_selectedusers[1].ToString().Trim();
            TempData["Rptname"] = "Income Statement";
            HttpContext.Session["code"] = "where cast(Dates as date)>='" + _startdate + "' and cast(Dates as date)<='" + _enddate + "'";

            // TB_startdate = ftodate;
            string ReportName = "";

            string results = "";
            string sql = "exec spincomestatement '" + ftodate + "','" + ftoendate + "'";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQuery(sql);
            PageToLoad = "Incomestatements";
            return View("Income");


        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoaderReconciliation(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";
            string _startdate = collection["bp.startdate"];
            string TB_startdate = collection["bp.startdate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            TB_startdate = ftodate;
            string ReportName = "";

            string results = "";
            string sql = "select  a.borrowerphone,b.transactioncode,b.paymentdate,B.transamount from loanapplications a,paymenthistory b WHERE a.loanref = b.loanref order by b.paymentdate asc ";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQuery(sql);
            PageToLoad = "Reconciliation";
            _SQLSelectionCriteria = null;
            if (collection["bp.startdate"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = "  (paymentdate>='" + _startdate + "'";
            }

            if (collection["bp.enddate"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND paymentdate<='" + _enddate + "')";
            }

            System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();


            // return Redirect("~/"+ AspPageToLoad + "");
            return View(PageToLoad);

        }
        //-----------------------------------
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult shoppingdetailsReport(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _custphone = collection["customerphone"];
            string _action = collection[4];
            string _SQLSelectionCriteria = "";
            string  _allshopping= collection["allshopping"];
            string _allvendor = collection["allvendor"];
            string _shoppingType= collection["shoppingType"];
            string _vendor = collection["vendor"];
            string PageToLoad = "";

            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            _startdate = ftodate;

            string ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
            _enddate = ftenddate;
            string ReportName = "";
            _SQLSelectionCriteria = null;




            _SQLSelectionCriteria = " where vendor<>''";



                    PageToLoad = "shoppingdetails";
                    
                    if (collection["bp.startdate"].ToString().Length > 0)
                    {
                _SQLSelectionCriteria= _SQLSelectionCriteria  + " AND  (shoppingdate>='" + _startdate + "'";
                    }

                    if (collection["bp.enddate"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND shoppingdate<='" + _enddate + "')";
                    }

                    if (collection["customerphone"].ToString().Length > 0)
                    {
                        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerphone='" + _custphone + "'";
                    }

            if (_allshopping.Contains("true"))
            {

            }

            else
            { 
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND shoppingtype='" + _shoppingType + "'";

            }

            


            
            _vendor = _vendor.Replace(".", ""); 
            if (_allvendor .Contains("true"))
            {
               

            }

            else
            {
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND vendor like'" + _vendor + "%'";

            }

            System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();


            // return Redirect("~/"+ AspPageToLoad + "");
            return View(PageToLoad);

        }



        //-----------------------------------
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult outletsdetails(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _supermarketname  = collection["supermarketname"];
            string _enddate = collection["bp.enddate"];
            
            string _action = collection[4];
            string _SQLSelectionCriteria = "";
            
            string _alloutlets = collection["alloutlets"];
            
            
            string PageToLoad = "";

            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            _startdate = ftodate;

            string ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");

        
            string ReportName = "";

            PageToLoad = "outlets";
            _SQLSelectionCriteria = null;
            if (collection["bp.startdate"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = "  (shoppingdate>='" + _startdate + "'";
            }

            if (collection["bp.enddate"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND shoppingdate<='" + _enddate + "')";
            }

          if (_alloutlets.Contains("true"))
            {
               
                //ignore
            }

            else
            {
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND supermarketname like'" + _supermarketname + "%'";

            }


            System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();


            // return Redirect("~/"+ AspPageToLoad + "");
            return View(PageToLoad);

        }


        //-----------------------------------

        //-----------------------------------
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult audittraildetails(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            //string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            //string _itemno = collection["itemnumber"];
            string _action = collection[3];
            string _SQLSelectionCriteria = "";
            
            string PageToLoad = "";
            
            string ReportName = "";

            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            _startdate = ftodate;

            string ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
            _enddate = ftenddate;










            PageToLoad = "audittrail";
            _SQLSelectionCriteria = null;
            if (collection["bp.startdate"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = "where  dtDate>='" + _startdate + "'";
            }

            if (collection["bp.enddate"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND dtDate<='" + _enddate + "'";
            }

           
            else
            {


            }

            System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            return View(PageToLoad);
        }

        //-----------------------------------
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult stockitemsdetails(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _itemno = collection["itemnumber"];
            string _action = collection[4];
            string _SQLSelectionCriteria = "";
           
            string _allvendor = collection["allvendor"];
            
            string _vendor = collection["vendor"];
            string PageToLoad = "";
            //string[] _strempoyerID = collection["selectedreport"].Split('-');
            // string ReportID = _strempoyerID[0].ToString().Trim();
            string ReportName = "";
            string newvendor = "";

           // string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            //_startdate = ftodate;

           // string ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
           // _enddate = ftenddate;



            _SQLSelectionCriteria = null;



            _SQLSelectionCriteria = "where vendor<>'' ";


            PageToLoad = "stockdetails";

       
            if (collection["itemnumber"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND Itemno='" + _itemno + "'";
            }

            newvendor = _vendor.Replace(".", "");

            if (newvendor.ToString()!="")
            {

               
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND vendor like'" + newvendor + "%'";

            }

            else
            {


            }

            System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();


            // return Redirect("~/"+ AspPageToLoad + "");
            return View(PageToLoad);

        }

        

            [AcceptVerbs(HttpVerbs.Post)]
      public ActionResult statementsDetails(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";
            string ReportName = "";
            string results = "";
            string sql = "exec spstatements '" + _refno.Trim() + "'";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQuery(sql);
            PageToLoad = "statements";


            // return Redirect("~/"+ AspPageToLoad + "");
            return View(PageToLoad);

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoaderLedgers(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";

            string _startdate = collection["bp.startdate"];
            string TB_startdate = collection["bp.startdate"];
            // string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string[] _selectedusers = collection["selectaccount"].Split('-');
            string _accouncode = _selectedusers[0].ToString().Trim();
            string code = _selectedusers[0].ToString().Trim();
            TempData["Accountcode"] = _selectedusers[0].ToString().Trim();
            TempData["Accounttype"] = _selectedusers[1].ToString().Trim();
            TempData["From"] = collection["bp.startdate"];
            TempData["To"] = collection["bp.enddate"];
            TempData["Rptname"] = "Ledgers";

            string ReportName = "";

            string results = "";
            string sql = "exec spLedgers '" + _accouncode.Trim() + "'";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQueryLedgers(sql);
            PageToLoad = "Ledgers";

            if (code == "B035")
            {
                HttpContext.Session["code"] = "where cast(Dates as date)>='" + _startdate + "' and cast(Dates as date)<='" + _enddate + "' order by id desc";
            }
            else
            {
                HttpContext.Session["code"] = "where cast(Dates as date)>='" + _startdate + "' and cast(Dates as date)<='" + _enddate + "' order by id desc";

            }


            //_SQLSelectionCriteria = "where accountcode<>'' ";

            //if (collection["bp.startdate"].ToString().Length > 0)
            //{
            //    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND Dates>='" + _startdate + "'";
            //}

            //if (collection["bp.enddate"].ToString().Length > 0)
            //{
            //    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND Dates<='" + _enddate + "'";
            //}



            //System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();




            // return Redirect("~/"+ AspPageToLoad + "");
            return View("Ldgrs");
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReportLoaderLedgersOthers()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoaderLedgersOthers(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";

            string _startdate = collection["bp.startdate"];
            string TB_startdate = collection["bp.startdate"];
            // string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string[] _selectedusers = collection["selectaccount"].Split('-');
            string _accouncode = _selectedusers[0].ToString().Trim();
            string code = _selectedusers[0].ToString().Trim();
            TempData["Accountcode"] = _selectedusers[0].ToString().Trim();
            TempData["Accounttype"] = _selectedusers[1].ToString().Trim();
            TempData["From"] = collection["bp.startdate"];
            TempData["To"] = collection["bp.enddate"];
            TempData["Rptname"] = "Ledgers";

            string ReportName = "";

            string results = "";
            string sql = "exec spLedgers'" + _accouncode.Trim() + "'";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQueryLedgers(sql);
            PageToLoad = "Ledgers";

            if (code == "B035")
            {
                HttpContext.Session["code"] = "where cast(Dates as date)>='" + _startdate + "' and cast(Dates as date)<='" + _enddate + "' order by Dates desc";
            }
            else
            {
                HttpContext.Session["code"] = "where cast(Dates as date)>='" + _startdate + "' and cast(Dates as date)<='" + _enddate + "' order by Dates desc";

            }


            _SQLSelectionCriteria = "where accountcode<>'' ";

            if (collection["bp.startdate"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND Dates>='" + _startdate + "'";
            }

            if (collection["bp.enddate"].ToString().Length > 0)
            {
                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND Dates<='" + _enddate + "'";
            }



            System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();




            // return Redirect("~/"+ AspPageToLoad + "");
            return View("Ldgrs");

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoaderTrialbalance(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";
            string _startdate = collection["bp.startdate"];
            string TB_startdate = collection["bp.startdate"];
            string TB_Enddate = collection["bp.enddate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            TempData["startdate"] = collection["bp.startdate"];
            TempData["enddate"] = collection["bp.enddate"];
            TempData["Usrcode"] = "";//_selectedusers[0].ToString().Trim();
            TempData["username"] = HttpContext.Session["username"].ToString();//_selectedusers[1].ToString().Trim();
            TempData["Rptname"] = "Trial Balance";
            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            TB_startdate = ftodate;
            string ReportName = "";

            string results = "";
            string sql = "exec spTB2 '" + TB_startdate + "','" + TB_Enddate + "'";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQuery(sql);
            PageToLoad = "Trialbalance";
            return View("Trailbalance");

        }



        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult ReportLoaderReconciliation(FormCollection collection)
        //{
        //    string _viewtoDisplay = "";
        //    List<string> errors = new List<string>();
        //    ViewData["ModelErrors"] = errors;
        //    string _refno = collection["refno"];
        //    string _action = collection[2];
        //    string _SQLSelectionCriteria = "";
        //    string PageToLoad = "";
        //    string _startdate = collection["bp.startdate"];
        //    string TB_startdate = collection["bp.startdate"];
        //    string _accouncode = collection["selectaccount"];
        //    string _enddate = collection["bp.enddate"];
        //    string _mpesano = collection["mpesano"];
        //    string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
        //    TB_startdate = ftodate;
        //    string ReportName = "";

        //    string results = "";
        //     string sql = "select  a.borrowerphone,b.transactioncode,b.paymentdate,B.transamount from loanapplications a,paymenthistory b WHERE a.loanref = b.loanref order by b.paymentdate asc ";
        //    BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
        //    blogic.RunNonQuery(sql);
        //    PageToLoad = "Reconciliation";
        //    _SQLSelectionCriteria = null;
        //    if (collection["bp.startdate"].ToString().Length > 0)
        //    {
        //        _SQLSelectionCriteria = "  (paymentdate>='" + _startdate + "'";
        //    }

        //    if (collection["bp.enddate"].ToString().Length > 0)
        //    {
        //        _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND paymentdate<='" + _enddate + "')";
        //    }

        //    System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();


        //    // return Redirect("~/"+ AspPageToLoad + "");
        //    return View(PageToLoad);

        //}

       


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoaderAging(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";
            string _startdate = collection["bp.startdate"];
            string TB_startdate = collection["bp.startdate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string ReportName = "";
            

            string ffromdate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");

            string ftodate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
            string sql = "exec AgeSummarySalvon '" + ffromdate + "' ,  '" + ftodate + "'";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQuery(sql);
            PageToLoad = "Ageingreport";
            // return Redirect("~/"+ AspPageToLoad + "");
            return View(PageToLoad);

        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReportLoadernewaging()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReportLoadernewaging(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _refno = collection["refno"];
            string _action = collection[2];
            string _SQLSelectionCriteria = "";
            string PageToLoad = "";
            string _startdate = collection["bp.startdate"];
            string TB_startdate = collection["bp.startdate"];
            string _accouncode = collection["selectaccount"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string ReportName = "";


            string ffromdate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");

            string ftodate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
            string sql = "exec agesammary '" + ffromdate + "' ,  '" + ftodate + "'";
            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
            blogic.RunNonQuery(sql);
            // PageToLoad = "Ageingreport";
            // return Redirect("~/"+ AspPageToLoad + "");
            return View("Aging");

        }

    }
}