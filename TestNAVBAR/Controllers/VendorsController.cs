﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class VendorsController : Controller
    {
        // GET: /Station/
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();

        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisterVendors()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult Registervendors(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["vendorname"] = collection["vendorname"];
            ViewData["paybill"] = collection["paybill"];
            ViewData["contact"] = collection["contact"];
           
            if (collection["vendorname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the vendorname.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select * from Vendors WHERE vendorname = '" + collection["vendorname"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the vendorname are unique for each Station.This user record already exists for another Station.");
            }


            if (collection["paybill"].ToString().Length == 0)
            {
                errors.Add("You must ADD the paybill.Its Mandatory");
            }

            if (collection["contact"].ToString().Length == 0)
            {
                errors.Add("You must ADD the contact.Its Mandatory for notificiations.");
            }

           
            //Stationcode
            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Vendors WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "RegisterVendor";
                ViewData["vendorname"] = collection["vendorname"];
                ViewData["paybill"] = collection["paybill"];
                ViewData["contact"] = collection["contact"];
               
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    data["vendorname"] = collection["vendorname"];
                    data["paybill"] = collection["paybill"];
                    data["contact"] = collection["contact"];
                   
                    //-----------------------------------------------------------
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    sql = Logic.DMLogic.InsertString("Vendors", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The record has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }


    }
}