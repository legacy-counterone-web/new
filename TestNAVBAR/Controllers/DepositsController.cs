﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class DepositsController : Controller
    {
        //
        // GET: /Deposits/
        int logintime = 1;
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        int balances = 0;
        int currentHour = 0;
        int Year = 0;
        int minute= 0;
        int seconds = 0;
        int Month = 0;
        int Day = 0;
        Double Tamount = 0;
        string mref = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetDepositDetails()
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            return Json(MemberRepo.GetAllDeposits(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult AddDeposits()
        {

            return View();
        }
        public ActionResult Deposit()
        {

            return View();
        }
        public ActionResult ViewDeposit()
        {

            return View();
        }
        public JsonResult GetDeposit()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllDeposits(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Withdrawal()
        {

            return View();
        }
        //Add Employee details with json data    
        [HttpPost]
        public JsonResult AddDeposits(SaccoDeposits MbrDet)

        {
            try
            {

                TransactionRepo MemberRepo = new TransactionRepo();
                MemberRepo.AddDeposits(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        //Get record by Empid for edit    
        public ActionResult Edit(int? id)
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            return View(MemberRepo.GetAllDeposits().Find(mbr => mbr.Transid == id));
        }
        //Updated edited records    
        [HttpPost]
        public JsonResult Edit(SaccoDeposits MemberUpdateDet)
        {

            try
            {

                TransactionRepo MemberRepo = new TransactionRepo();
                MemberRepo.UpdateDeposits(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }


        //Delete the records by id    
        [HttpPost]
        public JsonResult Delete(int id)
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            MemberRepo.DeleteDeposits(id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }




        //Get employee list of Partial view     
        [HttpGet]
        public PartialViewResult MemberDetails()
        {

            return PartialView("_MemberDetails");

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Deposit(FormCollection collection)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["Accountno"] = collection["Accountno"];
            ViewData["idno"] = collection["idno"];
            ViewData["Mobileno"] = collection["Mobileno"];
            ViewData["Firstname"] = collection["Firstname"];
            ViewData["Lastname"] = collection["Lastname"];
            ViewData["accounttype"] = collection["accounttype"];
            ViewData["Depositamount"] = collection["Depositamount"];
            string phonecode = Blogic.RunStringReturnStringValue("select * from tblsavingaccount WHERE Phone = '" + collection["Mobileno"].ToString().Trim() + "'");

            if (phonecode != "" && phonecode != "0")
            {
                errors.Add("You must ADD the correct mobilenumber.Its Mandatory");
            }
            if (collection["Accountno"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Accountno.Its Mandatory");
            }

            if (collection["Firstname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Firstname.Its Mandatory");
            }
            if (collection["Lastname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Lastname.Its Mandatory");
            }
            if (collection["idno"].ToString().Length == 0)
            {
                errors.Add("You must ADD the idno.Its Mandatory for notificiations.");
            }

           
            if (collection["accounttype"].ToString().Length == 0)
            {
                errors.Add("You must ADD the accounttype.Its Mandatory for notifications");
            }

            if (collection["Depositamount"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Depositamount.Its Mandatory ");
            }








          
            if (errors.Count > 0)
            {
                _successfailview = "Deposit";

                ViewData["Accountno"] = collection["Accountno"];
                ViewData["idno"] = collection["idno"];
                ViewData["Mobileno"] = collection["Mobileno"];
                ViewData["Firstname"] = collection["Firstname"];
                ViewData["Lastname"] = collection["Lastname"];
                ViewData["accounttype"] = collection["accounttype"];
                ViewData["Depositamount"] = collection["Depositamount"];
            }
            else
            {
                try
                {
                    Random generator = new Random();
                    String RandomPassword = generator.Next(0, 10).ToString("D1");
                    string encPassword = userdata.EncyptString(RandomPassword);
                    DateTime getdate = DateTime.Now;
                    Year = DateTime.Now.Year;
                    Month = DateTime.Now.Month;
                    Day = DateTime.Now.Day;
                    currentHour = DateTime.Now.Hour;
                    minute = DateTime.Now.Minute;
                    seconds = DateTime.Now.Second;
                    mref = "Tish/" + Year + Month + Day + currentHour + minute + seconds;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    //string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    
                    string[] _accounttype = collection["accounttype"].Split('-');
                    string[] _Duration = collection["Duration"].Split('-');
                    data["accounttype"] = _accounttype[1].ToString().Trim();
                    //data["Duration"] = _accounttype[1].ToString().Trim();
                    data["Accountno"] = collection["Accountno"];
                    data["Firstname"] = collection["Firstname"];
                    data["Lastname"] = collection["Lastname"];
                    data["Mobileno"] = collection["Mobileno"];
                    data["Withdrawamount"] = "0";
                    data["idno"] = collection["idno"];
                    data["Typecode"] = _accounttype[0].ToString().Trim();
                    data["Depositamount"] = collection["Depositamount"];
                    data["refid"] = mref;
                    string recipients = (collection["Mobileno"]);
                    string deposit = (collection["Depositamount"]);
                    data["CreateBy"] = (string)HttpContext.Session["username"];
                    data["CreateDate"] = dt.ToString();
                    double totalamount = Double.Parse(deposit);
                    sql = Logic.DMLogic.InsertString("tblcurrentaccount", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Transaction successful.");
                        _successfailview = "ChurchRegistrationSuccess";


                        string messagetosend = "Dear " + collection["Firstname"] +" "+ collection["Lastname"] + ",you have deposited Ksh:"+ collection["Depositamount"]+ ".Thank You" ;

                        SendSms(messagetosend, recipients);
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Deposited: " + collection["Accountno"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Withdrawal(FormCollection collection)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["Accountno"] = collection["Accountno"];
            ViewData["idno"] = collection["idno"];
            ViewData["Mobileno"] = collection["Mobileno"];
            ViewData["Firstname"] = collection["Firstname"];
            ViewData["Lastname"] = collection["Lastname"];
            ViewData["accounttype"] = collection["accounttype"];
            ViewData["Depositamount"] = collection["Depositamount"];

            string phonecode = Blogic.RunStringReturnStringValue("select * from loanusers WHERE phonenumber = '" + collection["Mobileno"].ToString().Trim() + "'");

            if (phonecode != "" && phonecode != "0")
            {
                errors.Add("You must ADD the correct mobilenumber.Its Mandatory");
            }
            string idno = Blogic.RunStringReturnStringValue("select * from loanusers WHERE idno = '" + collection["idno"].ToString().Trim() + "'");

            if (idno != "" && idno != "0")
            {
                errors.Add("You must ADD the correct idno.Its Mandatory");
            }
            if (collection["Accountno"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Accountno.Its Mandatory");
            }

            if (collection["Firstname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Firstname.Its Mandatory");
            }
            if (collection["Lastname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Lastname.Its Mandatory");
            }
            if (collection["idno"].ToString().Length == 0)
            {
                errors.Add("You must ADD the idno.Its Mandatory for notificiations.");
            }


            if (collection["accounttype"].ToString().Length == 0)
            {
                errors.Add("You must ADD the accounttype.Its Mandatory for notifications");
            }

            if (collection["Depositamount"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Depositamount.Its Mandatory ");
            }









            if (errors.Count > 0)
            {
                _successfailview = "Deposit";

                ViewData["Accountno"] = collection["Accountno"];
                ViewData["idno"] = collection["idno"];
                ViewData["Mobileno"] = collection["Mobileno"];
                ViewData["Firstname"] = collection["Firstname"];
                ViewData["Lastname"] = collection["Lastname"];
                ViewData["accounttype"] = collection["accounttype"];
                ViewData["Depositamount"] = collection["Depositamount"];
            }
            else
            {
                try
                {
                    Random generator = new Random();
                    String RandomPassword = generator.Next(0, 10).ToString("D1");
                    string encPassword = userdata.EncyptString(RandomPassword);
                    DateTime getdate = DateTime.Now;
                    Year = DateTime.Now.Year;
                    Month = DateTime.Now.Month;
                    Day = DateTime.Now.Day;
                    currentHour = DateTime.Now.Hour;
                    minute = DateTime.Now.Minute;
                    seconds = DateTime.Now.Second;
                    mref = "KRAY/" + Year + Month + Day + currentHour + minute + seconds;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    //string SelectedChurch = (string)HttpContext.Session["selectedChurch"];

                    string[] _accounttype = collection["accounttype"].Split('-');
                    data["accounttype"] = _accounttype[1].ToString().Trim();

                    data["Accountno"] = collection["Accountno"];
                    data["Firstname"] = collection["Firstname"];
                    data["Lastname"] = collection["Lastname"];
                    data["Mobileno"] = collection["Mobileno"];
                    data["idno"] = collection["idno"];
                  
                    data["Typecode"] = _accounttype[0].ToString().Trim();
                    data["Withdrawamount"] = collection["Depositamount"];
                    data["Depositamount"] = "0";
                    data["refid"] = mref;
                    string recipients = (collection["Mobileno"]);
                    string deposit = (collection["Depositamount"]);
                    data["CreateBy"] = (string)HttpContext.Session["username"];
                    data["CreateDate"] = dt.ToString();
                    double totalamount = Double.Parse(deposit);
                    sql = Logic.DMLogic.InsertString("tblcurrentaccount", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The user has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        string messagetosend = "Dear " + collection["Firstname"] + " " + collection["Lastname"] + ",you have Withdraw Ksh:" + collection["Depositamount"] + ".Thank You";

                        SendSms(messagetosend, recipients);
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Deposited: " + collection["Accountno"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Registersavingsypes()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Registersavingsypes(FormCollection collection)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            // ViewData["username"] = collection["username"];
            // ViewData["loancode"] = collection["loancode"];
            ViewData["Savingtype"] = collection["Savingtype"];
            ViewData["Duration"] = collection["Duration"];
            ViewData["adminfee"] = collection["adminfee"];
            ViewData["interest"] = collection["interest"];
            ViewData["maturity"] = collection["maturity"];
            ViewData["Savingcode"] = collection["Savingcode"];





            if (collection["Savingtype"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Savingtype.Its Mandatory");
            }
            if (collection["Duration"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Duration.Its Mandatory for notificiations.");
            }

            if (collection["adminfee"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate adminfee.Its Mandatory");
            }
            if (collection["interest"].ToString().Length == 0)
            {
                errors.Add("You must ADD the interest.Its Mandatory for notifications");
            }

            //if (collection["maturity"].ToString().Length == 0)
            //{
            //    errors.Add("You must ADD the alternate maturity.Its Mandatory or same as Mobile number one.");
            //}




            if (errors.Count > 0)
            {
                _successfailview = "Registersavingsypes";
                // ViewData["loancode"] = collection["loancode"];
                ViewData["Savingtype"] = collection["Savingtype"];
                ViewData["Duration"] = collection["Duration"];
                ViewData["adminfee"] = collection["adminfee"];
                ViewData["interest"] = collection["interest"];
                ViewData["maturity"] = collection["maturity"];
                ViewData["Savingcode"] = collection["Savingcode"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    string[] _loantype = collection["Savingtype"].Split('-');
                    string[] _Duration = collection["Duration"].Split('-');
                    string[] _Repayperiod = collection["Duration"].Split('-');
                    data["Savingtype"] = _loantype[1].ToString().Trim();
                    data["Duration"] = _Duration[0].ToString().Trim();
                  //  data["loancode"] = _loantype[0].ToString().Trim();
                     data["maturity"] = _Repayperiod[1].ToString().Trim();
                    data["adminfee"] = collection["adminfee"];
                    data["interest"] = collection["interest"];
                    data["Savingcode"] = _loantype[0].ToString().Trim();

                    data["username"] = (string)HttpContext.Session["username"];
                    // data["CreateDate"] = dt.ToString();

                    sql = Logic.DMLogic.InsertString("tblsavings", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The Saving has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult modifysavingtypes(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "modifysavingtypes";
                string uniqueID = id.ToString();
                sql = " select * from tblsavings where id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                           
                            ViewData["id"] = rd["id"];
                            ViewData["Savingcode"] = rd["Savingcode"]; 
                            ViewData["Savingtype"] = rd["Savingtype"];
                            ViewData["Duration"] = rd["Duration"];
                            ViewData["adminfee"] = rd["adminfee"];
                            ViewData["interest"] = rd["interest"];
                            ViewData["maturity"] = rd["maturity"];


                        }
                    }
                }
            }
            else
            {

                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult modifysavingtypes(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";
            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();

            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            string _viewToDisplayData = "Error";
            BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                ViewData["Savingtype"] = collection["Savingtype"];
                ViewData["Duration"] = collection["Duration"];
                ViewData["adminfee"] = collection["adminfee"];
                ViewData["interest"] = collection["interest"];
                ViewData["maturity"] = collection["maturity"];
                ViewData["Savingcode"] = collection["Savingcode"];
                sql = "UPDATE tblsavings  SET Duration='" + collection["Duration"] + "',Savingtype='" + collection["Savingtype"] + "',maturity='" + collection["maturity"] + "',interest='" + collection["interest"] + "',Savingcode='" + collection["Savingcode"] + "' WHERE id= '" + id + "'";
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    _viewToDisplayData = "ViewsavingtypesFee";
                }
            }
            catch
            {
                _viewToDisplayData = "ErrorPage";
            }

            try
            {
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN OFFICE", "Registered Admin and intr rate for loan id =: " + collection["loantypeid"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }

            catch
            {

            }

            return View(_viewToDisplayData);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ViewsavingtypesFee()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewsavingtypesFee(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";
            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();

            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            string _viewToDisplayData = "Error";
            BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

            try
            {

            }
            catch
            {
                _viewToDisplayData = "ErrorPage";
            }

            try
            {
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN OFFICE", "Registered Admin and intr rate for loan id =: " + collection["loantypeid"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }

            catch
            {

            }

            return View(_viewToDisplayData);
        }
        [NonAction]
        private string SendSms(string message, string recipient)
        {
            string username = "counterone";
            string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
            string ffrom = "counterone";
            string status = "";

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {
                // Thats it, hit send and we'll take care of the rest
                dynamic results = gateway.sendMessage(recipient, message, ffrom);
                foreach (dynamic result in results)
                {
                    //Console.Write((string)result["number"] + ",");
                    //Console.Write((string)result["status"] + ","); // status is either "Success" or "error message"
                    //Console.Write((string)result["messageId"] + ",");
                    //Console.WriteLine((string)result["cost"]);


                    //if (results.IndexOf("Success") == -1)
                    //{
                    //    _successfailview = "SalaryReviewError";
                    //}
                    status = (string)result["status"];

                }
            }
            catch (AfricasTalkingGatewayException e)
            {

                Console.WriteLine("Encountered an error: " + e.Message);

            }
            return status;
        }

    }
}


