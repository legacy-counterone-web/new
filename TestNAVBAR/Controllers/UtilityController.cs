﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class UtilityController : Controller
    {
        string _successfailview = "";
        string sql = "";
        string selectedchurch = "";
        string selectedchurchbranch = "";
        string selectedbankbranch = "";
        System.Data.SqlClient.SqlDataReader rd;
        Dictionary<string, string> where = new Dictionary<string, string>();
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult RegisterUtility()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult RegisterUtilityApproval()
        {
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult RegisterUtilityEdit()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult RegisteredUtilityEdit(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "UtilityEditPage";
                string uniqueID = id.ToString();
                sql = " select * from Stations WHERE stationid= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["StationName"] = rd["StationName"];
                            ViewData["stationcode"] = rd["stationcode"];
                            ViewData["churchurl"] = rd["url"];
                            ViewData["stationmanager"] = rd["stationmanager"];
                            ViewData["managersemail"] = rd["managersemail"];
                            ViewData["managersmobile"] = rd["managersmobile"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult RegisteredUtilityApprove(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "UtilityApprovalPage";
                string uniqueID = id.ToString();
                sql = " select * from Stations WHERE stationid= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["StationName"] = rd["StationName"];
                            ViewData["stationcode"] = rd["stationcode"];
                            ViewData["churchurl"] = rd["url"];
                            ViewData["stationmanager"] = rd["stationmanager"];
                            ViewData["managersemail"] = rd["managersemail"];
                            ViewData["managersmobile"] = rd["managersmobile"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


    }
}
