﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using TestNAVBAR.Filters;
using TestNAVBAR.Utilities;
using System.Web;

using TestNAVBAR.Models;
//using TestNAVBAR.U 
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using System.Text;
using System.IO;
using Newtonsoft.Json;
//using Microsoft.Reporting;
//using Microsoft.Repor;ting.WinForms;

namespace TestNAVBAR.Controllers
{

    public class MyGlobalVariables
    {
        public static Dictionary<string, string> userrights = new Dictionary<string, string>();
        public static Dictionary<string, string> userrightsx = new Dictionary<string, string>();
        private Utilities.Users ulogicx = new Utilities.Users();

        public static string username;
    }






    [Authorize]
    [InitializeSimpleMembership]

    public class AccountController : Controller
    {

        public string post_urls = "https://counterone.net:8181/counterone/counteronetest/MailOTP.php";
        public string post_url = "http://107.20.199.106/sms/1/text/single";
        string _viewToDisplay = "";
        string SMS_Message = "";
        string Email_Message = "";
        string viewstr = "";
        string OTP_Key = "";
        string _GivenOTP = "";
        string OTPController = "";
        string _successfailview = "";
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        private Utilities.Users ulogic = new Utilities.Users();




        //
        // GET: /Account/Login

        [AllowAnonymous]

        public ActionResult Login(string returnUrl)
        {

            Session.Clear();
            Session.Abandon();
            Response.Cookies.Clear();
            // WebSecurity.Logout();
            // return RedirectToAction("Login", "Account");
            if (OTPController == "true")
            {
                return RedirectToAction("OTP", "Home");


            }
            else
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }

        }



        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        //[NoDirectAccessAttribute]       
        //[NoDirectAccessAttribute]
        public ActionResult Login(LoginModel model, string returnUrl, FormCollection collection)
        {


            string Username = model.UserName;
            string password = model.Password;
            string EncyptedPassword = "";
            string StrEncrypted = "";
            string strUsername = "";
            string Approved = "";
            string Email1 = "";
            string Email2 = "";
            String Mobile1 = "";
            String Mobile2 = "";
            int db_id = 0;
            string strPassword = "";
            string dbid = "";
            string userlevels = "";
            string _approved = "";
            string userfullnames = "";
            string fname = "";
            string lname = "";
            string firstimelogin = "";
            int currentHour = 0;
            int NextHour = 0;
            string Days = "";
            int dy = 0;
            string Time1 = "";
            string Time2 = "";
            int t1 = 0;
            int t2 = 0;
            int dys = 0;
            //--------------------------------

            //--------------------------------
            List<string> errors = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();

            MyGlobalVariables.userrights = new Dictionary<string, string>();

            MyGlobalVariables.username = "";

            //retain values while posting errors

            //check if its change of password

            string collectioncount = "";

            collectioncount = collection.Count.ToString();





            if (string.Compare("6", collectioncount.ToString(), false) == 0)

            {
                //start
                string sql = "";
                bool flag = false;

                List<string> response = new List<string>();
                ViewData["username"] = collection["username"];
                ViewData["currentpassword"] = collection["currentpassword"];
                ViewData["newpassword"] = collection["newpassword"];
                ViewData["confirmnewpassword"] = collection["confirmnewpassword"];

                string _successfailview = "changepassword";

                if (collection["username"].ToString().Length == 0)
                {
                    errors.Add("You must ADD the username.Its Mandatory");
                }

                string codeExists = Blogic.RunStringReturnStringValue("select username from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");

                if (codeExists == "" && codeExists == "0")
                {
                    errors.Add("Please MAKE SURE the username name exist.");
                }

                string passwordc = Blogic.RunStringReturnStringValue("select Password from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");
                string FullNames = Blogic.RunStringReturnStringValue("select FullNames from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");
                string mobileno = Blogic.RunStringReturnStringValue("select Mobile1 from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");
                string mobileno2 = Blogic.RunStringReturnStringValue("select Mobile2 from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");
                string EncyptedPasswordc = userdata.EncyptString(collection["currentpassword"].ToString().Trim());
                string StrEncryptedc = EncyptedPasswordc.ToString();
                if (string.Compare(passwordc, StrEncryptedc.ToString(), false) == 1)
                {

                    errors.Add("The current password you entered is not valid.Please enter the correct password you were sent");

                }

                if (collection["newpassword"].ToString().Length == 0)
                {
                    errors.Add("You must ADD the new password.");
                }

                if (collection["confirmnewpassword"].ToString().Length == 0)
                {
                    errors.Add("You must confirm the new password.");
                }


                if (string.Compare(collection["newpassword"].ToString().Trim(), collection["confirmnewpassword"].ToString().Trim(), false) == 1)
                {

                    errors.Add("The new password does not match with the confirm new password.Please try again");

                }


                if (errors.Count > 0)
                {
                    _successfailview = "changepassword";
                    ViewData["username"] = collection["username"];
                    ViewData["currentpassword"] = collection["currentpassword"];
                    ViewData["newpassword"] = collection["newpassword"];
                    ViewData["confirmnewpassword"] = collection["confirmnewpassword"];
                }
                else
                {
                    try
                    {
                        DateTime getdate = DateTime.Now;
                        string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                        Dictionary<string, string> data1 = new Dictionary<string, string>();
                        string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                        Dictionary<string, string> where = new Dictionary<string, string>();
                        string rawpassword = collection["newpassword"].ToString().Trim();
                        string encPasswordc = userdata.EncyptString(collection["newpassword"].ToString().Trim());

                        data1["firstimelogin"] = "0";
                        string recipients = (mobileno + "," + mobileno2);
                        data1["password"] = encPasswordc.ToString().Trim();
                        //-----------------------------------------------------------
                        data1["CreateBy"] = "ADMIN";
                        data1["CreateDate"] = dt.ToString();
                        where["username"] = collection["username"];
                        sql = Logic.DMLogic.UpdateString("tbUsers", data1, where);
                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            HttpContext.Session["changepassword"] = "no";
                            response.Add("Password changed succesfully successfully.");
                            _successfailview = "Login";


                        }
                    }
                    catch
                    {
                        _successfailview = "changepassword";
                    }
                }
                ViewData["ModelErrors"] = errors;
                ViewData["Response"] = response;
                return View(_successfailview);


                //end start
            }




            if (!string.IsNullOrEmpty(HttpContext.Session["view"] as string))
            {
                viewstr = HttpContext.Session["view"].ToString();
            }

            if (viewstr != "OTP")
            {
                ViewData["UserName"] = collection["UserName"];
                ViewData["Password"] = collection["Password"];

                if (Username != null)
                {


                    if (collection["UserName"].ToString().Length == 0)
                    {
                        errors.Add("The User name is MANDATORY.");
                    }

                    if (collection["Password"].ToString().Length == 0)
                    {
                        errors.Add("The password is MANDATORY.");
                    }
                    string record_data = "SELECT * FROM tbUsers where   Username='" + Username + "'";
                    SqlDataReader reader = Blogic.RunQueryReturnDataReader(record_data);
                    using (reader)

                        if (reader != null && reader.HasRows)
                        {
                            _viewToDisplay = "OTP";
                            HttpContext.Session["view"] = _viewToDisplay.ToString();
                            while (reader.Read())
                            {
                                DateTime d1 = DateTime.Now;
                                currentHour = DateTime.Now.Hour;
                                NextHour = currentHour + 6;
                                DateTime datevalue = (Convert.ToDateTime(d1.ToString()));
                                dy = datevalue.Day;
                                string _strEmployerIDSelection = "";
                                dbid = reader["id"].ToString();
                                db_id = int.Parse(dbid);
                                Time1 = reader["StartTime"].ToString();
                                Time2 = reader["EndTime"].ToString();
                                Days = reader["Days"].ToString();
                                t1 = int.Parse(Time1);
                                t2 = int.Parse(Time2);
                                dys = int.Parse(Days);
                                _approved = reader["Approved"].ToString();

                                strUsername = reader["UserName"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["UserName"];
                                strPassword = reader["Password"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["Password"];
                                Email1 = reader["Email1"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["Email1"];
                                Email2 = reader["Email2"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["Email2"];
                                Mobile1 = reader["Mobile1"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["Mobile1"];
                                Mobile2 = reader["Mobile2"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["Mobile2"];
                                userlevels = reader["userlevel"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["userlevel"];
                                firstimelogin = reader["firstimelogin"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["firstimelogin"];
                                fname = reader["FullNames"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["FullNames"];
                                Session["fullnames"] = fname.ToString();
                                EncyptedPassword = userdata.EncyptString(password);
                                StrEncrypted = EncyptedPassword.ToString();
                                if (strUsername == "ADMIN")
                                {

                                    HttpContext.Session["Employerid"] = 1;
                                }
                                else
                                { HttpContext.Session["Employerid"] = reader["EmployerId"]; }
                                if (HttpContext.Session["Employerid"].ToString() == "0")
                                {
                                }
                                else
                                {
                                    _strEmployerIDSelection = " AND EmployerID='" + System.Web.HttpContext.Current.Session["Employerid"].ToString() + "'";

                                }

                                HttpContext.Session["selectedemployer"] = _strEmployerIDSelection.ToString();
                            }
                        }
                        else
                        {
                            _viewToDisplay = "Login";
                            errors.Add("The username is not valid.Please verify and login again.");
                            ViewData["ModelErrors"] = errors;
                            HttpContext.Session["view"] = null;
                            return View(_viewToDisplay);
                        }

                    if (string.Compare(strPassword, StrEncrypted.ToString(), false) == 0)
                    {



                        if (firstimelogin.ToString().Trim().Contains('1'))
                        {

                            HttpContext.Session["changepassword"] = "yes";

                            _viewToDisplay = "changepassword";

                            ViewData["ModelErrors"] = errors;
                            HttpContext.Session["view"] = null;
                            return View(_viewToDisplay);


                        }

                        if (_approved.ToString().Trim().Contains('0'))
                        {
                            HttpContext.Session["Approved"] = "0";
                            _viewToDisplay = "Login";
                            errors.Add("You have not been Approved as a Diamond Portal User .Kindly Contact your Administrator");
                            ViewData["ModelErrors"] = errors;
                            HttpContext.Session["view"] = null;
                            return View(_viewToDisplay);
                        }

                        //----------------------------------------------------------------------------------------------------------
                        OTPController = "true";
                        HttpContext.Session["otpcontroller"] = OTPController;

                        if (OTPController == "true")
                        {

                            _viewToDisplay = "OTP";


                            HttpContext.Session["otpcontroller"] = OTPController;
                            HttpContext.Session["password"] = strPassword;
                            HttpContext.Session["username"] = strUsername;
                            HttpContext.Session["mobile1"] = Mobile1;
                            HttpContext.Session["mobile2"] = Mobile2;
                            HttpContext.Session["Email1"] = Email1;
                            HttpContext.Session["Email2"] = Email2;
                            HttpContext.Session["id"] = db_id;

                            int diff = t2 - currentHour;


                            string sqls = "exec Counter_par";
                            BusinessLogic.DataConnections blogic = new BusinessLogic.DataConnections();
                            blogic.RunNonQueryLedgers(sqls);

                            string sqlY = " Dash_spincomestatement";
                            BusinessLogic.DataConnections blogicY = new BusinessLogic.DataConnections();
                            blogicY.RunNonQueryLedgers(sqlY);
                            if (currentHour < t2 && diff <= 6 && dys == dy)
                            {
                                if (errors.Count > 0)
                                {
                                    _successfailview = "ErrorPage";

                                }
                                else
                                {
                                    try
                                    {



                                    }
                                    catch
                                    {
                                        _successfailview = "ErrorPage";
                                    }

                                }

                            }
                            else
                            {
                                Random generator = new Random();
                                OTP_Key = generator.Next(0, 1000000).ToString("D6");
                                string messagetoadmin = "Your One Time Password for accessing the admin portal is  " + OTP_Key;
                                SendSms(messagetoadmin, Mobile1);
                                //string sURL = smskeys.posturls + "key=" + smskeys.API_KEY + "&message= " + messagetoadmin + "&contacts= " + Mobile1;
                                //string mesg = DataRequestToServer(sURL);                                
                                //update the OTP password
                                Send(Mobile1, messagetoadmin);
                                string sql = "UPDATE tbUsers SET OTP='" + OTP_Key + "',StartTime='" + currentHour + "',EndTime='" + NextHour + "',Days='" + dy + "' WHERE ID='" + db_id + "'";
                                Blogic.RunNonQuery(sql);


                                //send the EMail or SMS
                                Email_Message = "Your One Time Password is " + OTP_Key + ".Please use the OTP Key to access the portal";
                                SMS_Message = "Your One Time Password is " + OTP_Key + ".Please use the OTP Key to access the Internet portal";
                                //-----------------------
                                if (Email1.Length > 0)
                                {
                                    sql = "";
                                    errors.Add(Email1);
                                    sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + Email1 + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                                    //sql = "Insert into OutEmails(Email,Message,EncriptedMessage)VALUES('" + Email1 + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";

                                    Blogic.RunNonQuery(sql);


                                }

                                if (Email2.Length > 0)
                                {
                                    errors.Add(Email2);
                                    sql = "";
                                    //sql = "Insert into OutEmails(Email,Message,EncriptedMessage)VALUES('" + Email1 + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                                    sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + Email2 + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                                    Blogic.RunNonQuery(sql);
                                }


                                if (Mobile1.Length > 0)
                                {
                                    sql = "";
                                    sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + Mobile1 + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                                    Blogic.RunNonQuery(sql);
                                }

                                if (Mobile2.Length > 0)
                                {
                                    sql = "";
                                    sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + Mobile2 + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                                    Blogic.RunNonQuery(sql);
                                }
                            }

                            HttpContext.Session["view"] = "OTP";
                        }

                        //redirect the page to the OTP page                           
                    }
                    else
                    {
                        _viewToDisplay = "Login";
                        errors.Add("The password you entered is not valid.Please try to login in again.");
                        ViewData["ModelErrors"] = errors;
                        HttpContext.Session["view"] = null;
                        return View(_viewToDisplay);
                    }

                }
                else
                {
                    _successfailview = "ErrorPage";
                }
            }
            //If its OTS stuff
            else
            {
                string _userssession = HttpContext.Session["username"].ToString();
                if (_userssession != null)
                {
                    string _action = collection[2];

                    try
                    {
                        if (_action == "Proceed")
                        {
                            string record_data = "SELECT * FROM tbUsers where Username='" + _userssession + "' AND OTP='" + collection["otpassword"].ToString() + "'";
                            SqlDataReader reader = Blogic.RunQueryReturnDataReader(record_data);
                            using (reader)

                                if (reader != null && reader.HasRows)
                                {
                                    while (reader.Read())
                                    {
                                        _viewToDisplay = "OTP";
                                        HttpContext.Session["view"] = _viewToDisplay.ToString();
                                        dbid = reader["id"].ToString();
                                        db_id = int.Parse(dbid);
                                        strUsername = reader["UserName"].GetType() == System.DBNull.Value.GetType() ? "" : (string)reader["UserName"];
                                        HttpContext.Session["view"] = null;

                                        //audit trail

                                        try
                                        {

                                            MyGlobalVariables.username = strUsername;

                                            Boolean status = ulogic.SessionRights(strUsername, ref MyGlobalVariables.userrights);

                                            HttpContext.Session["userrights"] = MyGlobalVariables.userrights;

                                        }

                                        catch
                                        {

                                        }

                                        TestNAVBAR.Models.ListDatabaseElements bp = new TestNAVBAR.Models.ListDatabaseElements();
                                        string res = bp.getrights();


                                        HttpContext.Session["UserHostName"] = Environment.MachineName;
                                        HttpContext.Session["UserHostAddress"] = Dns.GetHostAddresses(Environment.MachineName)[0].ToString();
                                        HttpContext.Session["UserHostAddress"] = Request.UserHostAddress.ToString();
                                        string[] computer_name = Dns.GetHostEntry(Request.ServerVariables["remote_addr"]).HostName.Split(new Char[] { '.' });
                                        string ecname = Environment.MachineName;
                                        string txtComputerName = computer_name[0].ToString();
                                        HttpContext.Session["UserHostName"] = txtComputerName;
                                        try
                                        {

                                            Utilities.Users.ManageAuditTrail(strUsername.ToString(), (string)HttpContext.Session["mobile1"], "MainOffice", "User logged in: " + strUsername.ToString(), "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                        }

                                        catch
                                        {

                                        }
                                        return RedirectToAction("Index", "Dashhome");
                                    }
                                }
                        }
                        else if (_action == "Resend")
                        {
                            try
                            {
                                string gquery = "select OTP from tbUsers WHERE UserName= '" + _userssession + "'";
                                string OTP = Blogic.RunStringReturnStringValue(gquery);
                                //string sURL = smskeys.posturls + "key=" + smskeys.API_KEY + "&message= " + OTP + "&contacts= " + Mobile1;
                                //string mesg = DataRequestToServer(sURL);
                                // SendSms(OTP, Mobile1);
                                //Sending OTP via mail
                                Send(Mobile1, OTP);
                                string postdata = "UserName=" + _userssession;
                                string re = DataRequestToServers(postdata, post_urls);
                                //_successfailview = "ChurchRegistrationSuccess";
                                //response.Add("The user has been reset successfully." + OTP);
                                return RedirectToAction("Login", "Account");


                            }
                            catch
                            {
                                _successfailview = "ErrorPage";
                            }

                            ViewData["ModelErrors"] = errors;
                            return View(_successfailview);
                        }
                    }

                    catch
                    {
                        _successfailview = "ErrorPage";
                    }
                }




                else
                {
                    _viewToDisplay = "Login";
                    errors.Add("The OTP is not a valid one.Please check what you received on your Email or phone as a text message.");
                    ViewData["ModelErrors"] = errors;
                    HttpContext.Session["view"] = null;
                    return View(_viewToDisplay);
                }
            }
            return View(_viewToDisplay);
        }



        public string DataRequestToServers(string postData, string post_urls)
        {
            try
            {
                //System.Net.ServicePointManager.Expect100Continue = false;  
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(post_urls);
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] data = encoding.GetBytes(postData);
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                string responses = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return responses;

            }
            catch (Exception e)
            {
                return e.Message;
            }

        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult changepassword(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["username"] = collection["username"];
            ViewData["currentpassword"] = collection["currentpassword"];
            ViewData["newpassword"] = collection["newpassword"];
            ViewData["confirmnewpassword"] = collection["confirmnewpassword"];

            string _successfailview = "changepassword";

            if (collection["username"].ToString().Length == 0)
            {
                errors.Add("You must ADD the username.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select username from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");

            if (codeExists == "" && codeExists == "0")
            {
                errors.Add("Please MAKE SURE the username name exist.");
            }

            string password = Blogic.RunStringReturnStringValue("select Password from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");
            string FullNames = Blogic.RunStringReturnStringValue("select FullNames from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");
            string mobileno = Blogic.RunStringReturnStringValue("select Mobile1 from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");
            string mobileno2 = Blogic.RunStringReturnStringValue("select Mobile2 from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");
            string EncyptedPassword = userdata.EncyptString(password);
            string StrEncrypted = EncyptedPassword.ToString();


            if (string.Compare(collection["currentpassword"].ToString().Trim(), StrEncrypted.ToString(), false) == 1)
            {

                errors.Add("The current password you entered is not valid.Please enter the correct password you were given");

            }

            if (collection["newpassword"].ToString().Length == 0)
            {
                errors.Add("You must ADD the new password.");
            }

            if (collection["confirmnewpassword"].ToString().Length == 0)
            {
                errors.Add("You must confirm the new password.");
            }


            if (string.Compare(collection["newpassword"].ToString().Trim(), collection["confirmnewpassword"].ToString().Trim(), false) == 1)
            {

                errors.Add("The new password does not match with the confirm new password.Please try again");

            }

            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from tbUsers WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "changepassword";
                ViewData["username"] = collection["username"];
                ViewData["currentpassword"] = collection["currentpassword"];
                ViewData["newpassword"] = collection["newpassword"];
                ViewData["confirmnewpassword"] = collection["confirmnewpassword"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    Dictionary<string, string> where = new Dictionary<string, string>();
                    string encPassword = userdata.EncyptString(collection["newpassword"].ToString());
                    data["firstimelogin"] = "0";
                    string recipients = (mobileno + "," + mobileno2);
                    data["password"] = encPassword.ToString().Trim();
                    //-----------------------------------------------------------
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    where["username"] = collection["username"];
                    sql = Logic.DMLogic.UpdateString("tbUsers", data, where);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Password changed succesfully.");
                        _successfailview = "Login";


                        string messagetosend = "Dear " + FullNames + " your temporary password is  " + encPassword + ". Kindly use this to sign in and change your password.";

                        // SendSms(messagetosend, recipients);
                        Send(recipients, messagetosend);
                    }
                }
                catch
                {
                    _successfailview = "changepassword";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        public string GetUser_IP()
        {
            string VisitorsIPAddr = string.Empty;
            if (System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (System.Web.HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = System.Web.HttpContext.Current.Request.UserHostAddress;
            }
            return VisitorsIPAddr;
        }
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult LogOff()
        {

            Session.Abandon();
            Session.Clear();
            Response.Cookies.Clear();
            WebSecurity.Logout();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult OTP()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult OTP(FormCollection collection)
        {
            //WebSecurity.Logout();

            return RedirectToAction("Index", "Home");
            //return RedirectToAction("Index", "Tests");
        }


        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                return RedirectToLocal(returnUrl);
            }

            if (User.Identity.IsAuthenticated)
            {
                // If the current user is logged in add the new account
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // User is new, ask for their desired membership name
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Insert a new user into the database
                using (UsersContext db = new UsersContext())
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                    // Check if user already exists
                    if (user == null)
                    {
                        // insert name into the profile table
                        db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
                        db.SaveChanges();
                        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("UserName", "User name already exists. Please enter a different user name.");
                    }
                }
            }

            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }




        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }
        [HttpPost]
        public ActionResult Send(string phone, string message)
        {

            string clint = "4";
            var postData = new
            {
                phone = phone,
                message = message,
                client = clint

            };
            // var userData = new UserData { UserId = 0 };
            var userDataString = JsonConvert.SerializeObject(postData);
            var json = "Message sent succesfully";//JsonConvert.SerializeObject(accounts);
            string sURL1 = "https://counterone.net:8181/counterone/sms/send.php";
            //string postData = "phone=" + phone + "&message= " + message + "&client= " + clint;
            string mesg = DataRequestToServer1(userDataString, sURL1);
            //SendSms(userDataString, "254716328426");
            //SendSms(mesg, "254716328426");
            Console.Write("success");
            return Json(new { success = true, message = "Message sent succesfully " }, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public string Sends([System.Web.Http.FromBody]string phone, string message)
        {
            string clint = "4";
            var penelisation = new
            {
                phone = phone,
                message = message,
                client = clint

            };
            var json = "Message sent succesfully";//JsonConvert.SerializeObject(accounts);
            string sURL1 = "https://counterone.net:8181/counterone/sms/send.php";
            string postData = "phone=" + phone + "&message= " + message + "&client= " + clint;
            string mesg = DataRequestToServer1(postData, sURL1);
            //SendSms(postData, "254716328426");
            Console.Write("success");
            return json;
        }
        public string DataRequestToServer1(string postData, string post_url)
        {
            try
            {
                //System.Net.ServicePointManager.Expect100Continue = false;   
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(post_url);
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] data = encoding.GetBytes(postData);
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/json";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                string responses = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return responses;

            }
            catch (Exception e)
            {
                return e.Message;
            }

        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
        public string DataRequestToServer(string postData, string post_url)
        {
            try
            {


                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(post_url);
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] data = encoding.GetBytes(postData);
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.Accept = "application/json";
                httpWReq.Headers.Add("Authorization", "Basic VmljdG9yLk11dHVhOkRyZWFtMjMr");
                //   httpWReq.Headers.Add("Accept-Encoding", "gzip, deflate, br");
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                string responses = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return responses;

            }
            catch (Exception e)
            {
                return e.Message;
            }

        }
        [NonAction]
        private string SendSms(string message, string recipient)
        {
            string username = "counterone";
            string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
            string status = "";

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {

                // Thats it, hit send and we'll take care of the rest
                string from = "counterone";
                dynamic results = gateway.sendMessage(recipient, message, from);
                foreach (dynamic result in results)
                {
                    status = (string)result["status"];

                }
            }
            catch (AfricasTalkingGatewayException e)
            {

                Console.WriteLine("Encountered an error: " + e.Message);

            }
            return status;
        }
    }
}
