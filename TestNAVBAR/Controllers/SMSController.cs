﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
	  public class SMSController : Controller
	  {
			string T24Response = "";
			string selectedchurchbranch = "";
			string _successfailview = "";
			string selectedchurch = "";
			string sql = "";
			string Duser = "";
			Dictionary<string, string> where = new Dictionary<string, string>();
			System.Data.SqlClient.SqlDataReader rd;
			BusinessLogic.Users userdata = new BusinessLogic.Users();
			BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
			Logic.TCPLogic tcplogic = new Logic.TCPLogic();
			Logic.DMLogic bizzlogic = new Logic.DMLogic();
			//
			// GET: /SMS/

			public ActionResult Index()
			{
				  return View();
			}

			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult CallLogs()
			{

				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult Lu()
			{

				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult othersms()
			{

				  return View();
			}
		
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult CallLogs(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  string _action = collection[2];
				  string _action_w = collection[3];
				  string _SQLSelectionCriteria = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;

				  if (_action_w.ToString() == "Search") //phone
				  {

						string _mpesano = collection["mpesano"];


						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = "WHERE customerphone='" + _mpesano + "'";
						}

				  }
				  else if (_action.ToString() == "Load")//dates
				  {
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						//string _mpesano = collection["mpesano"];

						string ftodate = "";


						string ftenddate = "";

						if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  errors.Add("You must Indicate a valid start Date.");
						}

						if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
						{
							  errors.Add("You must Indicate a valid  End Date.");
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
							  _startdate = ftodate;
							  _SQLSelectionCriteria = " WHERE CallDate>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
							  _enddate = ftenddate;
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND CallDate<='" + _enddate + "'";
						}
				  }
				  if (_SQLSelectionCriteria.Length == 0)
				  {
						errors.Add("The selection Criteria cannot be empty!!");
				  }

				  if (errors.Count > 0)
				  {
						_viewtoDisplay = "CallLogs";
				  }
				  else
				  {
						_viewtoDisplay = "CallLogDates";

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

				  }


				  return View(_viewtoDisplay);
			}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult Lu(FormCollection collection)
			{
				  string _viewtoDisplay = "Lu";
				  string _action = collection[4];
				  if (_action == "Load")
				  {
						//-------------------------
						_viewtoDisplay = "LuDates";
						List<string> errors = new List<string>();
						ViewData["ModelErrors"] = errors;
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						string _mpesano = collection["customerphone"];

						string _SQLSelectionCriteria = "";

						if (collection["bp.startdate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE CallDate>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND CallDate<='" + _enddate + "'";
						}

						if (collection["customerphone"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerphone='" + _mpesano + "'";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------


				  }
				  return View(_viewtoDisplay);
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult SMSLogs()
			{
				  return View();
			}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult SMSLogs(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  string _action = collection[2];
				  string _action_w = collection[3];
				  string _SQLSelectionCriteria = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;

				  if (_action_w.ToString() == "Search") //phone
				  {

						string _mpesano = collection["mpesano"];


						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = "WHERE phonenumber='" + _mpesano + "'";
						}

				  }
				  else if (_action.ToString() == "Load")//dates
				  {
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						//string _mpesano = collection["mpesano"];

						string ftodate = "";


						string ftenddate = "";

						if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  errors.Add("You must Indicate a valid start Date.");
						}

						if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
						{
							  errors.Add("You must Indicate a valid  End Date.");
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
							  _startdate = ftodate;
							  _SQLSelectionCriteria = " WHERE datecreated>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
							  _enddate = ftenddate;
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datecreated<='" + _enddate + "'";
						}
				  }
				  if (_SQLSelectionCriteria.Length == 0)
				  {
						errors.Add("The selection Criteria cannot be empty!!");
				  }

				  if (errors.Count > 0)
				  {
						_viewtoDisplay = "SMSLogs";
				  }
				  else
				  {
						_viewtoDisplay = "MpesaDates";

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

				  }


				  return View(_viewtoDisplay);
			}
		
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult BList(FormCollection collection)
			{
				  string _viewtoDisplay = "BList";
				  string _action = collection[4];
				  if (_action == "Load")
				  {
						//-------------------------
						_viewtoDisplay = "BListDates";
						List<string> errors = new List<string>();
						ViewData["ModelErrors"] = errors;
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						string _mpesano = collection["mpesano"];

						string _SQLSelectionCriteria = "";

						if (collection["bp.startdate"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the start Date.");
						}

						if (collection["bp.enddate"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the End Date.");
						}
						if (collection["bp.startdate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE datelogged>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datelogged<='" + _enddate + "'";
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

				  }
				  return View(_viewtoDisplay);
			}

			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult PrintCallLogs(string id)
			{
				  return View();
			}

			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult PrintSMSLogs(string id)
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult LoanUsers()
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult LoanUserss()
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult LoanUsersList()
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult LoanUsersWList()
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult BListL()
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult BList()
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult WList()
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult LoanUsersBList()
			{
				  return View();
			}

			//''''''''''''''''



			public ActionResult SendMessagephone(string myParam)
			{
				  Session["phonenumber"] = myParam;
				  return View();



			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult ViewLoanUsersBList(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {
						_viewToDisplayData = "ViewLoanUsersBList";
						string uniqueID = id.ToString();
						sql = " select * from LoanUsers WHERE userid= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{

										  ViewData["firstname"] = rd["firstname"];
										  ViewData["middlename"] = rd["middlename"];
										  ViewData["lastname"] = rd["lastname"];
										  ViewData["emailaddress"] = rd["emailaddress"];
										  ViewData["phonenumber"] = rd["phonenumber"];
										  ViewData["dateofbirth"] = rd["dateofbirth"];
										  ViewData["facebook"] = rd["FacebookLink"];
										  ViewData["gmail"] = rd["GoogleLink"];
										  ViewData["gender"] = rd["gender"];
										  ViewData["photo"] = rd["customerphoto"];
										  ViewData["facebook_email"] = rd["facebook_email"];
										  ViewData["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
										  ViewData["deliquencystatuscode"] = rd["deliquencystatuscode"];
										  ViewData["crbcreditscore"] = rd["crbcreditscore"];
										  ViewData["iprscheckdescription"] = rd["iprscheckdescription"];
										  Session["userphonex"] = "";
										  Session["userphonex"] = rd["phonenumber"].ToString();

									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}

			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult ViewLoanUsersWList(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {
						_viewToDisplayData = "ViewLoanUsersWList";
						string uniqueID = id.ToString();
						sql = " select * from LoanUsers WHERE userid= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{

										  ViewData["firstname"] = rd["firstname"];
										  ViewData["middlename"] = rd["middlename"];
										  ViewData["lastname"] = rd["lastname"];
										  ViewData["emailaddress"] = rd["emailaddress"];
										  ViewData["phonenumber"] = rd["phonenumber"];
										  ViewData["dateofbirth"] = rd["dateofbirth"];
										  ViewData["facebook"] = rd["FacebookLink"];
										  ViewData["gmail"] = rd["GoogleLink"];
										  ViewData["gender"] = rd["gender"];
										  ViewData["photo"] = rd["customerphoto"];
										  ViewData["facebook_email"] = rd["facebook_email"];
										  ViewData["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
										  ViewData["deliquencystatuscode"] = rd["deliquencystatuscode"];
										  ViewData["crbcreditscore"] = rd["crbcreditscore"];
										  ViewData["iprscheckdescription"] = rd["iprscheckdescription"];
										  Session["userphonex"] = "";
										  Session["userphonex"] = rd["phonenumber"].ToString();

									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}

			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult SendMessagephone(FormCollection collection)
			{
				  string sql = "";
				  string sql1 = "";
				  bool flag = false;
				  string messagetosend = "";
				  string _action = collection[3];
				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  Dictionary<string, string> data = new Dictionary<string, string>();
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  ViewData["customerphone"] = collection["customerphone"];
				  ViewData["adminmessage"] = collection["adminmessage"];


				  if (_action == "SendToOne")
				  {
						string No = collection["customerphone"];
						if (collection["customerphone"].ToString().Length == 0)
						{
							  errors.Add("You must ADD the Phone Number.Its Mandatory");
						}
						sql1 = "select phonenumber from Loanusers where phonenumber='" + No + "'";
						string codeExists = Blogic.RunStringReturnStringValue(sql1);

						if (codeExists == "" && codeExists != "0")
						{
							  errors.Add("Please MAKE SURE the Phone Number is valid. This Phone Number does not exist.");
						}


						if (collection["adminmessage"].ToString().Length == 0)
						{
							  errors.Add("You must ADD the message.Its Mandatory");
						}

						if (errors.Count > 0)
						{
							  _successfailview = "SendMessagephone";
							  ViewData["customerphone"] = collection["customerphone"];
							  ViewData["adminmessage"] = collection["adminmessage"];
						}
						else
						{
							  try
							  {
									data["customerphone"] = collection["customerphone"];
									data["adminmessage"] = collection["adminmessage"];
									data["sendtime"] = dt.ToString();
									data["sent"] = "1";
									data["sender"] = "ADMIN";
									sql = Logic.DMLogic.InsertString("AdminMessages", data);

									flag = Blogic.RunNonQuery(sql);
									if (flag)
									{
										  messagetosend = collection["adminmessage"];
										  if (SendSms(messagetosend, collection["customerphone"]) == "Success")
										  {
												response.Add("The message has been send successfully.");
												_successfailview = "LoanUsers";
										  }

									}
							  }
							  catch
							  {
									_successfailview = "ErrorPage";
							  }
						}
				  }//
				  else if (_action == "SendToMany")
				  {
						if (collection["adminmessage"].ToString().Length == 0)
						{
							  errors.Add("You must ADD the message.Its Mandatory");
						}

						if (errors.Count > 0)
						{
							  _successfailview = "SendMessage";
							  ViewData["customerphone"] = collection["customerphone"];
							  ViewData["adminmessage"] = collection["adminmessage"];
						}
						else
						{
							  
							  string phoneno = collection["customerphone"];
							  messagetosend = collection["adminmessage"];
							  if (SendSms(messagetosend, phoneno) == "Success")
							  {
									data["customerphone"] = phoneno;
									data["adminmessage"] = collection["adminmessage"];
									data["sendtime"] = dt.ToString();
									data["sent"] = "2";
									data["sender"] = "ADMIN";
									sql = Logic.DMLogic.InsertString("AdminMessages", data);
							  }
							  response.Add("The message has been send successfully.");
							  _successfailview = "ChurchRegistrationSuccess";
						}
				  }


				  ////
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;
				  return View(_successfailview);
			}//
			private string SendSms(string message, string recipient)
			{
				  // Specify your login credentials
				  string username = "counterone";
				  string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
				  string ffrom = "counterone";
				  string status = "";

				  // Create a new instance of our awesome gateway class
				  AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
				  // Any gateway errors will be captured by our custom Exception class below,
				  // so wrap the call in a try-catch block   
				  try
				  {

						// Thats it, hit send and we'll take care of the rest
						dynamic results = gateway.sendMessage(recipient, message, ffrom);
						foreach (dynamic result in results)
						{
							  //Console.Write((string)result["number"] + ",");
							  //Console.Write((string)result["status"] + ","); // status is either "Success" or "error message"
							  //Console.Write((string)result["messageId"] + ",");
							  //Console.WriteLine((string)result["cost"]);


							  //if (results.IndexOf("Success") == -1)
							  //{
							  //    _successfailview = "SalaryReviewError";
							  //}
							  status = (string)result["status"];

						}
				  }
				  catch (AfricasTalkingGatewayException e)
				  {

						Console.WriteLine("Encountered an error: " + e.Message);

				  }


				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						string dt = DateTime.Now.ToString("dd/MM/yyyyhh:mm:ss", CultureInfo.InvariantCulture);

						data["customerphone"] = recipient;
						data["message"] = message;
						data["senttime"] = dt.ToString();
						data["enquirytime"] = dt.ToString();
						data["sent"] = status;
						data["sender"] = MyGlobalVariables.username;

						sql = Logic.DMLogic.InsertString("customerenquiries_ALL", data);
						Boolean flag = Blogic.RunNonQuery(sql);

				  }

				  catch
				  {



				  }


				  return status;
			}


			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult LoanUsers(FormCollection collection)
			{
				  List<string> errors = new List<string>();

				  string _action = collection[2];
				  string _viewtoDisplay = "";
				  string _mpesano = "";
				  string _SQLSelectionCriteria = "";
				  if (_action == "Load")
				  {
						//-------------------------

						_mpesano = collection["mpesano"];

						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");
							  //ViewData["ModelErrors"] = errors;
							  _viewtoDisplay = "LoanUsers";

						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE phonenumber='" + _mpesano + "'";

							  _viewtoDisplay = "LoanUsersDates";

						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------

				  }
				  ViewData["ModelErrors"] = errors;
				  return View(_viewtoDisplay);
			}

			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult LoanUsersList(FormCollection collection)
			{
				  List<string> errors = new List<string>();

				  string _action = collection[2];
				  string _viewtoDisplay = "";
				  string _mpesano = "";
				  string _SQLSelectionCriteria = "";
				  if (_action == "Load")
				  {
						//-------------------------

						_mpesano = collection["mpesano"];

						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");
							  //ViewData["ModelErrors"] = errors;
							  _viewtoDisplay = "BList";

						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE phonenumber='" + _mpesano + "'";

							  _viewtoDisplay = "BlistDates";

						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------

				  }
				  ViewData["ModelErrors"] = errors;
				  return View(_viewtoDisplay);
			}

			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult LoanUsersWList(FormCollection collection)
			{
				  List<string> errors = new List<string>();

				  string _action = collection[2];
				  string _viewtoDisplay = "";
				  string _mpesano = "";
				  string _SQLSelectionCriteria = "";
				  if (_action == "Load")
				  {
						//-------------------------

						_mpesano = collection["mpesano"];

						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");
							  //ViewData["ModelErrors"] = errors;
							  _viewtoDisplay = "WList";

						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE phonenumber='" + _mpesano + "'";

							  _viewtoDisplay = "WListDates";

						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------

				  }
				  ViewData["ModelErrors"] = errors;
				  return View(_viewtoDisplay);
			}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult BListL(FormCollection collection)
			{
				  List<string> errors = new List<string>();

				  string _action = collection[2];
				  string _viewtoDisplay = "";
				  string _mpesano = "";
				  string _SQLSelectionCriteria = "";
				  if (_action == "Load")
				  {
						//-------------------------

						_mpesano = collection["mpesano"];

						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");

							  _viewtoDisplay = "BList";

						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE phonenumber='" + _mpesano + "'";

							  _viewtoDisplay = "BListDates";

						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------

				  }
				  ViewData["ModelErrors"] = errors;
				  return View(_viewtoDisplay);
			}

			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult LoanUserss(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  string _action = collection[2];
				  string _action_w = collection[3];
				  string _SQLSelectionCriteria = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;

				  if (_action_w.ToString() == "Search") //phone
				  {

						string _mpesano = collection["mpesano"];


						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = "WHERE phonenumber='" + _mpesano + "'";
						}

				  }
				  else if (_action.ToString() == "Load")//dates
				  {
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						string ftodate = "";
						string ftenddate = "";
						if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  errors.Add("You must Indicate a valid start Date.");
						}

						if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
						{
							  errors.Add("You must Indicate a valid  End Date.");
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
							  _startdate = ftodate;
							  _SQLSelectionCriteria = " WHERE datecreated>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
							  _enddate = ftenddate;
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datecreated<='" + _enddate + "'";
						}
				  }
				  if (_SQLSelectionCriteria.Length == 0)
				  {
						errors.Add("The selection Criteria cannot be empty!!");
				  }

				  if (errors.Count > 0)
				  {
						_viewtoDisplay = "LoanUserss";
				  }
				  else
				  {
						_viewtoDisplay = "LoanUsersDates";

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

				  }


				  return View(_viewtoDisplay);
			}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult othersms(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  string _action = collection[2];
				  string _action_w = collection[3];
				  string _SQLSelectionCriteria = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;

				  if (_action_w.ToString() == "Search") //phone
				  {

						string _mpesano = collection["mpesano"];


						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = "WHERE customerphone='" + _mpesano + "'";
						}

				  }
				  else if (_action.ToString() == "Load")//dates
				  {
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						string ftodate = "";
						string ftenddate = "";

						if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  errors.Add("You must Indicate a valid start Date.");
						}

						if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
						{
							  errors.Add("You must Indicate a valid  End Date.");
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
							  _startdate = ftodate;
							  _SQLSelectionCriteria = " WHERE senttime>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
						{
							  ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
							  _enddate = ftenddate;
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND senttime<='" + _enddate + "'";
						}
				  }
				  if (_SQLSelectionCriteria.Length == 0)
				  {
						errors.Add("The selection Criteria cannot be empty!!");
				  }

				  if (errors.Count > 0)
				  {
						_viewtoDisplay = "othersms";
				  }
				  else
				  {
						_viewtoDisplay = "othersmsdates";

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

				  }


				  return View(_viewtoDisplay);
			}

			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult ViewLoanUser(FormCollection collection ,string id)
			{
				
				  var sql = "";
				  var flag = false;
				  var errors = new List<string>();
				  var response = new List<string>();
				  ViewData["firstname"] = collection["firstname"];
				  ViewData["middlename"] = collection["middlename"];
				  ViewData["lastname"] = collection["lastname"];
				  ViewData["emailadddress"] = collection["emailadddress"];
				  ViewData["phonenumber"] = collection["phonenumber"];
				  ViewData["Emailaddress"] = collection["Emailaddress"];
				  ViewData["dateofbirth"] = collection["dateofbirth"];
				  ViewData["crbcreditscore"] = collection["crbcreditscore"];
				  ViewData["deliquencystatuscode"] = collection["deliquencystatuscode"];
				  ViewData["deliquencystatusdescription"] = collection["deliquencystatuscode"];
				  ViewData["mpesascore"] = collection["mpesascore"];
				  ViewData["gender"] = collection["gender"];
				  ViewData["iprscheckdescription"] = collection["iprscheckdescription"];


				  //Stationcode
				  var stationCode = Blogic.RunStringReturnStringValue("select stationcode from LoanUsers WHERE username = 'admin'");

				  if (errors.Count > 0)
				  {
						_successfailview = "ViewLoanUser";
						ViewData["firstname"] = collection["firstname"];
						ViewData["middlename"] = collection["middlename"];
						ViewData["lastname"] = collection["lastname"];
						ViewData["emailadddress"] = collection["emailadddress"];
						ViewData["phonenumber"] = collection["phonenumber"];
						ViewData["crbcreditscore"] = collection["crbcreditscore"];
						ViewData["gender"] = collection["gender"];
						ViewData["iprscheckdescription"] = collection["iprscheckdescription"];
				  }
				  else
				  {
						try
						{

							  var data = new Dictionary<string, string>();
							  data["firstname"] = collection["firstname"];
							  data["middlename"] = collection["middlename"];
							  data["lastname"] = collection["lastname"];
							  data["phonenumber"] = collection["phonenumber"];
							  data["Emailaddress"] = collection["Emailaddress"];
							  data["dateofbirth"] = collection["dateofbirth"];
							  data["crbcreditscore"] = collection["crbcreditscore"];
							  data["deliquencystatuscode"] = collection["deliquencystatuscode"];
							  data["deliquencystatusdescription"] = collection["deliquencystatusdescription"];
							  data["gender"] = collection["gender"];
							  data["iprscheckdescription"] = collection["iprscheckdescription"];

							  
							  //Random generator = new Random();
							  //String RandomPassword = generator.Next(0, 1000000).ToString("D6");
							  //string encPassword = userdata.EncyptString(RandomPassword);

							  string recipients = (collection["phonenumber"]);
							  //a1d21894f628167e5f4e72c71c32463f
							  data["pin"] = "a1d21894f628167e5f4e72c71c32463f";
							  //-----------------------------------------------------------
							  where["UserID"] = id;
							  sql = Logic.DMLogic.UpdateString("loanusers", data, where);
							
							 
							  flag = Blogic.RunNonQuery(sql);
							  if (flag)
							  {
									response.Add("The user pin been reset successfully.");
									_successfailview = "ChurchRegistrationSuccess";


									string messagetosend = "Dear " + collection["firstname"] + "  " + collection["lastname"] + "  " + "your pin has been reset and your new pin is 1234 .Kindly use this pin to reset your account.Thanks." ;

									SendSms(messagetosend, recipients);
							  }
						}
						catch
						{
							  _successfailview = "ErrorPage";
						}
						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

				  }
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;
				  return View(_successfailview);
			}

			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult ViewLoanUser(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {
						_viewToDisplayData = "ViewLoanUser";
						string uniqueID = id.ToString();
						sql = " select * from LoanUsers WHERE userid= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  // ViewData["id"] = rd["id"];
										  ViewData["firstname"] = rd["firstname"];
										  ViewData["middlename"] = rd["middlename"];
										  ViewData["lastname"] = rd["lastname"];
										  ViewData["emailaddress"] = rd["emailaddress"];
										  ViewData["phonenumber"] = rd["phonenumber"];
										  ViewData["dateofbirth"] = rd["dateofbirth"];
										  ViewData["facebook"] = rd["FacebookLink"];
										  ViewData["gmail"] = rd["GoogleLink"];
										  ViewData["gender"] = rd["gender"];
										  ViewData["photo"] = rd["customerphoto"];
										  ViewData["facebook_email"] = rd["facebook_email"];
										  ViewData["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
										  ViewData["deliquencystatuscode"] = rd["deliquencystatuscode"];
										  ViewData["crbcreditscore"] = rd["crbcreditscore"];
										  ViewData["iprscheckdescription"] = rd["iprscheckdescription"];
										  Session["userphonex"] = "";
										  Session["userphonex"] = rd["phonenumber"].ToString();

									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult ViewBList(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {
						_viewToDisplayData = "ViewBList";
						string uniqueID = id.ToString();
						sql = " select * from LoanUsers WHERE userid= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  // ViewData["id"] = rd["id"];
										  ViewData["firstname"] = rd["firstname"];
										  ViewData["middlename"] = rd["middlename"];
										  ViewData["lastname"] = rd["lastname"];
										  ViewData["emailaddress"] = rd["emailaddress"];
										  ViewData["phonenumber"] = rd["phonenumber"];
										  ViewData["dateofbirth"] = rd["dateofbirth"];
										  ViewData["facebook"] = rd["FacebookLink"];
										  ViewData["gmail"] = rd["GoogleLink"];
										  ViewData["gender"] = rd["gender"];
										  ViewData["photo"] = rd["customerphoto"];
										  ViewData["facebook_email"] = rd["facebook_email"];
										  ViewData["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
										  ViewData["deliquencystatuscode"] = rd["deliquencystatuscode"];
										  ViewData["crbcreditscore"] = rd["crbcreditscore"];
										  ViewData["iprscheckdescription"] = rd["iprscheckdescription"];
										  Session["userphonex"] = "";
										  Session["userphonex"] = rd["phonenumber"].ToString();

									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult ViewWList(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {
						_viewToDisplayData = "ViewWList";
						string uniqueID = id.ToString();
						sql = " select * from LoanUsers WHERE userid= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  // ViewData["id"] = rd["id"];
										  ViewData["firstname"] = rd["firstname"];
										  ViewData["middlename"] = rd["middlename"];
										  ViewData["lastname"] = rd["lastname"];
										  ViewData["emailaddress"] = rd["emailaddress"];
										  ViewData["phonenumber"] = rd["phonenumber"];
										  ViewData["dateofbirth"] = rd["dateofbirth"];
										  ViewData["facebook"] = rd["FacebookLink"];
										  ViewData["gmail"] = rd["GoogleLink"];
										  ViewData["gender"] = rd["gender"];
										  ViewData["photo"] = rd["customerphoto"];
										  ViewData["facebook_email"] = rd["facebook_email"];
										  ViewData["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
										  ViewData["deliquencystatuscode"] = rd["deliquencystatuscode"];
										  ViewData["crbcreditscore"] = rd["crbcreditscore"];
										  ViewData["iprscheckdescription"] = rd["iprscheckdescription"];
										  Session["userphonex"] = "";
										  Session["userphonex"] = rd["phonenumber"].ToString();

									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}
			//[NoDirectAccessAttribute]
			//[AcceptVerbs(HttpVerbs.Get)]
			//public ActionResult ViewLoanUsersBList(string id)
			//{
			//    string _viewToDisplayData = "Error";
			//    if (id != null)
			//    {
			//
			//        _viewToDisplayData = "ViewLoanUsersBList";
			//        string uniqueID = id.ToString();
			//        sql = " select * from LoanUsers WHERE id= '" + uniqueID + "'";
			//        rd = Blogic.RunQueryReturnDataReader(sql);
			//        using (rd)
			//        {
			//            if (rd != null && rd.HasRows)
			//            {
			//                while (rd.Read())
			//                {
			//                    ViewData["recordid"] = rd["id"];
			//                    ViewData["username"] = rd["username"];
			//                    ViewData["fullnames"] = rd["fullnames"];
			//                    ViewData["createby"] = rd["CreateBy"];
			//                    ViewData["createdate"] = rd["CreateDate"];
			//                    ViewData["email1"] = rd["Email1"];
			//                    ViewData["email2"] = rd["Email2"];
			//                    ViewData["mobile1"] = rd["Mobile1"];
			//                    ViewData["mobile2"] = rd["Mobile2"];

			//                }
			//            }
			//        }
			//    }
			//    else
			//    {
			//        //report the record selection criteria was not right.
			//        _viewToDisplayData = "ErrorPage";
			//    }

			//    return View(_viewToDisplayData);

			//}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult ViewLoanUsersBList(FormCollection collection)
			{
				  var id = "";
				  var sql = "";
				  var flag = false;
				  var errors = new List<string>();
				  var response = new List<string>();
				  ViewData["firstname"] = collection["firstname"];
				  ViewData["middlename"] = collection["middlename"];
				  ViewData["lastname"] = collection["lastname"];
				  ViewData["emailadddress"] = collection["emailadddress"];
				  ViewData["phonenumber"] = collection["phonenumber"];
				  ViewData["Emailaddress"] = collection["Emailaddress"];
				  ViewData["datebirth"] = collection["datebirth"];
				  ViewData["crbcreditscore"] = collection["crbcreditscore"];
				  ViewData["delinquencystatuscode"] = collection["delinquencystatuscode"];
				  ViewData["delinquencystatusdescription"] = collection["delinquencystatusdescription"];
				  ViewData["mpesascore"] = collection["mpesascore"];
				  ViewData["gender"] = collection["gender"];
				  ViewData["iprscheckdescription"] = collection["iprscheckdescription"];


				  //Stationcode
				  var stationCode = Blogic.RunStringReturnStringValue("select stationcode from LoanUsers WHERE username = 'admin'");

				  if (errors.Count > 0)
				  {
						_successfailview = "ViewLoanUsersBList";
						ViewData["firstname"] = collection["firstname"];
						ViewData["middlename"] = collection["middlename"];
						ViewData["lastname"] = collection["lastname"];
						ViewData["emailadddress"] = collection["emailadddress"];
						ViewData["phonenumber"] = collection["phonenumber"];
						//ViewData["Emailaddress"] = collection["Emailaddress"];
						//ViewData["datebirth"] = collection["datebirth"];
						ViewData["crbcreditscore"] = collection["crbcreditscore"];
						//ViewData["delinquencystatuscode"] = collection["delinquencystatuscode"];
						//ViewData["delinquencystatusdescription"] = collection["delinquencystatusdescription"];
						//ViewData["mpesascore"] = collection["mpesascore"];
						ViewData["gender"] = collection["gender"];
						ViewData["iprscheckdescription"] = collection["iprscheckdescription"];
				  }
				  else
				  {
						try
						{

							  var data = new Dictionary<string, string>();
							  data["firstname"] = collection["firstname"];
							  data["middlename"] = collection["middlename"];
							  data["lastname"] = collection["lastname"];
							  //data["emailadddress"] = collection["emailadddress"];
							  data["phonenumber"] = collection["phonenumber"];
							  data["Emailaddress"] = collection["Emailaddress"];
							  //data["datebirth"] = collection["datebirth"];
							  //data["crbcreditscore"] = collection["crbcreditscore"];
							  //data["delinquencystatuscode"] = collection["delinquencystatuscode"];
							  //data["delinquencystatusdescription"] = collection["delinquencystatusdescription"];
							  //data["mpesascore"] = collection["mpesascore"];
							  data["gender"] = collection["gender"];
							  data["iprscheckdescription"] = collection["iprscheckdescription"];
							  data["blacklist"] = "1";
							  where["UserID"] = collection["UserID"];
							  sql = Logic.DMLogic.UpdateString("LoanUsers", data, where);
							  flag = Blogic.RunNonQuery(sql);
							  if (flag)
							  {
									response.Add("The Client has been Blacklisted successfully.");
									_successfailview = "ChurchRegistrationSuccess";
							  }
						}
						catch
						{
							  _successfailview = "ErrorPage";
						}
				  }
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;
				  return View(_successfailview);
			}


			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult LoanUsersBList(FormCollection collection)
			{
				  List<string> errors = new List<string>();

				  string _action = collection[2];
				  string _viewtoDisplay = "";
				  string _mpesano = "";
				  string _SQLSelectionCriteria = "";
				  if (_action == "Load")
				  {
						//-------------------------

						_mpesano = collection["mpesano"];

						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the MPESA No.");
							  //ViewData["ModelErrors"] = errors;
							  _viewtoDisplay = "LoanUsers";

						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE phonenumber='" + _mpesano + "'";

							  _viewtoDisplay = "LoanUsersDates";

						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------

				  }
				  ViewData["ModelErrors"] = errors;
				  return View(_viewtoDisplay);
			}


			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult ViewBList(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";

				  string EmailAddress1 = "";
				  string PhoneNumber1 = "";

				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["firstname"] = collection["firstname"];
						data["middlename"] = collection["middlename"];
						data["lastname"] = collection["lastname"];
						data["phonenumber"] = collection["phonenumber"];
						data["crbcreditscore"] = collection["crbcreditscore"];
						data["gender"] = collection["gender"];
						data["iprscheckdescription"] = collection["iprscheckdescription"];
						data["blacklist"] = "1";
						where["UserID"] = id;
						sql = Logic.DMLogic.UpdateString("LoanUsers", data, where);
						flag = Blogic.RunNonQuery(sql);
						if (flag)
						{
							  response.Add("The User has been BlackListed successfully.");
							  _successfailview = "ChurchRegistrationSuccess";

						}
				  }
				  catch
				  {
						_successfailview = "ErrorPage";
				  }
				  ViewData["Response"] = response;
				  return View(_successfailview);
			}



			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult ViewWList(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";

				  string EmailAddress1 = "";
				  string PhoneNumber1 = "";

				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						//data["firstname"] = collection["firstname"];
						//data["middlename"] = collection["middlename"];
						//data["lastname"] = collection["lastname"];
						//data["phonenumber"] = collection["phonenumber"];
						//data["crbcreditscore"] = collection["crbcreditscore"];
						//data["gender"] = collection["gender"];
						//data["iprscheckdescription"] = collection["iprscheckdescription"];
						data["blacklist"] = "0";
						where["UserID"] = id;
						sql = Logic.DMLogic.UpdateString("LoanUsers", data, where);
						flag = Blogic.RunNonQuery(sql);
						if (flag)
						{
							  response.Add("The User has been WhiteListed successfully.");
							  _successfailview = "ChurchRegistrationSuccess";

						}
				  }
				  catch
				  {
						_successfailview = "ErrorPage";
				  }
				  ViewData["Response"] = response;
				  return View(_successfailview);
			}



			//[AcceptVerbs(HttpVerbs.Post)]
			//public ActionResult LoanUsersWList(FormCollection collection)
			//{
			//    List<string> errors = new List<string>();

			//    string _action = collection[2];
			//    string _viewtoDisplay = "";
			//    string _mpesano = "";
			//    string _SQLSelectionCriteria = "";
			//    if (_action == "Load")
			//    {
			//        //-------------------------

			//        _mpesano = collection["mpesano"];

			//        if (collection["mpesano"].ToString().Length == 0)
			//        {
			//            errors.Add("You must Indicate the MPESA No.");
			//            //ViewData["ModelErrors"] = errors;
			//            _viewtoDisplay = "LoanUsersWList";

			//        }

			//        if (collection["mpesano"].ToString().Length > 0)
			//        {
			//            _SQLSelectionCriteria = " WHERE phonenumber='" + _mpesano + "'";

			//            _viewtoDisplay = "LoanUsersDates";

			//        }

			//        System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
			//        // ----------------------------

			//    }
			//    ViewData["ModelErrors"] = errors;
			//    return View(_viewtoDisplay);
			//}

			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult ViewLoanUserBList(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {
						_viewToDisplayData = "ViewLoanUserBList";
						string uniqueID = id.ToString();
						sql = " select * from LoanUsers WHERE userid= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  // ViewData["id"] = rd["id"];
										  ViewData["firstname"] = rd["firstname"];
										  ViewData["middlename"] = rd["middlename"];
										  ViewData["lastname"] = rd["lastname"];
										  ViewData["emailaddress"] = rd["emailaddress"];
										  ViewData["phonenumber"] = rd["phonenumber"];
										  ViewData["dateofbirth"] = rd["dateofbirth"];
										  ViewData["facebook"] = rd["FacebookLink"];
										  ViewData["gmail"] = rd["GoogleLink"];
										  ViewData["gender"] = rd["gender"];
										  ViewData["photo"] = rd["customerphoto"];
										  ViewData["facebook_email"] = rd["facebook_email"];
										  ViewData["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
										  ViewData["deliquencystatuscode"] = rd["deliquencystatuscode"];
										  ViewData["crbcreditscore"] = rd["crbcreditscore"];
										  ViewData["iprscheckdescription"] = rd["iprscheckdescription"];
										  Session["userphonex"] = "";
										  Session["userphonex"] = rd["phonenumber"].ToString();

									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult ViewLoanUserWlist(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {
						_viewToDisplayData = "ViewLoanUserWlist";
						string uniqueID = id.ToString();
						sql = " select * from LoanUsers WHERE blacklist='0' userid= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  // ViewData["id"] = rd["id"];
										  ViewData["firstname"] = rd["firstname"];
										  ViewData["middlename"] = rd["middlename"];
										  ViewData["lastname"] = rd["lastname"];
										  ViewData["emailaddress"] = rd["emailaddress"];
										  ViewData["phonenumber"] = rd["phonenumber"];
										  ViewData["dateofbirth"] = rd["dateofbirth"];
										  ViewData["facebook"] = rd["FacebookLink"];
										  ViewData["gmail"] = rd["GoogleLink"];
										  ViewData["gender"] = rd["gender"];
										  ViewData["photo"] = rd["customerphoto"];
										  ViewData["facebook_email"] = rd["facebook_email"];
										  ViewData["deliquencystatusdescription"] = rd["deliquencystatusdescription"];
										  ViewData["deliquencystatuscode"] = rd["deliquencystatuscode"];
										  ViewData["crbcreditscore"] = rd["crbcreditscore"];
										  ViewData["iprscheckdescription"] = rd["iprscheckdescription"];
										  Session["userphonex"] = "";
										  Session["userphonex"] = rd["phonenumber"].ToString();

									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}

			//--------------------------------------------------------------search Logic here-----------------------------------------------------------------
			[ChildActionOnly]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult CallLogDates()
			{
				  System.Web.HttpContext.Current.Session["_selectedOption"] = "calllogs";
				  return PartialView("_CallLogDates");
			}
			[ChildActionOnly]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult LuDates()
			{
				  System.Web.HttpContext.Current.Session["_selectedOption"] = "Lu";
				  return PartialView("_LuDates");
			}
			//[AcceptVerbs(HttpVerbs.Post)]
			//public ActionResult CallLogDates(FormCollection collection)
			//{
			//    string _viewtoDisplay = "";
			//    List<string> errors = new List<string>();
			//    ViewData["ModelErrors"] = errors;
			//    string _startdate = collection["bp.startdate"];
			//    string _enddate = collection["bp.enddate"];
			//    string _mpesano = collection["customerphone"];
			//    string _action = collection[4];

			//    string _SQLSelectionCriteria = "";

			//    if (collection["bp.startdate"].ToString().Length == 0)
			//    {
			//        errors.Add("You must Indicate the start Date.");
			//    }

			//    if (collection["bp.enddate"].ToString().Length == 0)
			//    {
			//        errors.Add("You must Indicate the End Date.");
			//    }

			//    if (collection["customerphone"].ToString().Length == 0)
			//    {
			//        errors.Add("You must Indicate the MPESA No.");
			//    }

			//    if (errors.Count > 0)
			//    {
			//    }
			//    else
			//    {
			//        _SQLSelectionCriteria = null;
			//        if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "calllogs")
			//        {
			//            _viewtoDisplay = "CallLogs";
			//        }


			//        if (_action == "Load")
			//        {
			//            //Build SQL Selection criteria to pass as a single session value
			//            _viewtoDisplay = "CallLogDates";
			//            if (collection["bp.startdate"].ToString().Length > 0)
			//            {
			//                _SQLSelectionCriteria = " WHERE CallDate>='" + _startdate + "'";
			//            }

			//            if (collection["bp.enddate"].ToString().Length > 0)
			//            {
			//                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND CallDate<='" + _enddate + "'";
			//            }

			//            if (collection["customerphone"].ToString().Length > 0)
			//            {
			//                _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerPhone='" + _mpesano + "'";
			//            }
			//        }                

			//        System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

			//        //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
			//        if (_action == "Print")
			//        {
			//            //Call print method
			//        }
			//        else
			//        {
			//            //call Load method to the grid in question
			//        }
			//    }
			//    return View(_viewtoDisplay);
			//}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult CallLogDates(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;
				  string _startdate = collection["bp.startdate"];
				  string _enddate = collection["bp.enddate"];
				  string _mpesano = collection["mpesano"];
				  string _action = collection[4];

				  string _SQLSelectionCriteria = "";
				  string _SQLSelectionCriteriaWithoutPhone = "";

				  if (collection["bp.startdate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the start Date.");
				  }

				  if (collection["bp.enddate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the End Date.");
				  }

				  if (collection["mpesano"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the MPESA No.");
				  }

				  if (errors.Count > 0)
				  {
				  }
				  else
				  {
						_SQLSelectionCriteria = null;
						_SQLSelectionCriteriaWithoutPhone = null;
						if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "mpesalogs")
						{
							  _viewtoDisplay = "CallLogs";
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE CallDate>='" + _startdate + "'";
							  _SQLSelectionCriteriaWithoutPhone = " WHERE CallDate>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND CallDate<='" + _enddate + "'";
							  _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND CallDate<='" + _enddate + "'";
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerphone='" + _mpesano + "'";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

						//check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
						if (_action == "Print")
						{
							  //Call print method
						}
						else
						{
							  //call Load method to the grid in question
						}
				  }
				  return View(_viewtoDisplay);
			}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult LuDates(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;
				  string _startdate = collection["bp.startdate"];
				  string _enddate = collection["bp.enddate"];
				  string _mpesano = collection["customerphone"];
				  string _action = collection[4];

				  string _SQLSelectionCriteria = "";

				  if (collection["bp.startdate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the start Date.");
				  }

				  if (collection["bp.enddate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the End Date.");
				  }

				  if (collection["customerphone"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the MPESA No.");
				  }

				  if (errors.Count > 0)
				  {
				  }
				  else
				  {
						_SQLSelectionCriteria = null;
						if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "calllogs")
						{
							  _viewtoDisplay = "Lu";
						}


						if (_action == "Load")
						{
							  //Build SQL Selection criteria to pass as a single session value
							  _viewtoDisplay = "LuDates";
							  if (collection["bp.startdate"].ToString().Length > 0)
							  {
									_SQLSelectionCriteria = " WHERE CallDate>='" + _startdate + "'";
							  }

							  if (collection["bp.enddate"].ToString().Length > 0)
							  {
									_SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND CallDate<='" + _enddate + "'";
							  }

							  if (collection["customerphone"].ToString().Length > 0)
							  {
									_SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerPhone='" + _mpesano + "'";
							  }
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

						//check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
						if (_action == "Print")
						{
							  //Call print method
						}
						else
						{
							  //call Load method to the grid in question
						}
				  }
				  return View(_viewtoDisplay);
			}


			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult MpesaDates()
			{
				  System.Web.HttpContext.Current.Session["_selectedOption"] = "mpesalogs";
				  return PartialView("_MpesaDates");
			}


			//[AcceptVerbs(HttpVerbs.Post)]
			//public ActionResult MpesaDates(FormCollection collection)
			//{
			//    string _viewtoDisplay = "";
			//    List<string> errors = new List<string>();
			//    ViewData["ModelErrors"] = errors;
			//    string _startdate = collection["bp.startdate"];
			//    string _enddate = collection["bp.enddate"];
			//    string _mpesano = collection["mpesano"];
			//    string _action = collection[4];

			//    string _SQLSelectionCriteria = "";

			//    //if (collection["bp.startdate"].ToString().Length == 0)
			//    //{
			//    //    errors.Add("You must Indicate the start Date.");
			//    //}

			//    //if (collection["bp.enddate"].ToString().Length == 0)
			//    //{
			//    //    errors.Add("You must Indicate the End Date.");
			//    //}

			//   
			//    //{
			//    //    errors.Add("You must Indicate the MPESA No.");
			//    //}

			//    if (errors.Count > 0)
			//    {
			//    }
			//    else
			//    {
			//        _SQLSelectionCriteria = null;
			//        if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "mpesalogs")
			//        {
			//            _viewtoDisplay = "SMSLogs";
			//        }
			//        //Build SQL Selection criteria to pass as a single session value
			//        if (collection["bp.startdate"].ToString().Length > 0)
			//        {
			//            _SQLSelectionCriteria = " WHERE datelogged>='" + _startdate + "'";
			//        }

			//        if (collection["bp.enddate"].ToString().Length > 0)
			//        {
			//            _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datelogged<='" + _enddate + "'";
			//        }

			//        if (collection["mpesano"].ToString().Length > 0)
			//        {
			//            _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
			//        }

			//        System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

			//        //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
			//        if (_action == "Print")
			//        {
			//            //Call print method
			//        }
			//        else
			//        {
			//            //call Load method to the grid in question
			//        }
			//    }
			//    return View(_viewtoDisplay);
			//}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult MpesaDates(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;
				  string _startdate = collection["bp.startdate"];
				  string _enddate = collection["bp.enddate"];
				  string _mpesano = collection["mpesano"];
				  string _action = collection[4];

				  string _SQLSelectionCriteria = "";
				  string _SQLSelectionCriteriaWithoutPhone = "";

				  if (collection["bp.startdate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the start Date.");
				  }

				  if (collection["bp.enddate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the End Date.");
				  }

				  if (collection["mpesano"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the MPESA No.");
				  }

				  if (errors.Count > 0)
				  {
				  }
				  else
				  {
						_SQLSelectionCriteria = null;
						_SQLSelectionCriteriaWithoutPhone = null;
						if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "mpesalogs")
						{
							  _viewtoDisplay = "SMSLogs";
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE datelogged>='" + _startdate + "'";
							  _SQLSelectionCriteriaWithoutPhone = " WHERE datelogged>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datelogged<='" + _enddate + "'";
							  _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND datelogged<='" + _enddate + "'";
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

						//check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
						if (_action == "Print")
						{
							  //Call print method
						}
						else
						{
							  //call Load method to the grid in question
						}
				  }
				  return View(_viewtoDisplay);
			}

			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult LoanUsersDates()
			{
				  System.Web.HttpContext.Current.Session["_selectedOption"] = "loanusers";
				  return PartialView("_LoanUsersDates");
			}
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult othersmsdates()
			{
				  System.Web.HttpContext.Current.Session["_selectedOption"] = "";
				  return PartialView("_othersmsdates");
			}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult LoanUsersDates(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;
				  string _startdate = collection["bp.startdate"];
				  string _enddate = collection["bp.enddate"];
				  string _mpesano = collection["mpesano"];
				  string _action = collection[4];

				  string _SQLSelectionCriteria = "";
				  string _SQLSelectionCriteriaWithoutPhone = "";

				  if (collection["bp.startdate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the start Date.");
				  }

				  if (collection["bp.enddate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the End Date.");
				  }

				  if (collection["mpesano"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the MPESA No.");
				  }

				  if (errors.Count > 0)
				  {
				  }
				  else
				  {
						_SQLSelectionCriteria = null;
						_SQLSelectionCriteriaWithoutPhone = null;
						if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "loanusers")
						{
							  _viewtoDisplay = "LoanUsers";
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE datecreated>='" + _startdate + "'";
							  _SQLSelectionCriteriaWithoutPhone = " WHERE datecreated>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datecreated<='" + _enddate + "'";
							  _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND datecreated<='" + _enddate + "'";
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

						//check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
						if (_action == "Print")
						{
							  //Call print method
						}
						else
						{
							  //call Load method to the grid in question
						}
				  }
				  return View(_viewtoDisplay);
			}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult othersmsdates(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;
				  string _startdate = collection["bp.startdate"];
				  string _enddate = collection["bp.enddate"];
				  string _mpesano = collection["mpesano"];
				  string _action = collection[4];

				  string _SQLSelectionCriteria = "";
				  string _SQLSelectionCriteriaWithoutPhone = "";

				  if (collection["bp.startdate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the start Date.");
				  }

				  if (collection["bp.enddate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the End Date.");
				  }

				  if (collection["mpesano"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the MPESA No.");
				  }

				  if (errors.Count > 0)
				  {
				  }
				  else
				  {
						_SQLSelectionCriteria = null;
						_SQLSelectionCriteriaWithoutPhone = null;
						if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "")
						{
							  _viewtoDisplay = "othersms";
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE senttime>='" + _startdate + "'";
							  _SQLSelectionCriteriaWithoutPhone = " WHERE senttime>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND senttime<='" + _enddate + "'";
							  _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND senttime<='" + _enddate + "'";
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerphone='" + _mpesano + "'";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

						//check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
						if (_action == "Print")
						{
							  //Call print method
						}
						else
						{
							  //call Load method to the grid in question
						}
				  }
				  return View(_viewtoDisplay);
			}
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult BListDates()
			{

				  return PartialView();
			}
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult WListDates()
			{

				  return PartialView();
			}

			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult BListDates(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  string _action = collection[2];
				  List<string> errors = new List<string>();
				  //string _startdate = collection["bp.startdate"];
				  //string _enddate = collection["bp.enddate"];
				  string _mpesano = "";

				  ViewData["ModelErrors"] = errors;
				  string _SQLSelectionCriteria = "";
				  if (collection["mpesano"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the MPESA No.");
				  }
				  if (collection["mpesano"].ToString().Length > 0)
				  {
						_mpesano = collection["mpesano"];
						_SQLSelectionCriteria = " where phonenumber='" + _mpesano + "'";

				  }


				  if (_SQLSelectionCriteria.ToString().Length > 0)
				  {
						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
				  }
				  if (_SQLSelectionCriteria.ToString().Length == 0)
				  {
						errors.Add("Selection Criteria cannot be null!!.");
				  }

				  if (errors.Count > 0)
				  {
						_viewtoDisplay = "BListDates";
				  }
				  else
				  {
						_viewtoDisplay = "BListDatesBlack";
				  }

				  return View(_viewtoDisplay);
			}

			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult WListDates(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  List<string> errors = new List<string>();
				  ViewData["ModelErrors"] = errors;
				  string _startdate = collection["bp.startdate"];
				  string _enddate = collection["bp.enddate"];
				  string _mpesano = collection["mpesano"];
				  string _action = collection[4];

				  string _SQLSelectionCriteria = "";
				  string _SQLSelectionCriteriaWithoutPhone = "";

				  if (collection["bp.startdate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the start Date.");
				  }

				  if (collection["bp.enddate"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the End Date.");
				  }

				  if (collection["mpesano"].ToString().Length == 0)
				  {
						errors.Add("You must Indicate the MPESA No.");
				  }

				  if (errors.Count > 0)
				  {
				  }
				  else
				  {
						_SQLSelectionCriteria = null;
						_SQLSelectionCriteriaWithoutPhone = null;
						if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "LoanUsersDatesBlist")
						{
							  _viewtoDisplay = "WListDates";
						}
						//Build SQL Selection criteria to pass as a single session value
						if (collection["bp.startdate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = " WHERE datecreated>='" + _startdate + "'";
							  _SQLSelectionCriteriaWithoutPhone = " WHERE datecreated>='" + _startdate + "'";
						}

						if (collection["bp.enddate"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datecreated<='" + _enddate + "'";
							  _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND datecreated<='" + _enddate + "'";
						}

						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

						//check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
						if (_action == "Print")
						{
							  //Call print method
						}
						else
						{
							  //call Load method to the grid in question
						}
				  }
				  return View(_viewtoDisplay);
			}
	  }
}
