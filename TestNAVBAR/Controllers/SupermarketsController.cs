﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
	  public class SupermarketsController : Controller
	  {
			// GET: /Station/
			string T24Response = "";
			string selectedchurchbranch = "";
			string _successfailview = "";
			string selectedchurch = "";
			string sql = "";
			Dictionary<string, string> where = new Dictionary<string, string>();
			System.Data.SqlClient.SqlDataReader rd;
			BusinessLogic.Users userdata = new BusinessLogic.Users();
			BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
			Logic.TCPLogic tcplogic = new Logic.TCPLogic();
			Logic.DMLogic bizzlogic = new Logic.DMLogic();

			public ActionResult deletestock()
			{
				  // Session["stockid"] = id;
				  return View();
			}

			public ActionResult RegisterStockItemsdates()
			{
				  // Session["stockid"] = id;
				  return View();
			}


			public ActionResult Index()
			{
				  return View();
			}



			public ActionResult RegisterSupermarkets()
			{
				  return View();
			}


			public ActionResult viewattendands(string id)
			{
				  Session["id"] = id;
				  return View();
			}

			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]

			public ActionResult RegisterStockItems()
			{
				  return View();
			}

			public ActionResult Modifyvendors()
			{
				  return View();
			}

			public ActionResult viewbranchdates()
			{
				  return View();
			}



			public ActionResult ModifySupermarket()
			{
				  return View();
			}

			//deletesupermarket

			public ActionResult Registervendors()
			{
				  return View();
			}

			public ActionResult deletebranch()
			{
				  return View();
			}

			public ActionResult deleteattendands()
			{
				  return View();
			}


			public ActionResult deletesupermarket()
			{
				  return View();
			}

			public ActionResult deletevendors()
			{
				  return View();
			}



			//
			//mofifybranch


			public ActionResult mofifybranch()
			{
				  //Session["bid"] = id;
				  return View();
			}
			//viewattendands
			//ModifyStock

			public ActionResult ModifyStock(string id)
			{
				  Session["stockid"] = id;
				  return View();
			}




			public ActionResult Addattendands(string id)
			{
				  Session["id"] = id;
				  return View();
			}

			public ActionResult editattendands()
			{

				  return View();
			}



			public ActionResult RegisterSupermarketsdates()
			{

				  return View();
			}

			public ActionResult Removeattendands(string id)
			{
				  Session["id"] = id;
				  return View();
			}



			public ActionResult viewbranch(string myParam)
			{
				  Session["supermarketcode"] = myParam;
				  return View();
			}
			//viewbranch
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult RegisterSupermarkets(FormCollection collection)
			{
				  string sql = "";
				  bool flag = false;
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  ViewData["supermarketcode"] = collection["supermarketcode"];
				  ViewData["supermarketname"] = collection["supermarketname"];

				  string _action = "";

				  string _successfailview = "RegisterSupermarkets";
				  if (collection.Count > 0)
				  {
						_action = collection[2];
				  }


				  if (_action == "Search")
				  {
						//-------------------------
						_successfailview = "RegisterSupermarketsdates";

						ViewData["ModelErrors"] = errors;
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						string _mpesano = collection["mpesano"];


						string _SQLSelectionCriteria = "";

						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the supermarketname.");
						}




						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = "  WHERE supermarketname like  '" + _mpesano + "%' ";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------
						return View(_successfailview);

				  }

				  if (collection["supermarketcode"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the supermarketcode.Its Mandatory");
				  }
				  if (collection["supermarketname"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the supermarketname.Its Mandatory");
				  }

				  string codeExists = Blogic.RunStringReturnStringValue("select * from Supermarket WHERE supermarketcode = '" + collection["supermarketcode"].ToString().Trim() + "'");

				  if (codeExists != "" && codeExists != "0")
				  {
						errors.Add("Please MAKE SURE the supermarketcode are unique for each Supermarket.This supermarketcode record already exists.");
				  }

				  string nameExists = Blogic.RunStringReturnStringValue("select * from Supermarket WHERE supermarketname = '" + collection["supermarketname"].ToString().Trim() + "'");

				  if (nameExists != "" && nameExists != "0")
				  {
						errors.Add("Please MAKE SURE the supermarketName is unique for each Supermarket.This supermarketname record already exists.");
				  }



				  //Stationcode
				  string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Supermarket WHERE username = 'admin'"); ;

				  if (errors.Count > 0)
				  {
						_successfailview = "RegisterSupermarkets";
						ViewData["supermarketcode"] = collection["supermarketcode"];
						ViewData["supermarketname"] = collection["supermarketname"];

				  }
				  else
				  {
						try
						{
							  DateTime getdate = DateTime.Now;
							  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
							  Dictionary<string, string> data = new Dictionary<string, string>();
							  string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
							  //string Dsupermarket = collection["supermarketname"] + " " + collection["supermarketcode"];
							  data["supermarketcode"] = collection["supermarketcode"];
							  data["supermarketname"] = collection["supermarketname"];
							  string strUsername = "";
							  MyGlobalVariables.username = strUsername;
							  //-----------------------------------------------------------
							  data["CreateBy"] = strUsername;
							  data["CreateDate"] = dt.ToString();
							  sql = Logic.DMLogic.InsertString("Supermarket", data);
							  flag = Blogic.RunNonQuery(sql);
							  if (flag)
							  {
									//  Utilities.Users.ManageAuditTrail(strUsername.ToString(), (string)HttpContext.Session["mobile1"], "MainOffice", "Created Supermarket : " + Dsupermarket.ToString(), "Created Supermarket", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);



									response.Add("The Supermarket has been created successfully.");
									_successfailview = "ChurchRegistrationSuccess";

							  }
						}
						catch
						{
							  _successfailview = "ErrorPage";
						}
				  }
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;

				  try
				  {

						bool resl = Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, HttpContext.Session["mobile1"].ToString(), "MAIN BRANCH", "Registered Supermarket: " + collection["supermarketname"].ToString() + " " + HttpContext.Session["username"].ToString(), "Parameters", HttpContext.Session["UserHostAddress"].ToString(), HttpContext.Session["UserHostName"].ToString());
				  }

				  catch
				  {

				  }

				  return View(_successfailview);
			}



			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult Registervendors(FormCollection collection)
			{
				  string sql = "";
				  bool flag = false;
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  ViewData["vendorname"] = collection["vendorname"];
				  ViewData["id"] = collection["id"];

				  string _action = "";

				  if (collection.Count > 0)
				  {
						_action = collection[2];
				  }

				  if (_action == "Search")
				  {
						//-------------------------
						_successfailview = "Registervendorsdates";

						ViewData["ModelErrors"] = errors;
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						string _mpesano = collection["mpesano"];
						string _vendor = collection["Vendor_s"];




						string _SQLSelectionCriteria = "";





						if (collection["Vendor_s"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the vendor.");
						}

						if (collection["Vendor_s"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = "  WHERE vendorname like  '" + _vendor + "%' ";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------
						return View(_successfailview);

				  }

				  if (collection["vendorname"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the vendorname.Its Mandatory");
				  }


				  string codeExists = Blogic.RunStringReturnStringValue("select * from tb_vendors WHERE vendorname = '" + collection["vendorname"].ToString().Trim() + "'");

				  if (codeExists != "" && codeExists != "0")
				  {
						errors.Add("Please MAKE SURE the vendorname are unique for each vendor.This vendorname record already exists.");
				  }

				  string nameExists = Blogic.RunStringReturnStringValue("select * from tb_vendors WHERE vendorname = '" + collection["vendorname"].ToString().Trim() + "'");

				  if (nameExists != "" && nameExists != "0")
				  {
						errors.Add("Please MAKE SURE the vendorname is unique for each vendor.This vendorname record already exists.");
				  }



				  //Stationcode
				  string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Supermarket WHERE username = 'admin'"); ;

				  if (errors.Count > 0)
				  {
						_successfailview = "Registervendors";
						ViewData["vendorname"] = collection["vendorname"];
						ViewData["id"] = collection["id"];

				  }
				  else
				  {
						try
						{
							  DateTime getdate = DateTime.Now;
							  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
							  Dictionary<string, string> data = new Dictionary<string, string>();
							  string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
							  data["vendorname"] = collection["vendorname"];



							  //-----------------------------------------------------------
							  data["CreateBy"] = "ADMIN";
							  data["CreateDate"] = dt.ToString();
							  sql = Logic.DMLogic.InsertString("tb_vendors", data);
							  flag = Blogic.RunNonQuery(sql);
							  if (flag)
							  {



									response.Add("The vendor has been created successfully.");
									_successfailview = "Registervendors";

							  }
						}
						catch
						{
							  _successfailview = "ErrorPage";
						}
				  }
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;

				  try
				  {

						bool resl = Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, HttpContext.Session["mobile1"].ToString(), "MAIN BRANCH", "Regisstered vendor: " + collection["vendorname"].ToString() + " " + HttpContext.Session["username"].ToString(), "Parameters", HttpContext.Session["UserHostAddress"].ToString(), HttpContext.Session["UserHostName"].ToString());
				  }

				  catch
				  {

				  }

				  return View(_successfailview);
			}




			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult RegisterStockItems(FormCollection collection)
			{
				  string sql = "";
				  bool flag = false;
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  ViewData["ItemNo"] = collection["ItemNo"];
				  ViewData["Description"] = collection["Description"];
				  ViewData["Vendor"] = collection["Vendor"];
				  ViewData["Contact"] = collection["Contact"];
				  ViewData["Discount"] = collection["Discount"];

				  string _action = "";


				  if (collection.Count > 0)
				  {
						_action = collection[3];
				  }



				  string _successfailview = "ChurchRegistrationSuccess";
				  if (collection.Count > 0)
				  {
						_action = collection[3];
				  }


				  if (_action == "Search")
				  {
						//-------------------------
						_successfailview = "RegisterStockItemsdates";

						ViewData["ModelErrors"] = errors;
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						string _mpesano = collection["mpesano"];
						string _vendor = collection["Vendor_s"];
						string _desc = collection["Description"];




						string _SQLSelectionCriteria = "";





						if (collection["Vendor_S"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the vendor.");
						}






						if (collection["Vendor_S"].ToString().Length > 0)
						{


							  _SQLSelectionCriteria = "  WHERE vendor like  '" + _vendor + "%' ";
						}
						if (collection["Description"].ToString().Length > 0)
						{

							  _SQLSelectionCriteria = _SQLSelectionCriteria + " and Description  like  '" + _desc + "%' ";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------
						return View(_successfailview);

				  }

				  if (collection["ItemNo"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the ItemNo.Its Mandatory");
				  }
				  if (collection["Description"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Description.Its Mandatory");
				  }

				  if (collection["Vendor"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Vendor.Its Mandatory");
				  }

				  if (collection["Discount"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Discount.Its Mandatory");
				  }

				  string codeExists = Blogic.RunStringReturnStringValue("select * from StockItems WHERE ItemNo = '" + collection["ItemNo"].ToString().Trim() + "'");

				  if (codeExists != "" && codeExists != "0")
				  {
						errors.Add("Please MAKE SURE the ItemNo are unique for each item.This item record already exists.");
				  }



				  //Stationcode
				  string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Supermarket WHERE username = 'admin'"); ;

				  if (errors.Count > 0)
				  {
						_successfailview = "RegisterStockItems";
						ViewData["ItemNoItemNo"] = collection["supermarketcode"];
						ViewData["Vendor"] = collection["Vendor"];

				  }
				  else
				  {
						try
						{
							  DateTime getdate = DateTime.Now;
							  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
							  Dictionary<string, string> data = new Dictionary<string, string>();
							  string SelectedChurch = (string)HttpContext.Session["selectedChurch"];


							  data["ItemNo"] = collection["ItemNo"];
							  data["Description"] = collection["Description"];
							  data["Vendor"] = collection["Vendor"];
							  data["Contact"] = collection["Contact"];
							  data["Discount"] = collection["Discount"];


							  //-----------------------------------------------------------
							  data["CreateBy"] = "ADMIN";
							  data["CreateDate"] = dt.ToString();
							  sql = Logic.DMLogic.InsertString("StockItems", data);
							  flag = Blogic.RunNonQuery(sql);
							  if (flag)
							  {
									response.Add("The Item has been created successfully.");
									_successfailview = "ChurchRegistrationSuccess";
							  }
						}
						catch
						{
							  _successfailview = "ErrorPage";
						}
				  }
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;
				  try
				  {
						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered User: " + collection["Description"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }
				  catch
				  {

				  }

				  return View(_successfailview);
			}


			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]




			public ActionResult modifystock(FormCollection collection, string ID)
			{
				  string sql = "";
				  bool flag = false;
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  ViewData["ItemNo"] = collection["ItemNo"];
				  ViewData["Description"] = collection["Description"];
				  ViewData["Vendor"] = collection["Vendor"];
				  ViewData["Contact"] = collection["Contact"];
				  ViewData["Discount"] = collection["Discount"];


				  if (collection["Description"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Description.Its Mandatory");
				  }

				  if (collection["Vendor"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Vendor.Its Mandatory");
				  }

				  if (collection["Discount"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Discount.Its Mandatory");
				  }





				  //Stationcode
				  string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Supermarket WHERE username = 'admin'"); ;

				  if (errors.Count > 0)
				  {
						_successfailview = "RegisterStockItems";
						ViewData["ItemNo"] = collection["supermarketcode"];
						ViewData["Vendor"] = collection["Vendor"];

				  }
				  else
				  {
						try
						{
							  DateTime getdate = DateTime.Now;
							  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
							  Dictionary<string, string> data = new Dictionary<string, string>();
							  string SelectedChurch = (string)HttpContext.Session["selectedChurch"];



							  data["Description"] = collection["Description"];
							  data["Vendor"] = collection["Vendor"];
							  data["Contact"] = collection["Contact"];
							  data["Discount"] = collection["Discount"];


							  //-----------------------------------------------------------
							  data["CreateBy"] = "ADMIN";
							  data["CreateDate"] = dt.ToString();

							  where["Itemno"] = ID;
							  sql = Logic.DMLogic.UpdateString("StockItems", data, where);
							  flag = Blogic.RunNonQuery(sql);
							  if (flag)
							  {
									response.Add("The Item has been modified successfully.");
									_successfailview = "RegisterStockItems";
							  }
						}
						catch
						{
							  _successfailview = "ErrorPage";
						}
				  }
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;
				  try
				  {

						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Modified stock: " + ViewData["Description"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }

				  return View(_successfailview);
			}




			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]

			public ActionResult deletestock(FormCollection collection, string ID)
			{
				  string sql = "";
				  bool flag = false;
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  ViewData["ItemNo"] = collection["ItemNo"];
				  ViewData["Description"] = collection["Description"];
				  ViewData["Vendor"] = collection["Vendor"];
				  ViewData["Contact"] = collection["Contact"];
				  ViewData["Discount"] = collection["Discount"];


				  if (collection["Description"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Description.Its Mandatory");
				  }

				  if (collection["Vendor"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Vendor.Its Mandatory");
				  }

				  if (collection["Discount"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Discount.Its Mandatory");
				  }





				  //Stationcode
				  string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Supermarket WHERE username = 'admin'"); ;

				  if (errors.Count > 0)
				  {
						_successfailview = "RegisterStockItems";
						ViewData["ItemNo"] = collection["supermarketcode"];
						ViewData["Vendor"] = collection["Vendor"];

				  }
				  else
				  {
						try
						{
							  DateTime getdate = DateTime.Now;
							  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
							  Dictionary<string, string> data = new Dictionary<string, string>();
							  string SelectedChurch = (string)HttpContext.Session["selectedChurch"];



							  data["Description"] = collection["Description"];
							  data["Vendor"] = collection["Vendor"];
							  data["Contact"] = collection["Contact"];
							  data["Discount"] = collection["Discount"];


							  //-----------------------------------------------------------
							  data["CreateBy"] = "ADMIN";
							  data["CreateDate"] = dt.ToString();

							  //where["Itemno"] = ID;
							  sql = "delete from  StockItems where Itemno='" + ID + "'";
							  flag = Blogic.RunNonQuery(sql);
							  if (flag)
							  {
									response.Add("The Item has been deleted successfully.");
									_successfailview = "RegisterStockItems";
							  }
						}
						catch
						{
							  _successfailview = "ErrorPage";
						}
				  }
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;

				  try
				  {

						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Deleted stock: " + collection["Description"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }

				  return View(_successfailview);
			}

			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]


			public ActionResult viewbranch(FormCollection collection)
			{
				  string sql = "";
				  bool flag = false;
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  ViewData["supermarketbranch"] = collection["supermarketbranch"];
				  ViewData["supermarketname"] = collection["supermarketname"];
				  string id4 = collection["id"].ToString();


				  string _action = "";

				  string _successfailview = "viewbranch";
				  if (collection.Count > 0)
				  {
						_action = collection[2];
				  }


				  if (_action == "Search")
				  {
						//-------------------------
						_successfailview = "viewbranchdates";

						ViewData["ModelErrors"] = errors;
						string _startdate = collection["bp.startdate"];
						string _enddate = collection["bp.enddate"];
						string _mpesano = collection["mpesano"];


						string _SQLSelectionCriteria = "";

						if (collection["mpesano"].ToString().Length == 0)
						{
							  errors.Add("You must Indicate the supermarketbranch.");
						}




						if (collection["mpesano"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = "  WHERE supermarketbranch like  '" + _mpesano + "%' ";
						}

						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
						// ----------------------------
						return View(_successfailview);

				  }


				  if (collection["supermarketbranch"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the supermarketbranch.Its Mandatory");
				  }


				  string codeExists = Blogic.RunStringReturnStringValue("select * from Supermarket WHERE supermarketbranch = '" + collection["supermarketbranch"].ToString().Trim() + "' and supermarketcode='" + collection["id"].ToString().Trim() + "'");

				  if (codeExists != "" && codeExists != "0")
				  {
						errors.Add("Please MAKE SURE the supermarketbranch are unique for each Supermarket.This supermarketbranch record already exists.");
				  }



				  //Stationcode
				  string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Supermarket WHERE username = 'admin'");

				  if (errors.Count > 0)
				  {
						_successfailview = "viewbranch";
						ViewData["supermarketbranch"] = collection["supermarketbranch"];


				  }
				  else
				  {
						try
						{
							  DateTime getdate = DateTime.Now;
							  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
							  Dictionary<string, string> data = new Dictionary<string, string>();
							  string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
							  data["supermarketbranch"] = collection["supermarketbranch"];

							  data["supermarketname"] = Blogic.RunStringReturnStringValue("select supermarketname from Supermarket WHERE supermarketcode = '" + collection["id"].ToString().Trim() + "'");

							  data["tillnumber"] = collection["Tillno"];
							  data["attendantphone"] = Blogic.RunStringReturnStringValue("select attendantphone from Supermarket WHERE supermarketcode = '" + collection["id"].ToString().Trim() + "'");


							  //-----------------------------------------------------------
							  data["supermarketcode"] = collection["id"].ToString().Trim();
							  data["CreateBy"] = "ADMIN";
							  data["CreateDate"] = dt.ToString();
							  sql = Logic.DMLogic.InsertString("Supermarket", data);
							  flag = Blogic.RunNonQuery(sql);
							  if (flag)
							  {
									response.Add("The Supermarket has been created successfully.");
									_successfailview = "ChurchRegistrationSuccess";
							  }
						}
						catch
						{
							  _successfailview = "ErrorPage";
						}
				  }
				  ViewData["ModelErrors"] = errors;
				  ViewData["Response"] = response;
				  return View(_successfailview);
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]


			public ActionResult Registerattendands()
			{
				  return View();
			}



			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult Registerattendands(FormCollection collection)
			{
				  string sql = "";
				  bool flag = false;
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  ViewData["selectedemployer"] = collection["selectedemployer"];
				  ViewData["supermarketattendant"] = collection["supermarketattendant"];
				  ViewData["tillnumber"] = collection["tillnumber"];
				  ViewData["attendantphone"] = collection["attendantphone"];


				  if (collection["selectedemployer"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the supermarketname.Its Mandatory");
				  }


				  if (collection["attendantphone"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the attendant phone.Its Mandatory");
				  }

				  if (collection["tillnumber"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the tillnumber");
				  }

				  if (collection["supermarketattendant"].ToString().Length == 0)
				  {
						errors.Add("You must ADD the Attendant name");
				  }


				  try
				  {
						DateTime getdate = DateTime.Now;
						string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
						Dictionary<string, string> data = new Dictionary<string, string>();
						string SelectedChurch = (string)HttpContext.Session["selectedChurch"];

						data["selectedemployer"] = collection["selectedemployer"];

						data["tillnumber"] = collection["tillnumber"];
						data["supermarketattendant"] = collection["supermarketattendant"];


						data["attendantphone"] = collection["attendantphone"];



						//-----------------------------------------------------------
						data["CreateBy"] = "ADMIN";
						data["CreateDate"] = dt.ToString();
						sql = Logic.DMLogic.InsertString("SupermarketAttendant", data);
						flag = Blogic.RunNonQuery(sql);
						if (flag)
						{
							  response.Add("The Attendant has been created successfully.");
							  _successfailview = "The Attendant has been created successfully.";
						}
				  }
				  catch
				  {
						_successfailview = "ErrorPage";
				  }

				  try
				  {
						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCHS", "Registered Supermarket Attendant: " + collection["supermarketattendant"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }
				  return View(_successfailview);
			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]

			public ActionResult EditStockItems()
			{
				  return View();
			}
			[NoDirectAccessAttribute]
			public ActionResult ViewStockItems(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "ViewStockItems";
						string StockID = id.ToString();
						sql = " select * from StockItems WHERE StockID= '" + StockID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{

										  ViewData["StockID"] = rd["StockID"];
										  ViewData["itemNo"] = rd["itemNo"];
										  ViewData["Description"] = rd["Description"];
										  ViewData["ItemNo"] = rd["ItemNo"];
										  ViewData["Vendor"] = rd["Vendor"];
										  ViewData["Contact"] = rd["Contact"];
										  ViewData["Website"] = rd["Website"];
									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);

			}
			[AcceptVerbs(HttpVerbs.Get)]
			[NoDirectAccessAttribute]
			public ActionResult modifystock(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "modifystock";
						string StockID = id.ToString();
						sql = " select * from StockItems WHERE itemno= '" + StockID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{

										  ViewData["StockID"] = rd["StockID"];
										  ViewData["itemNo"] = rd["itemNo"];
										  ViewData["Description"] = rd["Description"];
										  ViewData["ItemNo"] = rd["ItemNo"];
										  ViewData["Vendor"] = rd["Vendor"];
										  ViewData["Contact"] = rd["Contact"];
										  ViewData["Website"] = rd["Website"];
										  ViewData["Discount"] = rd["Discount"];
									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }



				  return View(_viewToDisplayData);

			}

			[AcceptVerbs(HttpVerbs.Get)]
			[NoDirectAccessAttribute]
			public ActionResult deletestock(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "deletestock";
						string StockID = id.ToString();
						sql = " select * from StockItems WHERE itemno= '" + StockID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{

										  ViewData["StockID"] = rd["StockID"];
										  ViewData["itemNo"] = rd["itemNo"];
										  ViewData["Description"] = rd["Description"];
										  ViewData["ItemNo"] = rd["ItemNo"];
										  ViewData["Vendor"] = rd["Vendor"];
										  ViewData["Contact"] = rd["Contact"];
										  ViewData["Website"] = rd["Website"];
										  ViewData["Discount"] = rd["Discount"];
									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);

			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult ModifyStockItems(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["ItemNo"] = collection["ItemNo"];
						data["Description"] = collection["Description"];
						data["Vendor"] = collection["Vendor"];
						data["Contact"] = collection["Contact"];
						data["Website"] = collection["Website"];
						data["Discount"] = collection["Discount"];
						where["StockID"] = id;
						sql = Logic.DMLogic.UpdateString("StockItems", data, where);
						flag = Blogic.RunNonQuery(sql);
						if (flag)
						{
							  response.Add("The record has been updated successfully.");
							  _successfailview = "ChurchRegistrationSuccess";
						}
				  }
				  catch
				  {
						_successfailview = "ErrorPage";
				  }
				  ViewData["Response"] = response;
				  return View(_successfailview);

			}
			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult FilterStockItems()
			{
				  return View();
			}
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult FilterStockItems(FormCollection collection)
			{
				  string _viewtoDisplay = "";
				  string _action = collection[3];
				  if (_action == "Load")
				  {
						//-------------------------
						_viewtoDisplay = "EditStockItems";
						List<string> errors = new List<string>();
						ViewData["ModelErrors"] = errors;
						string _ItemNo = collection["ItemNo"];
						string _Vendor = collection["Vendor"];
						string _SQLSelectionCriteria = "";

						if (collection["ItemNo"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = "(ItemNo='" + _ItemNo + "')";
						}

						if (collection["Vendor"].ToString().Length > 0)
						{
							  _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " (Vendor='" + _Vendor + "')";
						}


						System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
				  }
				  return View(_viewtoDisplay);
			}

			//-------------------------------------

			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult ModifySupermarket(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "ModifySupermarket";
						string uniqueID = id.ToString();
						sql = " select supermarketname,supermarketcode from Supermarket where supermarketcode= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  ViewData["supermarketname"] = rd["supermarketname"];
										  ViewData["supermarketcode"] = rd["supermarketcode"];



									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);

			}





			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult deletevendors(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "deletevendors";
						string uniqueID = id.ToString();
						sql = " select * from tb_vendors where id= '" + id + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  ViewData["id"] = rd["id"];
										  ViewData["vendorname"] = rd["vendorname"];



									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);

			}


			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult deletesupermarket(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "deletesupermarket";
						string uniqueID = id.ToString();
						sql = " select supermarketname,supermarketcode from Supermarket where supermarketcode= '" + uniqueID + "'";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  ViewData["supermarketname"] = rd["supermarketname"];
										  ViewData["supermarketcode"] = rd["supermarketcode"];



									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);

			}




			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult ModifySupermarket(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["supermarketname"] = collection["supermarketname"];
						data["supermarketcode"] = collection["supermarketcode"].ToString();


						sql = "UPDATE Supermarket SET supermarketname='" + data["supermarketname"] + "' WHERE supermarketcode= '" + data["supermarketcode"].ToString() + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "RegisterSupermarkets";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  try
				  {

						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Modified Supermarket: " + collection["supermarketname"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }
				  return View(_viewToDisplayData);
			}



			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult Modifyvendors(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["vendorname"] = collection["vendorname"];



						sql = "UPDATE tb_vendors SET vendorname='" + data["vendorname"] + "' WHERE id= '" + id + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "Registervendors";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  try
				  {

						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Modified vendor: " + collection["vendorname"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }
				  return View(_viewToDisplayData);
			}



			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult deletevendors(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["vendorname"] = collection["vendorname"];



						sql = "DELETE from tb_vendors  WHERE id= '" + id.ToString() + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "Registervendors";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  try
				  {

						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Deleted vendor: " + collection["vendorname"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }
				  return View(_viewToDisplayData);
			}



			[NoDirectAccessAttribute]
			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult deletesupermarket(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["supermarketname"] = collection["supermarketname"];
						data["supermarketcode"] = collection["supermarketcode"].ToString();


						sql = "DELETE from Supermarket  WHERE supermarketcode= '" + data["supermarketcode"].ToString() + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "RegisterSupermarkets";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  try
				  {

						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Deleted Supermarket: " + collection["supermarketname"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }
				  return View(_viewToDisplayData);
			}

			//--------------------------------


			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult Modifyvendors(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "Modifyvendors";
						string uniqueID = id.ToString();
						sql = " select * from tb_vendors where id= '" + id + "' ";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  ViewData["id"] = rd["id"];
										  ViewData["vendorname"] = rd["vendorname"];




									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);

			}
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult mofifybranch(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "mofifybranch";
						string uniqueID = id.ToString();
						sql = " select ID,Tillnumber, supermarketcode,supermarketbranch from Supermarket where ID= '" + id + "' ";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  ViewData["ID"] = rd["ID"];
										  ViewData["supermarketbranch"] = rd["supermarketbranch"];
										  ViewData["Tillnumber"] = rd["Tillnumber"];



									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);

			}


			//--------------------------------
			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult deletebranch(string id)
			{
				  string _viewToDisplayData = "Error";
				  if (id != null)
				  {

						_viewToDisplayData = "deletebranch";
						string uniqueID = id.ToString();
						sql = " select ID,Tillnumber, supermarketcode,supermarketbranch from Supermarket where ID= '" + id + "' ";
						rd = Blogic.RunQueryReturnDataReader(sql);
						using (rd)
						{
							  if (rd != null && rd.HasRows)
							  {
									while (rd.Read())
									{
										  ViewData["ID"] = rd["ID"];
										  ViewData["supermarketbranch"] = rd["supermarketbranch"];
										  ViewData["Tillnumber"] = rd["Tillnumber"];



									}
							  }
						}
				  }
				  else
				  {
						//report the record selection criteria was not right.
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);

			}


			[AcceptVerbs(HttpVerbs.Post)]

			public ActionResult mofifybranch(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["supermarketbranch"] = collection["supermarketbranch"];
						data["Tillnumber"] = collection["Tillnumber"];




						sql = "UPDATE Supermarket SET supermarketbranch='" + data["supermarketbranch"] + "' ,tillnumber='" + data["Tillnumber"] + "'   WHERE ID= '" + id + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "viewbranch";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  try
				  {
						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered Supermarket Branch: " + collection["supermarketname"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }
				  return View(_viewToDisplayData);
			}




			[AcceptVerbs(HttpVerbs.Post)]

			public ActionResult deletebranch(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();


				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["supermarketbranch"] = collection["supermarketbranch"];

						string scode = "";
						string gquery1 = "select supermarketcode from Supermarket  WHERE ID= '" + id + "'";

						scode = Blogic.RunStringReturnStringValue(gquery1);

						string count = "";
						string gquery = "select count(supermarketcode) from Supermarket  WHERE supermarketcode= '" + scode + "'";

						count = Blogic.RunStringReturnStringValue(gquery);

						if (count == "1")
						{
							  _viewToDisplayData = "viewbranch";
							  errors.Add("This branch cannot be deleted.Kindly modify this branch or add a new branch then proceed to delete this branch");
							  response.Add("This branch cannot be deleted.Kindly modify this branch or add a new branch then proceed to delete this branch");

							  ViewData["ModelErrors"] = errors;
							  ViewData["Response"] = response;
							  return View(_viewToDisplayData);

						}

						sql = "Delete from Supermarket  WHERE ID= '" + id + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "viewbranch";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  try
				  {
						Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered Supermarket Branch: " + collection["supermarketname"] + " " + MyGlobalVariables.username, "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
				  }

				  catch
				  {

				  }
				  return View(_viewToDisplayData);
			}



			[AcceptVerbs(HttpVerbs.Post)]

			public ActionResult Removeattendands(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {

						sql = "UPDATE Supermarket SET attendantphone='' ,attendantphone1='',attendantbame='',attendantbame1=''  WHERE ID= '" + id + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "mofifybranch";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}

			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult editattendands(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["attendantbame"] = collection["attendantbame"];
						data["attendantphone"] = collection["attendantphone"];
						data["attendantbame1"] = collection["attendantbame1"];
						data["attendantphone1"] = collection["attendantphone1"];

						string phoneno = collection["attendantphone"];

						if (phoneno != "")
						{

							  int n2;
							  bool isNumeric2 = int.TryParse(phoneno, out n2);

							  if (isNumeric2 == false)
							  {

							  }


							  phoneno = phoneno.Replace("+", "");

							  if ((phoneno.Trim().Length >= 1) && (phoneno.Trim().Length <= 10))
							  {
									phoneno = "254" + phoneno.Substring(1);

							  }

							  if ((phoneno.Trim().Length >= 10) && (phoneno.Trim().Length <= 12))
							  {
									if (phoneno.Substring(0, 3) != "254")
									{
										  errors.Add("The attendant one phone number it must start with 254XXXXXXXXX");
									}

							  }

						}


						string phoneno1 = collection["attendantphone1"];


						if (phoneno1 != "")
						{

							  int n;
							  bool isNumeric = int.TryParse(phoneno1.ToString().Trim(), out n);

							  if (isNumeric == false)
							  {
									errors.Add("The Attendant Mobile Number2  is invalid");
							  }

							  phoneno1 = phoneno1.Replace("+", "");

							  if ((phoneno1.Trim().Length >= 1) && (phoneno1.Trim().Length <= 10))
							  {
									phoneno1 = "254" + phoneno1.Substring(1);

							  }




							  if ((phoneno1.Trim().Length >= 10) && (phoneno1.Trim().Length <= 12))
							  {
									if (phoneno1.Substring(0, 3) != "254")
									{
										  errors.Add("The attendant one phone two it must start with 254XXXXXXXXX");
									}

							  }

						}








						//----------------------------


						sql = "UPDATE Supermarket SET attendantbame='" + data["attendantbame"] + "', attendantphone='" + phoneno + "' ,attendantbame1='" + data["attendantbame1"] + "',attendantphone1='" + phoneno1 + "' WHERE ID= '" + id + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "viewattendands";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}

			//----------------------------------



			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult deleteattendands(FormCollection collection, string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["attendantbame"] = collection["attendantbame"];
						data["attendantphone"] = collection["attendantphone"];
						data["attendantbame1"] = collection["attendantbame1"];
						data["attendantphone1"] = collection["attendantphone1"];



						sql = "UPDATE Supermarket SET attendantbame='', attendantphone='' ,attendantbame1='',attendantphone1='' WHERE ID= '" + id + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "viewattendands";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}

			//----------------------------------





			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult editattendands(string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
				  _viewToDisplayData = "editattendands";
				  string uniqueID = id.ToString();
				  sql = " select * from Supermarket where ID= '" + id + "' ";
				  rd = Blogic.RunQueryReturnDataReader(sql);
				  using (rd)
				  {
						if (rd != null && rd.HasRows)
						{
							  while (rd.Read())
							  {


									ViewData["attendantbame"] = rd["attendantbame"];
									ViewData["attendantphone"] = rd["attendantphone"];
									ViewData["attendantbame1"] = rd["attendantbame1"];
									ViewData["attendantphone1"] = rd["attendantphone1"];
							  }
						}


						else
						{
							  //report the record selection criteria was not right.
							  _viewToDisplayData = "ErrorPage";
						}

				  }

				  return View(_viewToDisplayData);
			}



			[AcceptVerbs(HttpVerbs.Get)]
			public ActionResult deleteattendands(string id)
			{
				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();

				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
				  _viewToDisplayData = "deleteattendands";
				  string uniqueID = id.ToString();
				  sql = " select * from Supermarket where ID= '" + id + "' ";
				  rd = Blogic.RunQueryReturnDataReader(sql);
				  using (rd)
				  {
						if (rd != null && rd.HasRows)
						{
							  while (rd.Read())
							  {


									ViewData["attendantbame"] = rd["attendantbame"];
									ViewData["attendantphone"] = rd["attendantphone"];
									ViewData["attendantbame1"] = rd["attendantbame1"];
									ViewData["attendantphone1"] = rd["attendantphone1"];



							  }
						}


						else
						{
							  //report the record selection criteria was not right.
							  _viewToDisplayData = "ErrorPage";
						}



						return View(_viewToDisplayData);
				  }

				  //----------------------------------

			}



			[AcceptVerbs(HttpVerbs.Post)]
			public ActionResult viewattendands(FormCollection collection, string id)
			{


				  string sql = "";
				  bool flag = false;
				  string EmailAddress = "";
				  string PhoneNumber = "";
				  string Email_Message = "";
				  string SMS_Message = "";
				  List<string> errors = new List<string>();
				  List<string> response = new List<string>();
				  DateTime getdate = DateTime.Now;
				  string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
				  string _viewToDisplayData = "Error";
				  BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

				  try
				  {
						Dictionary<string, string> data = new Dictionary<string, string>();
						data["attendantbame"] = collection["attendantbame"];
						data["attendantphone"] = collection["attendantphone"];
						data["attendantbame1"] = collection["attendantbame1"];
						data["attendantphone1"] = collection["attendantphone1"];


						string codeExists = Blogic.RunStringReturnStringValue("select attendantphone from Supermarket WHERE attendantphone = '" + collection["attendantphone"].ToString().Trim() + "'");

						if (codeExists != "" && codeExists != "0")
						{
							  errors.Add("The attendant one you are registering already exist in the system");
						}



						string phoneno = collection["attendantphone"];

						if (phoneno != "")
						{

							  int n2;
							  bool isNumeric2 = int.TryParse(phoneno, out n2);

							  if (isNumeric2 == false)
							  {
							  }


							  phoneno = phoneno.Replace("+", "");

							  if ((phoneno.Trim().Length >= 1) && (phoneno.Trim().Length <= 10))
							  {
									phoneno = "254" + phoneno.Substring(1);

							  }

							  if ((phoneno.Trim().Length >= 10) && (phoneno.Trim().Length <= 12))
							  {
									if (phoneno.Substring(0, 3) != "254")
									{
										  errors.Add("The attendant one phone number it must start with 254XXXXXXXXX");
									}

							  }

						}


						string phoneno1 = collection["attendantphone1"];


						if (phoneno1 != "")
						{

							  int n;
							  bool isNumeric = int.TryParse(phoneno1.ToString().Trim(), out n);

							  if (isNumeric == false)
							  {
									errors.Add("The Attendant Mobile Number2  is invalid");
							  }

							  phoneno1 = phoneno1.Replace("+", "");

							  if ((phoneno1.Trim().Length >= 1) && (phoneno1.Trim().Length <= 10))
							  {
									phoneno1 = "254" + phoneno1.Substring(1);

							  }




							  if ((phoneno1.Trim().Length >= 10) && (phoneno1.Trim().Length <= 12))
							  {
									if (phoneno1.Substring(0, 3) != "254")
									{
										  errors.Add("The attendant one phone two it must start with 254XXXXXXXXX");
									}

							  }

						}




						string codeExistsx = Blogic.RunStringReturnStringValue("select attendantphone1 from Supermarket WHERE attendantphone1 = '" + collection["attendantphone1"].ToString().Trim() + "'");

						if (codeExistsx != "" && codeExistsx != "0")
						{
							  errors.Add("The attendant two you are registering already exist in the system");
						}


						if (errors.Count > 0)
						{



							  _viewToDisplayData = "viewattendands";
							  ViewData["ModelErrors"] = errors;
							  ViewData["Response"] = response;
							  return View(_viewToDisplayData);
						}

						sql = "UPDATE Supermarket SET attendantbame='" + data["attendantbame"] + "', attendantphone='" + phoneno + "' ,attendantbame1='" + data["attendantbame1"] + "',attendantphone1='" + phoneno1 + "' WHERE ID= '" + id + "'";
						flag = Blogic.RunNonQuery(sql);

						if (flag)
						{
							  _viewToDisplayData = "viewattendands";
						}
				  }
				  catch
				  {
						_viewToDisplayData = "ErrorPage";
				  }

				  return View(_viewToDisplayData);
			}



	  }


}
