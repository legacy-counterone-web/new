﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class ReconciliationController : Controller
    {
        //
        // GET: /Reconciliation/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Getreconciliationreport()
        {
            RecoRepo MemberRepo = new RecoRepo();
            return Json(MemberRepo.Getreconciliation(),JsonRequestBehavior.AllowGet);

        }
    }
}
