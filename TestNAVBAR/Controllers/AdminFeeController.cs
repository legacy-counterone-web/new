﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class AdminFeeController : Controller
    {
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();

        public ActionResult Index()
        {
            return View();
        }
     
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisterAdminFee()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ViewloantypesFee()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Registerloantypes()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult RegisterAdminFee(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["adminfee"] = collection["adminfee"];
            ViewData["adminfee"] = collection["adminfee"];

            if (collection["adminfee"].ToString().Length == 0)
            {
                errors.Add("You must ADD the AdminFee.Its Mandatory");
            }

            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Loancodes WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "RegisterAdminFee";
                ViewData["adminfee"] = collection["adminfee"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    data["adminfee"] = collection["adminfee"];
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    sql = Logic.DMLogic.InsertString("AdminFee", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The record has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;

            
            return View(_successfailview);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyRates(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "ModifyRates";
                string uniqueID = id.ToString();
                sql = " select * from Loancodes where loantypeid= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["loantypeid"] = rd["loantypeid"];
                            ViewData["loantype"] = rd["loantype"];
                            ViewData["loancode"] = rd["loancode"];
                            ViewData["interestrate"] = rd["interestrate"];
                            ViewData["adminfee"] = rd["adminfee"];


                        }
                    }
                }
            }
            else
            {
               
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult modifyloantypes(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "modifyloantypes";
                string uniqueID = id.ToString();
                sql = " select * from tblloantypes where id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["Repayperiod"] = rd["Repayperiod"];
                            ViewData["loancode"] =rd["loancode"];
                            ViewData["loantype"] = rd["loantype"];
                            ViewData["Duration"] = rd["Duration"];
                            ViewData["Adminrate"] = rd["Adminrate"];
                            ViewData["interestrate"] = rd["interestrate"];
                            ViewData["commissionrate"] = rd["commissionrate"];


                        }
                    }
                }
            }
            else
            {

                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyRates(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";
            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();

            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            string _viewToDisplayData = "Error";
            BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data["loantypeid"] = collection["loantypeid"];
                data["interestrate"] = collection["interestrate"].ToString();
                data["adminfee"] = collection["adminfee"].ToString();
                
                sql = "UPDATE Loancodes SET interestrate='" + data["interestrate"] + "',adminfee='" + data["adminfee"] + "' WHERE loantypeid= '" + id + "'";
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    _viewToDisplayData = "RegisterAdminFee";
                }
            }
            catch
            {
                _viewToDisplayData = "ErrorPage";
            }

            try
            {
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN OFFICE", "Registered Admin and intr rate for loan id =: " + collection["loantypeid"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }

            catch
            {
               
            }

            return View(_viewToDisplayData);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult modifyloantypes(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";
            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();

            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            string _viewToDisplayData = "Error";
            BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
               // data["loantype"] = _loantype[1].ToString().Trim();
                data["Duration"] = collection["Duration"];
               // data["loancode"] = collection["loancode"];
                // data["username"] = collection["username"];
                data["loantype"] = collection["loantype"];
                data["Repayperiod"] = collection["Repayperiod"];
                data["Adminrate"] = collection["Adminrate"];
                data["interestrate"] = collection["interestrate"];
                data["commissionrate"] = collection["commissionrate"];
                sql = "UPDATE tblloantypes  SET Duration='" + data["Duration"] + "',loantype='" + data["loantype"] + "',Repayperiod='" + data["Repayperiod"] + "',Adminrate='" + data["Adminrate"] + "',interestrate='" + data["interestrate"] + "',commissionrate='" + data["commissionrate"] + "' WHERE id= '" + id + "'";
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    _viewToDisplayData = "Registerloantypes";
                }
            }
            catch
            {
                _viewToDisplayData = "ErrorPage";
            }

            try
            {
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN OFFICE", "Registered Admin and intr rate for loan id =: " + collection["loantypeid"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }

            catch
            {

            }

            return View(_viewToDisplayData);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewloantypesFee(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";
            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();

            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            string _viewToDisplayData = "Error";
            BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

            try
            {
               
            }
            catch
            {
                _viewToDisplayData = "ErrorPage";
            }

            try
            {
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN OFFICE", "Registered Admin and intr rate for loan id =: " + collection["loantypeid"] + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }

            catch
            {

            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Registerloantypes(FormCollection collection)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
           // ViewData["username"] = collection["username"];
           // ViewData["loancode"] = collection["loancode"];
            ViewData["loantype"] = collection["loantype"];
            ViewData["Duration"] = collection["Duration"];
            ViewData["Adminrate"] = collection["Adminrate"];
            ViewData["interestrate"] = collection["interestrate"];
            ViewData["commissionrate"] = collection["commissionrate"];
           
           



            if (collection["loantype"].ToString().Length == 0)
            {
                errors.Add("You must ADD the loantype.Its Mandatory");
            }
            if (collection["Duration"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Duration.Its Mandatory for notificiations.");
            }

            if (collection["Adminrate"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Adminrate.Its Mandatory");
            }
            if (collection["interestrate"].ToString().Length == 0)
            {
                errors.Add("You must ADD the interestrate.Its Mandatory for notifications");
            }

            if (collection["commissionrate"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate commissionrate.Its Mandatory or same as Mobile number one.");
            }
           



            if (errors.Count > 0)
            {
                _successfailview = "Registerloantypes";
               // ViewData["loancode"] = collection["loancode"];
                ViewData["loantype"] = collection["loantype"];
                ViewData["Duration"] = collection["Duration"];
                ViewData["Adminrate"] = collection["Adminrate"];
                ViewData["interestrate"] = collection["interestrate"];
                ViewData["commissionrate"] = collection["commissionrate"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    string[] _loantype = collection["loantype"].Split('-');
                    //string[] _loancode = collection["loancode"].Split('-');
                    string[] _Duration = collection["Duration"].Split('-');
                    string[] _Repayperiod = collection["Duration"].Split('-');
                    data["loantype"] = _loantype[1].ToString().Trim();
                    data["Duration"] = _Duration[0].ToString().Trim();
                    data["loancode"] = _loantype[0].ToString().Trim();
                    // data["username"] = collection["username"];
                    //data["loantype"] = collection["loantype"];
                    data["Repayperiod"] = _Repayperiod[1].ToString().Trim();
                    data["Adminrate"] = collection["Adminrate"];
                    data["interestrate"] = collection["interestrate"];
                    data["commissionrate"] = collection["commissionrate"];
                   
                    data["username"] = (string)HttpContext.Session["username"];
                   // data["CreateDate"] = dt.ToString();
                  
                    sql = Logic.DMLogic.InsertString("tblloantypes", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The Loantype has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

    }


}



