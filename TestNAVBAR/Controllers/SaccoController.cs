﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class SaccoController : Controller
    {
        public string post_url = "https://counterone.net:8181/counterone/counteronetest/resetpin.php";
        public string post_urls = "https://counterone.net:8181/counterone/counteronetest/newpin.php";
        int logintime = 1;
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        string surname = "";
        string phone = "";
        System.Data.SqlClient.SqlDataReader rd;
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        Dictionary<string, string> where2 = new Dictionary<string, string>();
        Dictionary<string, string> where3 = new Dictionary<string, string>();
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();
        List<string> response = new List<string>();
        //
        // GET: /Sacco/
        public ActionResult ussd()
        {
            return View();
        }
        public ActionResult scams()
        {
            return View();
        }
        public ActionResult Addmembers()
        {
            return View();
        }
        public ActionResult TPmembers()
        {
            return View();
        }
        public ActionResult PendingFLAdmins()
        {
            return View();
        }
        public ActionResult Floataccounts()
        {
            return View();
        }
        //public ActionResult TopupFloataccountstatement()
        //{
        //    return View();
        //}
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TopupFloataccountstatement(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                Session["code"] = id.ToString();
                //ModifyRegisteredUser
                _viewToDisplayData = "TopupFloataccountstatement";
                string uniqueID = id.ToString();
                // sql = " select * from loanapplications a, LoanUsers b WHERE a.borroweruniqueid=b.uniqueid and a.loanid= '" + uniqueID + "'";
                sql = "select  * from tblfloatstatement where code= '" + id + "' order by id DESC";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["description"] = rd["description"];
                            ViewData["Amount"] = rd["Amount"];
                            ViewData["Balance"] = rd["Balance"];
                            ViewData["Date"] = rd["Date"];
                            ViewData["account"] = rd["account"];
                            ViewData["type"] = rd["type"];
                            ViewData["code"] = rd["code"];

                            //ViewData["Mpesaref"] = rd["Mpesaref"];
                            //ViewData["Monthlycontribution"] = rd["Monthlycontribution"];
                            //ViewData["Months"] = rd["Months"].ToString().Replace("12:00:00 AM", "");
                            //ViewData["Rectifiedby"] = rd["Rectifiedby"];
                            //ViewData["idnos"] = rd["idnos"];
                            //ViewData["phonenumber"] = rd["phonenumber"];
                        }
                    }
                }

            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        public ActionResult pendingFloataccounts()
        {
            return View();
        }
        public ActionResult allfloatstatements()
        {
            return View();
        }
        public ActionResult ApprovedFLAdmins()
        {
            return View();
        }
        public ActionResult RejectedFLAdmins()
        {
            return View();
        }
        public ActionResult TPUssdmembers()
        {
            return View();
        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApproveFloatadmin(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "ApproveFloatadmin";
                string uniqueID = id.ToString();
                sql = " select * from tblthirdparty WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["Firstname"] = rd["Firstname"];
                            ViewData["Lastname"] = rd["Lastname"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["phonenumber1"] = rd["phonenumber1"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Country"] = rd["Country"];
                            ViewData["occupation"] = rd["occupation"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["idno"] = rd["idno"];
                            //ViewData["station"] = rd["station"];
                            // ViewData["idno"] = rd["idno"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ApproveFloatadmin(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                string _userssession = HttpContext.Session["username"].ToString();

                data["Approved"] = "1";
                data["Actionedby"] = _userssession;
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblthirdparty", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("The record has been approved successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TopupFloataccount(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "TopupFloataccount";
                string uniqueID = id.ToString();
                sql = " select * from tbllite WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["Amount"] ="0";
                            ViewData["Actionedby"] = rd["Actionedby"];
                            ViewData["approvedby"] = rd["approvedby"];
                            ViewData["approved"] = rd["approved"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["modifiedby"] = rd["modifiedby"];
                            ViewData["Code"] = rd["Code"];
                            ViewData["approval"] = rd["approval"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TopupFloataccount(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                string _userssession = HttpContext.Session["username"].ToString();
                data["Principle"] = collection["Amount"];
                data["Depositamount"] = collection["Amount"];
                data["Approved"] = "2";
                data["Actionedby"] = _userssession;
                data["modifiedby"] = _userssession;
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tbllite", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("Float Amount Added successfully waiting For Approval.");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApproveFloataccount(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "ApproveFloataccount";
                string uniqueID = id.ToString();
                sql = " select * from tbllite WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["Amount"] = rd["Depositamount"];
                            ViewData["Actionedby"] = rd["Actionedby"];
                            ViewData["approvedby"] = rd["approvedby"];
                            ViewData["approved"] = rd["approved"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["modifiedby"] = rd["modifiedby"];
                            ViewData["Code"] = rd["Code"];
                            ViewData["approval"] = rd["approval"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ApproveFloataccount(FormCollection collection, string id)
        {
            //string sql = "";
            //bool flag = false;
            //List<string> errors = new List<string>();
            //List<string> response = new List<string>();
            //try
            //{
            //    Dictionary<string, string> data = new Dictionary<string, string>();
            //    string userid = id;
            //    //string gquery = "select Amount from tbllite WHERE id= '" + userid + "'";
            //    //string Totalamount =  Blogic.RunStringReturnStringValue(gquery);
            //    //double amount = double.Parse(Totalamount);
            //    //Winnie says "If you dont leave your foot print somewhere, when you are gone you will be gone like the wind"
            //    // Winnie was here and this is my footprint

            //    string _userssession = HttpContext.Session["username"].ToString();
            //    string actionedby = collection["modifiedby"].ToString();
            //    if (_userssession != actionedby)
            //    {
            //    double Depositamount = double.Parse(collection["Amount2"].ToString());
            //    //data["Amount"] = Depositamount + Totalamount;
            //    data["Approved"] = "1";
            //    data["approvedby"] = _userssession;
            //    where["id"] = id;
            //    sql = Logic.DMLogic.UpdateString("tbllite", data, where);
            //    flag = Blogic.RunNonQuery(sql);
            //    if (flag)
            //    {
            //        response.Add("The record has been approved successfully.");
            //        _successfailview = "ChurchRegistrationSuccess";
            //    }
            //    }
            //    else
            //    {
            //        response.Add("You are not authorized to action this record");
            //        _successfailview = "ChurchRegistrationSuccess";
            //    }
            //}
            //catch
            //{
            //    _successfailview = "ErrorPage";
            //}
            //ViewData["Response"] = response;
            //return View(_successfailview);


            string sql = "";
            bool flag = false;
            string sql2 = "";
            bool flag2 = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();
                string _userssession = HttpContext.Session["username"].ToString();
                string actionedby = collection["modifiedby"].ToString();
                if (_userssession != actionedby)
                {  
                    string userid = id;
                    string gquery = "select Amount from tbllite WHERE id= '" + userid + "'";
                    string Totalamount =  Blogic.RunStringReturnStringValue(gquery);
                    double amount = double.Parse(Totalamount);
                    double Depositamount = double.Parse(collection["Amount"].ToString());
                    double total = Depositamount + amount;
                    data["Depositamount"] = collection["Amount"];
                    data["Amount"] = total.ToString();
                    data["Approved"] = "10";
                    data["approvedby"] = _userssession;
                    data["modifiedby"] = _userssession;
                    //string date = DateTime.Today.ToString("dd-MM-yyyy");
                    string date = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
                    data1["description"] = "PORTAL FLOAT TOPUP";
                    data1["Amount"] = collection["Amount"];
                    data1["Balance"] = total.ToString();
                    data1["Date"] = DateTime.Today.ToString("dd-MM-yyyy");
                    data1["account"] = "CR";
                    data1["type"] = "F";
                    data1["code"] = collection["Code"];
                    data1["TransDetails"] = "FLOAT TOP UP BY:" + _userssession + " on " + date ;
                    
                    where["id"] = id;
                    sql = Logic.DMLogic.UpdateString("tbllite", data, where);
                    sql2 = Logic.DMLogic.InsertString("tblfloatstatement", data1);
                    flag = Blogic.RunNonQuery(sql);
                    flag2 = Blogic.RunNonQuery(sql2);
                    if (flag)
                    {
                        response.Add("The record has been approved successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                else
                {
                    response.Add("You are not authorized to action this record");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RejectFloatadmin(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "ApproveFloatadmin";
                string uniqueID = id.ToString();
                sql = " select * from tblthirdparty WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["Firstname"] = rd["Firstname"];
                            ViewData["Lastname"] = rd["Lastname"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["phonenumber1"] = rd["phonenumber1"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Country"] = rd["Country"];
                            ViewData["occupation"] = rd["occupation"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["station"] = rd["station"];
                            // ViewData["idno"] = rd["idno"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RejectFloatadmin(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                string _userssession = HttpContext.Session["username"].ToString();

                data["Approved"] = "2";
                data["Actionedby"] = _userssession;
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblthirdparty", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("The record has been approved successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult blacklistmember(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "blacklistmember";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }


        public ActionResult Image(string id)

        {
            SaccoRepo MemberRepo = new SaccoRepo();
            var nj = MemberRepo.GetImages().Where(mbr => mbr.phone == id);
            //MemberRepo.GetImages().Where(mbr => mbr.Phone == id).ToString()
            var d = GetData(id);
            // ViewData["Message"] = GetData().ToString();
            var json = JsonConvert.SerializeObject(d);
            var model = JsonConvert.DeserializeObject<images.RootObject>(json);
            var jsons = JsonConvert.SerializeObject(model.Data);
            ViewData["Message"] = model.Data;
            return View(model.Data);
        }

        public ActionResult GetData(string id)
        {
            //using (DBModels db = new DBModels())
            //{GetAllLoans
            SaccoRepo MemberRepo = new SaccoRepo();
            //string id = "254771364026";
            //return Json(MemberRepo.Getnewpin(), JsonRequestBehavior.AllowGet);
            // List<csvpayments> empList = db.Employees.ToList<csvpayments>();
            return Json(MemberRepo.GetImages().Where(mbr => mbr.phone == id), JsonRequestBehavior.AllowGet);
            //}

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult blacklistmember(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();
                data["blacklist"] = "1";
                where["phonenumber"] = id;
                sql = Logic.DMLogic.UpdateString("LoanUsers", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Resetmember(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Resetmember";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Resetmember(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];
                //checking for admin
                //checking for modules status
                // string postData = collection["mobileno"];
                string Phone = collection["mobileno"];
                string postData = "phone=" + Phone;
                var responses = DataRequestToServer(postData, post_url);
                //SendSms(responses, postData);
                //data["Approved"] = "1";
                response.Add("Pin reset successfully.");
                _successfailview = "ChurchRegistrationSuccess";
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        public string DataRequestToServer(string postData, string post_url)
        {
            try
            {
                //System.Net.ServicePointManager.Expect100Continue = false;   
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(post_url);
                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] data = encoding.GetBytes(postData);
                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;
                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                string responses = new StreamReader(response.GetResponseStream()).ReadToEnd();
                return responses;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        public ActionResult modifyapprovedclients(string id)
        {

            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "modifyapprovedclients";
                string uniqueID = id.ToString();
                sql = " select * from tblfloat WHERE id= '" + id + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["id"] = rd["id"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["Amount"] = rd["Amount"];
                            ViewData["Actionedby"] = rd["Actionedby"];
                            ViewData["Datecreated"] = rd["Datecreated"];


                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        //[NoDirectAccessAttribute]
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult Modifymember(string id)
        //{
        //    string _viewToDisplayData = "Error";
        //    if (id != null)
        //    {

        //        _viewToDisplayData = "Modifymember";
        //        string uniqueID = id.ToString();
        //        sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
        //        rd = Blogic.RunQueryReturnDataReader(sql);
        //        using (rd)
        //        {
        //            if (rd != null && rd.HasRows)
        //            {
        //                while (rd.Read())
        //                {

        //                    ViewData["id"] = rd["Userid"];
        //                    Session["phone"] = rd["Phone"];
        //                    ViewData["mobileno"] = rd["Phone"];
        //                    ViewData["idno"] = rd["idno"];
        //                    ViewData["Accountno"] = rd["Accountno"];
        //                    ViewData["Email"] = rd["Email"];
        //                    ViewData["Loanlimit"] = rd["Loanlimit"];
        //                    ViewData["Lastname"] = rd["Surname"];
        //                    ViewData["Firstname"] = rd["OtherNames"];
        //                    ViewData["Airtimelimit"] = rd["Airtimelimit"];
        //                    ViewData["DateJoined"] = rd["DateJoined"];
        //                    if (rd["idfront"].ToString() == "")
        //                    {
        //                        ViewData["idfront"] = "~/Content/images/idphoto.jpg";
        //                    }
        //                    else
        //                    {
        //                        ViewData["idfront"] = rd["idfront"];
        //                    }
        //                    if (rd["idback"].ToString() == "")
        //                    {
        //                        ViewData["idback"] = "~/Content/images/idphoto.jpg";
        //                    }
        //                    else
        //                    {
        //                        ViewData["idback"] = rd["idback"];
        //                    }
        //                    if (rd["profile"].ToString() == "")
        //                    {
        //                        ViewData["photo"] = "~/Content/images/profil.png";
        //                    }
        //                    else
        //                    {
        //                        ViewData["photo"] = rd["profile"];
        //                    }

        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        //report the record selection criteria was not right.
        //        _viewToDisplayData = "ErrorPage";
        //    }

        //    return View(_viewToDisplayData);

        //}
        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Modifymember(FormCollection collection, string id)
        //{
        //    string sql = "";
        //    string sql1 = "";
        //    bool flag = false;
        //    //string _action = collection[10];
        //    List<string> errors = new List<string>();
        //    List<string> response = new List<string>();
        //    string codeExists = Blogic.RunStringReturnStringValue("select * from tblloanusersapproval WHERE Phone  = '" + collection["mobileno"].ToString().Trim() + "' and adminright='0' and userright='1'");

        //    if (codeExists != "" && codeExists != "0")
        //    {
        //        errors.Add("This account already exists");

        //    }
        //    if (errors.Count > 0)
        //    {
        //        _successfailview = "Modifymember";
        //        ViewData["mobileno"] = collection["mobileno"];



        //    }
        //    else
        //    {

        //        try
        //        {
        //            Dictionary<string, string> data = new Dictionary<string, string>();
        //            Dictionary<string, string> data1 = new Dictionary<string, string>();

        //            string automatic = collection["automatic"];



        //            //checking for admin

        //            //checking for modules status

        //            if (automatic.ToString().Contains("true"))
        //            {
        //                automatic = "1";

        //            }

        //            else
        //            {
        //                automatic = "0";

        //            }
        //            string _userssession = HttpContext.Session["username"].ToString();
        //            //data["id"] = id;
        //            //string[] _loantype = collection["Loantype"].Split('-');
        //            //data["Loantype"] = _loantype[1].ToString().Trim();
        //            // data1["Loantype"] = _loantype[1].ToString().Trim();
        //            //data["Loancode"] = _loantype[0].ToString().Trim();
        //            data["Phone"] = collection["mobileno"];
        //            data["idno"] = collection["idno"];
        //            data["Loanlimit"] = collection["Loanlimit"];
        //            data["Airtimelimit"] = collection["Airtimelimit"];
        //            data["Firstname"] = collection["Firstname"];
        //            data["Lastname"] = collection["Lastname"];
        //            data1["actionedby"] = _userssession;
        //            data["actionedby"] = _userssession;
        //            data1["Rectifiedby"] = _userssession;
        //            data["Rectifiedby"] = _userssession;
        //            data1["Phone"] = collection["mobileno"];
        //            data1["idno"] = collection["idno"];
        //            data1["Loanlimit"] = collection["Loanlimit"];
        //            data1["Airtimelimit"] = collection["Airtimelimit"];
        //            data1["Surname"] = collection["Firstname"];
        //            data1["OtherNames"] = collection["Lastname"];
        //            data1["automatic"] = automatic;
        //            data1["Airautomatic"] = automatic;
        //            data1["useright"] = "1";
        //            data1["adminright"] = "0";
        //            //data["Approved"] = "1";
        //            where["Phone"] = id;
        //            sql1 = Logic.DMLogic.InsertString("tblloanusersapproval", data1);
        //            // sql = Logic.DMLogic.UpdateString("tblloandisbursement", data, where);
        //            flag = Blogic.RunNonQuery(sql1);
        //            if (flag)
        //            {
        //                response.Add("The record has been modified successfully.");
        //                _successfailview = "ChurchRegistrationSuccess";

        //            }
        //        }
        //        catch
        //        {
        //            _successfailview = "ErrorPage";
        //        }
        //    }
        //    ViewData["Response"] = response;
        //    return View(_successfailview);

        //}

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult modifyapprovedclients(FormCollection collection, string id)
        {

            string sql = "";
            bool flag = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";

            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;


            //string _startdate = collection["bp.startdate"];

            //string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");







            ViewData["ModelErrors"] = errors;


            if (collection["Name"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the name .Its Mandatory");
            }
            if (collection["Depositamount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Depositamount.Its Mandatory");
            }





            if (collection["Amount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Amount.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "floatclients";






            }
            else
            {
                try
                {
                           string _userssession = HttpContext.Session["username"].ToString();
                           

                            data["Depositamount"] = collection["Depositamount"];

                    // data["Name"] = collection["Debtorsname"];
                           data["approval"] = "0";

                    

                           data["modifiedby"] = _userssession;
                            //data["Datecreated"] = dt;

                            where["id"] = id.ToString();
                            //-----------------------------------------------------------
                            //data["Createdby"] = _userssession;

                            sql = Logic.DMLogic.UpdateString("tblfloat", data,where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Float client modified successfully waiting approval.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    //Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }


                       


                       
                    
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        public ActionResult approvefloatclients(string id)
        {

            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "approvefloatclients";
                string uniqueID = id.ToString();
                sql = " select * from tblfloat WHERE id= '" + id + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["id"] = rd["id"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["Amount"] = rd["Amount"];
                            ViewData["Actionedby"] = rd["approvedby"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["Depositamount"] = rd["Depositamount"];
                            double amount = double.Parse(rd["Amount"].ToString());
                            double Deposit = double.Parse(rd["Depositamount"].ToString());
                            ViewData["Balance"] = amount + Deposit;


                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult approvefloatclients(FormCollection collection, string id)
        {

            string sql = "";
            bool flag = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";

            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;


            //string _startdate = collection["bp.startdate"];

            //string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");





            string action = collection[6];

            ViewData["ModelErrors"] = errors;


            if (collection["Name"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the name .Its Mandatory");
            }
            //if (collection["Code"].ToString().Length == 0)
            //{
            //    errors.Add("You must ENTER the Code.Its Mandatory");
            //}





            if (collection["Amount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Amount.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "approvefloatclients";






            }
            else
            {
                try
                {
                   
                    string actionedby = collection["Actionedby"];

                    string _userssession = HttpContext.Session["username"].ToString();
                    if (actionedby != _userssession)
                    {
                        if (action == "Approve")
                        {
                            data["Name"] = collection["Name"];
                            data["Amount"] = collection["Balance"];
                            data["approved"] = "1";
                            data["approvedby"] = _userssession;
                            data["approval"] = "1";
                            data["Depositamount"] = "0";
                            where["id"] = id.ToString();
                           

                            sql = Logic.DMLogic.UpdateString("tblfloat", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Float client approved successfully.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    //Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }


                        }
                        //}


                        else if(action == "Reject")
                        {
                            data["approved"] = "1";
                            where["id"] = id.ToString();
                            sql = Logic.DMLogic.UpdateString("tblfloat", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Float client rejected successfully.");
                                _successfailview = "ChurchRegistrationSuccess";
                            }
                        }
                    }
                    else
                    {
                        response.Add("You are not authorized to approve.");
                        _successfailview = "ChurchRegistrationSuccess";

                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        public ActionResult modifyfloatclients(string id)
        {

            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "modifyfloatclients";
                string uniqueID = id.ToString();
                sql = " select * from tblfloat WHERE id= '" + id + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["Amount"] = rd["Amount"];
                            ViewData["Actionedby"] = rd["Actionedby"];
                            ViewData["Datecreated"] = rd["Datecreated"];                          

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult modifyfloatclients(FormCollection collection,string id)
        {

            string sql = "";
            bool flag = false;
            string _viewtoDisplay = "";
           
            string _SQLSelectionCriteria = "";
            string _userssession = HttpContext.Session["username"].ToString();
            string action = collection[4];
            string actionedby = collection["Actionedby"];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            

            //string _startdate = collection["bp.startdate"];

            //string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            


           



            //ViewData["ModelErrors"] = errors;


            if (collection["Name"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the name .Its Mandatory");
            }
            //if (collection["Code"].ToString().Length == 0)
            //{
            //    errors.Add("You must ENTER the Code.Its Mandatory");
            //}





            if (collection["Amount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Amount.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "modifyfloatclients";






            }
            else
            {
                try

                {
                   
                   
                       
                    if (actionedby != _userssession)
                    {
                        if (action == "Approve")
                        {
                            data["Name"] = collection["Name"];
                            data["Amount"] = collection["Amount"];



                            // data["Name"] = collection["Debtorsname"];

                            data["approved"] = "1";
                            data["approval"] = "1";
                            data["approvedby"] = _userssession;
                            //data["Datecreated"] = dt;

                            where["id"] = id.ToString();
                            //-----------------------------------------------------------
                            //data["Createdby"] = _userssession;

                            sql = Logic.DMLogic.UpdateString("tblfloat", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Float client approved successfully.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    //Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }

                        
                    }
                        //}


                        else if (action == "Reject")
                       // {
                            {
                        data["approved"] = "1";
                        where["id"] = id.ToString();
                        sql = Logic.DMLogic.UpdateString("tblfloat", data, where);
                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            response.Add("Float client approved successfully.");
                            _successfailview = "ChurchRegistrationSuccess";
                        }
                    }
                    }
                    else
                    {
                        response.Add("You are not authorized to approve.");
                        _successfailview = "ChurchRegistrationSuccess";

                    }
                }//}
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View("floatclients");
           // return View(_successfailview);
        }
        public ActionResult floatclients()
        {

            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult floatclients(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";

            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);


            //string _startdate = collection["bp.startdate"];

            //string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            string codeExists = Blogic.RunStringReturnStringValue("SELECT * FROM tblfloat where Name='" + collection["Name"] + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Client already exists");

            }


            string gquery = "select max(id) from tblfloat ";
            string accountno = "";
            accountno = Blogic.RunStringReturnStringValue(gquery);

            int accountnos = Convert.ToInt32(accountno);



            ViewData["ModelErrors"] = errors;


            if (collection["Name"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the name .Its Mandatory");
            }
            //if (collection["Code"].ToString().Length == 0)
            //{
            //    errors.Add("You must ENTER the Code.Its Mandatory");
            //}

           



            if (collection["Amount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Amount.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "floatclients";
               





            }
            else
            {
                try
                {
                    string _userssession = HttpContext.Session["username"].ToString();
                    data["Name"] = collection["Name"];
                    data["Amount"] = collection["Amount"];
                    data["Code"] = (accountnos + 1).ToString();//collection["Code"];

                    data["Actionedby"] = _userssession;
                    // data["Name"] = collection["Debtorsname"];
                    data["Datecreated"] = dt;
                    data["approved"] = "0";
                    data["approval"] = "0";

                    data["approvedby"] = _userssession;
                    //data["Datecreated"] = dt;


                    //-----------------------------------------------------------
                    //data["Createdby"] = _userssession;

                    sql = Logic.DMLogic.InsertString("tblfloat", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Float client waiting approval.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            //Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }

                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View("floatclients");
        }

        public ActionResult Floatapprovals()
        {
            return View();
        }

        public ActionResult Float()
        {
            return View();
        }
        public ActionResult members()
        {
            return View();
        }
        public ActionResult Pendingmembers()
        {
            return View();
        }
        public ActionResult memberspendingapproval()
        {
            //HttpContext.Session["Tp"] = "1";
            return View();
        }
        public ActionResult Ptmemberspendingapproval()
        {
            return View();
        }
        public ActionResult Membersrejected()
        {
            return View();
        }
        public ActionResult floatpendingmember()
        {
            return View();
        }
        public ActionResult floatapprovedmembers()
        {
            return View();
        }
        public ActionResult pending()
        {
            return View();
        }
        public ActionResult viewcommission()
        {
            return View();
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ThirdParty()
        {
            return View();
        }
        public ActionResult ThirdPartyPending()
        {
            return View();
        }
        public ActionResult SalvonAdvance()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult updateauto()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult updateAirauto()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateauto(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                string accountno = collection["accountno"];
                string automatic = collection["automatic"];
                string automatics = collection["automatics"];


                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }

                else
                {
                    automatic = "0";

                }
                data["automatic"] = automatic;
                where["Accountno"] = accountno;
                sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("Members  Loan Auto rights assigned  successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult updateAirauto(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                string accountno = collection["accountno"];
                string automatic = collection["automatic"];
                string automatics = collection["Amount"];


                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }

                else
                {
                    automatic = "0";

                }
                data["Airautomatic"] = automatic;
                where["Accountno"] = accountno;
                sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("Members  AirTime Auto rights assigned  successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        public ActionResult AutoDisburse()
        {
            return View();
        }
        public ActionResult Newpin()
        {
            return View();
        }
        public ActionResult getloantodisburse()
        {
            return View();
        }
        public ActionResult GetRejectedMembersss()
        {
            return View();
        }
        public ActionResult AirtimeAutoDisburse()
        {
            return View();
        }
        public ActionResult SaccoMembers()
        {
            return View();
        }
        public JsonResult Saccoloans()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Getpin()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.Getnewpin(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult Getussdpin()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.Getussdpin(), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMembers()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }
       
        public JsonResult Getloan()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Getloans()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.Getloanstodisburse(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetThirdParty()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllThirdParty(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetSalvonadvanceMembers()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllSalvonadvance(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Getcommission()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllcommission(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetMemberss()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult  GetRejectedMemberss()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetRejectedMembers(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetpendingMembers()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllpendingMember(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetThirdPartypending()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetThirdPartypending(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetautogMembers()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllAutoMembers());

        }
        public ActionResult GetAirtimeautoMembers()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllAirTimeAutoMembers());

        }
        public JsonResult SearchMembers()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }

        //public ActionResult SearchMembers()
        //{
        //    SaccoRepo MemberRepo = new SaccoRepo();
        //    var population = MemberRepo.GetAllMember();
        //    return Json(population, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult AddMember()
        {

            return View();
        }
        public ActionResult Loantodisburse()
        {

            return View();
        }
        public ActionResult AddTrans()
        {

            return View();
        }
        public ActionResult Createussdpin(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.Getussdpin().Find(mbr => mbr.Userid == id));
        }
        public ActionResult Edit(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.GetAllMember().Find(mbr => mbr.Userid == id));
        }
        public ActionResult Createpin(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.Getmemberspin().Find(mbr => mbr.Userid == id));
        }

        public ActionResult Editpending(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.GetAllpendingMember().Find(mbr => mbr.Userid == id));
        }
        public ActionResult EditThirdParty(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.GetThirdPartypending().Find(mbr => mbr.Userid == id));
        }
        public ActionResult Rejectpending(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.GetAllpending().Find(mbr => mbr.Userid == id));
        }
        public ActionResult RejectTP(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.GetThirdPartypending().Find(mbr => mbr.Userid == id));
        }
        public ActionResult Dels(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.GetAllpendingMember().Find(mbr => mbr.Userid == id));
        }
        public ActionResult deleteussdpin(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.Getussdpin().Find(mbr => mbr.Userid == id));
        }
        [HttpPost]
        public JsonResult deleteussdpin(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.deleteussdpin(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpPost]
        public JsonResult Createussdpin(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.Createussdpin(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpPost]
        public JsonResult AddMember(SafrikaMembers MbrDet, FormCollection collection)

        {
            try
            {
                //phone = collection["Phone"];
                //surname = collection["Surname"];
                string messagetoadmin = surname + "Your now been registeed as Tishel sacco member";
                SendSms(messagetoadmin, phone);
                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.AddMember(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        [HttpPost]
        public JsonResult Loantodisburse(SafrikaMembers MbrDet, FormCollection collection)

        {
            try
            {
                //phone = collection["Phone"];
                //surname = collection["Surname"];
                string messagetoadmin = surname + "Your now been registeed as Tishel sacco member";
                SendSms(messagetoadmin, phone);
                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.Loantodisburse(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        [HttpPost]
        public JsonResult AddTrans(testtransaction MbrDet)

        {
            try
            {

                Loanrepo MemberRepo = new Loanrepo();
                MemberRepo.Addtrans(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            MemberRepo.DeleteMember(id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Editpending(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.UpdateMember(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpPost]
        public JsonResult EditThirdParty(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.UpdateUpdateTP(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }

        [HttpPost]
        public JsonResult Rejectpending(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.RejectpendingMember(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpPost]
        public JsonResult RejectTP(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.RejectTPMember(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }

        [HttpPost]
        public JsonResult Edit(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.UpdateMember(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }

        [HttpPost]
        public JsonResult Createpin(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.Createpin(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpPost]
        public JsonResult Dels(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.DelMember(MemberUpdateDet);
                return Json("Records Deleted waiting approval.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpGet]
        public PartialViewResult MemberDetails()
        {

            return PartialView("_MemberDetails");

        }
        [HttpGet]
        public PartialViewResult LoanDetails()
        {

            return PartialView("LoanDetails");

        }
        public ActionResult ApproveRejectedmember(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            bool flag1 = false;
            string _action = collection[9];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {

                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];


                data["useright"] = "0";
                data["adminright"] = "0";
                data["Membership"] = "0";
                data["Reject"] = "0";
                where["Phone"] = id;
                sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                flag = Blogic.RunNonQuery(sql);


                if (flag)
                {
                    response.Add("The record has been Reapproved successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }



            }
            catch
            {
                _successfailview = "ErrorPage";
            }



            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApproveRejectedmember(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "ApproveRejectedmember";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["id"] = rd["Userid"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        public ActionResult Rejectpendingmember(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            bool flag1 = false;
            string _action = collection[9];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {

                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];


                data["useright"] = "0";
                data["adminright"] = "0";
                data["Membership"] = "0";
                data["Reject"] = "1";
                where["Phone"] = id;
                sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                flag = Blogic.RunNonQuery(sql);


                if (flag)
                {
                    response.Add("The record has been rejected successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }



            }
            catch
            {
                _successfailview = "ErrorPage";
            }



            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Rejectpendingmember(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Rejectpendingmember";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["id"] = rd["Userid"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
       
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Modifypending(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Modifypending";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            Session["idno"] = rd["idno"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Modifypending(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            bool flag1 = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];
                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }

                else
                {
                    automatic = "0";

                }
                string _userssession = HttpContext.Session["username"].ToString();
                //data["id"] = id;
                //string[] _loantype = collection["Loantype"].Split('-');
                //data["Loantype"] = _loantype[1].ToString().Trim();
                //data["Loancode"] = _loantype[0].ToString().Trim();
                //data["Phone"] = collection["mobileno"];
                //data["idno"] = collection["idno"];
                //data["Loanlimit"] = collection["Loanlimit"];
                //data["Airtimelimit"] = collection["Airtimelimit"];
                //data["Firstname"] = collection["Firstname"];
                //data["Lastname"] = collection["Lastname"];
                data1["actionedby"] = _userssession;
                data1["Rectifiedby"] = _userssession;
                data1["Phone"] = collection["mobileno"];
                data1["idno"] = collection["idno"];
                data1["Loanlimit"] = collection["Loanlimit"];
                data1["Airtimelimit"] = collection["Airtimelimit"];
                data1["Surname"] = collection["Firstname"];
                data1["OtherNames"] = collection["Lastname"];
                data1["automatic"] = automatic;
                data1["Airautomatic"] = automatic;
                data1["useright"] = "1";
                data1["adminright"] = "0";
                data1["Tp"] = "0";
                // data["Approved"] = "1";
                where["Phone"] = id;
                sql = Logic.DMLogic.InsertString("tblloanusersapproval", data1);
                // sql = Logic.DMLogic.UpdateString("tblloandisbursement", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }




            }
            catch
            {
                _successfailview = "ErrorPage";
            }



            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifypendingTP(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "ModifypendingTP";
                string uniqueID = id.ToString();
                sql = " select a.idfront,a.idback,a.photo,a.Userid,a.Phone,a.idno,a.Accountno,a.Email,a.Loanlimit,a.Surname,a.OtherNames,a.Airtimelimit,a.DateJoined," +
                    "b.occupation,b.description,b.location,b.salary,b.hrcomment,b.company,b.coordinates,b.date," +
                    "b.username,b.employer,b.company,b.employmentyear,b.grosspay,b.netpay,b.loanlimit,b.email from tblloanusers a INNER JOIN tbldatamain b on a.Phone=b.phone WHERE a.Phone= '" + uniqueID + "'";



                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];

                            ViewData["coordinates"] = rd["coordinates"];
                            ViewData["date"] = rd["date"];
                            ViewData["username"] = rd["username"];
                            ViewData["employer"] = rd["employer"];
                            ViewData["company"] = rd["company"];
                            ViewData["employmentyear"] = rd["employmentyear"];
                            ViewData["grosspay"] = rd["grosspay"];
                            ViewData["netpay"] = rd["netpay"];
                            ViewData["loanlimit"] = rd["loanlimit"];
                            ViewData["email"] = rd["email"];

                            ViewData["occupation"] = rd["occupation"];
                            ViewData["description"] = rd["description"];
                            ViewData["location"] = rd["location"];
                            ViewData["salary"] = rd["salary"];
                            ViewData["hrcomment"] = rd["hrcomment"];
                            ViewData["company"] = rd["company"];

                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                            if (rd["idfront"].ToString() == "")
                            {
                                ViewData["idfront"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idfront"] = rd["idfront"];
                            }
                            if (rd["idback"].ToString() == "")
                            {
                                ViewData["idback"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idback"] = rd["idback"];
                            }
                            if (rd["photo"].ToString() == "")
                            {
                                ViewData["photo"] = "~/Content/images/profil.png";
                            }
                            else
                            {
                                ViewData["photo"] = rd["photo"];
                            }

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifypendingTP(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            string sql2 = "";
            bool flag = false;
            bool flag1 = false;
            bool flag2 = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();
                Dictionary<string, string> data2 = new Dictionary<string, string>();

                string automatic = collection["automatic"];



                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }

                else
                {
                    automatic = "0";
                }
                string _userssession = HttpContext.Session["username"].ToString();
                //data["Phone"] = collection["mobileno"];
                //data["idno"] = collection["idno"];
                //data["Loanlimit"] = collection["Loanlimit"];
                //data["Airtimelimit"] = collection["Airtimelimit"];
                //data["Firstname"] = collection["Firstname"];
                //data["Lastname"] = collection["Lastname"];
                data1["actionedby"] = _userssession;
                //data["actionedby"] = _userssession;
                data1["Rectifiedby"] = _userssession;
                data1["Phone"] = collection["mobileno"];
                data1["idno"] = collection["idno"];
                data1["Loanlimit"] = collection["Loanlimit"];
                data1["Airtimelimit"] = collection["Airtimelimit"];
                data1["Surname"] = collection["Firstname"];
                data1["OtherNames"] = collection["Lastname"];
                data1["automatic"] = automatic;
                data1["Airautomatic"] = automatic;
                data["Loancode"] = "CASH ADVANCE";
                data["loantype"] = "L0003";
                data2["Loanlimit"] = collection["Loanlimit"];
                data1["useright"] = "1";
                data1["adminright"] = "0";
                data["Tp"] = "1";
                where["Phone"] = id;
                where2["mobileno"] = id;
                sql = Logic.DMLogic.InsertString("tblloanusersapproval", data1);
                sql1 = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                sql2 = Logic.DMLogic.UpdateString("tblloandisbursement", data2, where2);
                flag = Blogic.RunNonQuery(sql);
                flag1 = Blogic.RunNonQuery(sql1);
                flag2 = Blogic.RunNonQuery(sql2);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }




            }
            catch
            {
                _successfailview = "ErrorPage";
            }



            ViewData["Response"] = response;
            return View(_successfailview);

        }



        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult viewFloatmember(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "viewFloatmember";
                string uniqueID = id.ToString();
                sql = " select a.idfront,a.idback,a.photo,a.Userid,a.Phone,a.idno,a.Accountno,a.Email,a.Loanlimit,a.Surname,a.OtherNames,a.Airtimelimit,a.DateJoined," +
                    "b.occupation,b.description,b.location,b.salary,b.hrcomment,b.company,b.coordinates,b.date," +
                    "b.username,b.employer,b.company,b.employmentyear,b.grosspay,b.netpay,b.loanlimit,b.email from tblloanusers a INNER JOIN tbldatamain b on a.Phone=b.phone WHERE a.Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["coordinates"] = rd["coordinates"];
                            ViewData["date"] = rd["date"];
                            ViewData["username"] = rd["username"];
                            ViewData["employer"] = rd["employer"];
                            ViewData["company"] = rd["company"];
                            ViewData["employmentyear"] = rd["employmentyear"];
                            ViewData["grosspay"] = rd["grosspay"];
                            ViewData["netpay"] = rd["netpay"];
                            ViewData["loanlimit"] = rd["loanlimit"];
                            ViewData["email"] = rd["email"];

                            ViewData["occupation"] = rd["occupation"];
                            ViewData["description"] = rd["description"];
                            ViewData["location"] = rd["location"];
                            ViewData["salary"] = rd["salary"];
                            ViewData["hrcomment"] = rd["hrcomment"];
                            ViewData["company"] = rd["company"];

                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                            if (rd["idfront"].ToString() == "")
                            {
                                ViewData["idfront"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idfront"] = rd["idfront"];
                            }
                            if (rd["idback"].ToString() == "")
                            {
                                ViewData["idback"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idback"] = rd["idback"];
                            }
                            if (rd["photo"].ToString() == "")
                            {
                                ViewData["photo"] = "~/Content/images/profil.png";
                            }
                            else
                            {
                                ViewData["photo"] = rd["photo"];
                            }

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifypendingFloat(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "ModifypendingFloat";
                string uniqueID = id.ToString();
                sql = " select a.idfront,a.idback,a.photo,a.Userid,a.Phone,a.idno,a.Accountno,a.Email,a.Loanlimit,a.Surname,a.OtherNames,a.Airtimelimit,a.DateJoined," +
                    "b.occupation,b.description,b.location,b.salary,b.hrcomment,b.company,b.coordinates,b.date," +
                    "b.username,b.employer,b.company,b.employmentyear,b.grosspay,b.netpay,b.loanlimit,b.email from tblloanusers a INNER JOIN tbldatamain b on a.Phone=b.phone WHERE a.Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["coordinates"] = rd["coordinates"];
                            ViewData["date"] = rd["date"];
                            ViewData["username"] = rd["username"];
                            ViewData["employer"] = rd["employer"];
                            ViewData["company"] = rd["company"];
                            ViewData["employmentyear"] = rd["employmentyear"];
                            ViewData["grosspay"] = rd["grosspay"];
                            ViewData["netpay"] = rd["netpay"];
                            ViewData["loanlimit"] = rd["loanlimit"];
                            ViewData["email"] = rd["email"];

                            ViewData["occupation"] = rd["occupation"];
                            ViewData["description"] = rd["description"];
                            ViewData["location"] = rd["location"];
                            ViewData["salary"] = rd["salary"];
                            ViewData["hrcomment"] = rd["hrcomment"];
                            ViewData["company"] = rd["company"];

                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                            if (rd["idfront"].ToString() == "")
                            {
                                ViewData["idfront"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idfront"] = rd["idfront"];
                            }
                            if (rd["idback"].ToString() == "")
                            {
                                ViewData["idback"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idback"] = rd["idback"];
                            }
                            if (rd["photo"].ToString() == "")
                            {
                                ViewData["photo"] = "~/Content/images/profil.png";
                            }
                            else
                            {
                                ViewData["photo"] = rd["photo"];
                            }

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifypendingFloat(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            string sql2 = "";
            bool flag = false;
            bool flag1 = false;
            bool flag2 = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();
                Dictionary<string, string> data2 = new Dictionary<string, string>();

                string automatic = collection["automatic"];
                

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }


                else
                {
                    automatic = "0";
                }
                string _userssession = HttpContext.Session["username"].ToString();                
                data["actionedby"] = _userssession;
                data["Rectifiedby"] = _userssession;
                data1["hrcomment"] = collection["hrcomment"];                
                data["adminright"] = "1";               
                data["useright"] = "1";
                where["Phone"] = id;
                where2["phone"] = id;
                sql1 = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                sql2 = Logic.DMLogic.UpdateString("tbldatamain", data1, where2);
               
                flag1 = Blogic.RunNonQuery(sql1);
                flag2 = Blogic.RunNonQuery(sql2);
                if (flag1)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }




            }
            catch
            {
                _successfailview = "ErrorPage";
            }



            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyTP(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                
                _viewToDisplayData = "ModifyTP";
                string uniqueID = id.ToString();
                sql = " select a.idfront,a.idback,a.photo,a.Userid,a.Phone,a.idno,a.Accountno,a.Email,a.Loanlimit,a.Surname,a.OtherNames,a.Airtimelimit,a.DateJoined," +
                    "b.occupation,b.description,b.location,b.salary,b.hrcomment,b.company,b.coordinates,b.date," +
                    "b.username,b.employer,b.company,b.employmentyear,b.grosspay,b.netpay,b.loanlimit,b.email from tblloanusers a INNER JOIN tbldatamain b on a.Phone=b.phone WHERE a.Phone= '" + uniqueID + "'";

      

        rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                           
                            ViewData["coordinates"] = rd["coordinates"];
                            ViewData["date"] = rd["date"];
                            ViewData["username"] = rd["username"];
                            ViewData["employer"] = rd["employer"];
                            ViewData["company"] = rd["company"];
                            ViewData["employmentyear"] = rd["employmentyear"];
                            ViewData["grosspay"] = rd["grosspay"];
                            ViewData["netpay"] = rd["netpay"];
                            ViewData["loanlimit"] = rd["loanlimit"];
                            ViewData["email"] = rd["email"];

                            ViewData["occupation"] = rd["occupation"];
                            ViewData["description"] = rd["description"];
                            ViewData["location"] = rd["location"];
                            ViewData["salary"] = rd["salary"];
                            ViewData["hrcomment"] = rd["hrcomment"];
                            ViewData["company"] = rd["company"];

                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                            if (rd["idfront"].ToString() == "")
                            {
                                ViewData["idfront"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idfront"] = rd["idfront"];
                            }
                            if (rd["idback"].ToString() == "")
                            {
                                ViewData["idback"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idback"] = rd["idback"];
                            }
                            if (rd["photo"].ToString() == "")
                            {
                                ViewData["photo"] = "~/Content/images/profil.png";
                            }
                            else
                            {
                                ViewData["photo"] = rd["photo"];
                            }

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyTP(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            bool flag1 = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];



                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }

                else
                {
                    automatic = "0";
                }
                string _userssession = HttpContext.Session["username"].ToString();
                //data["Phone"] = collection["mobileno"];
                //data["idno"] = collection["idno"];
                //data["Loanlimit"] = collection["Loanlimit"];
                //data["Airtimelimit"] = collection["Airtimelimit"];
                //data["Firstname"] = collection["Firstname"];
                //data["Lastname"] = collection["Lastname"];
                data1["actionedby"] = _userssession;
                //data["actionedby"] = _userssession;
                data1["Rectifiedby"] = _userssession;
                data1["Phone"] = collection["mobileno"];
                data1["idno"] = collection["idno"];
                data1["Loanlimit"] = collection["Loanlimit"];
                data1["Airtimelimit"] = collection["Airtimelimit"];
                data1["Surname"] = collection["Firstname"];
                data1["OtherNames"] = collection["Lastname"];
                data1["automatic"] = automatic;
                data1["Airautomatic"] = automatic;
                data["Loancode"] = "CASH ADVANCE";
                data["loantype"] = "L0003";
                data1["useright"] = "1";
                data1["adminright"] = "0";
                data["Tp"] = "1";
                where["Phone"] = id;
                sql = Logic.DMLogic.InsertString("tblloanusersapproval", data1);
                sql1 = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                flag = Blogic.RunNonQuery(sql);
                flag1 = Blogic.RunNonQuery(sql1);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }




            }
            catch
            {
                _successfailview = "ErrorPage";
            }



            ViewData["Response"] = response;
            return View(_successfailview);

        }
        
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult viewussdtpmember(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                
                _viewToDisplayData = "viewussdtpmember";
                string uniqueID = id.ToString();
                sql = " select * from tbldatamain WHERE Phone= '" + uniqueID + "'";
                
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                             ViewData["occupation"] = rd["occupation"];
                            Session["phone"] = rd["phone"];
                            ViewData["description"] = rd["description"];
                            ViewData["location"] = rd["location"];
                            ViewData["phone"] = rd["phone"];
                            ViewData["idno"] = rd["idno"];
                            
                            ViewData["coordinates"] = rd["coordinates"];
                            ViewData["date"] = rd["date"];
                            ViewData["status"] = rd["status"];
                            ViewData["employer"] = rd["employer"];
                            ViewData["company"] = rd["company"];

                            ViewData["netpay"] = rd["netpay"];
                            ViewData["gender"] = rd["gender"];
                            ViewData["loanlimit"] = rd["loanlimit"];
                            ViewData["hrcomment"] = rd["hrcomment"];
                            ViewData["email"] = rd["email"];
                            ViewData["firstname"] = rd["firstname"];
                            ViewData["lastname"] = rd["lastname"];

                            ViewData["employmentyear"] = rd["employmentyear"];
                            ViewData["salary"] = rd["salary"];
                            ViewData["hr"] = rd["hr"];
                            ViewData["hrcomment"] = rd["hrcomment"];
                            ViewData["grosspay"] = rd["grosspay"];                           

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult viewussdtpmember(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            bool flag1 = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];



                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }

                else
                {
                    automatic = "0";

                }
                string _userssession = HttpContext.Session["username"].ToString();
                //data["id"] = id;
                //string[] _loantype = collection["Loantype"].Split('-');
                //data["Loantype"] = _loantype[1].ToString().Trim();
                //data["Loancode"] = _loantype[0].ToString().Trim();
                //data["Phone"] = collection["mobileno"];
                //data["idno"] = collection["idno"];
                //data["Loanlimit"] = collection["Loanlimit"];
                //data["Airtimelimit"] = collection["Airtimelimit"];
                //data["Firstname"] = collection["Firstname"];
                //data["Lastname"] = collection["Lastname"];
                data1["actionedby"] = _userssession;
                //data["actionedby"] = _userssession;
                //data["Rectifiedby"] = _userssession;
                // data1["Loantype"] = _loantype[1].ToString().Trim();
                // data1["Loancode"] = _loantype[0].ToString().Trim();
                data1["Rectifiedby"] = _userssession;
                data1["Phone"] = collection["mobileno"];
                data1["idno"] = collection["idno"];
                data1["Loanlimit"] = collection["Loanlimit"];
                data1["Airtimelimit"] = collection["Airtimelimit"];
                data1["Surname"] = collection["Firstname"];
                data1["OtherNames"] = collection["Lastname"];
                data1["automatic"] = automatic;
                data1["Airautomatic"] = automatic;
                data1["useright"] = "1";
                data1["adminright"] = "0";
                data["Tp"] = "1";
                where["Phone"] = id;
                sql = Logic.DMLogic.InsertString("tblloanusersapproval", data1);
                sql1 = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                flag = Blogic.RunNonQuery(sql);
                flag1 = Blogic.RunNonQuery(sql1);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }




            }
            catch
            {
                _successfailview = "ErrorPage";
            }



            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Modifymember(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Modifymember";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            Session["idno"] = rd["idno"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Modifymember(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];



                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }

                else
                {
                    automatic = "0";

                }
                string _userssession = HttpContext.Session["username"].ToString();
                //data["id"] = id;
                // string[] _loantype = collection["Loantype"].Split('-');
                //data["Loantype"] = _loantype[1].ToString().Trim();
                // data1["Loantype"] = _loantype[1].ToString().Trim();
                // data["Loancode"] = _loantype[0].ToString().Trim();
                data["Phone"] = collection["mobileno"];
                data["idno"] = collection["idno"];
                data["Loanlimit"] = collection["Loanlimit"];
                data["Airtimelimit"] = collection["Airtimelimit"];
                data["Firstname"] = collection["Firstname"];
                data["Lastname"] = collection["Lastname"];
                data1["actionedby"] = _userssession;
                data["actionedby"] = _userssession;
                data1["Rectifiedby"] = _userssession;
                data["Rectifiedby"] = _userssession;
                data1["Phone"] = collection["mobileno"];
                data1["idno"] = collection["idno"];
                data1["Loanlimit"] = collection["Loanlimit"];
                data1["Airtimelimit"] = collection["Airtimelimit"];
                data1["Surname"] = collection["Firstname"];
                data1["OtherNames"] = collection["Lastname"];
                data1["automatic"] = automatic;
                data1["Airautomatic"] = automatic;
                data1["useright"] = "1";
                data1["adminright"] = "0";
                data1["Tp"] = "0";
                //data["Approved"] = "1";
                where["Phone"] = id;
                sql1 = Logic.DMLogic.InsertString("tblloanusersapproval", data1);
                // sql = Logic.DMLogic.UpdateString("tblloandisbursement", data, where);
                flag = Blogic.RunNonQuery(sql1);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }



        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyTPmember(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Modifymember";
                string uniqueID = id.ToString();
                sql = " select a.Userid,a.Phone,a.idno,a.Accountno,a.Email,a.Loanlimit,a.Surname,a.OtherNames,a.Airtimelimit,a.DateJoined,b.occupation,b.description,b.location,b.salary,b.hrcomment,b.company from tblloanusers a INNER JOIN tbldatamain b on a.Phone=b.phone WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyTPmember(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];



                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";

                }

                else
                {
                    automatic = "0";

                }
                string _userssession = HttpContext.Session["username"].ToString();
                //data["id"] = id;
                // string[] _loantype = collection["Loantype"].Split('-');
                //data["Loantype"] = _loantype[1].ToString().Trim();
                // data1["Loantype"] = _loantype[1].ToString().Trim();
                // data["Loancode"] = _loantype[0].ToString().Trim();
                data["Phone"] = collection["mobileno"];
                data["idno"] = collection["idno"];
                data["Loanlimit"] = collection["Loanlimit"];
                data["Airtimelimit"] = collection["Airtimelimit"];
                data["Firstname"] = collection["Firstname"];
                data["Lastname"] = collection["Lastname"];
                data1["actionedby"] = _userssession;
                data["actionedby"] = _userssession;
                data1["Rectifiedby"] = _userssession;
                data["Rectifiedby"] = _userssession;
                data1["Phone"] = collection["mobileno"];
                data1["idno"] = collection["idno"];
                data1["Loanlimit"] = collection["Loanlimit"];
                data1["Airtimelimit"] = collection["Airtimelimit"];
                data1["Surname"] = collection["Firstname"];
                data1["OtherNames"] = collection["Lastname"];
                data1["automatic"] = automatic;
                data1["Airautomatic"] = automatic;
                data1["useright"] = "1";
                data1["adminright"] = "0";
                //data["Approved"] = "1";
                where["Phone"] = id;
                sql1 = Logic.DMLogic.InsertString("tblloanusersapproval", data1);
                // sql = Logic.DMLogic.UpdateString("tblloandisbursement", data, where);
                flag = Blogic.RunNonQuery(sql1);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult pendingapproval(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "pendingapproval";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusersapproval WHERE  adminright=0 and  Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["id"] = rd["Userid"];
                            Session["rights"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];
                            //HttpContext.Session["Tp"] = rd[" Tp"];
                           
                            ViewData["Actionedby"] = rd["actionedby"];


                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult pendingapproval(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            string sql2 = "";
            string sql3 = "";
            bool flag = false;
            bool flag1 = false;
            bool flag2 = false;
            bool flag3 = false;
            string _action = collection[9];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            if (_action == "Approve")
            {
                try
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    Dictionary<string, string> data1 = new Dictionary<string, string>();
                    Dictionary<string, string> data2 = new Dictionary<string, string>();
                    Dictionary<string, string> data3 = new Dictionary<string, string>();

                    string automatic = collection["automatic"];



                    //checking for admin

                    //checking for modules status

                    if (automatic.ToString().Contains("true"))
                    {
                        automatic = "1";

                    }

                    else
                    {
                        automatic = "0";

                    }

                    string _userssession = HttpContext.Session["username"].ToString();
                    string actionedby = collection["Actionedby"];
                    //data["id"] = id;
                    if (_userssession != actionedby)
                    {

                        //string admin = Blogic.RunStringReturnStringValue("select * from tblloanusers WHERE adminright= 1 and phone  = '" + collection["mobileno"].ToString().Trim() + "'");
                       
                            string codeExists = Blogic.RunStringReturnStringValue("select * from tbldatamain WHERE platform= 0 and phone  = '" + collection["mobileno"].ToString().Trim() + "'");

                            if (codeExists != "" && codeExists != "0")
                            {
                            string codeExist = Blogic.RunStringReturnStringValue("SELECT * FROM tblloanusers where adminright=1 and phone='" + collection["mobileno"] + "'");

                            if (codeExist != "" && codeExist != "0")
                            {
                                data["Phone"] = collection["mobileno"];
                                data["idno"] = collection["idno"];
                                data["Loanlimit"] = collection["Loanlimit"];
                                data["Airtimelimit"] = collection["Airtimelimit"];
                                data["Surname"] = collection["Firstname"];
                                data["OtherNames"] = collection["Lastname"];
                                data1["Phone"] = collection["mobileno"];
                                data1["Rectifiedby"] = _userssession;
                                data["Rectifiedby"] = _userssession;
                                data1["idno"] = collection["idno"];
                                data1["Loanlimit"] = collection["Loanlimit"];
                                data1["Airtimelimit"] = collection["Airtimelimit"];
                                data1["Surname"] = collection["Firstname"];
                                data1["OtherNames"] = collection["Lastname"];
                                data1["automatic"] = automatic;
                                data1["Airautomatic"] = automatic;
                                data["automatic"] = automatic;
                                data["Airautomatic"] = automatic;
                                data["Membership"] = "1";
                                data1["useright"] = "1";
                                data1["adminright"] = "1";
                                data["useright"] = "1";
                                data["adminright"] = "1";
                                data["loantype"] = "CASH ADVANCE";
                                data["loancode"] = "L0003";
                                // data2["Loanlimit"] = collection["Loanlimit"];
                                //data2["approved"] = "1";
                                // data["Approved"] = "1";
                                where["Phone"] = id;
                                where2["mobileno"] = id;                                
                                sql1 = Logic.DMLogic.UpdateString("tblloanusersapproval", data1, where);
                                sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                               // sql2 = Logic.DMLogic.UpdateString("tblloandisbursement", data2, where);
                                flag1 = Blogic.RunNonQuery(sql1);
                                flag = Blogic.RunNonQuery(sql);
                               // flag2 = Blogic.RunNonQuery(sql2);
                                //jjjj
                                if (flag)
                                {
                                    response.Add("The record has been modified successfully.");
                                    _successfailview = "ChurchRegistrationSuccess";
                                }

                            }
                            else
                            {
                                data["Phone"] = collection["mobileno"];
                                data["idno"] = collection["idno"];
                                data["Loanlimit"] = collection["Loanlimit"];
                                data["Airtimelimit"] = collection["Airtimelimit"];
                                data["Surname"] = collection["Firstname"];
                                data["OtherNames"] = collection["Lastname"];
                                data1["Phone"] = collection["mobileno"];
                                data1["Rectifiedby"] = _userssession;
                                data["Rectifiedby"] = _userssession;
                                data1["idno"] = collection["idno"];
                                data1["Loanlimit"] = collection["Loanlimit"];
                                data1["Airtimelimit"] = collection["Airtimelimit"];
                                data1["Surname"] = collection["Firstname"];
                                data1["OtherNames"] = collection["Lastname"];
                                data1["automatic"] = automatic;
                                data1["Airautomatic"] = automatic;
                                data["automatic"] = automatic;
                                data["Airautomatic"] = automatic;
                                data3["Loanlimit"] = collection["Loanlimit"];
                                data["Membership"] = "1";
                                data1["useright"] = "1";
                                data1["adminright"] = "1";
                                data["useright"] = "1";
                                data["adminright"] = "1";
                                data2["approved"] = "1";
                                data["loantype"] = "CASH ADVANCE";
                                data["loancode"] = "L0003";
                                // data["Approved"] = "1";
                                where["Phone"] = id;
                               // where2["mobileno"] = id;
                                sql1 = Logic.DMLogic.UpdateString("tblloanusersapproval", data1, where);
                                sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                                sql2 = Logic.DMLogic.UpdateString("tbldatamain", data2, where);
                               // sql3 = Logic.DMLogic.UpdateString("tblloandisbursement", data3, where2);
                                flag1 = Blogic.RunNonQuery(sql1);
                                flag = Blogic.RunNonQuery(sql);
                                flag2 = Blogic.RunNonQuery(sql2);
                               // flag3 = Blogic.RunNonQuery(sql3);
                                //jjjj
                                if (flag)
                                {

                                    response.Add("The record has been modified successfully.");
                                    _successfailview = "ChurchRegistrationSuccess";
                                    string messagetosend = "Dear " + collection["Firstname"] + ", your account has been activated. You can borrow Ksh  " + collection["Loanlimit"] + " Anytime. Your pin will be sent to you shortly. Your limit will grow when pay on time. Go to Counterone App or USSD to borrow. Thank you!";
                                    SendSms(messagetosend, collection["mobileno"]);

                                }

                            }


                        }
                        else
                        {
                            data["Phone"] = collection["mobileno"];
                            data["idno"] = collection["idno"];
                            data["Loanlimit"] = collection["Loanlimit"];
                            data["Airtimelimit"] = collection["Airtimelimit"];
                            data["Surname"] = collection["Firstname"];
                            data["OtherNames"] = collection["Lastname"];

                            data1["Phone"] = collection["mobileno"];
                            data1["Rectifiedby"] = _userssession;
                            data["Rectifiedby"] = _userssession;
                            data1["idno"] = collection["idno"];
                            data1["Loanlimit"] = collection["Loanlimit"];
                            data1["Airtimelimit"] = collection["Airtimelimit"];
                            data1["Surname"] = collection["Firstname"];
                            data1["OtherNames"] = collection["Lastname"];
                            data1["automatic"] = automatic;
                            data1["Airautomatic"] = automatic;
                            data["automatic"] = automatic;
                            data["Airautomatic"] = automatic;
                            data["Membership"] = "1";
                            data1["useright"] = "1";
                            data1["adminright"] = "1";
                            data["useright"] = "1";
                            data["adminright"] = "1";
                            data["loantype"] = "CASH ADVANCE";
                            data["loancode"] = "L0001";
                            // data["Approved"] = "1";
                            where["Phone"] = id;
                            sql1 = Logic.DMLogic.UpdateString("tblloanusersapproval", data1, where);
                            sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                            flag1 = Blogic.RunNonQuery(sql1);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("The record has been modified successfully.");
                                _successfailview = "ChurchRegistrationSuccess";

                            }
                        }

                        
                    }
                    else
                    {
                        response.Add("You are not authorized to Approve the records.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            else if (_action == "Reject")
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data["useright"] = "1";
                data["adminright"] = "1";
                data["Membership"] = "1";

                where["Phone"] = id;
                sql = Logic.DMLogic.UpdateString("tblloanusersapproval", data, where);
                flag = Blogic.RunNonQuery(sql);


                if (flag)
                {
                    response.Add("The record has been rejected successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }

            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult pendingTPapproval(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "pendingTPapproval";
                string uniqueID = id.ToString();
                
                    sql = " select a.idfront,a.idback,a.photo,a.Userid,a.Phone,a.idno,a.Accountno,a.Email,c.Loanlimit,a.Surname,a.OtherNames,a.Airtimelimit,a.DateJoined,b.occupation,b.description,b.location,b.salary,b.hrcomment,b.company,c.actionedby from tblloanusers a INNER JOIN tblloanusersapproval c on a.Phone=c.phone INNER JOIN tbldatamain b on a.Phone=b.phone WHERE a.Phone= '" + uniqueID + "' and c.adminright=0";
               
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["mobileno"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Actionedby"] = rd["actionedby"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["occupation"] = rd["occupation"];
                            ViewData["description"] = rd["description"];
                            ViewData["location"] = rd["location"];
                            ViewData["salary"] = rd["salary"];
                            ViewData["hrcomment"] = rd["hrcomment"];
                            ViewData["company"] = rd["company"];

                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];

                            if (rd["idfront"].ToString() == "")
                            {
                                ViewData["idfront"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idfront"] = rd["idfront"];
                            }
                            if (rd["idback"].ToString() == "")
                            {
                                ViewData["idback"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idback"] = rd["idback"];
                            }
                            if (rd["photo"].ToString() == "")
                            {
                                ViewData["photo"] = "~/Content/images/profil.png";
                            }
                            else
                            {
                                ViewData["photo"] = rd["photo"];
                            }



                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ussdcreatepin(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "ussdcreatepin";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            ViewData["Phone"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Surname"] = rd["Surname"];
                            ViewData["OtherNames"] = rd["OtherNames"];
                            ViewData["Email"] = rd["Email"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ussdcreatepin(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            bool flag1 = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string Phone = collection["Phone"];
                string postData = "phone=" + Phone;
                var responses = DataRequestToServer(postData, post_urls);
                response.Add("Pin Created Successfully.");
                _successfailview = "ChurchRegistrationSuccess";


            }
            catch
            {
                _successfailview = "ErrorPage";
            }

            ViewData["Response"] = response;
            return View(_successfailview);

        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult pendingTPapproval(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            string sql2 = "";
            bool flag = false;
            bool flag1 = false;
            bool flag2 = false;
            string _action = collection[15];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            if (_action == "Approve")
            {
                try
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    Dictionary<string, string> data1 = new Dictionary<string, string>();
                    Dictionary<string, string> data2 = new Dictionary<string, string>();

                    string automatic = collection["automatic"];
                    //checking for admin

                    //checking for modules status

                    if (automatic.ToString().Contains("true"))
                    {
                        automatic = "1";
                    }
                    else
                    {
                        automatic = "0";
                    }

                    string _userssession = HttpContext.Session["username"].ToString();
                    string actionedby = collection["Actionedby"];                     
                    if (_userssession != actionedby)
                    {
                        string codeExists = Blogic.RunStringReturnStringValue("SELECT * FROM tblloanusers where adminright=1 and phone='" + collection["mobileno"] + "'");

                        if (codeExists != "" && codeExists != "0")
                        {
                            data["Phone"] = collection["mobileno"];
                            data["idno"] = collection["idno"];
                            data["Loanlimit"] = collection["Loanlimit"];
                            data["Airtimelimit"] = collection["Airtimelimit"];
                            data["Surname"] = collection["Firstname"];
                            data["OtherNames"] = collection["Lastname"];
                            data1["Phone"] = collection["mobileno"];
                            data1["Rectifiedby"] = _userssession;
                            data["Rectifiedby"] = _userssession;
                            data1["idno"] = collection["idno"];
                            data1["Loanlimit"] = collection["Loanlimit"];
                           // data2["Loanlimit"] = collection["Loanlimit"];
                            data1["Airtimelimit"] = collection["Airtimelimit"];
                            data1["Surname"] = collection["Firstname"];
                            data1["OtherNames"] = collection["Lastname"];
                            data1["automatic"] = automatic;
                            data1["Airautomatic"] = automatic;
                            data1["useright"] = "1";
                            data1["adminright"] = "1";
                            data["automatic"] = automatic;
                            data["Airautomatic"] = automatic;
                            data["Membership"] = "1";
                            data["useright"] = "1";
                            data["adminright"] = "1";
                            where["Phone"] = id;
                            where2["mobileno"] = id;
                            data["loantype"] = "CASH ADVANCE";
                            data["loancode"] = "L0003";
                            sql1 = Logic.DMLogic.UpdateString("tblloanusersapproval", data1, where);
                            sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                            //sql2 = Logic.DMLogic.UpdateString("tblloandisbursement", data2, where2);
                            flag1 = Blogic.RunNonQuery(sql1);
                            flag = Blogic.RunNonQuery(sql);
                            //flag2 = Blogic.RunNonQuery(sql2);
                            //flag2
                            if (flag)
                            {
                                response.Add("The record has been modified successfully.");
                                _successfailview = "ChurchRegistrationSuccess";
                            }
                        }
                        else{
                            data["Phone"] = collection["mobileno"];
                            data["idno"] = collection["idno"];
                            data["Loanlimit"] = collection["Loanlimit"];
                            data["Airtimelimit"] = collection["Airtimelimit"];
                            data["Surname"] = collection["Firstname"];
                            data["OtherNames"] = collection["Lastname"];
                            data1["Phone"] = collection["mobileno"];
                            data1["Rectifiedby"] = _userssession;
                            data["Rectifiedby"] = _userssession;
                            data1["idno"] = collection["idno"];
                            data1["Loanlimit"] = collection["Loanlimit"];
                            data1["Airtimelimit"] = collection["Airtimelimit"];
                            data1["Surname"] = collection["Firstname"];
                            data1["OtherNames"] = collection["Lastname"];
                            data1["automatic"] = automatic;
                            data1["Airautomatic"] = automatic;
                            data["automatic"] = automatic;
                            data["Airautomatic"] = automatic;
                            data["Membership"] = "1";
                            data1["useright"] = "1";
                            data1["adminright"] = "1";
                            data["useright"] = "1";
                            data["adminright"] = "1";
                            data["loantype"] = "CASH ADVANCE";
                            data["loancode"] = "L0003";
                            where["Phone"] = id;
                            sql1 = Logic.DMLogic.UpdateString("tblloanusersapproval", data1, where);
                            sql = Logic.DMLogic.UpdateString("tblloanusers", data, where);
                            flag1 = Blogic.RunNonQuery(sql1);
                            flag = Blogic.RunNonQuery(sql);

                            if (flag)
                            {
                                response.Add("The record has been modified successfully.");
                                _successfailview = "ChurchRegistrationSuccess";
                                string messagetosend = "Dear " + collection["Firstname"] + ", your account has been activated. You can borrow Ksh  " + collection["Loanlimit"] + " Anytime.Your limit will grow when pay on time. Go to Counterone App or USSD to borrow. Thank you!";
                                SendSms(messagetosend, collection["mobileno"]);
                            }

                        }

                           
                    }
                    else
                    {
                        response.Add("You are not authorized to Approve the records.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            else if (_action == "Reject")
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data["useright"] = "1";
                data["adminright"] = "1";
                data["Membership"] = "1";
                where["Phone"] = id;
                sql = Logic.DMLogic.UpdateString("tblloanusersapproval", data, where);
                flag = Blogic.RunNonQuery(sql);


                if (flag)
                {
                    response.Add("The record has been rejected successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }

            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Addmembers(FormCollection collection, string id)
        {
            string sql2 = "";
            string sql1 = "";
            bool flag = false;
            bool flag2 = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();
                string _userssession = HttpContext.Session["username"].ToString();
                string codeExists = Blogic.RunStringReturnStringValue("select * from tbldatamain WHERE platform= 0 and phone  = '" + collection["mobileno"].ToString().Trim() + "'");
                if (codeExists != "" && codeExists != "0")
                {
                    DateTime d1 = DateTime.Now;
                    DateTime datevalue = (Convert.ToDateTime(d1.ToString()));
                    data1["DateJoined"] = datevalue.ToShortDateString();
                    data1["actionedby"] = _userssession;
                    data1["Rectifiedby"] = _userssession;
                    data1["Phone"] = collection["mobileno"];
                    data1["idno"] = collection["idno"];
                    data1["Totalshares"] = "0";
                    data1["sharecontibution"] = "0";
                    data1["Loanlimit"] = collection["Loanlimit"];
                    data1["Airtimelimit"] = collection["Airtimelimit"];
                    data1["Surname"] = collection["Firstname"];
                    data1["OtherNames"] = collection["Lastname"];
                    data1["useright"] = "1";
                    data1["adminright"] = "0";
                    data1["Membership"] = "1";
                    //data1["loantype"] = "CASH ADVANCE"; 
                    //data1["loancode"] = "L0003";
                    data["actionedby"] = _userssession;
                    data["Rectifiedby"] = _userssession;
                    data["Phone"] = collection["mobileno"];
                    data["idno"] = collection["idno"];
                    data["Loanlimit"] = collection["Loanlimit"];
                    data["Airtimelimit"] = collection["Airtimelimit"];
                    data["Surname"] = collection["Firstname"];
                    data["OtherNames"] = collection["Lastname"];
                    data["automatic"] = "1";
                    data["Airautomatic"] = "1";
                    data["useright"] = "1";
                    data["adminright"] = "0";
                    data["Tp"] = "0";
                    data1["Tp"] = "1";
                    where["Phone"] = id;
                    sql2 = Logic.DMLogic.InsertString("tblloanusersapproval", data);
                    // sql = Logic.DMLogic.UpdateString("tblloandisbursement", data, where);
                    flag2 = Blogic.RunNonQuery(sql2);
                    //data["approved"] = "1";
                    ///where["Phone"] = collection["mobileno"];
                    sql1 = Logic.DMLogic.InsertString("tblloanusers", data1);
                    //sql = Logic.DMLogic.UpdateString("tbldatamain", data, where);
                    flag = Blogic.RunNonQuery(sql1);
                    if (flag)
                    {
                        response.Add("The record has been added successfully.");
                        _successfailview = "ChurchRegistrationSuccess";

                    }
                }
                else
                {
                    DateTime d1 = DateTime.Now;
                    DateTime datevalue = (Convert.ToDateTime(d1.ToString()));
                    data1["DateJoined"] = datevalue.ToShortDateString();
                    // data1["Accountno"] = collection["Accountno"];
                    data1["actionedby"] = _userssession;
                    data1["Rectifiedby"] = _userssession;
                    data1["Phone"] = collection["mobileno"];
                    data1["idno"] = collection["idno"];
                    data1["Totalshares"] = "0";
                    data1["sharecontibution"] = "0";
                    data1["Loanlimit"] = collection["Loanlimit"];
                    data1["Airtimelimit"] = collection["Airtimelimit"];
                    data1["Surname"] = collection["Firstname"];
                    data1["OtherNames"] = collection["Lastname"];
                    data1["useright"] = "0";
                    data1["adminright"] = "0";
                    data1["Membership"] = "0";
                    data1["loantype"] = "CASH ADVANCE";
                    data1["loancode"] = "L0001";
                    sql1 = Logic.DMLogic.InsertString("tblloanusers", data1);
                    //sql = Logic.DMLogic.UpdateString("tbldatamain", data, where);
                    flag = Blogic.RunNonQuery(sql1);
                    if (flag)
                    {
                        response.Add("The record has been added successfully.");
                        _successfailview = "ChurchRegistrationSuccess";

                    }
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [NonAction]
        private string SendSms(string message, string recipient)
        {
            string username = "counterone";
            string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
            string status = "";
            string ffrom = "counterone";

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {

                // Thats it, hit send and we'll take care of the rest
                // string from = "";
                dynamic results = gateway.sendMessage(recipient, message, ffrom);
                foreach (dynamic result in results)
                {
                    status = (string)result["status"];

                }
            }
            catch (AfricasTalkingGatewayException e)
            {

                Console.WriteLine("Encountered an error: " + e.Message);

            }
            return status;
        }
    }

}
