﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;

namespace TestNAVBAR.Controllers
{
    public class LoansController : Controller
    {

        string _successfailview = "";
        string sql = "";
        string sql2 = "";
        string sql3 = "";
        string preborrowid = "";
        string selectedchurch = "";
        string selectedchurchbranch = "";
        string selectedbankbranch = "";
        System.Data.SqlClient.SqlDataReader rd;
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        Dictionary<string, string> where2 = new Dictionary<string, string>();
        Dictionary<string, string> where3 = new Dictionary<string, string>();
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();

        //
        // GET: /Loans/
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult loansapplied360()
        {

            return View();
        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult loanreview360(string myParam)
        {
            Session["phoneno"] = myParam;
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult audittrail()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Addpenalty(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "Addpenalty";
                string uniqueID = id.ToString();
                sql = "select loanref,loanid,borrowerphone,loanamount,loanbalance,loandisbursedate,loantype,loancode,principle,interestamount,rateapplied,(select firstname+' '+lastname from loanusers where phonenumber=borrowerphone) as Name from loanapplications where loanref='" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["loanid"] = rd["loanid"];
                            ViewData["borrowerphone"] = rd["borrowerphone"];
                            ViewData["loanamount"] = rd["loanamount"];
                            ViewData["loanbalance"] = rd["loanbalance"];
                            ViewData["loandisbursedate"] = rd["loandisbursedate"];
                            ViewData["loantype"] = rd["loantype"];
                            ViewData["loancode"] = rd["loancode"];
                            ViewData["principle"] = rd["principle"];
                            ViewData["interestamount"] = rd["interestamount"];
                            ViewData["rateapplied"] = rd["rateapplied"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["loanref"] = rd["loanref"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Addpenalty(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string _startdate = collection["bp.startdate"];
            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
            _startdate = ftodate;
            string _userssession = HttpContext.Session["username"].ToString();
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            string penalty = "";
            string codeExists = Blogic.RunStringReturnStringValue("select Penaltyamount from loanapplications WHERE loanref  = '" + collection["loancode"].ToString().Trim() + "'");

            if (codeExists == "")
            {
                penalty = "0";

            }
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data["modifiedby"] = _userssession;
                data["approvedby"] = _userssession;
                data["Penaltystatus"] = "0";
                data["Penalty"] = collection["Penalty"];
                data["Nextpayday"] = _startdate;
                data["Penaltyamount"] = penalty;
                where["loanref"] = id.ToString();
                sql = Logic.DMLogic.UpdateString("loanapplications", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The record has been actioned successfully waiting aproval.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        public ActionResult PenaltyApprovals(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "PenaltyApprovals";
                string uniqueID = id.ToString();
                sql = "select loanref,modifiedby,Penalty,Penaltyamount,Nextpayday,approvedby,loanid,borrowerphone,loanamount,loanbalance,loandisbursedate,loantype,loancode,principle,interestamount,rateapplied,(select firstname+' '+lastname from loanusers where phonenumber=borrowerphone) as Name from loanapplications where loanid='" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["loanid"] = rd["loanid"];
                            ViewData["borrowerphone"] = rd["borrowerphone"];
                            ViewData["loanamount"] = rd["loanamount"];
                            ViewData["loanbalance"] = rd["loanbalance"];
                            ViewData["loandisbursedate"] = rd["loandisbursedate"];
                            ViewData["loantype"] = rd["loantype"];
                            ViewData["loancode"] = rd["loancode"];
                            ViewData["principle"] = rd["principle"];
                            ViewData["interestamount"] = rd["interestamount"];
                            ViewData["rateapplied"] = rd["rateapplied"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["loanref"] = rd["loanref"];
                            ViewData["modifiedby"] = rd["modifiedby"];
                            ViewData["approvedby"] = rd["approvedby"];
                            ViewData["approvedby"] = rd["approvedby"];
                            ViewData["Penalty"] = rd["Penalty"];
                            //ViewData["Penaltyamount"] = rd["Penaltyamount"];
                            ViewData["Nextpenalty"] = rd["Nextpayday"];
                            double penaltyamount = double.Parse(rd["Penaltyamount"].ToString());
                            double penalty = double.Parse(rd["Penalty"].ToString());
                            ViewData["Penaltyamount"] = (penaltyamount + penalty);
                            //double totalpenalty =


                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Penaltyapproval()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PenaltyApprovals(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;

            string sql1 = "";
            bool flag1 = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            string interest = "select interestamount from loanapplications WHERE loanid= " + id + " ";
            double Totalinterest = double.Parse(interest);
            string _userssession = HttpContext.Session["username"].ToString();
            string actionedby = collection["modifiedby"];

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                if (_userssession != actionedby)
                {

                    // double loanbalances = double.Parse(rd["loanbalance"].ToString());
                    // double loanamounts = double.Parse(rd["loanamount"].ToString());
                    // string Penaltyamount = collection["Penaltyamount"];
                    double loanbalances = double.Parse(collection["loanbalance"]);
                    double loanamounts = double.Parse(collection["loanamount"]);
                    string Penaltyamount = collection["Penaltyamount"];
                    double interestamount = double.Parse(collection["Penaltyamount"]);
                    string[] _F = Penaltyamount.Split('.');
                    string amount = _F[0].ToString().Trim();
                    double Penaltya = Convert.ToDouble(amount);
                    double loanbalance = Penaltya + loanbalances;
                    double loanamount = Penaltya + loanamounts;
                    data["approvedby"] = _userssession;
                    data["Penaltystatus"] = "1";
                    data["Penaltyamount"] = collection["Penaltyamount"];
                    data["loanamount"] = loanamount.ToString();
                    data["loanbalance"] = loanbalance.ToString();
                    data["interestamount"] = (Totalinterest + interestamount).ToString();



                    where["loanid"] = id.ToString();
                    sql = Logic.DMLogic.UpdateString("loanapplications", data, where);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {

                        //string message = "Dear" + collection["Name"] + ", Your Loan of :" + collection["loanamount"] + " has been reversed successfully, Kindly apply again.";
                        //SendSms(message, collection["borrowerphone"]);
                        response.Add("The record has been approved successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }



                }
                else
                {
                    response.Add("You are not authorized to action this record");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Loansreversal(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "Loansreversal";
                string uniqueID = id.ToString();
                sql = "select loanref,loanid,borrowerphone,loanamount,loanbalance,loandisbursedate,loantype,loancode,principle,interestamount,rateapplied,(select firstname+' '+lastname from loanusers where phonenumber=borrowerphone) as Name from loanapplications where loanid='" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["loanid"] = rd["loanid"];
                            ViewData["borrowerphone"] = rd["borrowerphone"];
                            ViewData["loanamount"] = rd["loanamount"];
                            ViewData["loanbalance"] = rd["loanbalance"];
                            ViewData["loandisbursedate"] = rd["loandisbursedate"];
                            ViewData["loantype"] = rd["loantype"];
                            ViewData["loancode"] = rd["loancode"];
                            ViewData["principle"] = rd["principle"];
                            ViewData["interestamount"] = rd["interestamount"];
                            ViewData["rateapplied"] = rd["rateapplied"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["loanref"] = rd["loanref"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Loansreversal(FormCollection collection, string id, Loanreversal rev)
        {
            string sql = "";
            bool flag = false;
            string _userssession = HttpContext.Session["username"].ToString();
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            string reversal = rev.Reversal.ToString();
            if(reversal=="New")
            {
                reversal = "1";
            }
            else
            {
                reversal = "0";
            }
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();


                data["modifiedby"] = _userssession;
                data["approvedby"] = _userssession;
                data["Reversalstatus"] ="0";
                data["Reversalcategory"] = reversal;
                
                where["loanid"] = id.ToString();
                sql = Logic.DMLogic.UpdateString("loanapplications", data, where);
                flag = Blogic.RunNonQuery(sql);
               
                if (flag)
                {
                    response.Add("The record has been actioned successfully waiting aproval.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        public ActionResult LoansreversalApproval(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "LoansreversalApproval";
                string uniqueID = id.ToString();
                sql = "select loanref,modifiedby,Reversalcategory,approvedby,loanid,borrowerphone,loanamount,loanbalance,loandisbursedate,loantype,loancode,principle,interestamount,rateapplied,(select firstname+' '+lastname from loanusers where phonenumber=borrowerphone) as Name from loanapplications where loanid='" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["loanid"] = rd["loanid"];
                            ViewData["borrowerphone"] = rd["borrowerphone"];
                            ViewData["loanamount"] = rd["loanamount"];
                            ViewData["loanbalance"] = rd["loanbalance"];
                            ViewData["loandisbursedate"] = rd["loandisbursedate"];
                            ViewData["loantype"] = rd["loantype"];
                            ViewData["loancode"] = rd["loancode"];
                            ViewData["principle"] = rd["principle"];
                            ViewData["interestamount"] = rd["interestamount"];
                            ViewData["rateapplied"] = rd["rateapplied"];
                            ViewData["Name"] = rd["Name"];
                            ViewData["loanref"] = rd["loanref"];
                            ViewData["modifiedby"] = rd["modifiedby"];
                            ViewData["approvedby"] = rd["approvedby"];
                            Session["Reversalcategory"] = rd["Reversalcategory"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoansreversalApproval(FormCollection collection, string id, Loanreversal rev)
        {
            string sql = "";
            bool flag = false;

            string sql1 = "";
            bool flag1 = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            string reversal = rev.Reversal.ToString();
            string _userssession = HttpContext.Session["username"].ToString();
            string actionedby = collection["modifiedby"];
            if (reversal == "New")
            {
                reversal = "1";
            }
            else
            {
                reversal = "0";
            }
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                if (_userssession != actionedby)
                {
                    if (reversal == "1")
                    {

                        data["approvedby"] = _userssession;
                        data["Reversalstatus"] = "0";

                        data["loanapproved"] = "0";
                        data["loandisbursed"] = "2";
                        data["vettingstatus"] = "PENDINGREVIEW";
                        data["vetted"] = "NO";

                        where["loanid"] = id.ToString();
                        sql = Logic.DMLogic.UpdateString("loanapplications", data, where);
                        flag = Blogic.RunNonQuery(sql);

                        if (flag)
                        {
                            string message = "A reversal has been made for this account Name:" + collection["Name"] + ", Phone:" + collection["borrowerphone"] + ", Loanref:" + collection["loanref"]+", Loanamount:"+collection["loanamount"]+" to new application by"+ _userssession;

                            SendSms(message, "254720207273");
                            response.Add("The record has been approved successfully.");
                            _successfailview = "ChurchRegistrationSuccess";

                        }
                    }
                    else
                    {
                        data["approvedby"] = _userssession;
                        data["Reversalstatus"] = "0";

                        data["loanapproved"] = "0";
                        data["loandisbursed"] = "2";
                        data["vettingstatus"] = "VETTED";
                        data["vetted"] = "YES";

                        where["loanid"] = id.ToString();
                        sql = Logic.DMLogic.UpdateString("loanapplications", data, where);
                        flag = Blogic.RunNonQuery(sql);

                        if (flag)
                        { string message = "A reversal has been made for this account Name:" + collection["Name"] + ", Phone:" + collection["borrowerphone"] + ", Loanref:" + collection["loanref"]+", Loanamount:"+collection["loanamount"]+" by"+ _userssession;

                            SendSms(message, "254720207273");
                            response.Add("The record has been approved successfully");
                            _successfailview = "ChurchRegistrationSuccess";

                        }
                    }

                    sql1 = "delete from JournalEntries where loanid='" + collection["loanref"] + "' ";
                    flag1 = Blogic.RunNonQuery(sql1);

                    if (flag1)
                    {
                        response.Add("The record has been approved");
                        _successfailview = "ChurchRegistrationSuccess";

                    }
                }
                else
                {
                    response.Add("You are not authorized to action this record");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        // GET: /Loans/
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult overpaidloan()
        {

            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult overpaidloanFL()
        {

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult overpaidloan(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "AND phonenumber='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string ftodate = "";
                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " AND datecreated>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datecreated<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "overpaidloan";
            }
            else
            {
                _viewtoDisplay = "overpaidloandates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult overpaidloandates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "";
            return PartialView("_overpaidloandates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult overpaidloandates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "loanusers")
                {
                    _viewtoDisplay = "overpaidloan";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND paymentdate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " AND paymentdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND paymentdate<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND paymentdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SomeAction(string myParam)
        {

            return View();

        }



        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult redeeemdiscount(string myParam)
        {

            return View();

        }
        public ActionResult viewclient360(string myParam)
        {
            Session["phoneno"] = myParam;
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]

        public ActionResult LoanSchedule()
        {
            //System.Web.HttpContext.Current.Session["LOANID"]=
            return View();
        }

        public ActionResult Tools()
        {

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Tools(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;

            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();


                data["adminfee"] = collection["adminfee"];
                data["interestfee"] = collection["interestfee"];
                data["loantype"] = collection["loantype"];
                if (flag)
                {
                    response.Add("The interest has been computed successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }



        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult shoppingDetails()
        {
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ShoppingDetailsDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "mpesalogs";
            return PartialView("_shoppingdetailsdates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ShoppingDetailsDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "mpesalogs")
                {
                    _viewtoDisplay = "ShoppingDetails";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE shoppingdate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE shoppingdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND shoppingdate<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND shoppingdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ShoppingDetails(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "WHERE customerphone='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string ftodate = "";
                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " WHERE shoppingdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND shoppingdate>='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "ShoppingDetails";
            }
            else
            {
                _viewtoDisplay = "ShoppingDetailsDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoansSchedule(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "LoansSchedule";
                string uniqueID = id.ToString();
                sql = "  select a.* from loanschedule  a,loanapplications b where a.loanreference=b.loanref  and b.loanid='" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["entryid"] = rd["entryid"];
                            ViewData["amounttopay"] = rd["amounttopay"];
                            ViewData["dateofpayment"] = rd["dateofpayment"];
                            ViewData["loanreference"] = rd["loanreference"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        public ActionResult DataScrub(string myParam)
        {
            Session["phoneno"] = myParam;
            return View();
        }

        [HttpPost]
        public void getscrub()
        {
            string bbbb = Session["custidno"].ToString();


            string kkk = "";
            // 
        }

        [HttpPost]
        public void getscrb()
        {
            // 
        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]

        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoansForReview()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoansDisbursed()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult fLoansDisbursed()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoansDisbursedapproval()
        {
            return View();
        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReviewedLoans()
        {
            return View();
        }


        public ActionResult SendMessagephone_loans(string myParam)
        {
            Session["phonenumber"] = myParam;
            return View();



        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult audittrail(FormCollection collection)
        {
            string _viewtoDisplay = "audittrail";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "audittraildates";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _mpesano = collection["mpesano"];

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }


                _SQLSelectionCriteria = " Where strusername <>''";
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND (dtDate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND dtDate<='" + _enddate + "')";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND strusername='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
            }
            return View(_viewtoDisplay);
        }




        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SendMessagephone_loans(FormCollection collection)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            string messagetosend = "";
            string _action = collection[3];
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            Dictionary<string, string> data = new Dictionary<string, string>();
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["customerphone"] = collection["customerphone"];
            ViewData["adminmessage"] = collection["adminmessage"];


            if (_action == "SendToOne")
            {
                string No = collection["customerphone"];
                if (collection["customerphone"].ToString().Length == 0)
                {
                    errors.Add("You must ADD the Phone Number.Its Mandatory");
                }
                sql1 = "select phonenumber from Loanusers where phonenumber='" + No + "'";
                string codeExists = Blogic.RunStringReturnStringValue(sql1);

                if (codeExists == "" && codeExists != "0")
                {
                    errors.Add("Please MAKE SURE the Phone Number is valid. This Phone Number does not exist.");
                }


                if (collection["adminmessage"].ToString().Length == 0)
                {
                    errors.Add("You must ADD the message.Its Mandatory");
                }

                if (errors.Count > 0)
                {
                    _successfailview = "ReviewLoan";
                    response.Add("The message has not been sent successfully.");
                    ViewData["customerphone"] = collection["customerphone"];
                    ViewData["adminmessage"] = collection["adminmessage"];
                }
                else
                {
                    try
                    {
                        data["customerphone"] = collection["customerphone"];
                        data["adminmessage"] = collection["adminmessage"];
                        data["sendtime"] = dt.ToString();
                        data["sent"] = "1";
                        data["sender"] = "ADMIN";
                        sql = Logic.DMLogic.InsertString("AdminMessages", data);

                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            messagetosend = collection["adminmessage"];
                            if (SendSms(messagetosend, collection["customerphone"]) == "Success")
                            {
                                response.Add("The message has been send successfully.");
                                _successfailview = "ReviewLoan";
                            }

                        }
                    }
                    catch
                    {
                        _successfailview = "ErrorPage";
                    }
                }
            }//
            else if (_action == "SendToMany")
            {
                if (collection["adminmessage"].ToString().Length == 0)
                {
                    errors.Add("You must ADD the message.Its Mandatory");
                }

                if (errors.Count > 0)
                {
                    _successfailview = "SendMessage";
                    ViewData["customerphone"] = collection["customerphone"];
                    ViewData["adminmessage"] = collection["adminmessage"];
                }
                else
                {
                    sql = "Select phonenumber from smstesting";// remember to change table name to loanusers
                    rd = Blogic.RunQueryReturnDataReader(sql);
                    using (rd)
                        if (rd != null && rd.HasRows)
                        {
                            while (rd.Read())
                            {
                                string phoneno = rd["phonenumber"].ToString();
                                messagetosend = collection["adminmessage"];
                                if (SendSms(messagetosend, phoneno) == "Success")
                                {
                                    data["customerphone"] = phoneno;
                                    data["adminmessage"] = collection["adminmessage"];
                                    data["sendtime"] = dt.ToString();
                                    data["sent"] = "1";
                                    data["sender"] = "ADMIN";
                                    sql = Logic.DMLogic.InsertString("AdminMessages", data);
                                }
                            }
                            response.Add("The message has been send successfully.");
                            _successfailview = "ReviewLoan";
                        }
                }
            }


            ////
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }//

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoansDisbursed(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "  WHERE borrowerphone ='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                //string _mpesano = collection["mpesano"];

                string ftodate = "";


                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " WHERE LoanDisbursedate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND LoanDisbursedate<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "LoansDisbursed";
            }
            else
            {
                _viewtoDisplay = "DisbursedDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReviewedLoans(FormCollection collection)
        {
            string _viewtoDisplay = "ReviewedLoans";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "ReviewedLoansDates";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _mpesano = collection["mpesano"];

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }

                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
            }
            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RejectedLoans()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RejectedLoans(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND phonenumber='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string ftodate = "";
                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " AND borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "RejectedLoans";
            }
            else
            {
                _viewtoDisplay = "RejectedLoansDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnpaidLoans(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "AND BorrowerPhone='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string ftodate = "";
                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " AND borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "UnpaidLoans";
            }
            else
            {
                _viewtoDisplay = "UnpaidLoansDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnpaidLoans()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnpaidLoansFL()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult redeeemdiscount(FormCollection collection)
        {
            string _viewtoDisplay = "redeeemdiscount";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "redeeemdiscountDates";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _mpesano = collection["mpesano"];

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (shoppingdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND shoppingdate<='" + _enddate + "')";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
            }
            return View(_viewtoDisplay);
        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult redeemcustomerdiscount(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "redeemcustomerdiscount";
                string uniqueID = id.ToString();

                sql = "select customerphone,  sum(ROUND(CAST(DiscountPrice AS decimal(18,2)), 2)) as discount from ShoppingDetails where (discountredeemed=0 or discountredeemed is null) AND customerphone = '" + id + "' group by customerphone";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["customerphone"] = rd["customerphone"];
                            ViewData["discount"] = rd["discount"];


                        }
                    }
                }


                sql = "select firstname +'    '+ lastname as Names from LoanUsers  where phonenumber = '" + id + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Names"] = rd["Names"];



                        }
                    }
                }



            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        public ActionResult redeemcustomerdiscount(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";
            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();


                sql = "UPDATE ShoppingDetails SET discountredeemed='1',dateredeemed='" + dt.ToString() + "' WHERE customerphone=" + collection["phonenumber"] + "";
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {

                    string messagetosend = "Your shopping discount of  Ksh " + collection["discount"] + " has been redeemed succesfully. Thank you for using counter one";


                    if (SendSms(messagetosend, collection["phonenumber"]) == "Success")

                    {


                    }

                    sql = "INSERT INTO tbredeemeddiscount (phonenumber,Names,redeemDate,amount)VALUES('" + collection["phonenumber"] + "','" + collection["fullnames"] + "','" + dt.ToString() + "','" + collection["discount"] + "')";
                    flag = Blogic.RunNonQuery(sql);



                    response.Add("The discount has been updated as redeemed successfully.");
                    _successfailview = "redeeemdiscount";



                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            return View(_successfailview);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PaidLoans()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PaidLoansFL()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PaidLoans(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "AND BorrowerPhone='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string ftodate = "";
                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " AND borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "PaidLoans";
            }
            else
            {
                _viewtoDisplay = "PaidLoansDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OverDueLoans()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OverDueLoansFL()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PartiallyPaidLoans()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PartiallyPaidLoansFL()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PartiallyPaidLoans(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "AND BorrowerPhone='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string ftodate = "";
                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " AND borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "PartiallyPaidLoans";
            }
            else
            {
                _viewtoDisplay = "PartiallyPaidLoansDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AppliedLoans()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult loanrepaymentsdata()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AppliedLoans(FormCollection collection)
        {
            string _viewtoDisplay = "AppliedLoans";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "AppliedLoansDates";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _mpesano = collection["mpesano"];

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }

                //if (collection["mpesano"].ToString().Length == 0)
                //{
                //    errors.Add("You must Indicate the MPESA No.");
                //}
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
            }
            return View(_viewtoDisplay);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult loanrepaymentsdata(FormCollection collection)
        {
            string _viewtoDisplay = "loanrepaymentsdata";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "loanrepaymentsdata_dates";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _mpesano = collection["mpesano"];

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }

                //if (collection["mpesano"].ToString().Length == 0)
                //{
                //    errors.Add("You must Indicate the MPESA No.");
                //}
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
            }
            return View(_viewtoDisplay);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult loanstatement(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                Session["loanid"] = id.ToString();
                //ModifyRegisteredUser
                _viewToDisplayData = "loanstatement";
                string uniqueID = id.ToString();
                // sql = " select * from loanapplications a, LoanUsers b WHERE a.borroweruniqueid=b.uniqueid and a.loanid= '" + uniqueID + "'";
                sql = "select * from vw_loanstatements where loanref= '" + id + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            string interest = "";
                            double totalloan = 0;
                            double principle = 0;
                            float rateapplied = 0;
                           
                             double totalinterst = 0;
                            ViewData["interestamount"] = rd["interestamount"].ToString();
                            
                            //totalloan = rd["loanbalance"].ToString();
                            principle = Convert.ToDouble(rd["principle"]);
                          
                            totalinterst = principle * rateapplied;
                            totalloan = totalinterst + principle;
                            ViewData["principle"] = rd["principle"];
                            ViewData["loanamount"] = totalloan;
                            ViewData["loanbalance"] = rd["loanbalance"];

                            ViewData["borrowdate"] = rd["borrowdate"].ToString().Replace("12:00:00 AM", "");
                            ViewData["repaymentperiod"] = rd["repaymentperiod"];
                            ViewData["Names"] = rd["Names"];
                            ViewData["borrowerphone"] = rd["borrowerphone"];
                            ViewData["idno"] = rd["idno"];



                        }
                    }
                }






            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        //===========================date loads for criteria====================
        [AcceptVerbs(HttpVerbs.Get)]

        public ActionResult DisbursedDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "disbursedloans";
            return PartialView("_DisbursedDate");
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DisbursedDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            //if (collection["mpesano"].ToString().Length == 0)
            //{
            //    errors.Add("You must Indicate the MPESA No.");
            //}

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "mpesalogs")
                {
                    _viewtoDisplay = "LoansDisbursed";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (loandisbursedate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " AND (loandisbursedate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "AND (loandisbursedate>='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND (loandisbursedate>='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }
                // HttpContext.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhones"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingReview()
        {
            return View();
        }
        

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingReviewFL()
        {
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PendingReviewDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "pendingreviewloans";
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PendingReviewDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "pendingreviewloans")
                {
                    _viewtoDisplay = "LoansForReview";
                }
                //Build SQL Selection criteria to pass as a single session value

                string EmployerID = System.Web.HttpContext.Current.Session["EmployerID"].ToString();
                if (EmployerID == "0")
                {
                }
                else
                {
                    _SQLSelectionCriteria = " AND EmployerID= '" + EmployerID + "'";
                }
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReviewedLoansDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "reviewedloansdates";
            return PartialView("_ReviewedLoansDates");

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReviewedLoansDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "reviewedloansdates")
                {
                    _viewtoDisplay = "ReviewedLoans";
                }
                //Build SQL Selection criteria to pass as a single session value
                string EmployerID = System.Web.HttpContext.Current.Session["EmployerID"].ToString();
                if (EmployerID == "0")
                {
                }
                else
                {
                    _SQLSelectionCriteria = " AND EmployerID= '" + EmployerID + "'";
                }

                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RejectedLoansDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "mpesalogs";
            return PartialView("_RejectedLoansDates");
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OverDueLoansDates()
        {

            System.Web.HttpContext.Current.Session["_selectedOption"] = "overdueloansdates";
            return PartialView("_OverDueLoansDates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RejectedLoansDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "loanusers")
                {
                    _viewtoDisplay = "RejectedLoans";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE borrowdate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND borrowdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phonenumber='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult AppliedLoansDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "appliedloansdates";
            return PartialView("_AppliedLoansDates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AppliedLoansDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "appliedloansdates")
                {
                    _viewtoDisplay = "AppliedLoans";
                }
                //Build SQL Selection criteria to pass as a single session value
                string EmployerID = System.Web.HttpContext.Current.Session["EmployerID"].ToString();
                if (EmployerID == "0")
                {
                }
                else
                {
                    _SQLSelectionCriteria = " AND EmployerID= '" + EmployerID + "'";
                }

                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UnpaidLoansDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "unpaidloansdates";
            return PartialView("_UnpaidLoansDates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UnpaidLoansDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "unpaidloansdates")
                {
                    _viewtoDisplay = "UnpaidLoans";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE borrowdate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND borrowdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PaidLoansDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "paidloansdates";
            return PartialView("_PaidLoansDates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PaidLoansDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "paidloansdates")
                {
                    _viewtoDisplay = "PaidLoans";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE paymentdate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE paymentdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND paymentdate<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND paymentdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RespondedEnquiriesDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "mpesalogs")
                {
                    _viewtoDisplay = "RespondedEnquiries";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE enquirytime>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE enquirytime>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND enquirytime<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND enquirytime<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OverDueLoansDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "overdueloansdates")
                {
                    _viewtoDisplay = "OverDueLoans";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE paymentdate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE paymentdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND paymentdate<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND paymentdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OverDueLoans(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "WHERE BorrowerPhone='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                //string _mpesano = collection["mpesano"];

                string ftodate = "";


                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " WHERE paymentdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND paymentdate<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "OverDueLoans";
            }
            else
            {
                _viewtoDisplay = "OverDueLoansDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }



        // [AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult OverDueLoansDates()
        //{
        //    System.Web.HttpContext.Current.Session["_selectedOption"] = "overdueloansdates";
        //    return PartialView("_OverDueLoansDates");
        //}

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult OverDueLoans(FormCollection collection)
        //{
        //    string _viewtoDisplay = "";
        //    List<string> errors = new List<string>();
        //    ViewData["ModelErrors"] = errors;
        //    string _startdate = collection["bp.startdate"];
        //    string _enddate = collection["bp.enddate"];
        //    string _mpesano = collection["mpesano"];
        //    string _action = collection[4];

        //    string _SQLSelectionCriteria = "";
        //    _viewtoDisplay = "OverDueLoansDates";

        //    if (collection["bp.startdate"].ToString().Length == 0)
        //    {
        //        errors.Add("You must Indicate the start Date.");
        //    }

        //    if (collection["bp.enddate"].ToString().Length == 0)
        //    {
        //        errors.Add("You must Indicate the End Date.");
        //    }



        //    if (errors.Count > 0)
        //    {
        //        _viewtoDisplay = "OverDueLoans";
        //    }
        //    else
        //    {
        //        _SQLSelectionCriteria = null;

        //        //Build SQL Selection criteria to pass as a single session value
        //        string EmployerID = System.Web.HttpContext.Current.Session["EmployerID"].ToString();
        //        if (EmployerID == "0")
        //        {
        //        }
        //        else
        //        {
        //            _SQLSelectionCriteria = " AND EmployerID= '" + EmployerID + "'";
        //        }

        //        if (collection["bp.startdate"].ToString().Length > 0)
        //        {
        //            _SQLSelectionCriteria = " AND (borrowdate>='" + _startdate + "'";
        //        }

        //        if (collection["bp.enddate"].ToString().Length > 0)
        //        {
        //            _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
        //        }

        //        if (collection["mpesano"].ToString().Length > 0)
        //        {
        //            _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
        //        }

        //        System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

        //        //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
        //        if (_action == "Print")
        //        {
        //            //Call print method
        //        }
        //        else
        //        {
        //            //call Load method to the grid in question
        //        }
        //    }
        //    return View(_viewtoDisplay);
        //}
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ViewLoanTypes()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewLoanTypes(FormCollection collection)
        {
            string _viewtoDisplay = "ViewLoanTypes";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "ViewLoanTypesDates";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _selectedLoanCodes = collection["selectedLoanCodes"].ToUpper();

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }

                if (collection["selectedLoanCodes"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the Loan Type.");
                }
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND (borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                }

                if (collection["selectedLoanCodes"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND loantype like '" + _selectedLoanCodes + "%'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
            }
            return View(_viewtoDisplay);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoanTypes()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult LoanTypes(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["loantype"] = collection["loantype"];
            ViewData["loancode"] = collection["loancode"];
            ViewData["interestrate"] = collection["intrestrate"];
            ViewData["repaymentperiod"] = collection["repaymentperiod"];
            ViewData["minamount"] = collection["minamount"];
            ViewData["maxamount"] = collection["maxamount"];


            if (collection["loantype"].ToString().Length == 0)
            {
                errors.Add("You must ADD the loantype.Its Mandatory");
            }

            if (collection["loancode"].ToString().Length == 0)
            {
                errors.Add("You must ADD the loancode.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select * from LoanCodes WHERE loancode  = '" + collection["loancode"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("loan code already exists");

            }


            //Stationcode
            //string stationCode = Blogic.RunStringReturnStringValue("select loanname from LoanTypes WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "LoanType";
                ViewData["loantype"] = collection["loantype"];
                ViewData["loancode"] = collection["loancode"];
                ViewData["interestrate"] = collection["interestrate"];
                ViewData["repaymentperiod"] = collection["repaymentperiod"];
                ViewData["minamount"] = collection["minamount"];
                ViewData["maxamount"] = collection["maxamount"];


            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    //string SelectedChurch = (string)HttpContext.Session["selectedChurch"];                  
                    data["loantype"] = collection["loantype"];
                    data["loancode"] = collection["loancode"];
                    data["interestrate"] = collection["interestrate"];
                    data["repaymentperiod"] = collection["repaymentperiod"];
                    data["minamount"] = collection["minamount"];
                    data["maxamount"] = collection["maxamount"];
                    data["approved"] = "0";
                    //-----------------------------------------------------------
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    sql = Logic.DMLogic.InsertString("LoanCodes", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The loan type has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult LoanTypeApproval()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApproveloanTypes()
        {
            string _viewToDisplayData = "";

            _viewToDisplayData = "RegisteredLoanTypeApprovalPage";
            //string uniqueID = id.ToString();
            sql = " select * from LoanCodes where approved='0' ";
            rd = Blogic.RunQueryReturnDataReader(sql);
            using (rd)
            {
                if (rd != null && rd.HasRows)
                {
                    while (rd.Read())
                    {
                        ViewData["loantypeid"] = rd["loantypeid"];
                        ViewData["loantype"] = rd["loantype"];
                        ViewData["loancode"] = rd["loancode"];
                        ViewData["interestrate"] = rd["interestrate"];
                        ViewData["maxloan"] = rd["maxloan"];
                        ViewData["maxperiod"] = rd["maxperiod"];
                        ViewData["CreateDate"] = rd["CreateDate"];
                        ViewData["CreateBy"] = rd["CreateBy"];
                    }
                }
                else
                {
                    _viewToDisplayData = "ErrorPage";
                }
            }

            return View(_viewToDisplayData);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ApproveLoanTypes(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                DateTime getdate = DateTime.Now;
                string dt = "";
                //data["id"] = id;
                // data["loantypeid"] = collection["loantypeid"];
                data["loantype"] = collection["loantype"];
                data["loancode"] = collection["loancode"];
                data["interestrate"] = collection["interestrate"];
                data["maxloan"] = collection["maxloan"];
                data["maxperiod"] = collection["maxperiod"];
                data["CreateBy"] = "ADMIN";
                data["CreateDate"] = dt.ToString();
                //data["PasswordChangeFlag"] = "0";
                // data["Approved"] = "1";
                //string Password = "";

                //Random generator = new Random();
                //String RandomPassword = generator.Next(0, 1000000).ToString("D6");
                //string encPassword = userdata.EncyptString(RandomPassword);
                //data["password"] = encPassword.ToString().Trim();
                //Password = RandomPassword.ToString().Trim();
                //-----------------------------------------------------------
                //------------------generate a password to be send to the user.....
                //Username:
                //Password:
                //Link:
                //extract Stationcode

                where["loantypeid"] = id;
                sql = Logic.DMLogic.UpdateString("loanCodes", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("The loanType has been approved successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OverDueInstallments()
        {

            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OverDueInstallments(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "AND BorrowerPhone='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                //string _mpesano = collection["mpesano"];

                string ftodate = "";


                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " AND borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "OverDueInstallments";
            }
            else
            {
                _viewtoDisplay = "OverDueInstallmentsDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult OverDueInstallmentsDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "paidloansdates";
            return PartialView("_OverDueInstallmentsDates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult OverDueInstallmentsDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "partiallypaidloans")
                {
                    _viewtoDisplay = "OverDueInstallments";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE borrowdate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND borrowdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ViewLoanTypesDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "viewloantypesdates";
            return PartialView("_ViewLoanTypes");
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewLoanTypesDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _selectedLoanCodes = collection["selectedLoanCodes"].ToUpper();
            string _action = collection[4];

            string _SQLSelectionCriteria = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["selectedLoanCodes"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the Loan Type.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "viewloantypesdates")
                {
                    _viewtoDisplay = "ViewLoanTypes";
                }
                //Build SQL Selection criteria to pass as a single session value
                string EmployerID = System.Web.HttpContext.Current.Session["EmployerID"].ToString();
                if (EmployerID == "0")
                {
                }
                else
                {
                    _SQLSelectionCriteria = " AND EmployerID= '" + EmployerID + "'";
                }

                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "(borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "')";
                }

                if (collection["selectedLoanCodes"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND loantype='" + _selectedLoanCodes + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PartiallyPaidLoansDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "partiallypaidloans";
            return PartialView("_PartiallyPaidLoansDates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PartiallyPaidLoansDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "partiallypaidloans")
                {
                    _viewtoDisplay = "PartiallyPaidLoans";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE borrowdate>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE borrowdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND borrowdate<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND borrowdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND borrowerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReviewLoan(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                //ModifyRegisteredUser
                _viewToDisplayData = "ReviewLoan";
                string uniqueID = id.ToString();
                // sql = " select * from loanapplications a, LoanUsers b WHERE a.borroweruniqueid=b.uniqueid and a.loanid= '" + uniqueID + "'";
                sql = "select b.deliquencystatuscode as dsc,b.deliquencystatusdescription as dsd,b.crbcreditscore as cs,* from loanapplications a, LoanUsers b WHERE a.borroweruniqueid = b.uniqueid and a.loanid = '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["loanid"];
                            ViewData["loanref"] = rd["loanref"];
                            ViewData["phonenumber"] = rd["borrowerphone"];
                            ViewData["firstname"] = rd["firstname"];
                            ViewData["borrowdate"] = rd["borrowdate"];
                            ViewData["mpesaavg"] = rd["mpesascore"];
                            ViewData["deliquencystatusdescription"] = rd["dsd"];
                            ViewData["deliquencystatuscode"] = rd["dsc"];
                            if (rd["paymentscore"].ToString() == "")
                            {
                                ViewData["paymentscore"] = "0";
                            }

                            else
                            {

                                ViewData["paymentscore"] = rd["paymentscore"];
                            }


                            ViewData["principle"] = rd["principle"];
                            ViewData["interestamount"] = rd["interestamount"];
                            ViewData["loanamount"] = rd["loanamount"];
                            ViewData["rateapplied"] = rd["rateapplied"];
                            ViewData["repayperiod"] = rd["repayperiod"];
                            ViewData["adminfee"] = rd["adminfee"];
                            ViewData["loantype"] = rd["loantype"];
                            //ViewData["qualifiedamount"] = rd["qualifedamount"];
                            ViewData["devicename"] = rd["devicename"];
                            ViewData["facebook"] = rd["FaceBookLink"];
                            ViewData["paymentscore"] = rd["paymentscore"];
                            ViewData["loancode"] = rd["loancode"];
                            ViewData["previousdevicename"] = rd["previousdevicename"];
                            Session["userphonex2"] = rd["borrowerphone"].ToString();

                            //Checking if  user changed his/her device  or not

                            if (rd["changeddevice"].ToString() == "1")
                            {
                                ViewData["changeddevice"] = "YES";
                            }
                            else
                            {
                                ViewData["changeddevice"] = "NO";
                            }

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReviewLoan(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            string sql4 = "";
            bool flag4 = false;
            bool flag = false;
            bool flag1 = false;
            bool flag2 = false;
            bool flag3 = false;
            List<string> response = new List<string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);
            string _viewtoDisplay = "PendingReview";
            string _action = collection[26];
            //string _actionA = collection[22];
            string userid = id;
            List<string> errors = new List<string>();
            string messagetosend = "";

            string gquery = "select borrowerphone from loanapplications WHERE loanid= '" + userid + "'";
            string recipients = "";
            recipients = Blogic.RunStringReturnStringValue(gquery);

            gquery = "select Tillnumber from loanapplications WHERE loanid= '" + userid + "'";
            string Tillnumber = "";
            Tillnumber = Blogic.RunStringReturnStringValue(gquery);

            gquery = "select Type from loanapplications WHERE loanid= '" + userid + "'";
            string BusinessCodes = "";
            BusinessCodes = Blogic.RunStringReturnStringValue(gquery);


            gquery = "select loanref from loanapplications WHERE loanid= '" + userid + "'";
            string lonref = "";
            lonref = Blogic.RunStringReturnStringValue(gquery);


            gquery = "select loandisbursed from loanapplications WHERE loanid= '" + userid + "'";
            string loandisbusre = "";
            loandisbusre = Blogic.RunStringReturnStringValue(gquery);

            gquery = "select preqid from preborrow where phone ='" + recipients + "'   order by preqid desc";

            preborrowid = Blogic.RunStringReturnStringValue(gquery);

            gquery = "select count(borrowerphone) from loanapplications WHERE borrowerphone='" + recipients + "'  and loandisbursed='1' and LoanCode = 'L0001' and loancleared='0' and loanref not in ('" + lonref + "')";
            string existingloan = "";
            existingloan = Blogic.RunStringReturnStringValue(gquery);

            double principle = 0;
            int reviewamount = 0;
            string LoanCode = "";
            string BusinessCode = "";
            string SendtoSafaricom = "";
            double interestamount = double.Parse(collection["interestamount"].ToString());
            double rateapplied = double.Parse(collection["rateapplied"].ToString());
            double loanamount = double.Parse(collection["loanamount"].ToString());


            double repayperiod = double.Parse(collection["repayperiod"].ToString());
            Boolean overpaid = false;
            double overpaymedamount = 0;
            overpaymedamount = double.Parse(collection["overpayment"].ToString());

            string actionuser = "";

            if (loandisbusre.ToString().Contains("1"))
            {

                errors.Add("This loan was already disbursed.illegal activity has been detected");

            }


            if (existingloan.ToString().Contains("0"))
            {



            }

            else
            {

                errors.Add("This client has an uncleared loan in the system.This application cannot not be reviewed unless he clears his/her previous loan");

            }


            if (loandisbusre.ToString().Contains("2"))
            {

                errors.Add("This loan was already rejected.illegal activity has been detected");

            }

            actionuser = MyGlobalVariables.username;

            if (!string.IsNullOrEmpty(HttpContext.Session["username"] as string))
            {
                actionuser = HttpContext.Session["username"].ToString();
            }

            if (actionuser == "")
            {

                actionuser = MyGlobalVariables.username;
            }


            if (overpaymedamount > 0) { overpaid = true; }




            if (_action == "Approve")
            {
                // _viewtoDisplay = "ReviewLoan";   
                //List<string> response = new List<string>();
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data2 = new Dictionary<string, string>();
               // Dictionary<string, string> data3 = new Dictionary<string, string>();
                //ViewData["loanref"] = collection["loanref"];
                ViewData["loanref"] = lonref;
                ViewData["phonenumber"] = collection["phonenumber"];
                ViewData["loancode"] = collection["loancode"];
                ViewData["firstname"] = collection["firstname"];
                ViewData["borrowdate"] = collection["borrowdate"];
                ViewData["mpesaavg"] = collection["mpesaavg"];
                ViewData["deliquencystatusdescription"] = collection["deliquencystatusdescription"];
                ViewData["deliquencystatuscode"] = collection["deliquencystatuscode"];
                ViewData["paymentscore"] = collection["paymentscore"];
                ViewData["principle"] = collection["principle"];
                ViewData["interestamount"] = collection["interestamount"];
                ViewData["loanamount"] = collection["loanamount"];
                ViewData["rateapplied"] = collection["rateapplied"];
                ViewData["repayperiod"] = collection["repayperiod"];
                LoanCode = collection["loancode"].ToString();
                ViewData["loantype"] = collection["loantype"];
                //ViewData["qualifiedamount"] = rd["aualifedamount"];
                ViewData["devicename"] = collection["devicename"];
                ViewData["facebook"] = collection["FaceBookLink"];
                ViewData["reviewamount"] = collection["reviewamount"];
                string adminrate = "select adminfee from loancodes WHERE loancode='" + LoanCode + "'";
                string adminrate_r = "";
                adminrate_r = Blogic.RunStringReturnStringValue(adminrate);
                double admRate = double.Parse(adminrate_r);
                double adminfee = double.Parse(collection["reviewamount"].ToString()) * admRate;
                double adminfeelessinterest = 0;
                double totalinterest = 0;
                principle = double.Parse(collection["principle"].ToString());

                if (collection["reviewamount"].ToString().Length == 0)
                {
                    errors.Add("You must ENTER the Review Amount.Its Mandatory");
                }
                else
                {
                    reviewamount = int.Parse(collection["reviewamount"].ToString());
                }

                if (reviewamount > principle)
                {
                    errors.Add("The Review Amount cannot be greater than the borrowed amount.");
                }


                if (reviewamount <= 100)
                {
                    errors.Add("The Review Amount cannot be less than shs 100");
                }


                if (errors.Count > 0)
                {
                    _successfailview = "ReviewLoan";
                    ViewData["loanref"] = collection["loanref"];
                    ViewData["phonenumber"] = collection["phonenumber"];
                    ViewData["firstname"] = collection["firstname"];
                    ViewData["borrowdate"] = collection["borrowdate"];
                    ViewData["mpesaavg"] = collection["mpesaavg"];
                    ViewData["deliquencystatusdescription"] = collection["deliquencystatusdescription"];
                    ViewData["deliquencystatuscode"] = collection["deliquencystatuscode"];
                    ViewData["paymentscore"] = collection["paymentscore"];
                    ViewData["principle"] = collection["principle"];
                    ViewData["interestamount"] = collection["interestamount"];
                    ViewData["loanamount"] = collection["loanamount"];
                    ViewData["rateapplied"] = collection["rateapplied"];
                    ViewData["repayperiod"] = collection["repayperiod"];
                    ViewData["adminfee"] = collection["adminfee"];
                    // ViewData["qualifiedamount"] = rd["aualifedamount"];
                    ViewData["devicename"] = collection["devicename"];
                    ViewData["facebook"] = collection["FaceBookLink"];
                    ViewData["reviewamount"] = collection["reviewamount"];

                }
                else
                {
                    // data["id"] = collection["loanid"];
                    messagetosend = "Dear " + collection["firstname"] + ",Your Loan application of KES: " + reviewamount + " has been successfully disbursed less service fee of Kshs. " + adminfee.ToString() + " .Kindly use paybill number 557649 and your phone number as the account number.Thank you for using counter one"; //B2C

                    if (reviewamount == principle)
                    {

                        //   data["loandisbursed"] = "1";
                        //  data["loanapproved"] = "1";
                        // data["vetted"] = "YES";
                        //data["principle"] = "1";
                        //ViewData["interestamount"] = 
                        string Year = DateTime.Parse(DateTime.Now.ToString()).Year.ToString();
                        principle = double.Parse(collection["reviewamount"].ToString());
                        double interestamt = rateapplied * principle;
                        totalinterest = (principle) * 2.6 / 100;
                        adminfeelessinterest = adminfee - totalinterest;
                        data["principle"] = collection["reviewamount"];
                       // data2["principle"] = collection["reviewamount"];
                        data["adminfee"] = collection["adminfee"];
                       // data2["interestamt"] = totalinterest.ToString();
                        //data2["interestamt"] = adminfee.ToString();
                        data["loanapprovaldate"] = dt.ToString();
                        data["loandisbursedate"] = dt.ToString();
                        //data2["loandisbursedate"] = dt.ToString();
                        DateTime sd = DateTime.Parse(data["loandisbursedate"]).AddDays(30);
                        data["expectedcleardate"] = sd.ToString();
                        data["actionedby"] = actionuser.ToString();
                        //data2["Years"] = Year;
                       
                        where["loanid"] = id;
                       // where2["loanid"] = id;
                        sql2 = Logic.DMLogic.InsertString("tblcharges",data2);
                        sql = Logic.DMLogic.UpdateString("loanapplications", data, where);
                        flag = Blogic.RunNonQuery(sql);
                        //flag2 = Blogic.RunNonQuery(sql2);

                    }
                    else if (reviewamount < principle)
                    {
                        // int interestamount = 0;
                        //double rateapplied = 0;
                        Dictionary<string, string> data1 = new Dictionary<string, string>();
                        Dictionary<string, string> data3 = new Dictionary<string, string>();
                        string Year = DateTime.Parse(DateTime.Now.ToString()).Year.ToString();
                        //collection["interestamount"] = System.Convert.ToString(interestamt);
                        principle = double.Parse(collection["reviewamount"].ToString());
                        double interestamt = rateapplied * principle;
                        totalinterest = (principle) * 2.6 / 100;
                        adminfeelessinterest = adminfee - totalinterest;
                        double loanamt = (principle + interestamt + adminfee) / (repayperiod);
                        data["amounttopay"] = System.Convert.ToString(loanamt);
                        where["loanreference"] = collection["loanref"];
                        sql = Logic.DMLogic.UpdateString("loanschedule", data, where);
                        flag = Blogic.RunNonQuery(sql);
                      //  data3["interestamt"] = totalinterest.ToString();
                        data1["adminfee"] = collection["adminfee"];
                        data1["principle"] = principle.ToString();
                       // data3["principle"] = principle.ToString();
                        data1["interestamount"] = interestamt.ToString();
                        double lnamt = (principle + interestamt + adminfee);
                        
                        // data1["loanamount"] = lnamt.ToString();
                        // data1["loanbalance"] = data1["loanamount"];
                        data1["loanapprovaldate"] = dt.ToString();
                        data1["loandisbursedate"] = dt.ToString();
                       // data3["loandisbursedate"] = dt.ToString();
                        //data3["Years"] = Year;
                       
                        data1["actionedby"] = actionuser.ToString();
                        where1["loanid"] = id;
                        //where3["loanid"] = id;
                        //sql3 = Logic.DMLogic.InsertString("tblcharges", data3);
                        sql1 = Logic.DMLogic.UpdateString("loanapplications", data1, where1);
                        flag1 = Blogic.RunNonQuery(sql1);
                        //flag3 = Blogic.RunNonQuery(sql3);

                    }

                }
                //for test
                int strcontinue = 1;
                if (flag)//am assuming this is the success scenario that follows disburse or the approve button..
                {
                    //Send SMS
                    if (strcontinue == 1)
                    {
                        ///*AccountNo	ItemName	AccountType	  selectcode
                        //J005	Bank Charges	Expense	           8
                        //F200	Processing Fee & Commissions On Loan Portfolio	Income	7
                        //F900	Interest Income On Loan Penalties	Income	6
                        //F800	Interest Income On Hatua Loans	Income	5
                        //F100	Interest Income On Salvon Emergency Loan	Income	4
                        //B010	Hatua Loan	Asset	3
                        //B020	Salvon Emergency Loan	Asset	2
                        //B035	M-Pesa (Paybill Account)	Asset	1 */

                        string MpesaPayBill = Blogic.RunStringReturnStringValue("exec fetchaccounts '1'");
                        string SalvonNormalEmergency = Blogic.RunStringReturnStringValue("exec fetchaccounts '2'");
                        string SalvonNormalEmergency1 = Blogic.RunStringReturnStringValue("exec fetchaccounts '9'");
                        string HatuaLoan = Blogic.RunStringReturnStringValue("exec fetchaccounts '3'");
                        string InterestOnNormalEmergency = Blogic.RunStringReturnStringValue("exec fetchaccounts '4'");
                        string InterestOnHatuaLoan = Blogic.RunStringReturnStringValue("exec fetchaccounts '5'");
                        string LoanPenalties = Blogic.RunStringReturnStringValue("exec fetchaccounts '6'");
                        string AdminFee = Blogic.RunStringReturnStringValue("exec fetchaccounts '7'");
                        string Bankcharges = Blogic.RunStringReturnStringValue("exec fetchaccounts '8'");
                        string suspenseacc = Blogic.RunStringReturnStringValue("exec fetchaccounts '12'");
                        string Interestcharges= Blogic.RunStringReturnStringValue("exec fetchaccounts '10'");
                        //string interestincome_cashadvance
                        double amountdisbursed = 0;
                        double amountborrowed = double.Parse(collection["principle"]);

                        if (LoanCode == "L0002")
                        {
                            amountdisbursed = reviewamount;
                        }
                        else if(LoanCode == "L0001")
                        {
                            amountdisbursed = reviewamount;
                        }
                        else if (LoanCode == "L0003")
                        {
                            amountdisbursed = reviewamount;
                        }
                        else
                        {

                            amountdisbursed = (reviewamount - adminfee);
                        }

                        //double commiss = interestamount + adminfee;
                        // double commission = commiss;
                        if (BusinessCodes == "B2B")
                        {
                            BusinessCode = "B2B";

                        }
                        else
                        {
                            BusinessCode = "B2C";

                        }

                        double transactioncost = 0;

                        if (amountdisbursed < 1000)
                        {
                            transactioncost = 15.27;
                        }
                        else
                        {
                            transactioncost = 22.40;

                        }
                        //if (BusinessCode == "B2B")
                        //{
                        //vsendsms.SaveJournals(amountborrowed, 0, 0, BankLoanDebitAccount, CustomerReceivableLoanTill, DateTime.Now, "LOAN DISBURSED", "LOAN");//MAIN LOAN
                        //vsendsms.SaveJournals(commission, 0, 0, CustomerReceivableLoanTill, CommissionsuspenceAccount, DateTime.Now, "LOAN DISBURSED", "COMMISSION");//COMMSION
                        //    //send to Kimani/Mobile
                        //}

                        //school fees loan
                        if (LoanCode == "L0002")
                        {
                            BusinessCode = "";
                            //NameValueCollection myNameValueCollection = new NameValueCollection();

                            //string amount = "2000";
                            //string phone = "073562843479";
                            //string loanreference = "SXSKJDB322837492370";
                            //String result = PHPRequest(uri, amount, phone, loanreference);

                            messagetosend = "Your shopping loan request of Ksh " + amountdisbursed + " has been reviewed and approved.You can now proceed to shop. Thank you for using Makupa app";


                            if (SendSms(messagetosend, recipients) == "Success")
                            {
                                response.Add("The loanType has  been approved successfully.");///check this message too..loan application has been approved successsfully..
                                _successfailview = "ChurchRegistrationSuccess";
                            }


                            else
                            {


                                response.Add("The loanType has  been approved successfully however the sms notification has not been sent to the customer.Please check the sms stock balance");///check this message too..loan application has been approved successsfully..
                                _successfailview = "ChurchRegistrationSuccess";
                            }


                            Blogic.RunNonQuery("update loanapplications set  isShoppingLimit=1, amounttodisburse=" + amountdisbursed + ",loanapproved=1,vetted='YES',loandisbursed='0',vettingstatus='VETTED',OriginalLoanAMount='" + amountdisbursed + "',loanapprovaldate='" + dt.ToString() + "' where loanid='" + id + "'");

                        }



                        //pppppppppppppppp
                        if (BusinessCode == "B2C")
                        {

                            //NameValueCollection myNameValueCollection = new NameValueCollection();

                            //string amount = "2000";
                            //string phone = "073562843479";
                            //string loanreference = "SXSKJDB322837492370";
                            //String result = PHPRequest(uri, amount, phone, loanreference);

                            //send to Kimani/Mobile
                            string _mpesaresponse = tcplogic.PHPRequestvs2("https://counterone.net:8181/counterone/counteronetest/b2cnew.php", amountdisbursed.ToString(), recipients, lonref);
                            //mobileno-loanid-successorfail
                            if (_mpesaresponse == "1")
                            {
                                if (SendSms(messagetosend, recipients) == "Success")
                                {

                                }
                                if (LoanCode == "L0001")
                                {
                                    vsendsms.SaveJournals(amountdisbursed, SalvonNormalEmergency, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "CASH ADVANCE", collection["loanref"]);
                                    vsendsms.SaveJournals(adminfee, SalvonNormalEmergency, AdminFee, DateTime.Now, "LOAN DISBURSED", "ADMINFEE", collection["loanref"]);
                                    vsendsms.SaveJournals(transactioncost, Bankcharges, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "Transactioncost", collection["loanref"]);
                                   

                                    if (overpaymedamount > 0)
                                    {

                                        vsendsms.SaveJournals(overpaymedamount, suspenseacc, SalvonNormalEmergency, DateTime.Now, "LOAN DISBURSED", "Previous loan overpaid amount", collection["loanref"]);

                                        Blogic.RunNonQuery("update tboverpaidloans set ispaidback=1 where phonenumber='" + recipients + "'");

                                    }

                                    overpaid = false;

                                    response.Add("The loanType has been approved successfully.");///check this message too..loan application has been approved successsfully..
                                    _successfailview = "ChurchRegistrationSuccess";

                                    //get the actual loan balance
                                    double interestamtx = reviewamount * rateapplied;
                                    double actualbal = reviewamount + interestamtx - overpaymedamount;
                                    Blogic.RunNonQuery("update loanapplications set isShoppingLimit=0,loandisbursed=1,vettingstatus='VETTED',loanapproved=1,vetted='YES',errormsg='" + _mpesaresponse + "' ,OriginalLoanAMount=" + loanamount + " ,loanamount=" + actualbal + ",loanbalance=" + actualbal + ",amounttodisburse=" + amountdisbursed + ",interestamount=" + interestamtx + " where loanid='" + id + "'");



                                    try
                                    {
                                        double interestamt = reviewamount * rateapplied;
                                        double repamount = reviewamount + adminfee + interestamt - overpaymedamount;
                                        string lntamt = System.Convert.ToString(repamount);
                                        Boolean status = Blogic.RunNonQuery("EXEC dbo.SP_loanschedule '" + collection["borrowdate"].ToString() + "','" + collection["loanref"].ToString() + "','" + lntamt + "'");
                                    }

                                    catch
                                    {



                                    }

                                    //data1["amounttodisburse"] = amountdisbursed;

                                    //capture transaction fee MpesaPayBill,Bankcharges
                                }
                                
                               else if (LoanCode == "L0003")
                                {
                                    vsendsms.SaveJournals(amountdisbursed, SalvonNormalEmergency1, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "CASH ADVANCE", collection["loanref"]);
                                    vsendsms.SaveJournals(adminfee, SalvonNormalEmergency1, AdminFee, DateTime.Now, "LOAN DISBURSED", "ADMINFEE", collection["loanref"]);
                                    vsendsms.SaveJournals(transactioncost, Bankcharges, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "Transactioncost", collection["loanref"]);
                                   

                                    if (overpaymedamount > 0)
                                    {

                                        vsendsms.SaveJournals(overpaymedamount, suspenseacc, SalvonNormalEmergency1, DateTime.Now, "LOAN DISBURSED", "Previous loan overpaid amount", collection["loanref"]);

                                        Blogic.RunNonQuery("update tboverpaidloans set ispaidback=1 where phonenumber='" + recipients + "'");

                                    }

                                    overpaid = false;

                                    response.Add("The loanType has been approved successfully.");///check this message too..loan application has been approved successsfully..
                                    _successfailview = "ChurchRegistrationSuccess";

                                    //get the actual loan balance
                                    double interestamtx = reviewamount * rateapplied;
                                    double actualbal = reviewamount + interestamtx - overpaymedamount;
                                    Blogic.RunNonQuery("update loanapplications set isShoppingLimit=0,loandisbursed=1,vettingstatus='VETTED',loanapproved=1,vetted='YES',errormsg='" + _mpesaresponse + "' ,OriginalLoanAMount=" + loanamount + " ,loanamount=" + actualbal + ",loanbalance=" + actualbal + ",amounttodisburse=" + amountdisbursed + ",interestamount=" + interestamtx + " where loanid='" + id + "'");

                                    try
                                    {
                                        double interestamt = reviewamount * rateapplied;
                                        double repamount = reviewamount + adminfee + interestamt - overpaymedamount;
                                        string lntamt = System.Convert.ToString(repamount);
                                        Boolean status = Blogic.RunNonQuery("EXEC dbo.SP_loanschedule '" + collection["borrowdate"].ToString() + "','" + collection["loanref"].ToString() + "','" + lntamt + "'");
                                    }

                                    catch
                                    {



                                    }

                                    //data1["amounttodisburse"] = amountdisbursed;

                                    //capture transaction fee MpesaPayBill,Bankcharges
                                }

                                else
                                {
                                    vsendsms.SaveJournals(amountdisbursed, HatuaLoan, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "CASH ADVANCE", collection["loanref"]);
                                    vsendsms.SaveJournals(adminfee, HatuaLoan, AdminFee, DateTime.Now, "LOAN DISBURSED", "ADMINFEE", collection["loanref"]);
                                    vsendsms.SaveJournals(transactioncost, Bankcharges, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "Transactioncost", collection["loanref"]);

                                    if (overpaymedamount > 0)
                                    {

                                        vsendsms.SaveJournals(overpaymedamount, suspenseacc, SalvonNormalEmergency, DateTime.Now, "LOAN DISBURSED", "Previous loan overpaid amount", collection["loanref"]);

                                        Blogic.RunNonQuery("update tboverpaidloans set ispaidback=1 where phonenumber='" + recipients + "'");

                                    }

                                    overpaid = false;

                                    response.Add("The loanType has been approved successfully.");///check this message too..loan application has been approved successsfully..
                                    _successfailview = "ChurchRegistrationSuccess";

                                    //get the actual loan balance
                                    double interestamtx = reviewamount * rateapplied;
                                    double actualbal = reviewamount + interestamtx - overpaymedamount;
                                    Blogic.RunNonQuery("update loanapplications set isShoppingLimit=0,loandisbursed=1,vettingstatus='VETTED',loanapproved=1,vetted='YES',errormsg='" + _mpesaresponse + "' ,OriginalLoanAMount=" + loanamount + " ,loanamount=" + actualbal + ",loanbalance=" + actualbal + ",amounttodisburse=" + amountdisbursed + ",interestamount=" + interestamtx + " where loanid='" + id + "'");



                                    try
                                    {
                                        double interestamt = reviewamount * rateapplied;
                                        double repamount = reviewamount + adminfee + interestamt - overpaymedamount;
                                        string lntamt = System.Convert.ToString(repamount);
                                        Boolean status = Blogic.RunNonQuery("EXEC dbo.SP_loanschedule '" + collection["borrowdate"].ToString() + "','" + collection["loanref"].ToString() + "','" + lntamt + "'");
                                    }

                                    catch
                                    {



                                    }

                                    //update loan as lost

                                }
                            }
                            else
                            {  //if insufficent Funds Alert Charles

                                response.Add("The loanType has not been approved successfully. " + _mpesaresponse);///check this message too..loan application has been approved successsfully..
                                _successfailview = "ChurchRegistrationSuccess";

                                Blogic.RunNonQuery("update loanapplications set isShoppingLimit=0, loandisbursed=0,loanapproved=0,vetted='NO',vettingstatus='PENDINGREVIEW',errormsg='" + _mpesaresponse + "' where loanid='" + id + "'");
                            }
                        }
                        if (BusinessCode == "B2B")
                        {
                            //send to Kimani/Mobile
                            string _mpesaresponse = tcplogic.PHPRequestvs2("https://34.224.208.116/Tests/B2BRequest.php", amountdisbursed.ToString(), recipients, lonref);
                            //mobileno-loanid-successorfail
                            if (_mpesaresponse == "1")
                            {

                                if (SendSms(messagetosend, recipients) == "Success")
                                {

                                }
                                if (LoanCode == "L0001")
                                {
                                    vsendsms.SaveJournals(amountdisbursed, SalvonNormalEmergency, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "CASH ADVANCE", collection["loanref"]);
                                    vsendsms.SaveJournals(adminfee, SalvonNormalEmergency, AdminFee, DateTime.Now, "LOAN DISBURSED", "ADMINFEE", collection["loanref"]);
                                    vsendsms.SaveJournals(transactioncost, Bankcharges, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "Transactioncost", collection["loanref"]);

                                    if (overpaymedamount > 0)
                                    {

                                        vsendsms.SaveJournals(overpaymedamount, suspenseacc, SalvonNormalEmergency, DateTime.Now, "LOAN DISBURSED", "Previous loan overpaid amount", collection["loanref"]);

                                        Blogic.RunNonQuery("update tboverpaidloans set ispaidback=1 where phonenumber='" + recipients + "'");

                                    }

                                    overpaid = false;
                                    response.Add("The loanType has been approved successfully.");///check this message too..loan application has been approved successsfully..
                                    _successfailview = "ChurchRegistrationSuccess";




                                    //get the actual loan balance
                                    double interestamtx = reviewamount * rateapplied;
                                    double actualbal = reviewamount + interestamtx - overpaymedamount;
                                    Blogic.RunNonQuery("update loanapplications set loandisbursed=1,vettingstatus='VETTED',loanapproved=1,vetted='YES',errormsg='" + _mpesaresponse + "' ,OriginalLoanAMount=" + loanamount + " ,loanamount=" + actualbal + ",loanbalance=" + actualbal + ",amounttodisburse=" + amountdisbursed + ",interestamount=" + interestamtx + " where loanid='" + id + "'");



                                    try
                                    {
                                        double interestamt = reviewamount * rateapplied;
                                        double repamount = reviewamount + adminfee + interestamt - overpaymedamount;
                                        string lntamt = System.Convert.ToString(repamount);
                                        Boolean status = Blogic.RunNonQuery("EXEC dbo.SP_loanschedule '" + collection["borrowdate"].ToString() + "','" + collection["loanref"].ToString() + "','" + lntamt + "'");
                                    }

                                    catch
                                    {



                                    }

                                    //capture transaction fee MpesaPayBill,Bankcharges
                                }
                                else
                                {
                                    vsendsms.SaveJournals(amountdisbursed, HatuaLoan, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "CASH ADVANCE", collection["loanref"]);
                                    vsendsms.SaveJournals(adminfee, HatuaLoan, AdminFee, DateTime.Now, "LOAN DISBURSED", "ADMINFEE", collection["loanref"]);
                                    vsendsms.SaveJournals(transactioncost, Bankcharges, MpesaPayBill, DateTime.Now, "LOAN DISBURSED", "Transactioncost", collection["loanref"]);

                                    if (overpaymedamount > 0)
                                    {

                                        vsendsms.SaveJournals(overpaymedamount, suspenseacc, SalvonNormalEmergency, DateTime.Now, "LOAN DISBURSED", "Previous loan overpaid amount", collection["loanref"]);

                                        Blogic.RunNonQuery("update tboverpaidloans set ispaidback=1 where phonenumber='" + recipients + "'");

                                    }

                                    overpaid = false;


                                    response.Add("The loanType has been approved successfully.");///check this message too..loan application has been approved successsfully..
                                    _successfailview = "ChurchRegistrationSuccess";





                                    //get the actual loan balance
                                    double interestamtx = reviewamount * rateapplied;
                                    double actualbal = reviewamount + interestamtx - overpaymedamount;
                                    Blogic.RunNonQuery("update loanapplications set loandisbursed=1,vettingstatus='VETTED',loanapproved=1,vetted='YES',errormsg='" + _mpesaresponse + "' ,OriginalLoanAMount=" + loanamount + " ,loanamount=" + actualbal + ",loanbalance=" + actualbal + ",amounttodisburse=" + amountdisbursed + ",interestamount=" + interestamtx + " where loanid='" + id + "'");



                                    try
                                    {
                                        double interestamt = reviewamount * rateapplied;
                                        double repamount = reviewamount + adminfee + interestamt - overpaymedamount;
                                        string lntamt = System.Convert.ToString(repamount);
                                        Boolean status = Blogic.RunNonQuery("EXEC dbo.SP_loanschedule '" + collection["borrowdate"].ToString() + "','" + collection["loanref"].ToString() + "','" + lntamt + "'");
                                    }

                                    catch
                                    {



                                    }

                                    //update loan as lost

                                }
                            }
                            else
                            {  //if insufficent Funds Alert Charles

                                response.Add("The loanType has not been approved successfully. " + _mpesaresponse);///check this message too..loan application has been approved successsfully..
                                _successfailview = "ChurchRegistrationSuccess";

                                Blogic.RunNonQuery("update loanapplications set loandisbursed=0,loanapproved=0,vetted='NO',errormsg='" + _mpesaresponse + "' where loanid='" + id + "'");
                            }
                        }

                    }
                    else
                    {
                        Console.WriteLine("Message was not sent but record was updated.");
                        _successfailview = "ErrorPage";
                    }

                }
                else
                {
                    _successfailview = "ReviewLoan";
                }

                overpaymedamount = 0;
                overpaid = false;
                ViewData["ModelErrors"] = errors;
                ViewData["Response"] = response;
                return View(_successfailview);
            }

            else if (_action == "Reject")
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                messagetosend = "Dear " + collection["firstname"] + ", Sorry your loan has been declined due to your low CRB score.Thank you for using Counterone.";
                data["loandisbursed"] = "2";
                data["acceptrejectflag"] = "1";
                data["acceptrejectflag"] = "1";
                data["isShoppingLimit"] = "0";
                data["loanapprovaldate"] = dt.ToString();
                data["actionedby"] = actionuser.ToString();
                where["loanid"] = id;
                sql = Logic.DMLogic.UpdateString("loanapplications", data, where);
                flag = Blogic.RunNonQuery(sql);


                gquery = "Update preborrow set exhausted='1',closed='1' where preqid='" + preborrowid + "'";

                Blogic.RunNonQuery(gquery);
            }
            if (flag)//am assuming this is the success scenario that follows disburse or the approve button..
            {
                //Send SMS
                if (SendSms(messagetosend, recipients) == "Success")
                {
                    response.Add("The loan request has been rejected successfully.");///check this message too..loan application has been approved successsfully..
                    _successfailview = "ChurchRegistrationSuccess";
                }
                else
                {
                    response.Add("The loan Amount request has been rejected successfully but the sms notification was not sent.Kindly check the sms balance");///check this message too..loan application has been approved successsfully..
                    _successfailview = "ChurchRegistrationSuccess";
                    //_successfailview = "ErrorPage";
                }

            }
            else
            {
                _successfailview = "ReviewLoan";
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            try
            {
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Reviewed Loan: " + lonref + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }

            catch
            {



            }


            return View(_successfailview);
        }

        private string SendSms(string message, string recipient)
        {
            // Specify your login credentials
            string username = "counterone";
            string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
            string ffrom = "counterone";
            string status = "";

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {

                // Thats it, hit send and we'll take care of the rest
                dynamic results = gateway.sendMessage(recipient, message, ffrom);
                foreach (dynamic result in results)
                {
                    //Console.Write((string)result["number"] + ",");
                    //Console.Write((string)result["status"] + ","); // status is either "Success" or "error message"
                    //Console.Write((string)result["messageId"] + ",");
                    //Console.WriteLine((string)result["cost"]);


                    //if (results.IndexOf("Success") == -1)
                    //{
                    //    _successfailview = "SalaryReviewError";
                    //}
                    status = (string)result["status"];

                }
            }
            catch (AfricasTalkingGatewayException e)
            {

                Console.WriteLine("Encountered an error: " + e.Message);

            }

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                string dt = DateTime.Now.ToString("dd/MM/yyyyhh:mm:ss", CultureInfo.InvariantCulture);

                data["customerphone"] = recipient;
                data["message"] = message;
                data["senttime"] = dt.ToString();
                data["enquirytime"] = dt.ToString();
                data["sent"] = status;
                data["sender"] = MyGlobalVariables.username;

                sql = Logic.DMLogic.InsertString("customerenquiries_ALL", data);
                Boolean flag = Blogic.RunNonQuery(sql);

            }

            catch
            {



            }



            //log sent smsses here
            return status;
        }


        //yyyy



        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApproveLoan(string id)
        {
            bool flag = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            string _viewtodisplay = "PendingReview";
            string userid = id;
            string messagetosend = "";
            string gquery = "select borrowerphone from loanapplications WHERE loanid= '" + userid + "'";
            string recipients = Blogic.RunStringReturnStringValue(gquery);
            string query = "select principle,interestamount,adminfee from loanapplications WHERE loanid= '" + userid + "'";
            // string amt = Blogic.RunStringReturnStringValue(query);
            rd = Blogic.RunQueryReturnDataReader(query);
            using (rd)
            {
                if (rd != null && rd.HasRows)
                {
                    while (rd.Read())
                    {
                        ViewData["principle"] = rd["principle"];
                        ViewData["interestamount"] = rd["interestamount"];
                        ViewData["adminfee"] = rd["adminfee"];

                    }
                }
            }

            Dictionary<string, string> data = new Dictionary<string, string>();
            List<string> response = new List<string>();
            messagetosend = "Your loan has been successfully approved and disbursed.";
            double principle = 0;
            double interestamount = 0;
            double adminfee = 0;
            data["loandisbursed"] = "1";
            data["loanapproved"] = "1";
            data["vetted"] = "NO";
            data["loanapprovaldate"] = dt.ToString();
            data["loandisbursedate"] = dt.ToString();
            where["loanid"] = id;
            sql = Logic.DMLogic.UpdateString("loanapplications", data, where);
            flag = Blogic.RunNonQuery(sql);
            if (flag)
            {
                //Send SMS
                if (SendSms(messagetosend, recipients) == "Success")
                {
                    double amountborrowed = principle;
                    double commiss = interestamount + adminfee;
                    double commission = commiss;
                    vsendsms.SaveJournals(amountborrowed, "A-00010002", "L-00334533", DateTime.Now, "LOAN DISBURSED", "LOAN", id);//MAIN LOAN
                    vsendsms.SaveJournals(commission, "A-00010002", "L-00334533", DateTime.Now, "LOAN DISBURSED", "COMMISSION", id);//COMMMISSION


                    response.Add("The loan has been approved and disbursed.");///check this message too..loan application has been approved successsfully..
                    _successfailview = "ChurchRegistrationSuccess";
                }
                else
                {
                    Console.WriteLine("Message was not sent but record was updated.");
                    _successfailview = "ErrorPage";
                }

            }
            else
            {
                _successfailview = "ReviewLoan";
            }
            ViewData["Response"] = response;
            return View(_successfailview);
        }

    }



}

