﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class WithdrawalsController : Controller
    {
        //
        // GET: /Withdrawals/

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetWithdrawalsDetails()
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            return Json(MemberRepo.GetAllWithdrawals(), JsonRequestBehavior.AllowGet);

        }

        public ActionResult AddWithdrawals()
        {

            return View();
        }
        //Add Employee details with json data    
        [HttpPost]
        public JsonResult AddWithdrawals(SaccoWithdrawals MbrDet)

        {
            try
            {

                TransactionRepo MemberRepo = new TransactionRepo();
                MemberRepo.AddDWithdrawals(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        //Get record by Empid for edit    
        public ActionResult Edit(int? id)
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            return View(MemberRepo.GetAllWithdrawals().Find(mbr => mbr.Transid == id));
        }
        //Updated edited records    
        [HttpPost]
        public JsonResult Edit(SaccoWithdrawals MemberUpdateDet)
        {

            try
            {

                TransactionRepo MemberRepo = new TransactionRepo();
                MemberRepo.UpdateWithdrawals(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }


        //Delete the records by id    
        [HttpPost]
        public JsonResult Delete(int id)
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            MemberRepo.DeleteWithdrawals(id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }




        //Get employee list of Partial view     
        [HttpGet]
        public PartialViewResult MemberDetails()
        {

            return PartialView("_MemberDetails");

        }

    }
}

