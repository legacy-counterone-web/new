﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static TestNAVBAR.Models.ListDatabaseElements;

namespace TestNAVBAR.Controllers
{
    public class TestController : Controller
    {

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DisplayDates()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DisplayDates(FormCollection collection)
        {

            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " AND BorrowDate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria +" "+" AND BorrowDate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND BorrowPhone<='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                //call Load method to the grid in question
                }
            }
            return View();
        }
    }
}
