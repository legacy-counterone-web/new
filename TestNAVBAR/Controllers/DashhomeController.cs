﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;

namespace TestNAVBAR.Controllers
{
    public class DashhomeController : Controller
    {
        int logintime = 1;
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        Billspayment bp = new Billspayment();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();
        //
        // GET: /Dashhome/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Payments()
        {
            DateTime todaysDate = DateTime.Now.Date;
            int day = todaysDate.Day;
            if (day == 2)
            {
                ViewData["remain"] = "3";
            }
            else if (day == 3)
            {
                ViewData["remain"] = "2";
            }
            else if (day == 4)
            {
                ViewData["remain"] = "1";
            }
            else if (day == 5)
            {
                ViewData["remain"] = "0";
            }
            return View();
        }
        [HttpPost]
        public ActionResult Payments(FormCollection collection)
        {
            

            string amountdisbursed = collection["Payable"];
            string _mpesaresponse = "";
            _mpesaresponse = bp.PHPRequestvs2("https://counterone.net:8181/counterone/counteronetest/payable.php", amountdisbursed);
            //mobileno-loanid-successorfail
            if (_mpesaresponse == "1")
            {

                List<string> errors = new List<string>();
                List<string> response = new List<string>();
                _successfailview = "ChurchRegistrationSuccess";
                string gquery = "Update tblBillings set Paid='1' where Paid='0' and Months=month(getDate())-1 AND Years=year(getDate())";

                Blogic.RunNonQuery(gquery);
                vsendsms.SaveJournals(amountdisbursed, "J006", "B090", DateTime.Now, "COMMISSION PAYABLE", "PAYABLE", "CNT0611");
            }
            return View();
        }
        public ActionResult Paymentfinal()
        {
            DateTime todaysDate = DateTime.Now.Date;
            int day = todaysDate.Day;
            if (day == 2)
            {
                ViewData["remain"] = "3";
            }
            else if (day == 3)
            {
                ViewData["remain"] = "2";
            }
            else if (day == 4)
            {
                ViewData["remain"] = "1";
            }
            else if (day == 5)
            {
                ViewData["remain"] = "0";
            }
            else if (day > 5)
            {
                ViewData["remain"] = "0";
            }
            return View();
        }
        [HttpPost]
        public ActionResult Paymentfinal(FormCollection collection)
        {
           
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                
                string amountdisbursed = collection["Payable"];
                string _mpesaresponse = "";
                _mpesaresponse = bp.PHPRequestvs2("https://counterone.net:8181/counterone/counteronetest/payable.php", amountdisbursed);
                string messagetosend = _mpesaresponse;

               //SendSms(messagetosend, "0716328426");
                //mobileno-loanid-successorfail
                if (_mpesaresponse == "1")
                {
                    response.Add("Payment has been made successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                    string gquery = "Update tblBillings set Paid='1' where Paid='0' and Months=month(getDate())-1 AND Years=year(getDate())";
                    Blogic.RunNonQuery(gquery);
                    vsendsms.SaveJournals(amountdisbursed, "J006", "B090", DateTime.Now, "COMMISSION PAYABLE", "PAYABLE", "CNT0611");
            }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        public ActionResult UnlessPayment()
        {
            return View();
        }
        [HttpPost]
        public ActionResult UnlessPayment(FormCollection collection)
        {

            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                string amountdisbursed = collection["Payable"];
                string _mpesaresponse = "";
                _mpesaresponse = bp.PHPRequestvs2("https://counterone.net:8181/counterone/counteronetest/payable.php", amountdisbursed);
                //mobileno-loanid-successorfail
                if (_mpesaresponse == "1")
                {
                    response.Add("Payment has been made successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                    string gquery = "Update tblBillings set Paid='1' where Paid='0' and Months=month(getDate())-1 AND Years=year(getDate())";
                    Blogic.RunNonQuery(gquery);

                    vsendsms.SaveJournals(amountdisbursed, "J006", "B090", DateTime.Now, "COMMISSION PAYABLE", "PAYABLE", "CNT0611");
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [NonAction]
        private string SendSms(string message, string recipient)
        {
            string username = "counterone";
            string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
            string status = "";

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {

                // Thats it, hit send and we'll take care of the rest
                string from = "counterone";
                dynamic results = gateway.sendMessage(recipient, message, from);
                foreach (dynamic result in results)
                {
                    status = (string)result["status"];

                }
            }
            catch (AfricasTalkingGatewayException e)
            {

                Console.WriteLine("Encountered an error: " + e.Message);

            }
            return status;
        }
    }
}
