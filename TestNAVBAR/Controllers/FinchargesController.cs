﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class FinchargesController : Controller
    {
        //
        // GET: /Fincharges/
        public ActionResult WeeklyInterest()
        {
            return View();
        }
        public ActionResult DailyInterest()
        {
            return View();
        }
        public ActionResult MonthlyInterest()
        {
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Interest()
        {
            Finchargerepo MemberRepo = new Finchargerepo();
            return Json(MemberRepo.GetAllmMonthly());

        }
        public JsonResult Interests()
        {
            Finchargerepo MemberRepo = new Finchargerepo();
            return Json(MemberRepo.GetAllDailyinterest(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Daily()
        {
            Finchargerepo MemberRepo = new Finchargerepo();
            return Json(MemberRepo.GetAllDailylist(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Weekly()
        {
            Finchargerepo MemberRepo = new Finchargerepo();
            return Json(MemberRepo.GetWeeklylist(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult Monthly()
        {
            Finchargerepo MemberRepo = new Finchargerepo();
            return Json(MemberRepo.GetAlllistMonthly(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult Edit(int? id)
        {
            Finchargerepo IRepo = new Finchargerepo();
            return View(IRepo.GetAllmMonthly().Find(mbr => mbr.charid == id));
        }
        [HttpPost]
        public JsonResult Edit(Fincharges MemberUpdateDet)
        {

            try
            {

                Finchargerepo MemberRepo = new Finchargerepo();
                MemberRepo.Addcharge(MemberUpdateDet);
                return Json("Records Added successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
    }
}
