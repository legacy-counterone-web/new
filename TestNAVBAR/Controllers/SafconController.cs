﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class SafconController : Controller
    {
        
        //
        // GET: /Safcon/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Addcontribution()
        {
            return View();
        }
        public ActionResult Quickedit()
        {
            return View();
        }

        public JsonResult GetContributions()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllMembersContribution(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetContributionss()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllMembersContribution());

        }
        public ActionResult Contribution()
        {

            return View();
        }
        public ActionResult Edit(int? id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return View(MemberRepo.GetAllMembersContribution().Find(mbr => mbr.Userid == id));
        }
        [HttpPost]
        public JsonResult Contribution(Safcontribution MbrDet)

        {
            try
            {


                MbrDet.sharecontibution = "";
                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.AddMemberContribution(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            MemberRepo.DeleteMemberContibution(id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Edit(Safcontribution MemberUpdateDet)
        {

            try
            {

                SaccoRepo MemberRepo = new SaccoRepo();
                MemberRepo.UpdateMemberContribution(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpGet]
        public PartialViewResult SContribution()
        {

            return PartialView("_SContribution");

        }
        [HttpGet]
        public PartialViewResult Vcontribution()
        {

            return PartialView("_Vcontribution");

        }
    }
}
