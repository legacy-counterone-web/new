﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using TestNAVBAR.Filters;
using TestNAVBAR.Utilities;
using System.Web;
using TestNAVBAR.Models;
//using TestNAVBAR.U 
using System.Data.SqlClient;
using System.Net;
using System.Globalization;
using System.IO;
using System.Text;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Xml;
using System.Data;

namespace TestNAVBAR.Controllers
{
    public class pdfController : Controller
    {
        string _successfailview = "";
        string sql = "";
        string sql2 = "";
        string sql3 = "";
        string preborrowid = "";
        string selectedchurch = "";
        string selectedchurchbranch = "";
        string selectedbankbranch = "";
        System.Data.SqlClient.SqlDataReader rd;
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        Dictionary<string, string> where2 = new Dictionary<string, string>();
        Dictionary<string, string> where3 = new Dictionary<string, string>();
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();

        [HttpGet]
        public ActionResult mpesamessages()
        {
            return View();
        }

        //
        // GET: /pdf/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase postedFile, pdfclass detail)
        {
            string password = detail.id.ToString();
            if (postedFile != null)
            {
                if (Path.GetExtension(postedFile.FileName) == ".pdf")
                {
                    string filePath = string.Empty;
                    string path = Server.MapPath("~/PDF Files/");

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    filePath = path + Path.GetFileName(postedFile.FileName);
                    string extension = Path.GetExtension(postedFile.FileName);
                    if (!System.IO.File.Exists(filePath))
                    {
                        postedFile.SaveAs(filePath);
                    }

                    DateTime now = DateTime.Now;
                    string tempfirst;
                    string templast;
                    string tempphone;
                    string pathToPdf = filePath;
                    string pathToExcel = Path.ChangeExtension(pathToPdf, ".xml");

                    var resultss = pathToPdf.Substring(pathToPdf.IndexOf('_') + 1);
                    var resull = resultss.Substring(resultss.IndexOf('_') + 1);
                    string[] startdate = resull.Split('_');
                    var resu = resull.Substring(resull.IndexOf('_') + 1);

                    var resulls = resu.Substring(resu.IndexOf('_') + 1);
                    string[] senddate = resulls.Split('_');
                    var resullssss = resulls.Substring(resulls.IndexOf('_') + 1);
                    var resullsss = resullssss.Substring(resullssss.IndexOf('_') + 1);
                    string[] sendphone = resullsss.Split('.');

                    string period;
                    string firstdate = startdate[0].Trim();
                    string laststdate = senddate[0].Trim();
                    string phone = sendphone[0].Trim();
                    SqlConnection n = new SqlConnection();
                    string nn = (string)ConfigurationSettings.AppSettings["pdfconnection"];
                    SqlConnection nnn = new SqlConnection(nn);
                    nnn.Open();
                    string que = "SELECT startdate, enddate, phone,period FROM uploadedfiles WHERE startdate ='" + firstdate + "' and enddate='" + laststdate + "' and phone='" + phone + "'";
                    SqlCommand mddd = new SqlCommand(que, nnn);
                    SqlDataReader dr = mddd.ExecuteReader();
                    string one = "";
                    if (dr.Read())
                    {
                        one = dr.GetValue(0).ToString();
                        period = dr.GetValue(3).ToString();
                        if (one != "")
                        {
                            ViewBag.Message += string.Format("These file was uploaded on :-----<br />" + period, "<b>{0}</b> ");
                            return View("Index");
                            nnn.Close();
                        }
                    }
                    // Convert only tables from PDF to XLS spreadsheet and skip all textual data.
                    SautinSoft.PdfFocus f = new SautinSoft.PdfFocus();

                    // This property is necessary only for registered version
                    f.Serial = ("10000962867");

                    // 'true' = Convert all data to spreadsheet (tabular and even textual).
                    // 'false' = Skip textual data and convert only tabular (tables) data.
                    f.XmlOptions.NodeLevel4 = "row";
                    f.XmlOptions.ConvertNonTabularDataToSpreadsheet = false;
                    f.XmlOptions.UseRowColSpan = false;
                    f.Password = password;

                    f.OpenPdf(pathToPdf);
                    if (f.PageCount > 0)
                    {
                        int result = f.ToXml(pathToExcel);

                        // Open the resulted Excel workbook. 
                        if (result == 0)
                        {
                            XmlDocument doc = new XmlDocument();

                            doc.Load(pathToExcel);
                            XmlNodeList xnlist = doc.SelectNodes("/document/page/table");
                            SqlConnection con = new SqlConnection();
                            string conn = (string)ConfigurationSettings.AppSettings["pdfconnection"];
                            SqlConnection connn = new SqlConnection(conn);
                            connn.Open();


                            foreach (XmlNode xn in xnlist)
                            {
                                if (xn.HasChildNodes)
                                {
                                    foreach (XmlNode item in xn.ChildNodes)
                                    {
                                        XmlNode first = item.SelectSingleNode("cell[1]");
                                        XmlNode second = item.SelectSingleNode("cell[2]");
                                        XmlNode third = item.SelectSingleNode("cell[3]");
                                        XmlNode fourth = item.SelectSingleNode("cell[4]");
                                        XmlNode fifth = item.SelectSingleNode("cell[5]");
                                        XmlNode sixth = item.SelectSingleNode("cell[6]");

                                        if (second != null && fourth != null)
                                        {
                                            var sec = "";
                                            var thir = "";
                                            var four = "";
                                            var fift = "";
                                            var sixt = "";
                                            var fir = "";
                                            fir = first.InnerText;
                                            sec = second.InnerText;
                                            thir = third.InnerText;
                                            four = fourth.InnerText;


                                            fift = fifth.InnerText.Split('.')[0];
                                            string fifty = fift.Replace(",", "");
                                            sixt = sixth.InnerText.Split('.')[0];
                                            string sixty = sixt.Replace(",", "");
                                            sixty = sixty.Replace("-", "");
                                            //   var resultssSssss = sixt.Substring(sixt.LastIndexOf('-') + 1);
                                            string query = "INSERT INTO mpesa(transactionid,datetime,transactiontype,status,debited,credited,nationalid,period) values('" + fir + "','" + sec + "','" + thir + "','" + four + "','" + fifty + "','" + sixty + "','" + password + "','" + now + "')";
                                            string dele = "delete from mpesa where status='' and transactiontype='' and datetime='' ";
                                            SqlCommand cmd = new SqlCommand(query, connn);
                                            SqlCommand cmdd = new SqlCommand(dele, connn);
                                            cmd.ExecuteNonQuery();
                                            cmdd.ExecuteNonQuery();
                                        }

                                    }
                                }
                            }
                            string quer = "INSERT INTO uploadedfiles values('" + firstdate + "','" + laststdate + "','" + phone + "','" + now + "')";
                            SqlCommand cmddd = new SqlCommand(quer, connn);
                            cmddd.ExecuteNonQuery();
                            connn.Close();
                            ViewBag.Message += string.Format("<b>{0}</b> uploaded.<br />", pathToExcel);
                            return View("Index");
                        }
                    }
                    else
                    {
                        ViewBag.Message += string.Format("<b>{0}</b>Wrong National/Passport/Alien ID No<br />", "");
                        return View("Index");
                    }
                }
                else
                {
                    ViewBag.Message += string.Format("<b>{0}</b> please choose the correct pdf file<br />", "");
                    return View();
                }
            }
            return View();
        }

    }
}

