﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class BranchesController : Controller
    {
        // GET: /Station/
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
       

        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisterSalvonBranches()
        {
            return View();
        }
       
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegisterSalvonBranches(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["branchname"] = collection["branchname"];
            ViewData["manager"] = collection["manager"];
            ViewData["managerphone"] = collection["managerphone"];
            ViewData["officephone"] = collection["officephone"];
            ViewData["town"] = collection["town"];
        
            if (collection["branchname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the branchname.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select * from Branches WHERE branchname = '" + collection["branchname"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the manager are unique for each branch.This manager record already exists for another branch.");
            }


            if (collection["managerPhone"].ToString().Length == 0)
            {
                errors.Add("You must ADD the manager phone.Its Mandatory");
            }

            if (String.IsNullOrEmpty(collection["officephone"]))
           {
                errors.Add("You must ADD the office phone.Its Mandatory for notificiations.");
           }
        
            if (collection["town"].ToString().Length == 0)
            {
                errors.Add("You must ADD the town.Its Mandatory");
            }
          
            //Stationcode
            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Branches WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "RegisterSalvonBranches";
                ViewData["branchname"] = collection["branchname"];
                ViewData["manager"] = collection["manager"];
                ViewData["managerphone"] = collection["mangerphone"];
                ViewData["officephone"] = collection["officephone"];
                ViewData["town"] = collection["town"];
               
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    data["branchname"] = collection["branchname"];
                    data["manager"] = collection["manager"];
                    data["managerphone"] = collection["managerphone"];
                    data["officephone"] = collection["officephone"];
                    data["town"] = collection["town"];
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    sql = Logic.DMLogic.InsertString("Branches", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The branch has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

    }
}