﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class CustomerEnquiriesController: Controller
    {
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();


        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult CustomerEnquiries()
        {
            return View();
        }


        [NoDirectAccessAttribute]
     
        public ActionResult CloseMessage(string id)
        {
            string sql = "";
            bool flag = false;
            string messagetosend = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
       

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                //data["id"] = id;
                //string recipients = collection["customerphone"];
                data["response"] = "";
                data["responsetime"] = dt.ToString();
                data["responded"] = "2";
                data["responder"] = MyGlobalVariables.username; ;
                where["messageid"] = id;
                sql = Logic.DMLogic.UpdateString("customerenquiries", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    
                        response.Add("The customer enquiry has been responded to and closed successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                   
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [NoDirectAccessAttribute]
        public ActionResult ReplyEnquiry(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
        
                _viewToDisplayData = "ReplyEnquiry";
                string messageid = id.ToString();
                sql = " select * from customerenquiries WHERE messageid= '" + messageid + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["messageid"];
                            ViewData["customerphone"] = rd["customerphone"];
                            ViewData["enquiry"] = rd["enquiry"];
                            ViewData["reponse"] = rd["response"];                         
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReplyEnquiry(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string messagetosend = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                string recipients = collection["customerphone"];
                data["response"] = collection["response"];
                data["responsetime"] =dt.ToString() ;
                data["responded"] = "1";
                data["responder"]= "ADMIN";
                where["messageid"] = id;
                sql = Logic.DMLogic.UpdateString("customerenquiries", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    if (SendSms(collection["response"], recipients) == "Success")
                    {
                        response.Add("The message has been send successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }                     
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
     
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReminderMessages(string id)
        {
            DateTime getdate = DateTime.Now;
            string _viewToDisplayData = "";
            if (id != null)
            {
                _viewToDisplayData = "ReminderMessages";
                string uniqueID = id.ToString();
                sql = "  select * from AdminMessages order by messageid DESC"; 
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["customerphone"] = rd["customerphone"];
                            ViewData["sendtime"] = rd["sendtime"];
                            ViewData["sender"] = rd["sender"];
                            ViewData["adminmessage"] = rd["adminmessage"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ReminderMessages";
            }

            return View(_viewToDisplayData);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RespondedEnquiries()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RespondedEnquiries(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "WHERE customerphone='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                //string _mpesano = collection["mpesano"];

                string ftodate = "";


                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = " WHERE enquirytime>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND enquirytime<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "RespondedEnquiries";
            }
            else
            {
                _viewtoDisplay = "RespondedEnquiriesDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RespondedEnquiriesDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "mpesalogs";
            return PartialView("_RespondedEnquiriesDates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RespondedEnquiriesDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "mpesalogs")
                {
                    _viewtoDisplay = "RespondedEnquiries";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE enquirytime>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE enquirytime>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND enquirytime<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND enquirytime<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND customerphone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ReminderMessagesDates()
        {
            System.Web.HttpContext.Current.Session["_selectedOption"] = "mpesalogs";
            return PartialView("_ReminderMessagesDates");
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReminderMessagesDates(FormCollection collection)
        {
            string _viewtoDisplay = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;
            string _startdate = collection["bp.startdate"];
            string _enddate = collection["bp.enddate"];
            string _mpesano = collection["mpesano"];
            string _action = collection[4];

            string _SQLSelectionCriteria = "";
            string _SQLSelectionCriteriaWithoutPhone = "";

            if (collection["bp.startdate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the start Date.");
            }

            if (collection["bp.enddate"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the End Date.");
            }

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must Indicate the MPESA No.");
            }

            if (errors.Count > 0)
            {
            }
            else
            {
                _SQLSelectionCriteria = null;
                _SQLSelectionCriteriaWithoutPhone = null;
                if (System.Web.HttpContext.Current.Session["_selectedOption"].ToString() == "mpesalogs")
                {
                    _viewtoDisplay = "ReminderMessages";
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE datesent>='" + _startdate + "'";
                    _SQLSelectionCriteriaWithoutPhone = " WHERE datesent>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datesent<='" + _enddate + "'";
                    _SQLSelectionCriteriaWithoutPhone = _SQLSelectionCriteriaWithoutPhone + " " + " AND datesent<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND phone='" + _mpesano + "'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                System.Web.HttpContext.Current.Session["SQLSelectionCriteriaWithoutPhone"] = _SQLSelectionCriteriaWithoutPhone.ToString();

                //check if to print the report or Load the report to the dataGrid view depending on actions Print or Load
                if (_action == "Print")
                {
                    //Call print method
                }
                else
                {
                    //call Load method to the grid in question
                }
            }
            return View(_viewtoDisplay);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReminderMessages(FormCollection collection)
        {
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            ViewData["ModelErrors"] = errors;

            if (_action_w.ToString() == "Search") //phone
            {

                string _mpesano = collection["mpesano"];


                if (collection["mpesano"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the MPESA No.");
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = "phone='" + _mpesano + "'";
                }

            }
            else if (_action.ToString() == "Load")//dates
            {
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];


                string ftodate = "";


                string ftenddate = "";

                if (collection["bp.startdate"].ToString().Length == 0 || collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
                {
                    errors.Add("You must Indicate a valid  End Date.");
                }
                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
                    _startdate = ftodate;
                    _SQLSelectionCriteria = "datesent>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0 && !collection["bp.startdate"].ToString().Contains("mm/"))
                {
                    ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
                    _enddate = ftenddate;
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND datesent<='" + _enddate + "'";
                }
            }
            if (_SQLSelectionCriteria.Length == 0)
            {
                errors.Add("The selection Criteria cannot be empty!!");
            }

            if (errors.Count > 0)
            {
                _viewtoDisplay = "ReminderMessages";
            }
            else
            {
                _viewtoDisplay = "ReminderMessagesDates";

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();

            }


            return View(_viewtoDisplay);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SendMessage()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SendMessage(FormCollection collection)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            string messagetosend = "";
            string _action = collection[3];
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            Dictionary<string, string> data = new Dictionary<string, string>();
            List<string> errors = new List<string>();
            List<string> response = new List<string>();           
            ViewData["customerphone"] = collection["customerphone"];
            ViewData["adminmessage"] = collection["adminmessage"];


            if (_action == "SendToOne")
            {
                string No = collection["customerphone"];
                if (collection["customerphone"].ToString().Length == 0)
                {
                    errors.Add("You must ADD the Phone Number.Its Mandatory");
                }
                sql1 = "select phonenumber from Loanusers where phonenumber='" + No + "'";
                string codeExists = Blogic.RunStringReturnStringValue(sql1);

                if (codeExists == "" && codeExists != "0")
                {
                    errors.Add("Please MAKE SURE the Phone Number is valid. This Phone Number does not exist.");
                }


                if (collection["adminmessage"].ToString().Length == 0)
                {
                    errors.Add("You must ADD the message.Its Mandatory");
                }

                if (errors.Count > 0)
                {
                    _successfailview = "SendMessage";
                    ViewData["customerphone"] = collection["customerphone"];
                    ViewData["adminmessage"] = collection["adminmessage"];
                }
                else
                {
                    try
                    {
                        data["customerphone"] = collection["customerphone"];
                        data["adminmessage"] = collection["adminmessage"];
                        data["sendtime"] = dt.ToString();
                        data["sent"] = "1";
                        data["sender"] = "ADMIN";
                        sql = Logic.DMLogic.InsertString("AdminMessages", data);

                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            messagetosend = collection["adminmessage"];
                            if (SendSms(messagetosend, collection["customerphone"]) == "Success")
                            {
                                response.Add("The message has been send successfully.");
                                _successfailview = "ChurchRegistrationSuccess";
                            }

                        }
                    }
                    catch
                    {
                        _successfailview = "ErrorPage";
                    }
                }
            }
            else if (_action == "SendToMany")
            {
                if (collection["adminmessage"].ToString().Length == 0)
                {
                    errors.Add("You must ADD the message.Its Mandatory");
                }

                if (errors.Count > 0)
                {
                    _successfailview = "SendMessage";
                    ViewData["customerphone"] = collection["customerphone"];
                    ViewData["adminmessage"] = collection["adminmessage"];
                }
                else
                {
                    string phoneno = collection["customerphone"];
                    messagetosend = collection["adminmessage"];
                    if (SendSms(messagetosend, phoneno) == "Success")
                    {
                        data["customerphone"] = phoneno;
                        data["adminmessage"] = collection["adminmessage"];
                        data["sendtime"] = dt.ToString();
                        data["sent"] = "1";
                        data["sender"] = "ADMIN";
                        sql = Logic.DMLogic.InsertString("AdminMessages", data);

                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            response.Add("The message has been send successfully.");
                            _successfailview = "ChurchRegistrationSuccess";

                        }
                    }
                  
                }
            }

            ////
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }//

      
        private string SendSms(string message, string recipient)
        {
            // Specify your login credentials
            string username = "counterone";
            string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
            string ffrom = "counterone";
            string status = "";

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {

                // Thats it, hit send and we'll take care of the rest
                dynamic results = gateway.sendMessage(recipient, message,ffrom);
                foreach (dynamic result in results)
                {
                    
                    status = (string)result["status"];

                }
            }
            catch (AfricasTalkingGatewayException e)
            {

                Console.WriteLine("Encountered an error: " + e.Message);

            }


            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                string dt = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture);

                data["customerphone"] = recipient;
                data["message"] = message;
                data["senttime"] = dt.ToString();
                data["enquirytime"] = dt.ToString();
                data["sent"] = status;
                data["sender"] = MyGlobalVariables.username;

                sql = Logic.DMLogic.InsertString("customerenquiries_ALL", data);
                Boolean flag = Blogic.RunNonQuery(sql);

            }

            catch
            {



            }



            return status;
        }




    }
}