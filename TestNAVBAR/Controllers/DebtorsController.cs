﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;

namespace TestNAVBAR.Controllers
{
    public class DebtorsController : Controller
    {
        string _successfailview = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        Dictionary<string, string> where2 = new Dictionary<string, string>();
        Dictionary<string, string> where3 = new Dictionary<string, string>();
       
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();
        //
        // GET: /Debtors/


        public ActionResult ApprovedDebtors()
        {
            
            return View();
        }

        public ActionResult RejectedDebtors()
        {

            return View();
        }
        public ActionResult ApprovedInvoices()
        {

            return View();
        }

        public ActionResult RejectedInvoices()
        {

            return View();
        }
        public ActionResult Invoicepaymentapproval(int id)
        {

            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "Invoicepaymentapproval";
                string uniqueID = id.ToString();
                sql = " select * from tblinvoicepayments WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Debtorsname"] = rd["DebitAccount"];
                            ViewData["Salesaccount"] = rd["CreditAccount"];
                            ViewData["Amount"] = rd["Amount"];
                             ViewData["Description"] = rd["Narration"];
                            ViewData["TransDate"] = rd["TransDate"];
                            ViewData["Disbursesource"] = rd["Disbursesource"];
                            ViewData["Vourcherno"] = rd["Vourcherno"];

                            ViewData["Actionedby"] = rd["Actionedby"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Invoicepaymentapproval(FormCollection collection,string id)
        {

            string sql = "";
            bool flag = false;
            string sql1 = "";
            bool flag1 = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> data1 = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string _action = collection[7];
            string gquery = "select max(selectcode) from ChartOfAccounts ";
            string accountno = "";
            accountno = Blogic.RunStringReturnStringValue(gquery);

            int accountnos = Convert.ToInt32(accountno);
            int code = accountnos + 1;
            if (errors.Count > 0)
            {
                _successfailview = "Invoicepaymentapproval";

                ViewData["AccountNo"] = collection["AccountNo"];


            }
            else
            {
                try
                {
                    string actionedby = collection["Actionedby"];
                    string _userssession = HttpContext.Session["username"].ToString();
                    if (actionedby != _userssession)
                    {
                        if (_action == "Approve")
                        {
                            string[] _DebtorID = collection["Debtorsname"].Split('-');
                            string DebitAccount = _DebtorID[0].ToString().Trim();
                            string Debitnames = _DebtorID[1].ToString().Trim();

                            string[] _SalesID = collection["DisburseSource"].Split('-');
                            string CreditAccount = _SalesID[0].ToString().Trim();
                            string Creditnames = _SalesID[1].ToString().Trim();
                            DateTime dts = DateTime.Now;
                            int _min = 100;
                            int _max = 100;
                            Random _rdm = new Random();
                            Random random = new Random();
                            Random generator = new Random();

                            string value = generator.Next(0, 100).ToString("D2");
                            //string refs = dat + collection["Loanref"];
                            double amount = Convert.ToDouble(collection["Amount"]);

                            string dat = "BFPN" + "/" + DebitAccount + "/" + CreditAccount + "/" + dts.Year.ToString() + "/" + dts.Month.ToString() + "/" + dts.Day.ToString() + "/" + value;

                            //string DateString = collection["Datecreated"];
                            //IFormatProvider culture = new CultureInfo("en-US", true);
                            //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy-MM-dd", culture);
                            //string[] _creditorID = collection["Debtorsname"].Split('-');
                            /// data["Accountno"] = _creditorID[0].ToString().Trim();
                            // data["Creditorname"] = _creditorID[1].ToString().Trim();
                            data1["loanid"] = dat;
                            data1["Amount"] = amount.ToString();
                            data1["DebitAccount"] = DebitAccount;
                            data1["CreditAccount"] = CreditAccount;
                            data1["Narration"] = collection["Description"];
                            data1["TransDate"] = dt;//dateVal.ToString();
                            data1["LoanType"] = "Transaction";
                            data1["CommissionAmount"] = "0";
                            data1["mpesaref"] = dat;

                           // data["Debtorsname"] = collection["Debtorsname"];

                            data["approvedby"] = _userssession;

                            //-----------------------------------------------------------
                            data["approved"] = "1";
                            where["id"] = id;
                            sql1 = Logic.DMLogic.InsertString("JournalEntries", data1);

                            flag1 = Blogic.RunNonQuery(sql1);
                            sql = Logic.DMLogic.UpdateString("tblinvoicepayments", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Invoice Payment approved successfully.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }
                        }
                        else if (_action == "Reject")
                        {
                            data["Debtorsname"] = collection["Debtorsname"];

                            data["approvedby"] = _userssession;

                            //-----------------------------------------------------------
                            data["approved"] = "2";
                            where["id"] = id;
                            sql = Logic.DMLogic.UpdateString("tblinvoicepayments", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Invoice rejected successfully.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }
                        }
                    }
                    else
                    {
                        response.Add("You are not authorized to approve");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        public ActionResult DebtorsInvoicepayments()
        {
            
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DebtorsInvoicepayments(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";

            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
          

            string _startdate = collection["bp.startdate"];

            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");


           


           
            ViewData["ModelErrors"] = errors;


            if (collection["Debtorsname"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Debtors name .Its Mandatory");
            }
            if (collection["Salesaccount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Purchase Account.Its Mandatory");
            }
           
            if (collection["Vourcherno"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Vourcherno.Its Mandatory");
            }



            if (collection["Amount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Amount.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "DebtorsInvoicepayments";
                ViewData["Debtorsname"] = collection["Debtorsname"];
               




            }
            else
            {
                try
                {
                    string _userssession = HttpContext.Session["username"].ToString();
                    string[] _DebitID = collection["Debtorsname"].Split('-');
                    data["Name"] = _DebitID[1].ToString().Trim();
                   // data["Creditorname"] = _DebitID[1].ToString().Trim();
                   // data["Debtorsname"] = _creditorID[1].ToString().Trim();
                    //collection["Debtorsname"];
                    data["DebitAccount"] = collection["Debtorsname"];
                    data["CreditAccount"] = collection["Salesaccount"];
                    data["Vourcherno"] = collection["Vourcherno"];
                    data["Amount"] = collection["Amount"];
                    data["DisburseSource"] = collection["DisburseSource"];
                    data["Narration"] = collection["Description"];
                    data["Actionedby"] = _userssession;
                   // data["Name"] = collection["Debtorsname"];
                    data["TransDate"] = ftodate;
                    data["approved"] = "0";
                    data["Types"] = "1";
                    data["approvedby"] = _userssession;
                    //data["Datecreated"] = dt;


                    //-----------------------------------------------------------
                    //data["Createdby"] = _userssession;

                    sql = Logic.DMLogic.InsertString("tblinvoicepayments", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Invoce Payment submitted successfully waitng approval.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }

                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View("DebtorsInvoicepayments");
        }
        public ActionResult DebtorsInvoice()
        {
            ViewData["Vat"] = 0.16;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult DebtorsInvoice(FormCollection collection, Vat v)
        {
            string sql = "";
            bool flag = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";

            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string vt = v.Vats.ToString();

            string _startdate = collection["bp.startdate"];
           
            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");
           

            string vattype = "";
          
            double vatrate = 0.16;

            double totalinvoice = 0;


            if (vt == "Exclusive")
            {
                totalinvoice = Convert.ToDouble(collection["Invoiceamount"]);
                vattype = "Exclusive";
                vt = "0";
            }
            else if (vt == "Inclusive")
            {
                double Totalvat = Convert.ToDouble(collection["Invoiceamount"]) * vatrate;
                double Totalinvoice = Convert.ToDouble(collection["Invoiceamount"]) + Totalvat;

                totalinvoice = Totalinvoice;
                vt = Totalvat.ToString();
                vattype = "Inclusive";
            }
            ViewData["ModelErrors"] = errors;

            
            if (collection["Debtorsname"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Debtors name .Its Mandatory");
            }
            if (collection["Salesaccount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Purchase Account.Its Mandatory");
            }
            if (collection["Vats"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Vats.Its Mandatory");
            }

            if (collection["Invoiceno"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Invoiceno.Its Mandatory");
            }

            

            if (collection["Invoiceamount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Invoiceamount.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "DebtorsInvoice";
                ViewData["Debtorsname"] = collection["Debtorsname"];
                ViewData["Invoiceno"] = collection["Invoiceno"];
                ViewData["Salesaccount"] = collection["Salesaccount"];
               
              


            }
            else
            {
                try
                {
                    string _userssession = HttpContext.Session["username"].ToString();
                    //string[] _creditorID = collection["Debtorsname"].Split('-');
                    /// data["Accountno"] = _creditorID[0].ToString().Trim();
                    // data["Creditorname"] = _creditorID[1].ToString().Trim();
                    // data["Debtorsname"] = _creditorID[1].ToString().Trim(); //collection["Debtorsname"];
                    data["Debtorsname"] = collection["Debtorsname"];
                    data["Salesaccount"] = collection["Salesaccount"];
                    data["Invoiceno"] = collection["Invoiceno"];
                    data["Vat"] = vatrate.ToString();
                    data["Vats"] = vattype.ToString();
                    data["TotalInvoiceamount"] = totalinvoice.ToString();
                    data["Description"] = collection["Description"];
                    data["Invoiceamount"] = collection["Invoiceamount"];
                    data["Totalvat"] = vt;
                    data["Duedate"] = ftodate;
                    data["approved"] = "0";
                    data["approvedby"] = _userssession;
                    data["Datecreated"] = dt;
                   

                    //-----------------------------------------------------------
                    data["Createdby"] = _userssession;

                    sql = Logic.DMLogic.InsertString("tblsalesaccount", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Invoce submitted successfully waitng approval.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }

                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View("DebtorsInvoice");
        }
        public ActionResult Debtorinvoiceapproval(int id)
        {
            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "Debtorinvoiceapproval";
                string uniqueID = id.ToString();
                sql = " select * from tblsalesaccount WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Debtorsname"] = rd["Debtorsname"];
                            ViewData["Salesaccount"] = rd["Salesaccount"];
                            ViewData["Invoiceno"] = rd["Invoiceno"];
                            ViewData["Vat"] = rd["Vat"];
                            ViewData["Totalvat"] = rd["Totalvat"];
                            ViewData["TotalInvoiceamount"] = rd["TotalInvoiceamount"];
                            ViewData["Invoiceamount"] = rd["Invoiceamount"];
                            ViewData["Duedate"] = rd["Duedate"];
                            ViewData["Vats"] = rd["Vats"];
                            ViewData["CreateBy"] = rd["CreatedBy"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["Description"] = rd["Description"];
                            
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Debtorinvoiceapproval(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string sql1 = "";
            bool flag1 = false;
            string sql2 = "";
            bool flag2 = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> data1 = new Dictionary<string, string>();
            Dictionary<string, string> data2 = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string _action = collection[12];
            string gquery = "select max(selectcode) from ChartOfAccounts ";
            string accountno = "";
            accountno = Blogic.RunStringReturnStringValue(gquery);

            int accountnos = Convert.ToInt32(accountno);
            int code = accountnos + 1;
            if (errors.Count > 0)
            {
                _successfailview = "Debtorinvoiceapproval";

                ViewData["AccountNo"] = collection["AccountNo"];


            }
            else
            {
                try
                {
                    string actionedby = collection["CreateBy"];
                    string _userssession = HttpContext.Session["username"].ToString();
                    if (actionedby != _userssession)
                    {
                        if (_action == "Approve")
                        {
                            string[] _DebtorID = collection["Debtorsname"].Split('-');
                            string DebitAccount = _DebtorID[0].ToString().Trim();
                            string Debitnames = _DebtorID[1].ToString().Trim();

                            string[] _SalesID = collection["Salesaccount"].Split('-');
                            string CreditAccount = _SalesID[0].ToString().Trim();
                            string Creditnames = _SalesID[1].ToString().Trim();
                            DateTime dts = DateTime.Now;
                            int _min = 100;
                            int _max = 100;
                            Random _rdm = new Random();
                            Random random = new Random();
                            Random generator = new Random();

                            string value = generator.Next(0, 100).ToString("D2");
                            double amount = Convert.ToDouble(collection["Invoiceamount"]);
                            //string refs = dat + collection["Loanref"];
                            double Vatamount = Convert.ToDouble(collection["Totalvat"]);

                            string dat = "B" + "." + DebitAccount + "." + CreditAccount + "." + dts.Year.ToString() + "." + dts.Month.ToString() + "." + dts.Day.ToString() + "." + value;

                            //string DateString = collection["Datecreated"];
                            //IFormatProvider culture = new CultureInfo("en-US", true);
                            //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy-MM-dd", culture);
                            //string[] _creditorID = collection["Debtorsname"].Split('-');
                            /// data["Accountno"] = _creditorID[0].ToString().Trim();
                            // data["Creditorname"] = _creditorID[1].ToString().Trim();
                            data1["loanid"] = dat;
                            data1["Amount"] = amount.ToString();
                            data1["DebitAccount"] = DebitAccount;
                            data1["CreditAccount"] = CreditAccount;
                            data1["Narration"] = collection["Description"];
                            data1["TransDate"] = collection["Duedate"]; //dateVal.ToString();
                            data1["LoanType"] = "Transaction";
                            data1["CommissionAmount"] = "0";
                            data1["mpesaref"] = dat;


                            data2["loanid"] = dat;
                            data2["Amount"] = Vatamount.ToString();
                            data2["DebitAccount"] = DebitAccount;
                            data2["CreditAccount"] = "C010";
                            data2["Narration"] = collection["Description"];
                            data2["TransDate"] = collection["Duedate"]; //dateVal.ToString();
                            data2["LoanType"] = "Transaction";
                            data2["CommissionAmount"] = "0";
                            data2["mpesaref"] = dat;


                            data["Debtorsname"] = collection["Debtorsname"];

                            data["approvedby"] = _userssession;

                            //-----------------------------------------------------------
                            data["approved"] = "1";
                            where["id"] = id;
                            sql1 = Logic.DMLogic.InsertString("JournalEntries", data1);

                            flag1 = Blogic.RunNonQuery(sql1);
                            sql2 = Logic.DMLogic.InsertString("JournalEntries", data2);

                            flag2 = Blogic.RunNonQuery(sql2);
                            sql = Logic.DMLogic.UpdateString("tblsalesaccount", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Invoice approved successfully.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }
                        }
                        else if (_action == "Reject")
                        {
                            data["Debtorsname"] = collection["Debtorsname"];

                            data["approvedby"] = _userssession;

                            //-----------------------------------------------------------
                            data["approved"] = "2";
                            where["id"] = id;
                            sql = Logic.DMLogic.UpdateString("tblsalesaccount", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Invoice rejected successfully.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }
                        }
                    }
                    else
                    {
                        response.Add("You are not authorized to approve");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }


        public ActionResult Debtorapproval(int id)
        {
            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "Debtorapproval";
                string uniqueID = id.ToString();
                sql = " select * from tblDebtors WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Creditorname"] = rd["Creditorname"];
                            ViewData["Mobileno"] = rd["Mobileno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Address"] = rd["Address"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Contactperson"] = rd["Contactperson"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["CreateBy"] = rd["CreateBy"];
                            ViewData["AccountNo"] = rd["AccountNo"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
   
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Debtorapproval(FormCollection collection,string id)
        {
            string sql = "";
            bool flag = false;
           
           
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
          
            DateTime getdate = DateTime.Now;
           
            if (errors.Count > 0)
            {
                _successfailview = "Debtorapproval";

               


            }
            else
            {
                try
                {
                    string actionedby = collection["CreateBy"];
                    string _userssession = HttpContext.Session["username"].ToString();
                    if (actionedby != _userssession)
                    {
                      
                        data["Creditorname"] = collection["Creditorname"];
                        data["Accountno"] = collection["Accountno"];
                        data["approvedby"] = _userssession;

                        //-----------------------------------------------------------
                        data["approved"] = "1";
                        where["id"] = id;
                       
                     
                        sql = Logic.DMLogic.UpdateString("tblDebtors", data, where);
                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            response.Add("Debtor has been approved successfully.");
                            _successfailview = "ChurchRegistrationSuccess";


                            try
                            {
                                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                            }

                            catch
                            {

                            }
                        }
                    }
                    else
                    {
                        response.Add("You are not authorized to approve");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
          ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
    }

        public ActionResult Rejectdebtor(int id)
        {
            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "Rejectdebtor";
                string uniqueID = id.ToString();
                sql = " select * from tblDebtors WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Creditorname"] = rd["Creditorname"];
                            ViewData["Mobileno"] = rd["Mobileno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Address"] = rd["Address"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Contactperson"] = rd["Contactperson"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["CreateBy"] = rd["CreateBy"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Rejectdebtor(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            try
            {
                string actionedby = collection["CreateBy"];
                string _userssession = HttpContext.Session["username"].ToString();
                if (actionedby != _userssession)
                {
                    data["Creditorname"] = collection["Creditorname"];

                    data["approvedby"] = _userssession;

                    //-----------------------------------------------------------
                    data["approved"] = "2";
                    where["id"] = id;
                    sql = Logic.DMLogic.UpdateString("tblDebtors", data, where);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Debtor has been approved successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }
                }
                else
                {
                    response.Add("You are not authorized to approve");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }

            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        public ActionResult Reapprovedebtor(int id)
        {
            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "Reapprovedebtor";
                string uniqueID = id.ToString();
                sql = " select * from tblDebtors WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Creditorname"] = rd["Creditorname"];
                            ViewData["Mobileno"] = rd["Mobileno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Address"] = rd["Address"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Contactperson"] = rd["Contactperson"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["CreateBy"] = rd["CreateBy"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Reapprovedebtor(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            try
            {
                string actionedby = collection["CreateBy"];
                string _userssession = HttpContext.Session["username"].ToString();
                if (actionedby != _userssession)
                {
                    data["Creditorname"] = collection["Creditorname"];

                    data["approvedby"] = _userssession;

                    //-----------------------------------------------------------
                    data["approved"] = "0";
                    where["id"] = id;
                    sql = Logic.DMLogic.UpdateString("tblDebtors", data, where);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Debtor has been approved successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }
                }
                else
                {
                    response.Add("You are not authorized to approve");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }

            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        public ActionResult AddDebtors()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddDebtors(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            string sql1 = "";
            bool flag1 = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> data1 = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);


            ViewData["ModelErrors"] = errors;

            string codeExists = Blogic.RunStringReturnStringValue("SELECT * FROM tblDebtors where Mobileno='" + collection["Mobileno"] + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Debtor already exists");

            }
            if (collection["AccountNo"].ToString().Length == 0)
            {
                errors.Add("You must ENTER Unique AccountNo.Its Mandatory");
            }
           
            string gquery = "select max(selectcode) from ChartOfAccounts ";
            string accountno = "";
            accountno = Blogic.RunStringReturnStringValue(gquery);

            int accountnos = Convert.ToInt32(accountno);
            int code = accountnos + 1;
            if (collection["Mobileno"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Mobileno .Its Mandatory");
            }
            if (collection["Creditorname"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Creditorname.Its Mandatory");
            }
            if (collection["Email"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Email.Its Mandatory");
            }

            if (collection["Town"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Town.Its Mandatory");
            }

            if (collection["Contactperson"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Contactperson.Its Mandatory");
            }

            if (collection["Address"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Address.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "AddDebtors";
                ViewData["Address"] = collection["Address"];
                ViewData["Contactperson"] = collection["Contactperson"];
                ViewData["Town"] = collection["Town"];
                ViewData["Email"] = collection["Email"];
                ViewData["Mobileno"] = collection["Mobileno"];
                ViewData["Creditorname"] = collection["Creditorname"];
                ViewData["AccountNo"] = collection["AccountNo"];


            }
            else
            {
                try
                {
                    string _userssession = HttpContext.Session["username"].ToString();
                    data1["ItemName"] = collection["Creditorname"];
                    data1["AccountNo"] = collection["AccountNo"];
                    data1["AccountType"] = "Asset";
                    data1["createby"] = _userssession;
                    data1["createddate"] = dt.ToString();
                    data1["selectcode"] = code.ToString();
                    data1["typecode"] = "3";
                       data["Creditorname"] = collection["Creditorname"];
                      data["AccountNo"] = collection["AccountNo"];
                       data["Mobileno"] = collection["Mobileno"];
                        data["Email"] = collection["Email"];
                        data["Address"] = collection["Address"];
                        data["Town"] = collection["Town"];
                        data["Contactperson"] = collection["Contactperson"];
                        data["approved"] = "0";
                          data["approvedby"] = _userssession;
                         data["Datecreated"] = dt;
                        
                    //-----------------------------------------------------------
                    data["CreateBy"] = _userssession;
                    sql1 = Logic.DMLogic.InsertString("ChartOfAccounts", data1);
                    flag1 = Blogic.RunNonQuery(sql1);
                    sql = Logic.DMLogic.InsertString("tblDebtors", data);
                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            response.Add("Debtor has been created successfully.");
                            _successfailview = "ChurchRegistrationSuccess";


                            try
                            {
                                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                            }

                            catch
                            {

                            }
                        }
                   
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View("AddDebtors");
        }
    } }

  