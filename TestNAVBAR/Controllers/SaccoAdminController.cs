﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class SaccoAdminController : Controller
    {
        //
        // GET: /SaccoAdmin/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult DeleteMember()
        {
            return View();
        }
        public ActionResult SaccoMembers()
        {
            return View();
        }
        public JsonResult GetMembers()
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetDeleteMembers()
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return Json(MemberRepo.GetAllDeleteMember());

        }
        public ActionResult GetMemberss()
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return Json(MemberRepo.GetAllMember());

        }
        public JsonResult SearchMembers()
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }

        //public ActionResult SearchMembers()
        //{
        //    SaccoRepo MemberRepo = new SaccoRepo();
        //    var population = MemberRepo.GetAllMember();
        //    return Json(population, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult AddMember()
        {

            return View();
        }
        public ActionResult AddTrans()
        {

            return View();
        }
        public ActionResult Edit(int? id)
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return View(MemberRepo.GetAllMember().Find(mbr => mbr.Userid == id));
        }
        public ActionResult Del(int? id)
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return View(MemberRepo.GetAllDeleteMember().Find(mbr => mbr.Userid == id));
        }
        [HttpPost]
        public JsonResult Del(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.DelMember(MemberUpdateDet);
                return Json("Records Deleted successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Your are not authorized to perform this action");
            }
        }
        [HttpPost]
        public JsonResult AddMember(SafrikaMembers MbrDet)

        {
            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.AddMember(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        [HttpPost]
        
        public JsonResult Delete(int id)
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            MemberRepo.DeleteMember(id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Edit(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.UpdateMember(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Your are not authorized to perform this action");
            }
        }
        public ActionResult Reject(int? id)
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return View(MemberRepo.GetAllMember().Find(mbr => mbr.Userid == id));
        }
        [HttpPost]
    
        public JsonResult Reject(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.rejectMember(MemberUpdateDet);
                return Json("Records Rejected successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpGet]
        public PartialViewResult MemberDetails()
        {

            return PartialView("_MemberDetails");

        }

    }

}
