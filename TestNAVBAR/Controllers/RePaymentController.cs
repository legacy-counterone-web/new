﻿
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Net;
using System.Text;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{

    public class RePaymentController : Controller
    {
        bool flag = false;
        string _successfailview = "";
        // GET: /Station/
        string sql = "";
        string sql1 = "";
        string _loanamount = "";
        string _paymentscore = "";
        string _expectedpayment = "";
        string _borrowdate = "";
        string _loanbalance = "";
        string _actionedby = "";
        string _mpesa = "";
        string _MPesRefs;
        bool flag1 = false;
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        string strUsername = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        private Utilities.Users ulogic = new Utilities.Users();
        
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();
        public class MyGlobalVariables
        {
            public static Dictionary<string, string> userrights = new Dictionary<string, string>();
            public static Dictionary<string, string> userrightsx = new Dictionary<string, string>();
            private Utilities.Users ulogicx = new Utilities.Users();

            public static string username;
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ListLoanRefs()
        {
            HttpContext.Session["loanid"] = null;
            HttpContext.Session["msimu"] = null;
            HttpContext.Session["loanid"] = null;
            HttpContext.Session["mpesareff"] = null;
            HttpContext.Session["loanref"] = null;
            HttpContext.Session["msimu"] = null;
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ListLoanRefs(FormCollection collection)
        {
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            string _viewToDisp = "";
            string _Phone = "";

            if (collection["mpesano"].ToString().Length == 0)
            {
                errors.Add("You must enter the customer phone number.Its Mandatory");
                response.Add("You must enter the customer phone number.Its Mandatory");
            }
            else
            //(collection["mpesano"].ToString().Length > 0)
            {
                _Phone = collection["mpesano"];
                _viewToDisp = "ListLoanRefs";

                HttpContext.Session["msimu"] = _Phone;

            }

            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult GetRef(string id)
        {
            string loan_id = id.ToString();
            string _viewToDisplayData = "";
            HttpContext.Session["loanid"] = loan_id;
            if (id != null)
            {
                _viewToDisplayData = "GetRef";

                sql = " select loanref,borrowerphone,loanamount,paymentscore,actionedby,borrowdate,loanbalance from loanapplications WHERE loanid= '" + loan_id + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            // ViewData["id"] = rd["id"];
                            ViewData["loanref"] = rd["loanref"];
                            ViewData["mpesano"] = rd["borrowerphone"];
                            _loanamount = rd["loanamount"].ToString();
                            _paymentscore = rd["paymentscore"].ToString();
                            //ViewData["actionedby"] = rd["actionedby"];
                            _actionedby = rd["actionedby"].ToString();
                            _borrowdate = rd["borrowdate"].ToString();
                            _loanbalance = rd["loanbalance"].ToString();
                            //HttpContext.Session["loanamount"] = _loanamount;
                            //HttpContext.Session["paymentscore"] = _paymentscore;
                            //HttpContext.Session["actionedby"] = _actionedby;
                            //HttpContext.Session["borrowdate"] = _borrowdate;
                            //HttpContext.Session["loanbalance"] = _loanbalance;

                        }
                    }
                }
            }

            return View(_viewToDisplayData);

        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult GetRef(FormCollection collection)
        {
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            string _where_To = "";
            string _viewToDisp = "";
            string _Phone = collection["mpesano"];
            string _MPesRef = collection["mpesaref"];
             _MPesRefs = collection["mpesaref"];
            string _dateOfTrans = collection["bp.startdate"];
            string _loanref = collection["loanref"];
            string loanid = "";
            string reasonFor = collection["correctionreason"];
            string _TransAmount = collection["amountpaid"];
            string _majina = "";
            if (HttpContext.Session["loanid"].ToString() != null)
            {
                loanid = HttpContext.Session["loanid"].ToString();
                sql = " select loanref,borrowerphone,loanamount,paymentscore,actionedby,borrowdate,loanbalance,MPESATransactionREF from loanapplications WHERE loanid= '" + loanid + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            // ViewData["id"] = rd["id"];
                            ViewData["loanref"] = rd["loanref"];
                            ViewData["mpesano"] = rd["borrowerphone"];
                            _loanamount = rd["loanamount"].ToString();
                            _paymentscore = rd["paymentscore"].ToString();
                            //ViewData["actionedby"] = rd["actionedby"];
                            _actionedby = rd["actionedby"].ToString();
                            _borrowdate = rd["borrowdate"].ToString();
                            _loanbalance = rd["loanbalance"].ToString();
                            _mpesa = rd["MPESATransactionREF"].ToString();
                            //HttpContext.Session["loanamount"] = _loanamount;
                            //HttpContext.Session["paymentscore"] = _paymentscore;
                            //HttpContext.Session["actionedby"] = _actionedby;
                            //HttpContext.Session["borrowdate"] = _borrowdate;
                            //HttpContext.Session["loanbalance"] = _loanbalance;

                        }
                    }
                }
            }
            if (_dateOfTrans.ToString().Equals("") || _dateOfTrans.ToString().Contains("mm"))//transdate
            {
                errors.Add("You must enter the transaction date.Its Mandatory");
            }
            if (_MPesRef.ToString().Equals("") || _MPesRef.ToString().Length == 0)//mpesaref
            {
                errors.Add("You must enter the mpesa transaction reference.Its Mandatory");
            }
            if (_Phone.ToString().Equals("") || _Phone.ToString().Length == 0)//mpesaref
            {
                errors.Add("You must enter the Customer Phone.Its Mandatory");
            }
            if (_TransAmount.ToString().Equals("") || _TransAmount.ToString().Length == 0)//mpesaref
            {
                errors.Add("You must enter the Transaction Amount.Its Mandatory");
            }

            if (_MPesRef.ToString().Length > 0 && _loanref.ToString().Length > 0 && _Phone.ToString().Length > 0 && _TransAmount.ToString().Length > 0)
            {
						MyGlobalVariables.username = strUsername;

						Boolean status = ulogic.SessionRights(strUsername, ref MyGlobalVariables.userrights);

						HttpContext.Session["userrights"] = MyGlobalVariables.userrights;

						if (loanid.ToString().Length > 0) {


                    //check the loanref and mpesaref from paymenthistory
                    string _DisburseExists = "";
                    string _DisburseExists2 = "";
                    string _DisburseDate = "";
                    string _paymentExists = "";
                    string PayBI2B_Insert_Success = "";
                    

                    _paymentExists = Blogic.RunStringReturnStringValue("select loanref from paymenthistory  WHERE transactioncode = '" + _MPesRef + "'");
                    if (_paymentExists.Length > 0 && _paymentExists != null)
                    {
                        //the payment Exists
                        HttpContext.Session["mpesareff"] = _MPesRef;
                        HttpContext.Session["loanref"] = _loanref;
                        // ViewData["id"] = rd["id"];
                        ViewData["loanref"] = _loanref;
                        ViewData["mpesano"] = _Phone;
                        ViewData["amountpaid"] = _TransAmount;
                        ViewData["mpesaref"] = _MPesRef;
                        ViewData["bp.startdate"] = _DisburseDate;
                        errors.Add("The requested Transaction reference " + _MPesRef +  " already exists in the system. Kindly see the table below for more details.");

                        _viewToDisp = "GetRef";
                    }
                    else
                    {
                        string twende = "";
                        string _conti = "";
                        _DisburseExists = Blogic.RunStringReturnStringValue("select transactionclosed from paybillb2cb2b where loanreference= '" + _loanref + "'");
                        //_DisburseDate = Blogic.RunStringReturnStringValue("select borrowdate from loanapplications where loanref= '" + _loanref + "'");
                        _majina = Blogic.RunStringReturnStringValue("select firstname+'#'+lastname from loanusers where phonenumber= '" + _Phone + "'");
                        if (_DisburseExists != null  && !(_DisburseExists.Length == 0))
                        {
                                if (_DisburseExists == "1") {
                                _conti = "4";
                                errors.Add("The loan has already been cleared and closed.");
                            }
                            if(_DisburseExists == "0")
                            twende = "EXISTS_PUSH _TRANSACTION" + _majina;
                            _conti = "1";
                        }
                        else
                        {
                        
                            //we need to insert
                            Random rnd = new Random();
                            int RABDED = rnd.Next(1, 10);
                            string stats = "INSERT INTO PAYBILLB2CB2B(OriginatorConversationID,MpesaResponse,DateRequested,Status,customerphone,loanreference,amount,tillnumber,transactiontype,transactionclosed,rectifiedby)VALUES('" + RABDED + "','Accept the service request successfully.','" + _DisburseDate + "','success','" + _Phone + "','" + _loanref + "','970','','B2C','0','" + strUsername.ToString() + "')";
                            flag = Blogic.RunNonQuery(stats);
                            if (flag)
                            {
                                _conti = "1";
                            }
                            else {
                                errors.Add("An error occurred while communicating with database");
                                _conti = "4";
                            }
                        }

                        if (_conti == "1") {
										  //processs payment string amount, string phone, string mpesaref,string loanreference, string disbursedate, string custname
										  Utilities.Users.ManageAuditTrail(strUsername.ToString(), (string)HttpContext.Session["mobile1"], "MainOffice", "Corrected Missing Payment: " + strUsername.ToString()+ reasonFor, "REPAYMENT CORRECTION " + _MPesRef, (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                            Dictionary<string, string> data1 = new Dictionary<string, string>();

                            // string u = "90";
                            //string s = "100";
                            double lb = Double.Parse(_loanbalance);
                            double tm = Double.Parse(_TransAmount);
                            double expectpay = lb-tm;
                            data1["transamount"] = _TransAmount.ToString();
                            data1["balance"] = _loanbalance.ToString();
                            data1["paymentscore"] = _paymentscore.ToString();
                            data1["transactioncode"] = _MPesRefs.ToString();
                            data1["loanref"] = _loanref.ToString();
                            data1["paymentdate"] = _dateOfTrans.ToString();
                            data1["rectifiedby"] = HttpContext.Session["username"].ToString();
                            data1["Actionedy"] = _actionedby.ToString(); 
                            data1["borrrowdate"] = _borrowdate.ToString(); 
                             data1["expectedpayments"] = expectpay.ToString();
                            data1["useright"] = "1";
                            data1["adminright"] = "0";
                            data1["Loanid"] = loanid.ToString();
                            sql1 = Logic.DMLogic.InsertString("tblpaymenthistory", data1);
                            flag1 = Blogic.RunNonQuery(sql1);
                            
                            string _mpesaresponse = "success"; //cpayment("https://counterone.net:8181/counterone/safrika/correctpayment.php", _TransAmount,_Phone,_MPesRef,_loanref, _dateOfTrans,_majina,strUsername);
                            //mobileno-loanid-successorfail
                            if (_mpesaresponse == "success")
                            {
                                //_viewToDisp();
                                _viewToDisp = "ChurchRegistrationSuccess";
                                response.Add("Payment details corrected successfully,Waiting for approvals!!!");
                            }
                            
                            }
                        

                    }

                }

               
            }
            else {
                errors.Add("You need to enter all the required details");
            }
            ViewData["loanref"] = _loanref;
            ViewData["mpesano"] = _Phone;
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_viewToDisp);
        }
        [NonAction]
        public string cpayment(string strURL, string amount, string phone, string mpesaref, string loanreference, string disbursedate, string custname, string username)
        {
            // transamount,mpesano,mppesaref,loanref,ddate,custname 
            string pagesource = "";
            using (WebClient client = new WebClient())
            {
                NameValueCollection postData = new NameValueCollection()
    {
            { "transamount",amount },  //order: {"parameter name", "parameter value"}
            { "loanref",loanreference },
             { "mpesaref",mpesaref },
              { "ddate",disbursedate },
               { "custname",custname },
            { "mpesano", phone },
			  { "rectifiedby", username }
	};

                System.Net.ServicePointManager.ServerCertificateValidationCallback +=
    delegate (object sender, System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                    System.Security.Cryptography.X509Certificates.X509Chain chain,
                    System.Net.Security.SslPolicyErrors sslPolicyErrors)
    {
        return true; // **** Always accept
    };

                // client.UploadValues returns page's source as byte array (byte[])
                // so it must be transformed into a string
                pagesource = Encoding.UTF8.GetString(client.UploadValues(strURL, postData));


            }

            return pagesource;
        }
        public ActionResult Ipay()
        {
            return View();
        }
        public ActionResult Repay()
        {
            return View();
        }
        public ActionResult Paymentrectification()
        {
            return View();
        }
        public ActionResult Paymentrectificationapprovals()
        {
            return View();
        }
        public ActionResult Paymentapprovals()
        {
            return View();
        }
        public ActionResult CheckPayments()
        {
            

            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CheckPayments(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;

            string sql1 = "";
            bool flag1 = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            string stk = Blogic.RunStringReturnStringValue("SELECT '123' +'-'+ phonenumber +'-'+ responseid +'-'+ amount +'-'+ type +'-'+ '1' +'-'+ status+'-'+ phonenumber2 FROM STK where amount='" + collection["Amount"] + "' and responseid='" + collection["mpesaref"] + "' and phonenumber='" + collection["Phone"] + "' and loanbalance is null");
            //string stk = Blogic.RunStringReturnStringValue("SELECT '123' +'-'+ phonenumber +'-'+ responseid +'-'+ amount +'-'+ type +'-'+ '1' +'-'+ status FROM STK where amount='1000' and responseid='NIA8HPMVO4' and phonenumber='254721293715' and loanbalance is null");
            string stk1 =  Blogic.RunStringReturnStringValue("SELECT date FROM STK where amount='" + collection["Amount"] + "' and responseid='" + collection["Amount"] + "' and phonenumber='" + collection["Phone"] + "' and loanbalance is null");

            string[] _stk = stk.Split('-');
            string accountno = _stk[0].ToString().Trim();

            string phone1 = _stk[1].ToString().Trim();

            string mpesaref = _stk[2].ToString().Trim();

            string amount = _stk[3].ToString().Trim();

            string type = _stk[4].ToString().Trim();

            //string date= _stk[5].ToString().Trim();

            string responses = _stk[5].ToString().Trim();

            string status = _stk[6].ToString().Trim();

            string phone2 = _stk[7].ToString().Trim();


            ViewData["mpesaref"] = collection["mpesaref"];
            ViewData["Phone"] = collection["Phone"];
            ViewData["Amount"] = collection["Amount"];
            //ViewData["mobileno"] = collection["mobileno"];
            // ViewData["Mobile1"] = collection["Mobile1"];


            if (collection["mpesaref"].ToString().Length == 0)
            {
                errors.Add("You must ADD the mpesaref.Its Mandatory");
            }
            if (collection["Phone"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Phonenumber.Its Mandatory");
            }
            if (collection["Amount"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Amount.Its Mandatory");
            }
           





            if (errors.Count > 0)
            {
                _successfailview = "CheckPayments";
                ViewData["Amount"] = collection["Amount"];

                ViewData["Phone"] = collection["Phone"];

                ViewData["mpesaref"] = collection["mpesaref"];



            }
            else
            {
                try
                {

                    Dictionary<string, string> data3 = new Dictionary<string, string>();
                    Dictionary<string, string> data2 = new Dictionary<string, string>();
                    string _userssession = HttpContext.Session["username"].ToString();

                   



                    data2["Accountno"] = accountno;
                    data2["responseid"] = mpesaref;//collection["Savingtype"];
                    data2["type"] = type;
                    data2["Amount"] = amount;
                    data2["phonenumber"] = phone1;
                    data2["phonenumber2"] = phone2;
                    data2["date"] = stk1;
                    data2["response"] = responses;

                    data2["status"] = status;
                    data2["balance"] = "0";
                    data2["adminright"] = "0";
                    data2["useright"] = "1";
                    data2["Actionedby"] = _userssession;
                    data2["Rectifiedby"] = _userssession;
                    data3["Pay_exists"] = "1";
                    
                    where["responseid"] = mpesaref;
                    sql = Logic.DMLogic.InsertString("STK3", data2);
                    flag = Blogic.RunNonQuery(sql);

                    sql1 = Logic.DMLogic.UpdateString("STK", data3, where);
                    flag1 = Blogic.RunNonQuery(sql1);

                    if (flag1)
                    {
                        response.Add("Payment found and its waiting approval.");
                        _successfailview = "ChurchRegistrationSuccess";

                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        public ActionResult CheckPaymentsapproval(string id)
        {

            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "CheckPaymentsapproval";
                string uniqueID = id.ToString();
                sql = " select * from STK3 WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Phone"] = rd["Phonenumber"];
                            ViewData["Phone"] = rd["Phonenumber2"];
                            ViewData["Amount"] = rd["Amount"];
                            ViewData["Date"] = rd["Date"];
                            ViewData["mpesaref"] = rd["responseid"];
                            

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        
    }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CheckPaymentsapproval(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;

            string sql1 = "";
            bool flag1 = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            string stk = Blogic.RunStringReturnStringValue("SELECT '123' +'-'+ phonenumber +'-'+ responseid +'-'+ amount +'-'+ type +'-'+ '1' +'-'+ status+'-'+ phonenumber2 FROM STK where amount='" + collection["Amount"] + "' and responseid='" + collection["mpesaref"] + "' and phonenumber='" + collection["Phone"] + "' and loanbalance is null");
            //string stk = Blogic.RunStringReturnStringValue("SELECT '123' +'-'+ phonenumber +'-'+ responseid +'-'+ amount +'-'+ type +'-'+ '1' +'-'+ status FROM STK where amount='1000' and responseid='NIA8HPMVO4' and phonenumber='254721293715' and loanbalance is null");
            string stk1 = Blogic.RunStringReturnStringValue("SELECT date FROM STK where amount='" + collection["Amount"] + "' and responseid='" + collection["Amount"] + "' and phonenumber='" + collection["Phone"] + "' and loanbalance is null");

            string[] _stk = stk.Split('-');
            string accountno = _stk[0].ToString().Trim();

            string phone1 = _stk[1].ToString().Trim();

            string mpesaref = _stk[2].ToString().Trim();

            string amount = _stk[3].ToString().Trim();

            string type = _stk[4].ToString().Trim();

            //string date= _stk[5].ToString().Trim();

            string responses = _stk[5].ToString().Trim();

            string status = _stk[6].ToString().Trim();

            string phone2 = _stk[7].ToString().Trim();


            ViewData["mpesaref"] = collection["mpesaref"];
            ViewData["Phone"] = collection["Phone"];
            ViewData["Amount"] = collection["Amount"];
            //ViewData["mobileno"] = collection["mobileno"];
            // ViewData["Mobile1"] = collection["Mobile1"];


            if (collection["mpesaref"].ToString().Length == 0)
            {
                errors.Add("You must ADD the mpesaref.Its Mandatory");
            }
            if (collection["Phone"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Phonenumber.Its Mandatory");
            }
            if (collection["Amount"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Amount.Its Mandatory");
            }






            if (errors.Count > 0)
            {
                _successfailview = "CheckPaymentsapproval";
                ViewData["Amount"] = collection["Amount"];

                ViewData["Phone"] = collection["Phone"];

                ViewData["mpesaref"] = collection["mpesaref"];



            }
            else
            {
                try
                {

                    Dictionary<string, string> data3 = new Dictionary<string, string>();
                    Dictionary<string, string> data2 = new Dictionary<string, string>();
                    string _userssession = HttpContext.Session["username"].ToString();





                   
                    data2["responseid"] = mpesaref;//collection["Savingtype"];
                    
                    
                    data2["Rectifiedby"] = _userssession;
                    data2["adminright"] = "1";

                    where["responseid"] = mpesaref;
                    sql = Logic.DMLogic.UpdateString("STK3", data2,where);
                    flag = Blogic.RunNonQuery(sql);

                    

                    if (flag)
                    {
                        response.Add("Payment approved successfully.");
                        _successfailview = "ChurchRegistrationSuccess";

                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        public ActionResult RectifyPayments(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "RectifyPayments";
                string uniqueID = id.ToString();
                sql = " select * from tblreconciliation WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Accountno"] = rd["id"];
                            ViewData["phone"] = rd["phone"];
                            ViewData["phone1"] = rd["phone1"];
                            ViewData["Amount"] = rd["Amount"];
                            ViewData["Dates"] = rd["Dates"];
                            ViewData["mpesaref"] = rd["mpesaref"];
                            ViewData["Loanamount"] = rd["Loanamount"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RectifyPayments(FormCollection collection,string id)
        {
            string sql = "";
            bool flag = false;

            string sql1 = "";
            bool flag1 = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            ViewData["Accountno"] = collection["Accountno"];

            ViewData["phone1"] = collection["phone1"];
            ViewData["phone"] = collection["phone"];
            ViewData["Amount"] = collection["Amount"];
            //ViewData["mobileno"] = collection["mobileno"];
            // ViewData["Mobile1"] = collection["Mobile1"];
            

            if (collection["Accountno"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Accountno.Its Mandatory");
            }
            if (collection["phone1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Phonenumber.Its Mandatory");
            }



            



            if (errors.Count > 0)
            {
                _successfailview = "RectifyPayments";
                ViewData["Amount"] = collection["Amount"];

                ViewData["phone1"] = collection["phone1"];

                ViewData["Accountno"] = collection["Accountno"];
                


            }
            else
            {
                try
                {
                   
                    Dictionary<string, string> data3 = new Dictionary<string, string>();
                    Dictionary<string, string> data2 = new Dictionary<string, string>();
                    string _userssession = HttpContext.Session["username"].ToString();


                  

                    data2["Accountno"] = id;
                    data2["responseid"] = collection["responseid"];//collection["Savingtype"];
                    data2["type"] = "3";
                    data2["Amount"] = collection["amount"];
                    data2["phonenumber"] = collection["phone"];
                    data2["phonenumber2"] = collection["phone1"];
                    data2["date"] = collection["Dates"];
                    data2["response"] = "1";
                    data2["responseid"] = collection["mpesaref"];
                    data2["status"] = "1";
                    data2["balance"] = "0";
                    data2["adminright"] = "0";
                    data2["Actionedby"] = _userssession;
                    data2["Rectifiedby"] = _userssession;
                    data3["Actionedby"] = _userssession;
                    data3["Rectifiedby"] = _userssession;
                    data3["useright"] = "1";
                    data3["adminright"] = "0";
                    where["mpesaref"] = collection["mpesaref"];
                    //sql = Logic.DMLogic.UpdateString("STK2", data2,where);
                    //flag = Blogic.RunNonQuery(sql);

                    sql1 = Logic.DMLogic.UpdateString("tblreconciliation", data3, where);
                    flag1 = Blogic.RunNonQuery(sql1);

                    if (flag1)
                    {
                        response.Add("Payment successfully made waiting approval.");
                        _successfailview = "ChurchRegistrationSuccess";

                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        public ActionResult RectifyPaymentsapproval(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "RectifyPaymentsapproval";
                string uniqueID = id.ToString();
                sql = " select * from tblreconciliation WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Accountno"] = rd["id"];
                            ViewData["phone"] = rd["phone"];
                            ViewData["phone1"] = rd["phone1"];
                            ViewData["Amount"] = rd["Amount"];
                            ViewData["Dates"] = rd["Dates"];
                            ViewData["mpesaref"] = rd["mpesaref"];
                            ViewData["Loanamount"] = rd["Loanamount"];
                            ViewData["Actionedby"] = rd["Actionedby"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RectifyPaymentsapproval(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;

            string sql1 = "";
            bool flag1 = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            ViewData["Accountno"] = collection["Accountno"];

            ViewData["phone1"] = collection["phone1"];
            ViewData["phone"] = collection["phone"];
            ViewData["Amount"] = collection["Amount"];
           

            if (collection["Accountno"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Accountno.Its Mandatory");
            }
            if (collection["phone1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Phonenumber.Its Mandatory");
            }




            string actionedby= collection["Actionedby"];


            if (errors.Count > 0)
            {
                _successfailview = "RectifyPaymentsapproval";
                ViewData["Amount"] = collection["Amount"];

                ViewData["phone1"] = collection["phone1"];

                ViewData["Accountno"] = collection["Accountno"];



            }
            else
            {
                try
                {
                    string _userssession = HttpContext.Session["username"].ToString();
                    if (_userssession != actionedby)
                    {

                        Dictionary<string, string> data3 = new Dictionary<string, string>();
                        Dictionary<string, string> data2 = new Dictionary<string, string>();





                        data2["Accountno"] = id;
                        data2["responseid"] = collection["responseid"];//collection["Savingtype"];
                        data2["type"] = "3";
                        data2["Amount"] = collection["amount"];
                        data2["phonenumber"] = collection["phone1"];
                        data2["phonenumber2"] = collection["phone1"];
                        data2["date"] = dt;
                        data2["response"] = "1";
                        data2["responseid"] = collection["mpesaref"];
                        data2["status"] = "1";
                        data2["balance"] = "0";
                        data2["adminright"] = "0";
                        //data2["Actionedby"] = _userssession;
                        data2["Rectifiedby"] = _userssession;

                        data3["useright"] = "1";
                        data3["adminright"] = "1";
                        where["mpesaref"] = collection["mpesaref"];
                        sql = Logic.DMLogic.InsertString("STK2", data2);
                        flag = Blogic.RunNonQuery(sql);

                        sql1 = Logic.DMLogic.UpdateString("tblreconciliation", data3, where);
                        flag1 = Blogic.RunNonQuery(sql1);

                        if (flag)
                        {
                            response.Add("Payment successfully made.");
                            _successfailview = "ChurchRegistrationSuccess";

                        }
                    }
                    else
                    {
                        response.Add("You are not authorized to perform this action.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult Getpay()
        {
            RepayRepo MemberRepo = new RepayRepo();
            return Json(MemberRepo.GetAllRepays(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Edit(int? id)
        {
            RepayRepo MemberRepo = new RepayRepo();
            return View(MemberRepo.GetAllRepays().Find(mbr => mbr.Rid == id));
        }
        [HttpPost]
      
        public JsonResult Edit(Repay MemberUpdateDet)
        {

            try
            {

                RepayRepo MemberRepo = new RepayRepo();
                MemberRepo.Updatepayment(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Your not authorizedto perform this action");
            }
        }
        public ActionResult Reject(int? id)
        {
            RepayRepo MemberRepo = new RepayRepo();
            return View(MemberRepo.GetAllRepays().Find(mbr => mbr.Rid == id));
        }
        [HttpPost]

        public JsonResult Reject(Repay MemberUpdateDet)
        {

            try
            {

                RepayRepo MemberRepo = new RepayRepo();
                MemberRepo.rejectpayment(MemberUpdateDet);
                return Json("Records Rejected successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
    }
}