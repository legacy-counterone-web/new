﻿using System;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Globalization;

namespace TestNAVBAR.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /Station/
        int logintime = 1;
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Indexr()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisteredCustomer()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisterUser()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Registerloanofficers()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegisterUser(FormCollection collection)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["username"] = collection["username"];
            ViewData["fullnames"] = collection["fullnames"];
            ViewData["email1"] = collection["email1"];
            ViewData["email2"] = collection["email2"];
            ViewData["mobile1"] = collection["mobile1"];
            ViewData["mobile2"] = collection["mobile2"];


            if (collection["username"].ToString().Length == 0)
            {
                errors.Add("You must ADD the username.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select * from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the username are unique for each Station.This user record already exists for another Station.");
            }


            if (collection["fullnames"].ToString().Length == 0)
            {
                errors.Add("You must ADD the full names.Its Mandatory");
            }

            if (collection["email1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the email.Its Mandatory for notificiations.");
            }

            if (collection["email2"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate email or same as first email.Its Mandatory");
            }
            if (collection["mobile1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Mobile Number one.Its Mandatory for notifications");
            }

            if (collection["mobile2"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Mobile Number 2.Its Mandatory or same as Mobile number one.");
            }


            int n;
            bool isNumeric = int.TryParse(collection["mobile2"].ToString(), out n);

            if (isNumeric == false)
            {
                errors.Add("The  Mobile Number2  is invalid");
            }



            int n1;
            bool isNumeric1 = int.TryParse(collection["mobile1"].ToString(), out n1);

            if (isNumeric1 == false)
            {
                errors.Add("The  Mobile Number1  is invalid");
            }

            if (checkemail(collection["email1"].ToString()) == false)
            {
                errors.Add("The email format is wrong.");
            }


            if (checkemail(collection["email2"].ToString()) == false)
            {
                errors.Add("The email format is wrong.");
            }


            //Stationcode
            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from tbUsers WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "RegisterUser";
                ViewData["username"] = collection["username"];
                ViewData["fullnames"] = collection["fullnames"];
                ViewData["email1"] = collection["email1"];
                ViewData["email2"] = collection["email2"];
                ViewData["mobile1"] = collection["mobile1"];
                ViewData["mobile2"] = collection["mobile2"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];

                    string[] _strempoyerID = collection["selectedemployer"].Split('-');
                    data["EmployerID"] = _strempoyerID[1].ToString().Trim();
                    data["username"] = collection["username"];
                    data["fullnames"] = collection["fullnames"];
                    data["email1"] = collection["email1"];
                    data["email2"] = collection["email2"];
                    data["mobile1"] = collection["mobile1"];
                    data["mobile2"] = collection["mobile2"];
                    //string _allvendor = collection["isAddmin"];
                    data["userlevel"] = "nornal";
                    data["clientregister"] = "0";
                    data["parameters"] = "0";
                    data["Approved"] = "0";
                    data["Days"] = "1";
                    data["StartTime"] = "1";
                    data["EndTime"] = "1";
                    data["firstimelogin"] = "1";
                    data["usermanagement"] = "0";
                    data["Savings"] = "0";
                    data["Billing"] = "0";
                    data["floatmngt"] = "0";                    
                    data["accounts"] = "0";
                    data["PasswordChangeFlag"] = "0";
                    Random generator = new Random();
                    String RandomPassword = generator.Next(0, 1000000).ToString("D6");
                    string encPassword = userdata.EncyptString(RandomPassword);

                    string recipients = (collection["mobile1"] + "," + collection["mobile2"]);
                    data["password"] = encPassword.ToString().Trim();
                    //-----------------------------------------------------------
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    Duser = collection["username"];
                    sql = Logic.DMLogic.InsertString("tbUsers", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The user has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        string messagetosend = "Dear " + collection["fullnames"] + " you have been created as an AdminPortal user. Your username " + Duser + " and password is " + RandomPassword;

                        SendSms(messagetosend, recipients);
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Registerloanofficers(FormCollection collection)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["username"] = collection["username"];
            ViewData["Firstname"] = collection["Firstname"];
            ViewData["Lastname"] = collection["Lastname"];
            ViewData["Email"] = collection["Email"];
            ViewData["phonenumber1"] = collection["phonenumber1"];
            ViewData["phonenumber2"] = collection["phonenumber2"];
            ViewData["Town"] = collection["Town"];
            ViewData["Country"] = collection["Country"];
            ViewData["occuption"] = collection["occuption"];
            ViewData["idno"] = collection["idno"];
            ViewData["station"] = collection["station"];
            //ViewData["Salary"] = collection["mobile2"];
            //ViewData["Commission"] = collection["mobile2"];

            if (collection["username"].ToString().Length == 0)
            {
                errors.Add("You must ADD the username.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select * from tblthirdparty WHERE username = '" + collection["username"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the username are unique for each Station.This user record already exists for another Station.");
            }


            if (collection["Firstname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Firstname.Its Mandatory");
            }
            if (collection["Lastname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Lastname.Its Mandatory");
            }
            if (collection["Email"].ToString().Length == 0)
            {
                errors.Add("You must ADD the email.Its Mandatory for notificiations.");
            }

            if (collection["Town"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Town.Its Mandatory");
            }
            if (collection["Country"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Country.Its Mandatory for notifications");
            }

            if (collection["phonenumber1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Mobile Number 2.Its Mandatory or same as Mobile number one.");
            }
            if (collection["phonenumber2"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Mobile Number 2.Its Mandatory or same as Mobile number one.");
            }

            int n;
            bool isNumeric = int.TryParse(collection["phonenumber2"].ToString(), out n);

            if (isNumeric == false)
            {
                errors.Add("The  Mobile Number2  is invalid");
            }



            int n1;
            bool isNumeric1 = int.TryParse(collection["phonenumber1"].ToString(), out n1);

            if (isNumeric1 == false)
            {
                errors.Add("The  Mobile Number1  is invalid");
            }

            if (checkemail(collection["Email"].ToString()) == false)
            {
                errors.Add("The email format is wrong.");
            }


            //if (checkemail(collection["email2"].ToString()) == false)
            //{
            //    errors.Add("The email format is wrong.");
            //}


            //Stationcode
            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from tbUsers WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "Registerloanofficers";
                ViewData["username"] = collection["username"];
                ViewData["Firstname"] = collection["Firstname"];
                ViewData["Lastname"] = collection["Lastname"];
                ViewData["Email"] = collection["Email"];
                ViewData["phonenumber1"] = collection["mobile1"];
                ViewData["phonenumber2"] = collection["mobile2"];
                ViewData["Town"] = collection["Town"];
                ViewData["Country"] = collection["Country"];
                ViewData["occupation"] = collection["occupation"];
                ViewData["idno"] = collection["idno"];
                ViewData["station"] = collection["station"];
                //ViewData["Salary"] = collection["mobile2"];
                //ViewData["Commission"] = collection["mobile2"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    string[] _strempoyerID = collection["selectedemployer"].Split('-');
                    string[] _Country = collection["Country"].Split('-');
                    string[] _occupation = collection["occupation"].Split('-');
                    data["Country"] = _Country[0].ToString().Trim();
                    data["occupation"] = _occupation[0].ToString().Trim();
                    data["username"] = collection["username"];
                    data["Firstname"] = collection["Firstname"];
                    data["Lastname"] = collection["Lastname"];
                    data["Email"] = collection["Email"];
                    data["phonenumber1"] = collection["phonenumber1"];
                    data["phonenumber2"] = collection["phonenumber2"];
                    data["Town"] = collection["Town"];
                    // data["Country"] = collection["Country"];
                    // data["occupation"] = collection["occupation"];
                    data["idno"] = collection["idno"];
                    data["station"] = collection["station"];
                    //data["Commission"] = collection["Commission"];
                    //string _allvendor = collection["isAddmin"];
                    //data["userlevel"] = "nornal";
                    data["Salary"] = "0";
                    data["Commission"] = "0";
                    data["Approved"] = "0";
                    //data["Days"] = "1";
                    //data["StartTime"] = "1";
                    //data["EndTime"] = "1";
                    //data["firstimelogin"] = "1";
                    //data["usermanagement"] = "0";
                    //data["accounts"] = "0";
                    //data["PasswordChangeFlag"] = "0";
                    Random generator = new Random();
                    String RandomPassword = generator.Next(0, 10000).ToString("D4");
                    string encPassword = userdata.EncyptString(RandomPassword);

                    string recipients = (collection["phonenumber1"] + "," + collection["phonenumber1"]);
                    data["password"] = RandomPassword.ToString().Trim();
                    //-----------------------------------------------------------
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    Duser = collection["username"];
                    sql = Logic.DMLogic.InsertString("tblthirdparty", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The user has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        string messagetosend = "Dear " + collection["username"] + " you have been created as a Thirdparty user. Your username  is  " + Duser + " and password is " + RandomPassword;

                        SendSms(messagetosend, recipients);
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TRegisterUser()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult TRegisterUser(FormCollection collection)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["username"] = collection["username"];
            ViewData["fullnames"] = collection["fullnames"];
            ViewData["email1"] = collection["email1"];
            ViewData["email2"] = collection["email2"];
            ViewData["mobile1"] = collection["mobile1"];
            ViewData["mobile2"] = collection["mobile2"];


            if (collection["username"].ToString().Length == 0)
            {
                errors.Add("You must ADD the username.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select * from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the username are unique for each Station.This user record already exists for another Station.");
            }


            if (collection["fullnames"].ToString().Length == 0)
            {
                errors.Add("You must ADD the full names.Its Mandatory");
            }

            if (collection["email1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the email.Its Mandatory for notificiations.");
            }

            if (collection["email2"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate email or same as first email.Its Mandatory");
            }
            if (collection["mobile1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Mobile Number one.Its Mandatory for notifications");
            }

            if (collection["mobile2"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Mobile Number 2.Its Mandatory or same as Mobile number one.");
            }


            int n;
            bool isNumeric = int.TryParse(collection["mobile2"].ToString(), out n);

            if (isNumeric == false)
            {
                errors.Add("The  Mobile Number2  is invalid");
            }



            int n1;
            bool isNumeric1 = int.TryParse(collection["mobile1"].ToString(), out n1);

            if (isNumeric1 == false)
            {
                errors.Add("The  Mobile Number1  is invalid");
            }

            if (checkemail(collection["email1"].ToString()) == false)
            {
                errors.Add("The email format is wrong.");
            }


            if (checkemail(collection["email2"].ToString()) == false)
            {
                errors.Add("The email format is wrong.");
            }


            //Stationcode
            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from tbUsers WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "RegisterUser";
                ViewData["username"] = collection["username"];
                ViewData["fullnames"] = collection["fullnames"];
                ViewData["email1"] = collection["email1"];
                ViewData["email2"] = collection["email2"];
                ViewData["mobile1"] = collection["mobile1"];
                ViewData["mobile2"] = collection["mobile2"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];

                    string[] _strempoyerID = collection["selectedemployer"].Split('-');
                    data["EmployerID"] = _strempoyerID[1].ToString().Trim();
                    data["username"] = collection["username"];
                    data["fullnames"] = collection["fullnames"];
                    data["email1"] = collection["email1"];
                    data["email2"] = collection["email2"];
                    data["mobile1"] = collection["mobile1"];
                    data["mobile2"] = collection["mobile2"];
                    //string _allvendor = collection["isAddmin"];
                    data["userlevel"] = "nornal";
                    data["clientregister"] = "0";
                    data["parameters"] = "0";
                    data["Approved"] = "0";
                    data["Days"] = "1";
                    data["StartTime"] = "1";
                    data["EndTime"] = "1";
                    data["firstimelogin"] = "1";
                    data["usermanagement"] = "0";
                    data["accounts"] = "0";
                    data["PasswordChangeFlag"] = "0";
                    Random generator = new Random();
                    String RandomPassword = generator.Next(0, 1000000).ToString("D6");
                    string encPassword = userdata.EncyptString(RandomPassword);

                    string recipients = (collection["mobile1"] + "," + collection["mobile2"]);
                    data["password"] = encPassword.ToString().Trim();
                    //-----------------------------------------------------------
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    Duser = collection["username"];
                    sql = Logic.DMLogic.InsertString("tbUsers", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The user has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        string messagetosend = "Dear " + collection["fullnames"] + " you have been created as an AdminPortal user. Your username " + Duser + " and password is " + RandomPassword;

                        SendSms(messagetosend, recipients);
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Tools()
        {
            return View();
        }

        // [NoDirectAccessAttribute]
        //[AcceptVerbs(HttpVerbs.Get)]
        //public ActionResult assignrights()
        //{
        //    return View();
        //}


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyRegisteredUser(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "RegisterChurchUserModifyPage";
                string uniqueID = id.ToString();
                sql = " select * from tbUsers WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["recordid"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["fullnames"] = rd["fullnames"];
                            ViewData["createby"] = rd["CreateBy"];
                            ViewData["createdate"] = rd["CreateDate"];
                            ViewData["email1"] = rd["Email1"];
                            ViewData["email2"] = rd["Email2"];
                            ViewData["mobile1"] = rd["Mobile1"];
                            ViewData["mobile2"] = rd["Mobile2"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyRegisteredfficer(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "RegisterChurchUserModifyPage";
                string uniqueID = id.ToString();
                sql = " select * from tbUsers WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["recordid"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["fullnames"] = rd["fullnames"];
                            ViewData["createby"] = rd["CreateBy"];
                            ViewData["createdate"] = rd["CreateDate"];
                            ViewData["email1"] = rd["Email1"];
                            ViewData["email2"] = rd["Email2"];
                            ViewData["mobile1"] = rd["Mobile1"];
                            ViewData["mobile2"] = rd["Mobile2"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Resetpassword(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Resetpassword";
                string uniqueID = id.ToString();
                sql = " select * from tbUsers WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["recordid"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["fullnames"] = rd["fullnames"];
                            ViewData["createby"] = rd["CreateBy"];
                            ViewData["createdate"] = rd["CreateDate"];
                            ViewData["email1"] = rd["Email1"];
                            ViewData["email2"] = rd["Email2"];
                            ViewData["mobile1"] = rd["Mobile1"];
                            ViewData["mobile2"] = rd["Mobile2"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Resetofficerpassword(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Resetofficerpassword";
                string uniqueID = id.ToString();
                sql = " select * from tblthirdparty WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["recordid"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["Firstname"] = rd["Firstname"];
                            ViewData["Lastname"] = rd["Lastname"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["phonenumber1"] = rd["phonenumber1"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Country"] = rd["Country"];
                            ViewData["occupation"] = rd["occupation"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["station"] = rd["station"];
                            ViewData["idno"] = rd["idno"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyRegisteredUser(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "";

            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                data["username"] = collection["username"];
                data["fullnames"] = collection["fullnames"];
                data["email1"] = collection["email1"];
                data["email2"] = collection["email2"];
                data["mobile1"] = collection["mobile1"];
                data["mobile2"] = collection["mobile2"];
                data["PasswordChangeFlag"] = "0";
                data["Approved"] = "0";
                string Password = "ADMIN";
                PhoneNumber = data["mobile1"];
                EmailAddress = data["email1"];
                PhoneNumber1 = data["mobile2"];
                EmailAddress1 = data["email2"];
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tbUsers", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                    if (EmailAddress.Length > 0)
                    {
                        sql = "";
                        Email_Message = "Dear " + data["fullnames"] + "." + Environment.NewLine + "You have been created as a StationPortal Admin in the bank." + Environment.NewLine + "Your details are as follows" + Environment.NewLine + "Username:" + data["username"].ToString().Trim() + " " + Environment.NewLine + "Password:" + Password.ToString().Trim() + "" + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (EmailAddress1.Length > 0)
                    {
                        sql = "";
                        Email_Message = "Dear " + data["fullnames"] + "." + Environment.NewLine + "You have been created as a StationPortal Admin in the bank." + Environment.NewLine + "Your details are as follows" + Environment.NewLine + "Username:" + data["username"].ToString().Trim() + " " + Environment.NewLine + "Password:" + Password.ToString().Trim() + " " + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (PhoneNumber.Length > 0)
                    {
                        sql = "";
                        SMS_Message = "Your details are as follows" + Environment.NewLine + "Username:" + data["username"].ToString().Trim() + "" + Environment.NewLine + "Password:" + Password.ToString().Trim() + " " + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (PhoneNumber1.Length > 0)
                    {
                        sql = "";
                        SMS_Message = "Your details are as follows" + Environment.NewLine + "Username:" + data["username"].ToString().Trim() + " " + Environment.NewLine + "Password:" + Password.ToString().Trim() + "" + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyRegisteredfficer(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "";

            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                data["Country"] = collection["Country"];
                data["occupation"] = collection["occupation"];
                data["username"] = collection["username"];
                data["Firstname"] = collection["Firstname"];
                data["Lastname"] = collection["Lastname"];
                data["Email"] = collection["Email"];
                data["phonenumber1"] = collection["phonenumber1"];
                data["phonenumber2"] = collection["phonenumber2"];
                data["Town"] = collection["Town"];
                // data["Country"] = collection["Country"];
                // data["occupation"] = collection["occupation"];
                data["idno"] = collection["idno"];
                data["station"] = collection["station"];
                //data["Commission"] = collection["Commission"];
                //string _allvendor = collection["isAddmin"];
                //data["userlevel"] = "nornal";
                //data["Salary"] = "0";
                //data["Commission"] = "0";
                //data["Approved"] = "0";
                string Password = "ADMIN";
                PhoneNumber = data["phonenumber1"];
                EmailAddress = data["email"];
                PhoneNumber1 = data["phonenumber2"];
                EmailAddress1 = data["email"];
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblthirdparty", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                    if (EmailAddress.Length > 0)
                    {
                        sql = "";
                        Email_Message = "Dear " + data["Firstname"] + "." + Environment.NewLine + "You have been created as a Tp user." + Environment.NewLine + "Your details are as follows" + Environment.NewLine + "Username:" + data["username"].ToString().Trim() + " " + Environment.NewLine + "Password:" + Password.ToString().Trim() + "" + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (EmailAddress1.Length > 0)
                    {
                        sql = "";
                        Email_Message = "Dear " + data["fullnames"] + "." + Environment.NewLine + "You have been created as a StationPortal Admin in the bank." + Environment.NewLine + "Your details are as follows" + Environment.NewLine + "Username:" + data["username"].ToString().Trim() + " " + Environment.NewLine + "Password:" + Password.ToString().Trim() + " " + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (PhoneNumber.Length > 0)
                    {
                        sql = "";
                        SMS_Message = "Your details are as follows" + Environment.NewLine + "Username:" + data["username"].ToString().Trim() + "" + Environment.NewLine + "Password:" + Password.ToString().Trim() + " " + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (PhoneNumber1.Length > 0)
                    {
                        sql = "";
                        SMS_Message = "Your details are as follows" + Environment.NewLine + "Username:" + data["username"].ToString().Trim() + " " + Environment.NewLine + "Password:" + Password.ToString().Trim() + "" + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Tools(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;

            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                data["adminfee"] = collection["adminfee"];
                data["interestfee"] = collection["interestfee"];
                data["loantype"] = collection["loantype"];
                if (flag)
                {
                    response.Add("The interest has been computed successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PaychannelApprove(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "PayChannelApprove";
                string uniqueID = id.ToString();
                sql = " select * from dbo.tbPayChannels WHERE Channelid= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["channelid"] = rd["channelid"];
                            ViewData["channelname"] = rd["channelname"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PaychannelApprove(FormCollection collection, string id)
        {

            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data["channelname"] = collection["channellname"];
                data["active"] = "1";
                data["dateactivate"] = dt;
                where["channelid"] = id;
                sql = Logic.DMLogic.UpdateString("tbPayChannels", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            return View(_successfailview);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ViewLoanUsersBList(FormCollection collection)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["username"] = collection["username"];
            ViewData["fullnames"] = collection["fullnames"];
            ViewData["email1"] = collection["email1"];
            ViewData["email2"] = collection["email2"];
            ViewData["mobile1"] = collection["mobile1"];
            ViewData["mobile2"] = collection["mobile2"];



            //Stationcode
            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from tbUsers WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "RegisterUser";
                ViewData["username"] = collection["username"];
                ViewData["fullnames"] = collection["fullnames"];
                ViewData["email1"] = collection["email1"];
                ViewData["email2"] = collection["email2"];
                ViewData["mobile1"] = collection["mobile1"];
                ViewData["mobile2"] = collection["mobile2"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    data["username"] = collection["username"];
                    data["fullnames"] = collection["fullnames"];
                    data["email1"] = collection["email1"];
                    data["email2"] = collection["email2"];
                    data["mobile1"] = collection["mobile1"];
                    data["mobile2"] = collection["mobile2"];
                    //string _allvendor = collection["isAddmin"];                 
                    //-----------------------------------------------------------
                    data["blacklist"] = "1";
                    data["CreateDate"] = dt.ToString();
                    Duser = collection["username"];
                    sql = Logic.DMLogic.InsertString("LoanUsers", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The user has been blacklisted successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        //[AcceptVerbs(HttpVerbs.Post)]
        //public ActionResult Tools(FormCollection collection)
        //{
        //    string sql = "";
        //    string Duser = "";
        //    bool flag = false;
        //    List<string> errors = new List<string>();
        //    List<string> response = new List<string>();
        //    ViewData["username"] = collection["username"];
        //    ViewData["fullnames"] = collection["fullnames"];
        //    ViewData["email1"] = collection["email1"];
        //    ViewData["email2"] = collection["email2"];
        //    ViewData["mobile1"] = collection["mobile1"];
        //    ViewData["mobile2"] = collection["mobile2"];


        //    if (collection["username"].ToString().Length == 0)
        //    {
        //        errors.Add("You must ADD the username.Its Mandatory");
        //    }

        //    string codeExists = Blogic.RunStringReturnStringValue("select * from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");

        //    if (codeExists != "" && codeExists != "0")
        //    {
        //        errors.Add("Please MAKE SURE the username are unique for each Station.This user record already exists for another Station.");
        //    }


        //    if (collection["fullnames"].ToString().Length == 0)
        //    {
        //        errors.Add("You must ADD the full names.Its Mandatory");
        //    }

        //    if (collection["email1"].ToString().Length == 0)
        //    {
        //        errors.Add("You must ADD the email.Its Mandatory for notificiations.");
        //    }

        //    if (collection["email2"].ToString().Length == 0)
        //    {
        //        errors.Add("You must ADD the alternate email or same as first email.Its Mandatory");
        //    }
        //    if (collection["mobile1"].ToString().Length == 0)
        //    {
        //        errors.Add("You must ADD the Mobile Number one.Its Mandatory for notifications");
        //    }

        //    if (collection["mobile2"].ToString().Length == 0)
        //    {
        //        errors.Add("You must ADD the alternate Mobile Number 2.Its Mandatory or same as Mobile number one.");
        //    }


        //    int n;
        //    bool isNumeric = int.TryParse(collection["mobile2"].ToString(), out n);

        //    if (isNumeric == false)
        //    {
        //        errors.Add("The  Mobile Number2  is invalid");
        //    }



        //    int n1;
        //    bool isNumeric1 = int.TryParse(collection["mobile1"].ToString(), out n1);

        //    if (isNumeric1 == false)
        //    {
        //        errors.Add("The  Mobile Number1  is invalid");
        //    }

        //    if (checkemail(collection["email1"].ToString()) == false)
        //    {
        //        errors.Add("The email format is wrong.");
        //    }


        //    if (checkemail(collection["email2"].ToString()) == false)
        //    {
        //        errors.Add("The email format is wrong.");
        //    }


        //    //Stationcode
        //    string stationCode = Blogic.RunStringReturnStringValue("select stationcode from tbUsers WHERE username = 'admin'"); ;

        //    if (errors.Count > 0)
        //    {
        //        _successfailview = "RegisterUser";
        //        ViewData["username"] = collection["username"];
        //        ViewData["fullnames"] = collection["fullnames"];
        //        ViewData["email1"] = collection["email1"];
        //        ViewData["email2"] = collection["email2"];
        //        ViewData["mobile1"] = collection["mobile1"];
        //        ViewData["mobile2"] = collection["mobile2"];
        //    }
        //    else
        //    {
        //        try
        //        {
        //            DateTime getdate = DateTime.Now;
        //            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
        //            Dictionary<string, string> data = new Dictionary<string, string>();
        //            string SelectedChurch = (string)HttpContext.Session["selectedChurch"];

        //            string[] _strempoyerID = collection["selectedemployer"].Split('-');
        //            data["EmployerID"] = _strempoyerID[1].ToString().Trim();
        //            data["username"] = collection["username"];
        //            data["fullnames"] = collection["fullnames"];
        //            data["email1"] = collection["email1"];
        //            data["email2"] = collection["email2"];
        //            data["mobile1"] = collection["mobile1"];
        //            data["mobile2"] = collection["mobile2"];
        //            //string _allvendor = collection["isAddmin"];
        //            data["userlevel"] = "nornal";
        //            data["clientregister"] = "0";
        //            data["parameters"] = "0";
        //            data["usermanagement"] = "0";
        //            data["accounts"] = "0";
        //            data["PasswordChangeFlag"] = "0";
        //            Random generator = new Random();
        //            String RandomPassword = generator.Next(0, 1000000).ToString("D6");
        //            string encPassword = userdata.EncyptString(RandomPassword);

        //            string recipients = (collection["mobile1"] + "," + collection["mobile2"]);
        //            data["password"] = encPassword.ToString().Trim();
        //            //-----------------------------------------------------------
        //            data["CreateBy"] = "ADMIN";
        //            data["CreateDate"] = dt.ToString();
        //            Duser = collection["username"];
        //            sql = Logic.DMLogic.InsertString("tbUsers", data);
        //            flag = Blogic.RunNonQuery(sql);
        //            if (flag)
        //            {
        //                response.Add("The user has been created successfully.");
        //                _successfailview = "ChurchRegistrationSuccess";


        //                string messagetosend = "Dear " + collection["fullnames"] + " you have been created as an AdminPortal user your password is " + RandomPassword;

        //                SendSms(messagetosend, recipients);
        //            }
        //        }
        //        catch
        //        {
        //            _successfailview = "ErrorPage";
        //        }
        //        Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

        //    }
        //    ViewData["ModelErrors"] = errors;
        //    ViewData["Response"] = response;
        //    return View(_successfailview);
        //}
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult UsersToApprove()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]

        public ActionResult UserApproval()
        {
            return View();
        }
        public ActionResult Payl()
        {
            return View();
        }
        public ActionResult Registerloanofficer()
        {
            return View();
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisterChurchUserModify()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisterofficerModify()
        {
            return View();
        }

        public ActionResult Resetpassword()
        {
            return View();
        }
        public ActionResult Resetofficerpassword()
        {
            return View();
        }
        public ActionResult ModifyRegisteredofficers()
        {
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PayChannel()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PayChannelList()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ManualUsersRegister()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ManualUsersRegister(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            string Duser = "";
            List<string> errors = new List<string>();


            ViewData["firstname"] = collection["firstname"];
            ViewData["lastname"] = collection["lastname"];
            ViewData["emailaddress"] = collection["emailaddress"];
            ViewData["phonenumber"] = collection["phonenumber"];
            ViewData["gender"] = collection["gender"];
            ViewData["dateofbirth"] = collection["dateofbirth"];
            ViewData["idorpassport"] = collection["idorpassport"];
            ViewData["vrno"] = collection["vrno"];

            if (collection["firstname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the firstname.Its Mandatory");
            }
            string codeExists = Blogic.RunStringReturnStringValue("select * from Users WHERE emailaddress = '" + collection["emailaddress"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the username are unique for each Station.This user record already exists for another Station.");
            }

            if (collection["emailaddress"].ToString().Length == 0)
            {
                errors.Add("You must ADD the emailaddress.Its Mandatory");
            }

            if (collection["phonenumber"].ToString().Length == 0)
            {
                errors.Add("You must ADD the phone number.Its Mandatory for notificiations.");
            }

            if (collection["gender"].ToString().Length == 0)
            {
                errors.Add("You must ADD the gender.Its Mandatory");
            }
            if (collection["dateofbirth"].ToString().Length == 0)
            {
                errors.Add("You must ADD the dateofbirth.Its Mandatory.");
            }

            if (collection["idorpassport"].ToString().Length == 0)
            {
                errors.Add("You must ADD the ID or Passport.Its Mandatory.");
            }


            if (collection["vrno"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Vehicle Registration Number.Its Mandatory.");
            }
            ///Stationcode

            Random generator = new Random();
            String RandomPassword = generator.Next(0, 1000000).ToString("D6");
            string encPassword = userdata.EncyptString(RandomPassword);

            if (errors.Count > 0)
            {
                _successfailview = "ManualUsersRegister";
                ViewData["firstname"] = collection["firstname"];
                ViewData["lastname"] = collection["lastname"];
                ViewData["emailaddress"] = collection["emailaddress"];
                ViewData["phonenumber"] = collection["phonenumber"];
                ViewData["gender"] = collection["gender"];
                ViewData["dateofbirth"] = collection["dateofbirth"];
                ViewData["idorpassport"] = collection["idorpassport"];
                ViewData["vrno"] = collection["vrno"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    data["firstname"] = collection["firstname"];
                    data["lastname"] = collection["lastname"];
                    data["emailaddress"] = collection["emailaddress"];
                    data["phonenumber"] = collection["phonenumber"];
                    data["gender"] = collection["gender"];
                    data["dateofbirth"] = collection["dateofbirth"];
                    data["idorpp"] = collection["idorpassport"];
                    data["vrno"] = collection["vrno"];
                    data["CreatedBy"] = "ADMIN";
                    data["DateCreated"] = dt.ToString();
                    data["password"] = encPassword.ToString().Trim();
                    Duser = collection["username"];
                    sql = Logic.DMLogic.InsertString("Users", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Registered username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }
            ViewData["ModelErrors"] = errors;
            return View(_successfailview);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PayChannel(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();

            if (collection["channelname"].ToString().Length == 0)
            {
                errors.Add("Please Enter the channel Name.Thanks.");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select count(*) from tbPayChannels WHERE channelname = '" + collection["channelname"].ToString().Trim() + "'");
            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the Channel Name are unique.This Channel Name already exists.");
            }

            if (errors.Count > 0)
            {
                _successfailview = "PayChannel";
            }
            else
            {

                try
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();

                    data["channelName"] = collection["channelname"];
                    data["deactivateuser"] = "ADMIN";
                    data["DateActivate"] = dt.ToString();
                    data["Approved"] = "1";
                    sql = Logic.DMLogic.InsertString("tbPayChannels", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            return View(_successfailview);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyRegisteredUsers(FormCollection collection, string id)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "";

            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                string _allvendor = collection["isAddmin"];
                if (_allvendor == "true")
                {
                    data["userlevel"] = "admin";
                }

                else

                {

                    data["userlevel"] = "normal";
                }
                data["username"] = collection["username"];
                data["fullnames"] = collection["fullnames"];
                data["email1"] = collection["email1"];
                data["email2"] = collection["email2"];
                data["mobile1"] = collection["mobile1"];
                data["mobile2"] = collection["mobile2"];
                data["PasswordChangeFlag"] = "0";
                data["Approved"] = "0";
                PhoneNumber = data["mobile1"];
                EmailAddress = data["email1"];
                PhoneNumber1 = data["mobile2"];
                EmailAddress1 = data["email2"];
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tbUsers", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    _successfailview = "ChurchRegistrationSuccess";
                    if (EmailAddress1.Length > 0)
                    {
                        sql = "";
                        Email_Message = "Dear " + data["fullnames"] + "." + Environment.NewLine + "Your details have been modified by Zoghori." + " " + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (EmailAddress1.Length > 0)
                    {
                        sql = "";
                        Email_Message = "Dear " + data["fullnames"] + "." + Environment.NewLine + "Your details have been modified by Zoghori." + " " + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (PhoneNumber.Length > 0)
                    {
                        sql = "";
                        SMS_Message = "Your details have been modified by Zoghori" + "" + Environment.NewLine + "Thanks";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }
                    Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Modified username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

                    if (PhoneNumber1.Length > 0)
                    {
                        sql = "";
                        SMS_Message = "Your details have been modified by Zoghori" + "" + Environment.NewLine + "Thanks";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                }

            }

            catch
            {
                _successfailview = "ErrorPage";
            }
            {
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Modified username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }
            return View(_successfailview);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyRegisteredofficers(FormCollection collection, string id)
        {
            string sql = "";
            string Duser = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "";

            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                //string _allvendor = collection["isAddmin"];
                //if (_allvendor == "true")
                //{
                //    data["userlevel"] = "admin";
                //}

                //else

                //{

                //    data["userlevel"] = "normal";
                //}
                data["username"] = collection["username"];
                data["Firstname"] = collection["Firstname"];
                data["Lastname"] = collection["Lastname"];
                data["Email"] = collection["Email"];
                data["phonenumber1"] = collection["phonenumber1"];
                data["phonenumber2"] = collection["phonenumber2"];
                data["Town"] = collection["Town"];
                data["Country"] = collection["Country"];
                data["occupation"] = collection["occupation"];
                data["idno"] = collection["idno"];
                data["station"] = collection["station"];
                data["Approved"] = "0";
                PhoneNumber = data["phonenumber1"];
                EmailAddress = data["Email"];
                PhoneNumber1 = data["phonenumber2"];
                //EmailAddress1 = data["email2"];
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblthirdparty", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The interest has been computed successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                    if (EmailAddress.Length > 0)
                    {
                        sql = "";
                        Email_Message = "Dear " + data["Firstname"] + "." + Environment.NewLine + "Your details have been modified ." + " " + Environment.NewLine + "Kind regards ";
                        sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    //if (EmailAddress1.Length > 0)
                    //{
                    //    sql = "";
                    //    Email_Message = "Dear " + data["Firstname"] + "." + Environment.NewLine + "Your details have been modified." + " " + Environment.NewLine + "Kind regards ";
                    //    sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                    //    Blogic.RunNonQuery(sql);
                    //}

                    if (PhoneNumber.Length > 0)
                    {
                        sql = "";
                        SMS_Message = "Your details have been modified" + "" + Environment.NewLine + "Thanks";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }
                    Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Modified username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

                    if (PhoneNumber1.Length > 0)
                    {
                        sql = "";
                        SMS_Message = "Your details have been modified " + "" + Environment.NewLine + "Thanks";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                }

            }

            catch
            {
                _successfailview = "ErrorPage";
            }
            {
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Modified username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
            }
            return View(_successfailview);

        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Resetpassword(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "";

            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                Random generator = new Random();
                String RandomPassword = generator.Next(0, 1000000).ToString("D6");
                string encPassword = userdata.EncyptString(RandomPassword);

                string recipients = (collection["mobile1"] + "," + collection["mobile2"]);
                data["password"] = encPassword.ToString().Trim();
                data["firstimelogin"] = "1";

                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tbUsers", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The user has been reset successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                    string messagetosend = "Dear " + collection["fullnames"] + " kindly use this temporary passsword to login and reset your password " + RandomPassword;

                    SendSms(messagetosend, recipients);


                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Resetofficerpassword(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "";

            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                Random generator = new Random();
                String RandomPassword = generator.Next(0, 10000).ToString("D4");
                string encPassword = userdata.EncyptString(RandomPassword);

                string recipients = (collection["phonenumber1"] + "," + collection["phonenumber2"]);
                data["password"] = RandomPassword.ToString().Trim();
                // data["firstimelogin"] = "1";

                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblthirdparty", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The user has been reset successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                    string messagetosend = "Dear " + collection["Firstname"] + ",kindly use this password to login to TP and reset your password " + RandomPassword;

                    SendSms(messagetosend, recipients);


                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyRegisteredUsers(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "RegisterChurchUserModifyPage";
                string uniqueID = id.ToString();
                sql = " select * from tbUsers WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["fullnames"] = rd["fullnames"];
                            ViewData["createby"] = rd["CreateBy"];
                            ViewData["createdate"] = rd["CreateDate"];
                            ViewData["email1"] = rd["Email1"];
                            ViewData["email2"] = rd["Email2"];
                            ViewData["mobile1"] = rd["Mobile1"];
                            ViewData["mobile2"] = rd["Mobile2"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyRegisteredofficers(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Registerloanofficers";
                string uniqueID = id.ToString();
                sql = " select * from tblthirdparty WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["Firstname"] = rd["Firstname"];
                            ViewData["Lastname"] = rd["Lastname"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["phonenumber1"] = rd["phonenumber1"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Country"] = rd["Country"];
                            ViewData["occupation"] = rd["occupation"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["station"] = rd["station"];
                            // ViewData["idno"] = rd["idno"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ApproveRegisteredUsers(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "";

            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                data["username"] = collection["username"];
                data["fullnames"] = collection["fullnames"];
                data["email1"] = collection["email1"];
                data["email2"] = collection["email2"];
                data["mobile1"] = collection["mobile1"];
                data["mobile2"] = collection["mobile2"];
                data["PasswordChangeFlag"] = "0";
                data["Approved"] = "1";
                PhoneNumber = data["mobile1"];
                EmailAddress = data["email1"];

                PhoneNumber1 = data["mobile2"];
                EmailAddress1 = data["email2"];
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tbUsers", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("The record has been approved successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ApproveRegisteredofficers(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                //data["EmployerID"] = _strempoyerID[1].ToString().Trim();
                data["username"] = collection["username"];
                data["Firstname"] = collection["Firstname"];
                data["Lastname"] = collection["Lastname"];
                data["Email"] = collection["Email"];
                data["phonenumber1"] = collection["phonenumber1"];
                data["phonenumber2"] = collection["phonenumber2"];
                data["Town"] = collection["Town"];
                data["Country"] = collection["Country"];
                data["occupation"] = collection["occupation"];
                data["idno"] = collection["idno"];
                data["Approved"] = "1";
                //PhoneNumber = data["mobile1"];
                //EmailAddress = data["email1"];

                //PhoneNumber1 = data["mobile2"];
                //EmailAddress1 = data["email2"];
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblthirdparty", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("The record has been approved successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ApprovepayI(FormCollection collection, string id)
        {
            string years = "";
            string months = "";
            string sql = "";
            bool flag = false;
            string sql1 = "";
            bool flag1 = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "254721632207";
            string messagetosend = "";
            string Email_Message = "";
            string SMS_Message = "";
            string amountdisbursed = "";
            string _mpesaresponse = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                //data["id"] = id;
                data["years"] = collection["years"];
                data["months"] = collection["months"];
                data["principle"] = collection["principle"];
                data["adminfee"] = collection["adminfee"];
                data["name"] = collection["name"];
                data["phone"] = PhoneNumber1;
                data["Paid"] = "1";
                data1["Paid"] = "1";
                months = collection["months"];
                amountdisbursed = collection["adminfee"];
                messagetosend = "Dear " + " Counter1serve," + " " + "Your monthly interest is Ksh: " + collection["adminfee"] + " .Thank you";
                _mpesaresponse = tcplogic.PHPRequestvs2("https://counterone.net:8181/counteronetest/ch/b2cnew.php", amountdisbursed, PhoneNumber1, "CNT0611177065");
                //mobileno-loanid-successorfail
                if (_mpesaresponse == "1")
                {
                    if (SendSms(messagetosend, PhoneNumber1) == "Success")
                    {

                    }
                    vsendsms.SaveJournals(amountdisbursed, "J006", "B090", DateTime.Now, "COMMISSION PAYABLE", "PAYABLE", "CNT0611177065");
                }
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblcarhesapproval", data, where);
                flag = Blogic.RunNonQuery(sql);
                where1["loandisbursedate"] = months;
                sql1 = Logic.DMLogic.UpdateString("tblinterestcharges", data1, where1);
                flag1 = Blogic.RunNonQuery(sql1);

                if (flag)
                {
                    response.Add("The Payment has been approved successfully.");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegisterChurchUserModify(FormCollection collection)
        {
            string _viewtoDisplay = "RegisterChurchUserModify";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "RegisterChurchUserModifydates";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _mpesano = collection["mpesano"];

                string ftodate = Convert.ToDateTime(_startdate).ToString("dd-MM-yyyy");
                _startdate = ftodate;

                string ftenddate = Convert.ToDateTime(_enddate).ToString("dd-MM-yyyy");
                _enddate = ftenddate;

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }


                //{
                //    errors.Add("You must Indicate the MPESA No.");
                //}


                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE createdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND createdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND username like'" + _mpesano + "%'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                // ----------------------------


            }
            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegisterofficerModify(FormCollection collection)
        {
            string _viewtoDisplay = "RegisterChurchofficersModify";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "RegisterChurchofficersModify";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _mpesano = collection["mpesano"];

                string ftodate = Convert.ToDateTime(_startdate).ToString("dd-MM-yyyy");
                _startdate = ftodate;

                string ftenddate = Convert.ToDateTime(_enddate).ToString("dd-MM-yyyy");
                _enddate = ftenddate;

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }


                //{
                //    errors.Add("You must Indicate the MPESA No.");
                //}


                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE createdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND createdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND username like'" + _mpesano + "%'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                // ----------------------------


            }
            return View(_viewtoDisplay);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult UserApproval(FormCollection collection)
        {
            string _viewtoDisplay = "UserApproval";
            string Duser = "";
            string _action = collection[4];
            if (_action == "Load")
            {
                //-------------------------
                _viewtoDisplay = "UserApprovaldates";
                List<string> errors = new List<string>();
                ViewData["ModelErrors"] = errors;
                string _startdate = collection["bp.startdate"];
                string _enddate = collection["bp.enddate"];
                string _mpesano = collection["mpesano"];

                string ftodate = Convert.ToDateTime(_startdate).ToString("dd-MM-yyyy");
                _startdate = ftodate;

                string ftenddate = Convert.ToDateTime(_enddate).ToString("dd-MM-yyyy");
                _enddate = ftenddate;

                string _SQLSelectionCriteria = "";

                if (collection["bp.startdate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the start Date.");
                }

                if (collection["bp.enddate"].ToString().Length == 0)
                {
                    errors.Add("You must Indicate the End Date.");
                }


                //{
                //    errors.Add("You must Indicate the MPESA No.");
                //}


                //Build SQL Selection criteria to pass as a single session value
                if (collection["bp.startdate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = " WHERE createdate>='" + _startdate + "'";
                }

                if (collection["bp.enddate"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + " AND createdate<='" + _enddate + "'";
                }

                if (collection["mpesano"].ToString().Length > 0)
                {
                    _SQLSelectionCriteria = _SQLSelectionCriteria + " " + "  AND username like'" + _mpesano + "%'";
                }

                System.Web.HttpContext.Current.Session["SQLSelectionCriteria"] = _SQLSelectionCriteria.ToString();
                // ----------------------------
                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "Approved username: " + Duser + " " + (string)HttpContext.Session["username"], "Parameters", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);

            }
            return View(_viewtoDisplay);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApproveRegisteredUsers(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "RegisteredUserApprovalPage";
                string uniqueID = id.ToString();
                sql = " select * from tbUsers WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["fullnames"] = rd["fullnames"];
                            ViewData["createby"] = rd["CreateBy"];
                            ViewData["createdate"] = rd["CreateDate"];
                            ViewData["email1"] = rd["Email1"];
                            ViewData["email2"] = rd["Email2"];
                            ViewData["mobile1"] = rd["Mobile1"];
                            ViewData["mobile2"] = rd["Mobile2"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApproveRegisteredofficers(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "ApproveRegisteredofficers";
                string uniqueID = id.ToString();
                sql = " select * from tblthirdparty WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["Firstname"] = rd["Firstname"];
                            ViewData["Lastname"] = rd["Lastname"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["phonenumber1"] = rd["phonenumber1"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Country"] = rd["Country"];
                            ViewData["occupation"] = rd["occupation"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["station"] = rd["station"];
                            // ViewData["idno"] = rd["idno"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApprovepayI(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "Payinterestcharge";
                string uniqueID = id.ToString();
                sql = " select * from tblcarhesapproval WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["years"] = rd["years"];
                            ViewData["months"] = rd["months"];
                            ViewData["principle"] = rd["principle"];
                            ViewData["adminfee"] = rd["adminfee"];
                            ViewData["name"] = rd["name"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        //jjjjjjjjjjjjjjjjjjjjjjjjjj
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RejectRegisteredUsers(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";

            string EmailAddress1 = "";
            string PhoneNumber1 = "";

            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                data["username"] = collection["username"];
                data["fullnames"] = collection["fullnames"];
                data["email1"] = collection["email1"];
                data["email2"] = collection["email2"];
                data["mobile1"] = collection["mobile1"];
                data["mobile2"] = collection["mobile2"];
                data["PasswordChangeFlag"] = "0";
                data["Approved"] = "0";
                string Password = "";

                PhoneNumber = data["mobile1"];
                EmailAddress = data["email1"];

                PhoneNumber1 = data["mobile2"];
                EmailAddress1 = data["email2"];
                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tbUsers", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("The record has been rejected successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Rejectofficers(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                //data["id"] = id;
                data["username"] = collection["username"];
                data["Firstname"] = collection["Firstname"];
                data["Lastname"] = collection["Lastname"];
                data["Email"] = collection["Email"];
                data["phonenumber1"] = collection["phonenumber1"];
                data["phonenumber2"] = collection["phonenumber2"];
                data["Town"] = collection["Town"];
                data["Country"] = collection["Country"];
                data["occupation"] = collection["occupation"];
                data["idno"] = collection["idno"];
                data["Approved"] = "0";

                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblthirdparty", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("The record has been rejected successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        //jjjjjjjjjjjjjjjjjjjjjjjjjj
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult assignrights(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                string clientregister = collection["clientregister"];
                string parameters = collection["parameters"];
                string accounts = collection["accounts"];
                string userlevel = collection["admin"];
                string Approvals = collection["Approvals"];
                string Editapprovals = collection["Editapprovals"];
                string Savings = collection["Savings"];
                string Usermanagement = collection["Usermanagement"];
                string floatmngt = collection["floatmngt"];
                //checking for admin
                if (userlevel.ToString().Contains("true"))
                {
                    userlevel = "admin";
                }
                else
                {
                    userlevel = "normal";
                }
                //checking for modules status
                if (clientregister.ToString().Contains("true"))
                {
                    clientregister = "1";
                }
                else
                {
                    clientregister = "0";
                }
                if (parameters.ToString().Contains("true"))
                {
                    parameters = "1";
                }
                else
                {
                    parameters = "0";
                }

                if (Approvals.ToString().Contains("true"))
                {
                    Approvals = "1";
                }
                else
                {
                    Approvals = "0";
                }
                if (Editapprovals.ToString().Contains("true"))
                {
                    Editapprovals = "1";
                }
                else
                {
                    Editapprovals = "0";
                }
                if (Savings.ToString().Contains("true"))
                {
                    Savings = "1";
                }
                else
                {
                    Savings = "0";
                }
                if (floatmngt.ToString().Contains("true"))
                {
                    floatmngt = "1";
                }
                else
                {
                    floatmngt = "0";
                }
                if (accounts.ToString().Contains("true"))
                {
                    accounts = "1";
                }
                else
                {
                    accounts = "0";
                }
                data["userlevel"] = userlevel;
                data["clientregister"] = clientregister;
                data["parameters"] = parameters;
                data["accounts"] = accounts;
                data["Approvals"] = Approvals;
                data["Editapprovals"] = Editapprovals;
                data["Savings"] = Savings;
                data["floatmngt"] = floatmngt;
                if (userlevel == "admin")
                {
                    data["usermanagement"] = "1";
                    data["userlevel"] = "admin";
                }

                else
                {
                    data["usermanagement"] = "0";
                    data["userlevel"] = "normal";
                }



                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tbUsers", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("User rights assigned  successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult assignofficerrights(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                string commision = collection["Commission"];
                string salary = collection["Salary"];


                //checking for admin
                if (commision.ToString().Contains("true"))
                {
                    commision = "1";
                }
                else
                {
                    commision = "0";
                }
                //checking for modules status
                if (salary.ToString().Contains("true"))
                {
                    salary = "1";
                }
                else
                {
                    salary = "0";
                }

                data["Commission"] = commision;
                data["Salary"] = salary;



                where["id"] = id;
                sql = Logic.DMLogic.UpdateString("tblthirdparty", data, where);
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    response.Add("Officer rights assigned  successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }

        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RejectRegisteredUsers(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "RejectRegisteredUsers";
                string uniqueID = id.ToString();
                sql = " select * from tbUsers WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["fullnames"] = rd["fullnames"];
                            ViewData["createby"] = rd["CreateBy"];
                            ViewData["createdate"] = rd["CreateDate"];
                            ViewData["email1"] = rd["Email1"];
                            ViewData["email2"] = rd["Email2"];
                            ViewData["mobile1"] = rd["Mobile1"];
                            ViewData["mobile2"] = rd["Mobile2"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Rejectofficers(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "Rejectofficers";
                string uniqueID = id.ToString();
                sql = " select * from tblthirdparty WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["Firstname"] = rd["Firstname"];
                            ViewData["Lastname"] = rd["Lastname"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["phonenumber1"] = rd["phonenumber1"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Country"] = rd["Country"];
                            ViewData["occupation"] = rd["occupation"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["station"] = rd["station"];
                            // ViewData["approved"] = rd["approved"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }



        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult assignrights(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "assignrights";
                string uniqueID = id.ToString();
                sql = " select * from tbUsers WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            Session["rightsusername"] = rd["id"].ToString();
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["fullnames"] = rd["fullnames"];
                            ViewData["createby"] = rd["CreateBy"];
                            ViewData["createdate"] = rd["CreateDate"];
                            ViewData["email1"] = rd["Email1"];
                            ViewData["email2"] = rd["Email2"];
                            ViewData["mobile1"] = rd["Mobile1"];
                            ViewData["mobile2"] = rd["Mobile2"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult assignofficerrights(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "assignofficerrights";
                string uniqueID = id.ToString();
                sql = " select * from tblthirdparty WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {

                            Session["rightofficername"] = rd["id"].ToString();
                            ViewData["id"] = rd["id"];
                            ViewData["username"] = rd["username"];
                            ViewData["Firstname"] = rd["Firstname"];
                            ViewData["Lastname"] = rd["Lastname"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["phonenumber1"] = rd["phonenumber1"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Country"] = rd["Country"];
                            ViewData["occupation"] = rd["occupation"];
                            ViewData["phonenumber2"] = rd["phonenumber2"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["station"] = rd["station"];
                            ViewData["Salary"] = rd["Salary"];
                            ViewData["Commission"] = rd["Commission"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }






        //lllllllllllllllllllllllllll

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreateChurchUser(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            ViewData["username"] = collection["username"];
            ViewData["fullnames"] = collection["fullnames"];
            ViewData["email1"] = collection["email1"];
            ViewData["email2"] = collection["email2"];
            ViewData["mobile1"] = collection["mobile1"];
            ViewData["mobile2"] = collection["mobile2"];

            if (collection["username"].ToString().Length == 0)
            {
                errors.Add("You must ADD the username.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select * from tbUsers WHERE username = '" + collection["username"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the username are unique for each Station.This user record already exists for another Station.");
            }


            if (collection["fullnames"].ToString().Length == 0)
            {
                errors.Add("You must ADD the full names.Its Mandatory");
            }

            if (collection["email1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the email.Its Mandatory for notificiations.");
            }

            if (collection["email2"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate email or same as first email.Its Mandatory");
            }
            if (collection["mobile1"].ToString().Length == 0)
            {
                errors.Add("You must ADD the Mobile Number one.Its Mandatory for notifications");
            }

            if (collection["mobile2"].ToString().Length == 0)
            {
                errors.Add("You must ADD the alternate Mobile Number 2.Its Mandatory or same as Mobile number one.");
            }



            if (checkemail(collection["email1"].ToString()) == false)
            {
                errors.Add("The email format is wrong.");
            }


            if (checkemail(collection["email2"].ToString()) == false)
            {
                errors.Add("The email format is wrong.");
            }

            //Stationcodebool IsValidEmail(string email)

            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from tbUsers WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "CreateChurchUser";
                ViewData["username"] = collection["username"];
                ViewData["fullnames"] = collection["fullnames"];
                ViewData["email1"] = collection["email1"];
                ViewData["email2"] = collection["email2"];
                ViewData["mobile1"] = collection["mobile1"];
                ViewData["mobile2"] = collection["mobile2"];
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);



                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    data["username"] = collection["username"];
                    data["fullnames"] = collection["fullnames"];
                    data["email1"] = collection["email1"];
                    data["email2"] = collection["email2"];
                    data["mobile1"] = collection["mobile1"];
                    data["mobile2"] = collection["mobile2"];
                    data["PasswordChangeFlag"] = "0";
                    data["stationcode"] = stationCode.ToString().Trim();
                    //data["StationBranchCode"] = selectedchurchbranch.ToString().Trim();
                    data["BankBranchCode"] = "NA";

                    Random generator = new Random();
                    String RandomPassword = generator.Next(0, 1000000).ToString("D6");
                    string encPassword = userdata.EncyptString(RandomPassword);
                    data["password"] = encPassword.ToString().Trim();
                    //-----------------------------------------------------------
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    data["Section"] = "C";
                    data["Admin"] = "N";
                    sql = Logic.DMLogic.InsertString("tbUsers", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            return View(_successfailview);
        }


        public static Boolean checkemail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RevenueChannel(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();
            //------validate for loading------------
            ViewData["revenuename"] = collection["revenuename"];
            ViewData["stationcode"] = collection["selectedchurch"];
            ViewData["bankaccount"] = collection["bankaccount"];
            ViewData["bankcode"] = collection["selectedbank"];
            ViewData["mpesano"] = collection["mpesaacc"];
            //----------------------------------------
            if (collection["revenuename"].ToString().Length == 0)
            {
                errors.Add("Please ENTER revenue name.");
            }
            if (collection["selectedchurch"].Contains("select"))
            {
                errors.Add("Please SELECT a Station.");
            }
            if (collection["bankaccount"].ToString().Length == 0)
            {
                errors.Add("Please ENTER a bank Account");
            }
            if (collection["selectedbank"].ToString().Contains("select"))
            {
                errors.Add("Please SELECT a bank.");
            }
            if (collection["mpesaacc"].ToString().Contains("select"))
            {
                errors.Add("Please ENTER an MPESA account.");
            }


            if (errors.Count > 0)
            {
                _successfailview = "RevenueChannel";
            }
            else
            {
                try
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    data["revenuename"] = collection["revenuename"];
                    data["stationcode"] = collection["selectedchurch"];
                    string s = collection["selectedchurch"];
                    string[] churcodearr = collection["selectedchurch"].Split('-');
                    data["stationcode"] = churcodearr[1].ToString().Trim();
                    //-------------------------------------------------------


                    data["bankaccount"] = collection["bankaccount"];
                    data["bankcode"] = collection["selectedbank"];
                    string[] bankcodearr = collection["selectedbank"].Split('-');
                    data["bankcode"] = bankcodearr[0].ToString().Trim();
                    //-------------------------------------------------------
                    data["mpesano"] = collection["mpesaacc"];
                    data["Approved"] = "0";
                    data["createdby"] = "ADMIN";
                    data["createddate"] = dt.ToString().Trim();
                    sql = Logic.DMLogic.InsertString("tbRevenueChannels", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            return View(_successfailview);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ContributionTypes()
        {
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get)]

        public ActionResult ContributionTypesApproval()
        {
            return View();
        }
        //ContributionApprove
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ContributionApprove(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "ContributionTypeApprovePage";
                string uniqueID = id.ToString();
                sql = " select * from dbo.ContributionTypes WHERE contribid= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Contribid"] = rd["Contribid"];
                            ViewData["contributionname"] = rd["contributionname"];
                            ViewData["mpesaacc"] = rd["mpesaacc"];
                            ViewData["bankacc"] = rd["bankacc"];
                            ViewData["createdby"] = rd["createdby"];
                            ViewData["createddate"] = rd["createddate"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RevenueApprove(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "RevenueApprovePage";
                string uniqueID = id.ToString();
                sql = "select * from dbo.tbRevenueChannels WHERE revenueid= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["revenueid"] = rd["revenueid"];
                            ViewData["revenuename"] = rd["revenuename"];
                            ViewData["stationcode"] = rd["stationcode"];
                            ViewData["bankaccount"] = rd["bankaccount"];
                            ViewData["bankcode"] = rd["bankcode"];
                            ViewData["mpesano"] = rd["mpesano"];
                            ViewData["createdby"] = rd["createdby"];
                            ViewData["createddate"] = rd["createddate"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyRevenueApprove(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "ModifyRevenuePage";
                string uniqueID = id.ToString();
                sql = "select * from dbo.tbRevenueChannels WHERE revenueid= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["revenueid"] = rd["revenueid"];
                            ViewData["revenuename"] = rd["revenuename"];
                            ViewData["stationcode"] = rd["stationcode"];
                            ViewData["bankaccount"] = rd["bankaccount"];
                            ViewData["bankcode"] = rd["bankcode"];
                            ViewData["mpesano"] = rd["mpesano"];
                            ViewData["createdby"] = rd["createdby"];
                            ViewData["createddate"] = rd["createddate"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RevenueApprove(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                data["revenuename"] = collection["revenuename"];
                data["stationcode"] = collection["stationcode"];
                data["bankaccount"] = collection["bankaccount"];
                data["bankcode"] = collection["bankcode"];
                data["mpesano"] = collection["mpesano"];
                data["Approvedby"] = "ADMIN";
                data["ApproveDate"] = dt.ToString();
                data["Approved"] = "1";
                where["revenueid"] = collection["revenueid"];
                sql = Logic.DMLogic.UpdateString("tbRevenueChannels", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            return View(_successfailview);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyRevenueApprove(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                data["revenuename"] = collection["revenuename"];
                data["stationcode"] = collection["stationcode"];
                data["bankaccount"] = collection["bankaccount"];
                data["bankcode"] = collection["bankcode"];
                data["mpesano"] = collection["mpesano"];
                data["Approvedby"] = "ADMIN";
                data["ApproveDate"] = dt.ToString();
                data["Approved"] = "0";
                where["revenueid"] = collection["revenueid"];
                sql = Logic.DMLogic.UpdateString("tbRevenueChannels", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            return View(_successfailview);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContributionApprove(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();

                data["contributionname"] = collection["contributionname"];
                data["mpesaacc"] = collection["mpesaacc"];
                data["bankacc"] = collection["bankacc"];
                data["ApprovedBy"] = "ADMIN";
                data["ApproveDate"] = dt.ToString();
                data["Approved"] = "1";
                where["contribid"] = collection["contribid"];
                sql = Logic.DMLogic.UpdateString("ContributionTypes", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            return View(_successfailview);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ContributionTypes(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            List<string> errors = new List<string>();

            //--------------retain values after validation if there are errors            
            ViewData["contributionname"] = collection["contributionname"];
            ViewData["mpesaacc"] = collection["mpesaacc"];
            ViewData["bankacc"] = collection["bankaccount"];

            if (collection["contributionname"].ToString().Length == 0)
            {
                errors.Add("Please there must be a contribution name.Please enter a valid name.");
            }

            if (collection["mpesaacc"].ToString().Length == 0)
            {
                errors.Add("Enter an MPESA account for this contribution Type.");
            }

            if (collection["bankaccount"].ToString().Length == 0)
            {
                errors.Add("Enter a Bank Account for this contribution Type.");
            }


            string selectedcontribchurch = Request.Form["selectedchurch"].ToString();
            string[] stationcode = selectedcontribchurch.Split('-');
            string codeExists = Blogic.RunStringReturnStringValue("SELECT count(*) FROM contributiontypes WHERE stationcode='" + stationcode[1].ToString().Trim() + "' and contributionname='" + collection["contributionname"].ToString().Trim() + "'");


            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the Contribution name are unique for each Station.This record already exists.Modify if need be.");
            }


            if (selectedcontribchurch.ToString().Contains("select"))
            {
                errors.Add("Please SELECT a Stationto which this contribution belongs.");
            }

            //---------------------------------------------------------

            if (errors.Count > 0)
            {
                _successfailview = "ContributionTypes";
            }
            else
                try
                {
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    //string selectedcontribchurch = Request.Form["selectedchurch"].ToString();
                    //string[] stationcode = selectedcontribchurch.Split('-');

                    data["contributionname"] = collection["contributionname"];
                    data["mpesaacc"] = collection["mpesaacc"];
                    data["bankacc"] = collection["bankaccount"];
                    data["stationcode"] = stationcode[1].ToString();
                    data["createdby"] = "ADMIN";
                    data["createddate"] = dt.ToString();
                    sql = Logic.DMLogic.InsertString("ContributionTypes", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            ViewData["ModelErrors"] = errors;
            return View(_successfailview);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisteredUsersApprove(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "UsersToApprovePage";
                string uniqueID = id.ToString();
                sql = " select * from Users WHERE Userid= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {   //debit AC/Credit AC/Currency/Amount/Narration/fttype

                            ViewData["userid"] = rd["userid"];
                            ViewData["firstname"] = rd["firstname"];
                            ViewData["lastname"] = rd["lastname"];
                            ViewData["emailaddress"] = rd["emailaddress"];
                            ViewData["phonenumber"] = rd["phonenumber"];
                            ViewData["gender"] = rd["gender"];
                            ViewData["dateofbirth"] = rd["dateofbirth"];
                            ViewData["idorpp"] = rd["idorpp"];
                            ViewData["datecreated"] = rd["datecreated"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
            }





            return View(_viewToDisplayData);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ApprovedUsers()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DevUsers()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DevUser()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegisteredUsersApprove(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            string EmailAddress = "";
            string PhoneNumber = "";
            string Email_Message = "";
            string SMS_Message = "";
            List<string> errors = new List<string>();

            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                data["userid"] = collection["userid"];
                data["firstname"] = collection["firstname"].ToString();
                data["lastname"] = collection["lastname"].ToString();
                EmailAddress = collection["emailaddress"].ToString();
                PhoneNumber = collection["phonenumber"].ToString();
                sql = "UPDATE Users SET Approved='1',dateapproved='" + dt.ToString() + "',firstName='" + data["firstname"] + "',lastName='" + data["lastname"] + "' WHERE Userid=" + data["userid"] + "";
                flag = Blogic.RunNonQuery(sql);

                if (flag)
                {
                    _successfailview = "TransBookSuccess";

                    if (EmailAddress.Length > 0)
                    {
                        sql = "";
                        Email_Message = "";
                        sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }

                    if (PhoneNumber.Length > 0)
                    {
                        sql = "";
                        sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + PhoneNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                        Blogic.RunNonQuery(sql);
                    }


                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            return View(_successfailview);
        }

        [NonAction]
        private string SendSms(string message, string recipient)
        {
            string username = "counterone";
            string apiKey = "f70ad58983408592e61ae91d67036bfe5011c571b19d26d10cb09fe48afaf0d2";
            string ffrom = "counterone";
            string status = "";

            // Create a new instance of our awesome gateway class
            AfricasTalkingGateway gateway = new AfricasTalkingGateway(username, apiKey);
            // Any gateway errors will be captured by our custom Exception class below,
            // so wrap the call in a try-catch block   
            try
            {
                // Thats it, hit send and we'll take care of the rest
                dynamic results = gateway.sendMessage(recipient, message, ffrom);
                foreach (dynamic result in results)
                {
                    //Console.Write((string)result["number"] + ",");
                    //Console.Write((string)result["status"] + ","); // status is either "Success" or "error message"
                    //Console.Write((string)result["messageId"] + ",");
                    //Console.WriteLine((string)result["cost"]);


                    //if (results.IndexOf("Success") == -1)
                    //{
                    //    _successfailview = "SalaryReviewError";
                    //}
                    status = (string)result["status"];

                }
            }
            catch (AfricasTalkingGatewayException e)
            {

                Console.WriteLine("Encountered an error: " + e.Message);

            }
            return status;
        }

    }
}

