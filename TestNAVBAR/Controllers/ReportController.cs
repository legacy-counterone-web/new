﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using TestNAVBAR.Reports.RDLC;

namespace TestNAVBAR.Controllers
{
    public class ReportController : Controller
    {
        //
        // GET: /Report/

        public ActionResult Index()
        {
            return View();
        }
        DataSet1 ds = new DataSet1();
        public ActionResult ReportEmployee()
        {
            ReportViewer reportViewer = new ReportViewer();
            reportViewer.ProcessingMode = ProcessingMode.Local;
            reportViewer.SizeToReportContent = true;
            reportViewer.Width = Unit.Percentage(900);
            reportViewer.Height = Unit.Percentage(900);

            // var connectionString = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            string connectionString = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";

            SqlConnection conx = new SqlConnection(connectionString); SqlDataAdapter adp = new SqlDataAdapter("SELECT * FROM tblloanstatements", conx);

            adp.Fill(ds, ds.tblloanstatements.TableName);

            reportViewer.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Reports\RDLC\Report1.rdlc";
            reportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", ds.Tables[0]));


            ViewBag.ReportViewer = reportViewer;

            return View();
        }
        public RedirectResult RedirectToMembers()
        {
            return Redirect("/Reports/members.aspx");
        }
    }
}
