﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class TransfersController : Controller
    {
        //
        // GET: /Transfers/

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetTransferDetails()
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            return Json(MemberRepo.GetAllTransfers(), JsonRequestBehavior.AllowGet);

        }
        
        public ActionResult AddTransfer()
        {

            return View();
        }
        //Add Employee details with json data    
        [HttpPost]
        public JsonResult AddTransfer(SaccoTransfers MbrDet)

        {
            try
            {

                TransactionRepo MemberRepo = new TransactionRepo();
                MemberRepo.AddDTransfers(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        //Get record by Empid for edit    
        public ActionResult Edit(int? id)
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            return View(MemberRepo.GetAllTransfers().Find(mbr => mbr.Transerid == id));
        }
        //Updated edited records    
        [HttpPost]
        public JsonResult Edit(SaccoTransfers MemberUpdateDet)
        {

            try
            {

                TransactionRepo MemberRepo = new TransactionRepo();
                MemberRepo.UpdateTransfers(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
       
       
        //Delete the records by id    
        [HttpPost]
        public JsonResult Delete(int id)
        {
            TransactionRepo MemberRepo = new TransactionRepo();
            MemberRepo.DeleteTransfers(id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        
       
        
       
        //Get employee list of Partial view     
        [HttpGet]
        public PartialViewResult MemberDetails()
        {

            return PartialView("_MemberDetails");

        }
        
    }
}
