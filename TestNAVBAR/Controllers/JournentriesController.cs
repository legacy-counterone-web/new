﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class JournentriesController : Controller
    {
        int logintime = 1;
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        string surname = "";
        string phone = "";

        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        List<string> response = new List<string>();
        private Utilities.Users ulogic = new Utilities.Users();
        //
        // GET: /Journentries/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Journal()
        {
            ViewData["Name"] = HttpContext.Session["username"].ToString();
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Journal(FormCollection collection)
        {
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd");
            string sql = "";
            string sql1 = "";
            string sql2 = "";
            bool flag = false;
            bool flag1 = false;
            string totaldebits = collection["Tdebits"];
            string totaldcredits = collection["Tcredits"];
            string debits = collection["Dramount"];
            string credits = collection["Cramount"];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            string _enddate = collection["bp.enddate"];
            string[] _DebitID = collection["Draccount"].Split('-');
            string DebitAccount = _DebitID[0].ToString().Trim();
            string Debitnames = _DebitID[1].ToString().Trim();

            string[] _CreditID = collection["Craccount"].Split('-');
            string CreditAccount = _CreditID[0].ToString().Trim();
            string Creditnames = _CreditID[1].ToString().Trim();
            string ftenddate = Convert.ToDateTime(_enddate).ToString("yyyy-MM-dd");
            if(DebitAccount== CreditAccount)
            {
                errors.Add("Debit Account and Credit Account can not be same");
            }

            if (totaldebits != debits)
            {
                errors.Add("Debit amount not equal to Total Debit");
            }
            if (totaldcredits != credits)
            {
                errors.Add("Credit amount not equal to Total Credit");
            }
            if (collection["bp.enddate"].ToString().Length == 0 || collection["bp.enddate"].ToString().Contains("mm/"))
            {
                errors.Add("You must Indicate a valid Date.");
            }
            if (collection["Tdebits"].ToString().Length == 0 )
            {
                errors.Add("Total debits must have a value");
            }
            if (collection["Tcredits"].ToString().Length == 0)
            {
                errors.Add("Total Credits must have a value");
            }
            if (collection["Cramount"].ToString().Length == 0)
            {
                errors.Add("Credit amount must have a value");
            }
            if (collection["Dramount"].ToString().Length == 0)
            {
                errors.Add("Debit amount must have a value");
            }
            if (collection["Description"].ToString().Length == 0)
            {
                errors.Add("Description Required");
            }
            if (collection["Currency"].ToString().Length == 0)
            {
                errors.Add("Currency Required");
            }
            //bool flag1 = false;
            if (errors.Count > 0)
            {
                _successfailview = "Journal";
                ViewData["Currency"] = collection["Currency"];
                ViewData["Description"] = collection["Description"];
                ViewData["Dramount"] = collection["Dramount"];
                ViewData["Cramount"] = collection["Cramount"];
                ViewData["Tcredits"] = collection["Tcredits"];
                ViewData["Tdebits"] = collection["Tdebits"];
            }
            else
            {
                try
                {

                    Dictionary<string, string> data1 = new Dictionary<string, string>();
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string _userssession = HttpContext.Session["username"].ToString();
                    // double loan = Convert.ToDouble(collection["LoanBalance"]) - Convert.ToDouble(collection["amount"]);
                    DateTime dts = DateTime.Now;
                    int _min = 100;
                    int _max = 100;
                    Random _rdm = new Random();
                    Random random = new Random();
                    Random generator = new Random();
                   
                    string value = generator.Next(0, 100).ToString("D2");
                    //string refs = dat + collection["Loanref"];
                    double amount =Convert.ToDouble(collection["Dramount"]);
                    
                    string dat = "BFJN" + "/" + DebitAccount + "/" + CreditAccount + "/" + dts.Year.ToString() + "/" + dts.Month.ToString() + "/" + dts.Day.ToString() + "/" + value;

                    data["Drloanref"] = dat;
                    data["Draccountid"] = DebitAccount;
                    data["Draccount"] = Debitnames;
                    data["Dramount"] = collection["Dramount"];
                    data["Tdebits"] = collection["Tdebits"];
                    data["Craccount"] = Creditnames;
                    data["Craccountid"] = CreditAccount;
                    data["Cramount"] = collection["Cramount"];
                    data["Tcredits"] = collection["Tcredits"];
                    data["Description"] = collection["Description"];
                    data["Actionedby"] = _userssession;
                    data["Currency"] = collection["Currency"];

                    data1["loanid"] = dat;
                    data1["Amount"] = amount.ToString();
                    data1["DebitAccount"] = DebitAccount;
                    data1["CreditAccount"] = CreditAccount;
                    data1["Narration"] = collection["Description"];
                    data1["TransDate"] = ftenddate;
                    data1["LoanType"] = "Transaction";
                    data1["CommissionAmount"] = "0";
                    data1["mpesaref"] = dat;








                    sql2 = Logic.DMLogic.InsertString("Journaltrail", data);
                    flag = Blogic.RunNonQuery(sql2);

                    sql1 = Logic.DMLogic.InsertString("JournalEntries", data1);

                    flag1 = Blogic.RunNonQuery(sql1);
                    flag = Blogic.RunNonQuery(sql2);
                    if (flag)
                    {
                        response.Add("Entry Made successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["Response"] = response;
            return View(_successfailview);
        }
    }
}
