﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class PasswordController : Controller
    {


        string T24Response = "";
        string CustomerNumber = "";
        string _successfailview = "";
        string sql = "";
        string selectedchurch = "";
        string selectedchurchbranch = "";
        string selectedbankbranch = "";
        string _viewtodisplay = "";
        System.Data.SqlClient.SqlDataReader rd;
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where2 = new Dictionary<string, string>();
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();

        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();
        //
        // GET: /Password/
 
        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult PasswordGenerate()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult PasswordGenerate(FormCollection collection)
        {

            string _viewToDisplay = "PasswordGenerate";
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> cusdata = new Dictionary<string, string>();
            Boolean flag = false;
            try
            {
                if (ModelState.IsValid)
                {
                    string PreprationRequired = Request.Form["customerIds"];
                    string[] idstoupdate = PreprationRequired.ToString().Split(',');
                    int y = idstoupdate.Length;
                    for (int i = 0; i < y; i++)
                    {

                        //Password to be Generated
                        string Password = "";
                        Random generator = new Random();
                        String RandomPassword = generator.Next(0, 1000000).ToString("D6");
                        Password = RandomPassword.ToString().Trim();
                        string encPassword = userdata.EncyptString(RandomPassword);

                        data["password"] = encPassword.ToString().Trim();
                        data["PasswordGenerated"] = "1";
                        data["PasswordSend"] = "0";
                        data["GenerateDate"] = DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt");
                        data["GenerateBy"] = "QASAURIA";
                        where["channelid"] = idstoupdate[i];
                        sql = Logic.DMLogic.UpdateString("Customers", data, where);
                        flag = Blogic.RunNonQuery(sql);
                    }
                    return View(_viewToDisplay);
                }
                else
                {
                    return View(_viewToDisplay);
                }
            }
            catch
            {
                return View(_viewToDisplay);
            }
        }

        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult SendPassword()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SendPassword(FormCollection collection)
        {
            string Email_Message = "";
            string SMS_Message = "";
            string _viewToDisplay = "SendPassword";
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> cusdata = new Dictionary<string, string>();
            Boolean flag = false;
            try
            {
                if (ModelState.IsValid)
                {
                    string PreprationRequired = Request.Form["customerIds"];
                    string[] idstoupdate = PreprationRequired.ToString().Split(',');
                    int y = idstoupdate.Length;
                    for (int i = 0; i < y; i++)
                    {
                        string EmailAddress = Blogic.RunStringReturnStringValue("select EmailAddress from Customers WHERE ChannelID = '" + idstoupdate[i].ToString().Trim() + "'");
                        string MobileNumber = Blogic.RunStringReturnStringValue("select PhoneNumber from Customers WHERE  ChannelID = '" + idstoupdate[i].ToString().Trim() + "'");
                        string CustomerNumber = Blogic.RunStringReturnStringValue("select CustomerNumber from Customers WHERE  ChannelID = '" + idstoupdate[i].ToString().Trim() + "'");
                        string Password= Blogic.RunStringReturnStringValue("select Password from Customers WHERE  ChannelID = '" + idstoupdate[i].ToString().Trim() + "'");
                        Password = userdata.EncyptString(Password);
                        if (EmailAddress.Length > 0)
                        {
                            sql = "";
                            Email_Message = "Dear Bank Customer, " + Environment.NewLine + "You have been created as an Application user in the Bank XXX." + Environment.NewLine + "Your details are as follows:" + Environment.NewLine + "Username:" + CustomerNumber.ToString().Trim() + " " + Environment.NewLine + "Password:" + Password.ToString().Trim() + " " + Environment.NewLine + "Kind regards ";
                            sql = "Insert into OutEmail(Email,Message,EncyptedMessage)VALUES('" + EmailAddress + "','" + Email_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                            Blogic.RunNonQuery(sql);
                        }

                        if (MobileNumber.Length > 0)
                        {
                            sql = "";
                            SMS_Message = "Your Login Details are:" + Environment.NewLine + "Username:" + CustomerNumber.ToString().Trim() + "" + Environment.NewLine + "Password:" + Password.ToString().Trim() + " " + Environment.NewLine + "Thank you.";
                            sql = "Insert into OutSMS(Phone,Message,EncyptedMessage)VALUES('" + MobileNumber + "','" + SMS_Message + "','" + userdata.EncyptString(SMS_Message) + "') ";
                            Blogic.RunNonQuery(sql);
                        }
                        data["passwordsend"] = "1";
                        data["passwordsenddate"] = DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt");
                        data["PasswordSendBy"] = "QASAURIA";
                        where["channelid"] = idstoupdate[i];
                        sql = Logic.DMLogic.UpdateString("Customers", data, where);
                        flag = Blogic.RunNonQuery(sql);
                    }
                    return View(_viewToDisplay);
                }
                else
                {
                    return View(_viewToDisplay);
                }
            }
            catch
            {
                return View(_viewToDisplay);
            }
        }

    }
}
