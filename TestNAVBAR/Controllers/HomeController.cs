﻿

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using TestNAVBAR.Models;

using System;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;
using System.Xml;
using System.Web.Security;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{


    public class HomeController : Controller
    {
        string _successfailview = "";
        string sql = "";
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        //[HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //[NoDirectAccessAttribute]

        public ActionResult Index(string selectedid)
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";
            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult Details()
        {
            //ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            var empoyees = Employee.GetList();
            return View(empoyees);
            //The above caters for employee List in the main view

        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [NoDirectAccessAttribute]
        public ActionResult CustomerDetails()
        {
            PersonalData svc = new PersonalData();
            PersonalB personnelinfo = new PersonalB();
            personnelinfo = svc.AccountList();
            personnelinfo.CustDetails = svc.ProfileDetails();
            return View(personnelinfo);
        }
        [HttpGet]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        //[NoDirectAccessAttribute]
        public ActionResult QuickInfo()
        {
            //ViewBag.Message = "Your app description page.";
            return View();
        }

        public JsonResult GetGenders()
        {
            Loanrepo EmpRepo = new Loanrepo();
            return Json(EmpRepo.GetAllGenders(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Loandisbursed()
        {
            return View();
        }
        // BC dbsss = new BC();
        [HttpGet]
        public JsonResult GetLoandisbursed()
        {
            Loanrepo EmpRepo = new Loanrepo();
            return Json(EmpRepo.GetAllDisbursed(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult Loanpayments()
        {
            return View();
        }
        // BC dbsss = new BC();
        [HttpGet]
        public JsonResult GetLoanpayments()
        {
            Loanrepo EmpRepo = new Loanrepo();
            return Json(EmpRepo.GetAllRepayments(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetMonthlypar()
        {
            Loanrepo EmpRepo = new Loanrepo();
            return Json(EmpRepo.Getpar(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult Getmonthlyforecasting()
        {
            Loanrepo EmpRepo = new Loanrepo();
            return Json(EmpRepo.Getforecasting(), JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult Getproduct()
        {
            Loanrepo EmpRepo = new Loanrepo();
            return Json(EmpRepo.Getprod(), JsonRequestBehavior.AllowGet);
        }

    }
}
