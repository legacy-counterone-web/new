﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class EmployersController : Controller
    {
        // GET: /Station/
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisterEmployers()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegisterEmployers(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["companyname"] = collection["companyname"];
            ViewData["website"] = collection["website"];
            ViewData["HRcontact"] = collection["HRcontact"];
            ViewData["HRemail"] = collection["HRemail"];
          
            if (collection["companyname"].ToString().Length == 0)
            {
                errors.Add("You must ADD the companyname.Its Mandatory");
            }

            string codeExists = Blogic.RunStringReturnStringValue("select * from Employers WHERE companyname = '" + collection["companyname"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the companynames are unique for each employer.This companyname record already exists for another employer.");
            }


            if (collection["website"].ToString().Length == 0)
            {
                errors.Add("You must ADD the website.Its Mandatory");
            }

            if (String.IsNullOrEmpty(collection["HRcontact"]))
            {
                errors.Add("You must ADD the HRcontact.Its Mandatory for notificiations.");
            }

            if (String.IsNullOrEmpty(collection["HRemail"]))

            {
                errors.Add("You must ADD the HRemail.Its Mandatory");
            }

            //Stationcode
            string stationCode = Blogic.RunStringReturnStringValue("select stationcode from Employers WHERE username = 'admin'"); ;

            if (errors.Count > 0)
            {
                _successfailview = "RegisterEmployers";
                ViewData["companyname"] = collection["companyname"];
                ViewData["website"] = collection["website"];
                ViewData["HRcontact"] = collection["HRcontact"];
                ViewData["HRemail"] = collection["HRemail"];
              
            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string SelectedChurch = (string)HttpContext.Session["selectedChurch"];
                    data["companyname"] = collection["companyname"];
                    data["website"] = collection["website"];
                    data["HRcontact"] = collection["HRcontact"];
                    data["HRemail"] = collection["HRemail"];
                    data["CreateBy"] = "ADMIN";
                    data["CreateDate"] = dt.ToString();
                    sql = Logic.DMLogic.InsertString("Employers", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The employer has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

    }
}