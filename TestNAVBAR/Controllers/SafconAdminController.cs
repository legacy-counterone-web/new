﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class SafconAdminController : Controller
    {
        //
        // GET: /SafconAdmin/

        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetContributions()
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return Json(MemberRepo.GetAllMembersContribution(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetContributionss()
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return Json(MemberRepo.GetAllMembersContribution());

        }
        public ActionResult Contribution()
        {

            return View();
        }
        public ActionResult cpayment()
        {

            return View();
        }
        public ActionResult Edit(int? id)
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return View(MemberRepo.GetAllMembersContribution().Find(mbr => mbr.Userid == id));
        }
        [HttpPost]
        public JsonResult Contribution(Safcontribution MbrDet)

        {
            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.AddMemberContribution(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        [HttpPost]
        public JsonResult Delete(int id)
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            MemberRepo.DeleteMemberContibution(id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Edit(Safcontribution MemberUpdateDet)
        {

            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.UpdateMemberContribution(MemberUpdateDet);
                return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Your not authorized to perform this action");
            }
        }
        public ActionResult Reject(int? id)
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return View(MemberRepo.GetAllMembersContribution().Find(mbr => mbr.Userid == id));
        }
        [HttpPost]

        public JsonResult Reject(Safcontribution MemberUpdateDet)
        {

            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.rejectMemberContribution(MemberUpdateDet);
                return Json("Records Rejected successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpGet]
        public PartialViewResult SContribution()
        {

            return PartialView("_SContribution");

        }
    }
}

