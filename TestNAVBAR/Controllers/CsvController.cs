﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestNAVBAR.Controllers
{
    public class CsvController : Controller
    {
        //
        // GET: /Csv/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Upload(HttpPostedFileBase postedFile)
        {
            List<string> response = new List<string>();
            string filePath = string.Empty;
            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                //Create a DataTable.
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[14] { 
                                //new DataColumn("Userid", typeof(string)),
                                new DataColumn("Accountno", typeof(string)),
                                new DataColumn("Surname", typeof(string)),
                                new DataColumn("OtherNames", typeof(string)),
                              //  new DataColumn("Gender", typeof(string)),
                                new DataColumn("Email", typeof(string)),
                               new DataColumn("DateJoined", typeof(string)),
                               new DataColumn("Phone", typeof(string)),
                                new DataColumn("Idno", typeof(string)),
                               
                                new DataColumn("Membershipfee", typeof(string)),
                                new DataColumn("Totalshares",  typeof(string)),
                                new DataColumn("sharecontibution",typeof(string)),

                                new DataColumn("LoanLimit",typeof(string)),
                                new DataColumn("Mpesaref",typeof(string)),
                               new DataColumn("Loan", typeof(string)),
                               new DataColumn("Balance", typeof(string))});
                //new DataColumn("Expectedpaydate", typeof(string)),
                // new DataColumn("Actionedby",typeof(string)) });


                //Read the contents of CSV file.
                string csvData = System.IO.File.ReadAllText(filePath);

                //Execute a loop over the rows.
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;

                        //Execute a loop over the columns.
                        foreach (string cell in row.Split(','))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;
                        }
                    }
                }

               //string conString = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
                string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
                using (SqlConnection con = new SqlConnection(sConstr))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "dbo.tblloanusers";

                        //[OPTIONAL]: Map the DataTable columns with that of the database table
                       // response.Add("The data has been uploaded successfully.");
                        sqlBulkCopy.ColumnMappings.Add("Accountno", "Accountno");
                        sqlBulkCopy.ColumnMappings.Add("Surname", "Surname");
                        sqlBulkCopy.ColumnMappings.Add("OtherNames", "OtherNames");
                       // sqlBulkCopy.ColumnMappings.Add("Gender", "Gender");
                        sqlBulkCopy.ColumnMappings.Add("Idno", "Idno");
                        sqlBulkCopy.ColumnMappings.Add("Phone", "Phone");
                        sqlBulkCopy.ColumnMappings.Add("Email", "Email");
                        sqlBulkCopy.ColumnMappings.Add("DateJoined", "DateJoined");
                       // sqlBulkCopy.ColumnMappings.Add("Loandisbursed", "Loandisbursed");
                        sqlBulkCopy.ColumnMappings.Add("Membershipfee", "Membershipfee");
                       // sqlBulkCopy.ColumnMappings.Add("Openingshares", "Openingshares");
                        sqlBulkCopy.ColumnMappings.Add("Totalshares", "Totalshares");
                        sqlBulkCopy.ColumnMappings.Add("sharecontibution", "sharecontibution");
                        sqlBulkCopy.ColumnMappings.Add("LoanLimit", "LoanLimit");
                        sqlBulkCopy.ColumnMappings.Add("Mpesaref", "Mpesaref");
                        sqlBulkCopy.ColumnMappings.Add("Loan", "Loan");
                        sqlBulkCopy.ColumnMappings.Add("Balance", "Balance");

                        ViewData["error"] = "Data inserted successfully";
                        con.Open();
                        sqlBulkCopy.WriteToServer(dt);
                        con.Close();
                    }
                }
            }
           
            return View();
        }
        public ActionResult Statement(HttpPostedFileBase postedFile)
        {
            List<string> response = new List<string>();
            string filePath = string.Empty;
            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                //Create a DataTable.
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[7] { 
                                //new DataColumn("Userid", typeof(string)),
                                new DataColumn("Accountno", typeof(string)),
                               // new DataColumn("Phone", typeof(string)),
                              //  new DataColumn("Surname", typeof(string)),
                              //  new DataColumn("OtherNames", typeof(string)),
                              ////  new DataColumn("Gender", typeof(string)),
                              //  new DataColumn("Email", typeof(string)),
                              // new DataColumn("DateJoined", typeof(string)),
                              // new DataColumn("Phone", typeof(string)),
                              //  new DataColumn("Idno", typeof(string)),

                                new DataColumn("Membershipfee", typeof(string)),
                                //new DataColumn("Openingshares",typeof(string)),
                                 new DataColumn("Totalshares", typeof(string)),
                                new DataColumn("sharecontibution",typeof(string)),
                                 new DataColumn("Monthlycontribution", typeof(string)),
                                 new DataColumn("Mpesaref", typeof(string)),
                                 new DataColumn("Months", typeof(string)) });
            // new DataColumn("LoanLimit", typeof(string)),
            //new DataColumn("Borrowdate", typeof(string)),
            //new DataColumn("Expectedpaydate", typeof(string)),
            // new DataColumn("Actionedby",typeof(string)) });


            //Read the contents of CSV file.
            string csvData = System.IO.File.ReadAllText(filePath);

                //Execute a loop over the rows.
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;

                        //Execute a loop over the columns.
                        foreach (string cell in row.Split(','))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;
                        }
                    }
                }

                //string conString = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
                string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
                using (SqlConnection con = new SqlConnection(sConstr))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "dbo.tblloanstatements";

                        //[OPTIONAL]: Map the DataTable columns with that of the database table
                        response.Add("The data has been uploaded successfully.");
                        sqlBulkCopy.ColumnMappings.Add("Accountno", "Accountno");
                       // sqlBulkCopy.ColumnMappings.Add("Phone", "Phone");
                        //sqlBulkCopy.ColumnMappings.Add("Surname", "Surname");
                        //sqlBulkCopy.ColumnMappings.Add("OtherNames", "OtherNames");
                        // sqlBulkCopy.ColumnMappings.Add("Gender", "Gender");
                        //sqlBulkCopy.ColumnMappings.Add("Idno", "Idno");
                        //sqlBulkCopy.ColumnMappings.Add("Phone", "Phone");
                        //sqlBulkCopy.ColumnMappings.Add("Email", "Email");
                        //sqlBulkCopy.ColumnMappings.Add("DateJoined", "DateJoined");
                        // sqlBulkCopy.ColumnMappings.Add("Loandisbursed", "Loandisbursed");
                        sqlBulkCopy.ColumnMappings.Add("Membershipfee", "Membershipfee");
                       // sqlBulkCopy.ColumnMappings.Add("Openingshares", "Openingshares");
                        sqlBulkCopy.ColumnMappings.Add("Totalshares", "Totalshares");
                        sqlBulkCopy.ColumnMappings.Add("sharecontibution", "sharecontibution");
                        sqlBulkCopy.ColumnMappings.Add("Monthlycontribution", "Monthlycontribution");
                        sqlBulkCopy.ColumnMappings.Add("Months", "Months");
                        sqlBulkCopy.ColumnMappings.Add("Mpesaref", "Mpesaref");
                        //sqlBulkCopy.ColumnMappings.Add("Expectedpaydate", "Expectedpaydate");
                        //sqlBulkCopy.ColumnMappings.Add("Actionedby", "Actionedby");

                        ViewData["error"] = "Data inserted successfully";
                        con.Open();
                        sqlBulkCopy.WriteToServer(dt);
                        con.Close();
                    }
                }
            }

            return View();
        }
        public ActionResult Scams(HttpPostedFileBase postedFile)
        {
            List<string> response = new List<string>();
            string filePath = string.Empty;
            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                //Create a DataTable.
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[6] { 
                               
                                new DataColumn("name", typeof(string)),
                                new DataColumn("phoneno", typeof(string)),
                                new DataColumn("borrowdate", typeof(string)),
                                new DataColumn("loanammount",typeof(string)),
                                 new DataColumn("loanbalance", typeof(string)),
                                 new DataColumn("loandisbursedate", typeof(string)) });
               


                //Read the contents of CSV file.
                string csvData = System.IO.File.ReadAllText(filePath);

                //Execute a loop over the rows.
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;

                        //Execute a loop over the columns.
                        foreach (string cell in row.Split(','))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;
                        }
                    }
                }

                //string conString = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
                string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
                using (SqlConnection con = new SqlConnection(sConstr))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "dbo.tblscams";

                        //[OPTIONAL]: Map the DataTable columns with that of the database table
                        response.Add("The data has been uploaded successfully.");
                        sqlBulkCopy.ColumnMappings.Add("name", "name");
                        sqlBulkCopy.ColumnMappings.Add("phoneno", "phoneno");
                        sqlBulkCopy.ColumnMappings.Add("borrowdate", "borrowdate");
                        sqlBulkCopy.ColumnMappings.Add("loanammount", "loanammount");
                        sqlBulkCopy.ColumnMappings.Add("loanbalance", "loanbalance");
                        sqlBulkCopy.ColumnMappings.Add("loandisbursedate", "loandisbursedate");
                       
                       
                        ViewData["error"] = "Data inserted successfully";
                        con.Open();
                        sqlBulkCopy.WriteToServer(dt);
                        con.Close();
                    }
                }
            }

            return View();
        }
        public ActionResult phonestatement(HttpPostedFileBase postedFile)
        {
            List<string> response = new List<string>();
            string filePath = string.Empty;
            if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                filePath = path + Path.GetFileName(postedFile.FileName);
                string extension = Path.GetExtension(postedFile.FileName);
                postedFile.SaveAs(filePath);

                //Create a DataTable.
                DataTable dt = new DataTable();
                dt.Columns.AddRange(new DataColumn[2] { 
                               
                                new DataColumn("Accountno", typeof(string)),
                              

                                new DataColumn("Phone", typeof(string))
                              
                                 });
              
                string csvData = System.IO.File.ReadAllText(filePath);

                //Execute a loop over the rows.
                foreach (string row in csvData.Split('\n'))
                {
                    if (!string.IsNullOrEmpty(row))
                    {
                        dt.Rows.Add();
                        int i = 0;

                        //Execute a loop over the columns.
                        foreach (string cell in row.Split(','))
                        {
                            dt.Rows[dt.Rows.Count - 1][i] = cell;
                            i++;
                        }
                    }
                }

                //string conString = "Data Source=DESKTOP-2JM6T1Q;initial catalog=Salvons-db;user id=kip;pwd=123";
                string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
                using (SqlConnection con = new SqlConnection(sConstr))
                {
                    using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(con))
                    {
                        //Set the database table name.
                        sqlBulkCopy.DestinationTableName = "dbo.tblphone";

                        
                        response.Add("The data has been uploaded successfully.");
                        sqlBulkCopy.ColumnMappings.Add("Accountno", "Accountno");
                        
                        sqlBulkCopy.ColumnMappings.Add("Phone", "Phone");
                        
                       
                        ViewData["error"] = "Data inserted successfully";
                        con.Open();
                        sqlBulkCopy.WriteToServer(dt);
                        con.Close();
                    }
                }
            }

            return View();
        }
        public FileResult Users()
        {
            return new FilePathResult(Server.MapPath("~/Uploads/MembersRegistration.xlsx"), "application/vnd.ms-excel");
        }
        public FileResult statements()
        {
            return new FilePathResult(Server.MapPath("~/Uploads/SaccoMembersStatement.xlsx"), "application/vnd.ms-excel");
        }
    }
}
 
