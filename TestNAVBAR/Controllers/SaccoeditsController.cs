﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;
using TestNAVBAR.Repository;

namespace TestNAVBAR.Controllers
{
    public class SaccoeditsController : Controller
    {
        int logintime = 1;
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        string surname = "";
        string phone = "";
        Dictionary<string, string> where2 = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        List<string> response = new List<string>();
        //
        // GET: /SaccoAdmin/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SaccoMembers()
        {
            return View();
        }
        public JsonResult GetMembers()
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetMemberss()
        {
            SaccoRepo MemberRepo = new SaccoRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }
        public JsonResult SearchMembers()
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            return Json(MemberRepo.GetAllMember(), JsonRequestBehavior.AllowGet);

        }

        //public ActionResult SearchMembers()
        //{
        //    SaccoRepo MemberRepo = new SaccoRepo();
        //    var population = MemberRepo.GetAllMember();
        //    return Json(population, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult AddMember()
        {

            return View();
        }
        public ActionResult AddTrans()
        {

            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult Edit(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {

                _viewToDisplayData = "Edit";
                string uniqueID = id.ToString();
                sql = " select * from tblloanusers WHERE Phone= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            
                            ViewData["id"] = rd["Userid"];
                            Session["phone"] = rd["Phone"];
                            ViewData["Phone"] = rd["Phone"];
                            ViewData["idno"] = rd["idno"];
                            ViewData["Accountno"] = rd["Accountno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Loanlimit"] = rd["Loanlimit"];
                            ViewData["Lastname"] = rd["Surname"];
                            ViewData["Firstname"] = rd["OtherNames"];
                            ViewData["Airtimelimit"] = rd["Airtimelimit"];
                            ViewData["DateJoined"] = rd["DateJoined"];
                            if (rd["idfront"].ToString() == "")
                            {
                                ViewData["idfront"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idfront"] = rd["idfront"];
                            }
                            if (rd["idback"].ToString() == "")
                            {
                                ViewData["idback"] = "~/Content/images/idphoto.jpg";
                            }
                            else
                            {
                                ViewData["idback"] = rd["idback"];
                            }
                            if (rd["photo"].ToString() == "")
                            {
                                ViewData["photo"] = "~/Content/images/profil.png";
                            }
                            else
                            {
                                ViewData["photo"] = rd["photo"];
                            }


                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Edit(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            //string _action = collection[10];
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                Dictionary<string, string> data1 = new Dictionary<string, string>();

                string automatic = collection["automatic"];
                //checking for admin

                //checking for modules status

                if (automatic.ToString().Contains("true"))
                {
                    automatic = "1";
                }
                else
                {
                    automatic = "0";
                }
                //checking for maker- checker
                string _userssession = HttpContext.Session["username"].ToString();    
                data1["Rectifiedby"] = _userssession;
                data1["Phone"] = collection["Phone"];
                data1["idno"] = collection["idno"];
                data1["Surname"] = collection["Firstname"];
                data1["OtherNames"] = collection["Lastname"];
                data1["Email"] = collection["Email"];
                data1["automatic"] = automatic;
                where["Phone"] = id;
                sql1 = Logic.DMLogic.UpdateString("tblloanusers", data1, where);
                // sql = Logic.DMLogic.UpdateString("tblloandisbursement", data, where);
                flag = Blogic.RunNonQuery(sql1);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";

                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);

        }


        [HttpPost]
        public JsonResult AddMember(SafrikaMembers MbrDet)

        {
            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.AddMember(MbrDet);
                return Json("Records added Successfully.");

            }
            catch
            {
                return Json("Records not added,");
            }
        }
        [HttpPost]

        public JsonResult Delete(int id)
        {
            SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
            MemberRepo.DeleteMember(id);
            return Json("Records deleted successfully.", JsonRequestBehavior.AllowGet);
        }
        //[HttpPost]
        //public JsonResult Edit(SafrikaMembers MemberUpdateDet)
        //{

        //    try
        //    {

        //        SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
        //        MemberRepo.UpdateMember(MemberUpdateDet);
        //        return Json("Records updated successfully.", JsonRequestBehavior.AllowGet);

        //    }
        //    catch
        //    {
        //        return Json("Records not updated");
        //    }
        //}
        //public ActionResult Reject(int? id)
        //{
        //    SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
        //    return View(MemberRepo.GetAllMember().Find(mbr => mbr.Userid == id));
        //}
        [HttpPost]

        public JsonResult Reject(SafrikaMembers MemberUpdateDet)
        {

            try
            {

                SaccoAdminRepo MemberRepo = new SaccoAdminRepo();
                MemberRepo.rejectMember(MemberUpdateDet);
                return Json("Records Rejected successfully.", JsonRequestBehavior.AllowGet);

            }
            catch
            {
                return Json("Records not updated");
            }
        }
        [HttpGet]
        public PartialViewResult MemberDetails()
        {

            return PartialView("_MemberDetails");

        }

    }

}

