﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;

namespace TestNAVBAR.Controllers
{
    public class ChartOfAccountsController:Controller
    {
        string T24Response = "";
        string selectedchurchbranch = "";
        string _successfailview = "";
        string selectedchurch = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();


        public ActionResult Index()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult RegisterAccounts()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RegisterAccounts(FormCollection collection, Ledgerss mobile)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            ViewData["AccountNo"] = collection["AccountNo"];
            ViewData["ItemName"] = collection["ItemName"];
            ViewData["AccountType"] = collection["selectedAccountType"];



            if (collection["ItemName"].ToString().Length == 0)
            {
                errors.Add("You must ADD the ItemName.Its Mandatory");
            }

            if (collection["AccountNo"].ToString().Length == 0)
            {
                errors.Add("You must ADD the AccountNumber.Its Mandatory");
            }
            string codeExists = Blogic.RunStringReturnStringValue("select * from ChartOfAccounts WHERE AccountNo = '" + collection["AccountNo"].ToString().Trim() + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Please MAKE SURE the accountnumber are unique for each itemname.This record already exists for another item.");
            }
            if (collection["selectedAccountType"].ToString().Length == 0)
            {
                errors.Add("You must ADD the AccountType.Its Mandatory");
            }
            string gquery = "select max(selectcode) from ChartOfAccounts ";
            string accountno = "";
            accountno = Blogic.RunStringReturnStringValue(gquery);

            int accountnos = Convert.ToInt32(accountno);
            int code = accountnos + 1;
            if (errors.Count > 0)
            {
                _successfailview = "RegisterAccounts";
                ViewData["ItemName"] = collection["ItemName"];
                ViewData["AccountNo"] = collection["AccountNo"];
                ViewData["AccountType"] = collection["selectedAccountType"];

            }
            else
            {
                try
                {
                    DateTime getdate = DateTime.Now;
                    string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    Dictionary<string, string> data = new Dictionary<string, string>();
                    string money = mobile.Ledger.ToString();
                    if (money == "Digital")
                    {

                        money = "1";
                    }
                    else if (money == "Bridge")
                    {
                        money = "2";

                    }

                    else if (money == "others")
                    {
                        money = "3";

                    }
                    string _userssession = HttpContext.Session["username"].ToString();
                    data["ItemName"] = collection["ItemName"];
                    data["AccountNo"] = collection["AccountNo"];
                    data["AccountType"] = collection["selectedAccountType"];
                    data["createby"] = _userssession;
                    data["createddate"] = dt.ToString();
                    data["selectcode"] = code.ToString();
                    data["Typecode"] = money.ToString();

                    sql = Logic.DMLogic.InsertString("ChartOfAccounts", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("The account has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyAccounts()
        {
            return View();
        }
        [NoDirectAccessAttribute]
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult ModifyRegisteredAccount(string id)
        {
            string _viewToDisplayData = "Error";
            if (id != null)
            {
                _viewToDisplayData = "ModifyAccountsPage";
                string uniqueID = id.ToString();
                sql = " select * from ChartOfAccounts WHERE AccID= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["AccountNo"] = rd["AccountNo"];
                            ViewData["itemname"] = rd["itemname"];
                            ViewData["AccountType"] = rd["AccountType"];
                            ViewData["createby"] = rd["createby"];
                            ViewData["createddate"] = rd["Createddate"];                          
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);

        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ModifyRegisteredAccount(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();

            try
            {
                Dictionary<string, string> data = new Dictionary<string, string>();
                DateTime getdate = DateTime.Now;
                string dt = getdate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                data["ItemName"] = collection["ItemName"];
                data["AccountNo"] = collection["AccountNo"];
                data["AccountType"] = collection["selectedAccountType"];
                data["createby"] = "ADMIN";
                data["createddate"] = dt.ToString();
                where["AccID"] = id;
                sql = Logic.DMLogic.UpdateString("ChartOfAccounts", data, where);
                flag = Blogic.RunNonQuery(sql);
                if (flag)
                {
                    response.Add("The record has been modified successfully.");
                    _successfailview = "ChurchRegistrationSuccess";                  
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult DeleteAccount(string id)
        {
            bool flag = false;
            string sql = "";
            string _viewtodisplay = "ModifyAccounts";
            List<string> response = new List<string>();
            where["AccID"] = id;
            sql = Logic.DMLogic.DeleteString("ChartOfAccounts",where);
            flag = Blogic.RunNonQuery(sql);
            if (flag)
            {
                //Send SMS
                response.Add("The record has been deleted successfully.");
                _successfailview = "ChurchRegistrationSuccess";
            }
            else
            {
                _successfailview = "ErrorPage";
            }
            ViewData["Response"] = response;
            return View(_successfailview);
        }

    }

}

