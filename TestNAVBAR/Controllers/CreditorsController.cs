﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestNAVBAR.Models;

namespace TestNAVBAR.Controllers
{
    public class CreditorsController : Controller
    {
        string _successfailview = "";
        string sql = "";
        Dictionary<string, string> where = new Dictionary<string, string>();
        Dictionary<string, string> where1 = new Dictionary<string, string>();
        Dictionary<string, string> where2 = new Dictionary<string, string>();
        Dictionary<string, string> where3 = new Dictionary<string, string>();

        System.Data.SqlClient.SqlDataReader rd;
        BusinessLogic.Users userdata = new BusinessLogic.Users();
        BusinessLogic.DataConnections Blogic = new BusinessLogic.DataConnections();
        Logic.TCPLogic tcplogic = new Logic.TCPLogic();
        Logic.DMLogic bizzlogic = new Logic.DMLogic();
        BusinessLogic.HTTPSMS vsendsms = new BusinessLogic.HTTPSMS();
        //
        // GET: /Debtors/


        public ActionResult ApprovedCreditors()
        {

            return View();
        }

        public ActionResult RejectedCreditors()
        {

            return View();
        }
        public ActionResult ApprovedInvoices()
        {

            return View();
        }

        public ActionResult RejectedInvoices()
        {

            return View();
        }
        public ActionResult Invoicepaymentapproval(int id)
        {

            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "Invoicepaymentapproval";
                string uniqueID = id.ToString();
                sql = " select * from tblinvoicepayments WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Debtorsname"] = rd["DebitAccount"];
                            ViewData["Salesaccount"] = rd["CreditAccount"];
                            ViewData["Amount"] = rd["Amount"];
                            ViewData["Description"] = rd["Narration"];
                            ViewData["TransDate"] = rd["TransDate"];
                            ViewData["Disbursesource"] = rd["Disbursesource"];
                            ViewData["Vourcherno"] = rd["Vourcherno"];

                            ViewData["Actionedby"] = rd["Actionedby"];

                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Invoicepaymentapproval(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            string sql1 = "";
            bool flag1 = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> data1 = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string _action = collection[6];
            string gquery = "select max(selectcode) from ChartOfAccounts ";
            string accountno = "";
            accountno = Blogic.RunStringReturnStringValue(gquery);

            int accountnos = Convert.ToInt32(accountno);
            int code = accountnos + 1;
            if (errors.Count > 0)
            {
                _successfailview = "Invoicepaymentapproval";

                ViewData["AccountNo"] = collection["AccountNo"];


            }
            else
            {
                try
                {
                    string actionedby = collection["Actionedby"];
                    string _userssession = HttpContext.Session["username"].ToString();
                    if (actionedby != _userssession)
                    {
                        if (_action == "Approve")
                        {
                            string[] _DebtorID = collection["Debtorsname"].Split('-');
                            string DebitAccount = _DebtorID[0].ToString().Trim();
                            string Debitnames = _DebtorID[1].ToString().Trim();
                            string[] _SalesID = collection["DisburseSource"].Split('-');
                            string CreditAccount = _SalesID[0].ToString().Trim();
                            string Creditnames = _SalesID[1].ToString().Trim();
                            DateTime dts = DateTime.Now;
                            int _min = 100;
                            int _max = 100;
                            Random _rdm = new Random();
                            Random random = new Random();
                            Random generator = new Random();

                            string value = generator.Next(0, 100).ToString("D2");
                            //string refs = dat + collection["Loanref"];
                            double amount = Convert.ToDouble(collection["Amount"]);

                            string dat = "BFPN" + "/" + DebitAccount + "/" + CreditAccount + "/" + dts.Year.ToString() + "/" + dts.Month.ToString() + "/" + dts.Day.ToString() + "/" + value;

                            //string DateString = collection["Datecreated"];
                            //IFormatProvider culture = new CultureInfo("en-US", true);
                            //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy-MM-dd", culture);
                            //string[] _creditorID = collection["Debtorsname"].Split('-');
                            /// data["Accountno"] = _creditorID[0].ToString().Trim();
                            // data["Creditorname"] = _creditorID[1].ToString().Trim();
                            data1["loanid"] = dat;
                            data1["Amount"] = amount.ToString();
                            data1["DebitAccount"] = DebitAccount;
                            data1["CreditAccount"] = CreditAccount;
                            data1["Narration"] = collection["Description"];
                            data1["TransDate"] = dt;//dateVal.ToString();
                            data1["LoanType"] = "Transaction";
                            data1["CommissionAmount"] = "0";
                            data1["mpesaref"] = dat;

                            // data["Debtorsname"] = collection["Debtorsname"];

                            data["approvedby"] = _userssession;

                            //-----------------------------------------------------------
                            data["approved"] = "1";
                            where["id"] = id;
                            sql1 = Logic.DMLogic.InsertString("JournalEntries", data1);

                            flag1 = Blogic.RunNonQuery(sql1);
                            sql = Logic.DMLogic.UpdateString("tblinvoicepayments", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Invoice Payment approved successfully.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }
                        }
                        else if (_action == "Reject")
                        {
                            data["Debtorsname"] = collection["Debtorsname"];

                            data["approvedby"] = _userssession;

                            //-----------------------------------------------------------
                            data["approved"] = "2";
                            where["id"] = id;
                            sql = Logic.DMLogic.UpdateString("tblinvoicepayments", data, where);
                            flag = Blogic.RunNonQuery(sql);
                            if (flag)
                            {
                                response.Add("Invoice rejected successfully.");
                                _successfailview = "ChurchRegistrationSuccess";


                                try
                                {
                                    Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                                }

                                catch
                                {

                                }
                            }
                        }
                    }
                    else
                    {
                        response.Add("You are not authorized to approve");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        public ActionResult CreditorsInvoicepayments()
        {

            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreditorsInvoicepayments(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";

            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);


            string _startdate = collection["bp.startdate"];

            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");






            ViewData["ModelErrors"] = errors;


            if (collection["Debtorsname"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Debtors name .Its Mandatory");
            }
            if (collection["Salesaccount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Purchase Account.Its Mandatory");
            }

            if (collection["Vourcherno"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Vourcherno.Its Mandatory");
            }



            if (collection["Amount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Amount.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "DebtorsInvoicepayments";
                ViewData["Debtorsname"] = collection["Debtorsname"];





            }
            else
            {
                try
                {
                    string _userssession = HttpContext.Session["username"].ToString();
                    string[] _DebitID = collection["Debtorsname"].Split('-');
                    data["Name"] = _DebitID[1].ToString().Trim();
                    // data["Creditorname"] = _DebitID[1].ToString().Trim();
                    // data["Debtorsname"] = _creditorID[1].ToString().Trim();
                    //collection["Debtorsname"];
                    data["DebitAccount"] = collection["Salesaccount"];//collection["Debtorsname"];
                    data["CreditAccount"] = collection["Debtorsname"];//collection["Salesaccount"];
                    data["Vourcherno"] = collection["Vourcherno"];
                    data["Amount"] = collection["Amount"];
                    data["DisburseSource"] = collection["DisburseSource"];
                    data["Narration"] = collection["Description"];
                    data["Actionedby"] = _userssession;
                    // data["Name"] = collection["Debtorsname"];
                    data["TransDate"] = ftodate;
                    data["approved"] = "0";
                    data["Types"] = "2";
                    data["approvedby"] = _userssession;
                    //data["Datecreated"] = dt;


                    //-----------------------------------------------------------
                    //data["Createdby"] = _userssession;

                    sql = Logic.DMLogic.InsertString("tblinvoicepayments", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Invoce Payment submitted successfully waitng approval.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }

                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View("CreditorsInvoicepayments");
        }
        public ActionResult CreditorsInvoice()
        {
            ViewData["Vat"] = 0.16;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult CreditorsInvoice(FormCollection collection, Vat v)
        {
            string sql = "";
            bool flag = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";

            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string vt = v.Vats.ToString();

            string _startdate = collection["bp.startdate"];

            string ftodate = Convert.ToDateTime(_startdate).ToString("yyyy-MM-dd");


            string vattype = "";

            double vatrate = 0.16;

            double totalinvoice = 0;


            if (vt == "Exclusive")
            {
                totalinvoice = Convert.ToDouble(collection["Invoiceamount"]);
                vattype = "Exclusive";
                vt = "0";
            }
            else if (vt == "Inclusive")
            {
                double Totalvat = Convert.ToDouble(collection["Invoiceamount"]) * vatrate;
                double Totalinvoice = Convert.ToDouble(collection["Invoiceamount"]) + Totalvat;

                totalinvoice = Totalinvoice;
                vt = Totalvat.ToString();
                vattype = "Inclusive";
            }
            ViewData["ModelErrors"] = errors;


            if (collection["Creditorsname"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Debtors name .Its Mandatory");
            }
            if (collection["Purchaseaccount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Purchase Account.Its Mandatory");
            }
            if (collection["Vats"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Vats.Its Mandatory");
            }

            if (collection["Invoiceno"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Invoiceno.Its Mandatory");
            }



            if (collection["Invoiceamount"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Invoiceamount.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "CreditorsInvoice";
                ViewData["Creditorsname"] = collection["Creditorsname"];
                ViewData["Invoiceno"] = collection["Invoiceno"];
                ViewData["Purchaseaccount"] = collection["Purchaseaccount"];




            }
            else
            {
                try
                {
                    string _userssession = HttpContext.Session["username"].ToString();
                    //string[] _creditorID = collection["Creditorsname"].Split('-');
                    /// data["Accountno"] = _creditorID[0].ToString().Trim();
                    // data["Creditorname"] = _creditorID[1].ToString().Trim();
                    //data["Creditorsname"] = _creditorID[1].ToString().Trim();//collection["Creditorsname"];
                    data["Creditorsname"] = collection["Creditorsname"];
                    data["Purchaseaccount"] = collection["Purchaseaccount"];
                    data["Invoiceno"] = collection["Invoiceno"];
                    data["Vat"] = vatrate.ToString();
                    data["Vats"] = vattype.ToString();
                    data["Description"] = collection["Description"];
                    data["TotalInvoiceamount"] = totalinvoice.ToString();
                    data["Invoiceamount"] = collection["Invoiceamount"];
                    data["Totalvat"] = vt;
                    data["Duedate"] = ftodate;
                    data["approved"] = "0";
                    data["approvedby"] = _userssession;
                    data["Datecreated"] = dt;


                    //-----------------------------------------------------------
                    data["Createdby"] = _userssession;

                    sql = Logic.DMLogic.InsertString("tblpurchaseaccount", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Invoce submitted successfully waitng approval.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }

                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View("CreditorsInvoice");
        }
        public ActionResult Creditorsinvoiceapproval(int id)
        {
            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "Creditorsinvoiceapproval";
                string uniqueID = id.ToString();
                sql = " select * from tblpurchaseaccount WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Creditorsname"] = rd["Creditorsname"];
                            ViewData["Purchaseaccount"] = rd["Purchaseaccount"];
                            ViewData["Invoiceno"] = rd["Invoiceno"];
                            ViewData["Vat"] = rd["Vat"];
                            ViewData["Totalvat"] = rd["Totalvat"];
                            ViewData["TotalInvoiceamount"] = rd["TotalInvoiceamount"];
                            ViewData["Invoiceamount"] = rd["Invoiceamount"];
                            ViewData["Duedate"] = rd["Duedate"];
                            ViewData["Vats"] = rd["Vats"];
                            ViewData["CreatedBy"] = rd["CreatedBy"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["Description"] = rd["Description"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Creditorsinvoiceapproval(FormCollection collection, string id)
        {
            string sql = "";
            string sql1 = "";
            bool flag = false;
            bool flag1 = false;
            string sql2 = "";
            bool flag2 = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> data1 = new Dictionary<string, string>();
            Dictionary<string, string> data2 = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            string _action = collection[12];
            try
            {
                string actionedby = collection["CreatedBy"];
                string _userssession = HttpContext.Session["username"].ToString();
                if (actionedby != _userssession)
                {
                    if (_action == "Approve")
                    {       //string[] _creditorID = collection["Creditorsname"].Split('-');
                            /// data["Accountno"] = _creditorID[0].ToString().Trim();
                        // data["Creditorname"] = _creditorID[1].ToString().Trim();
                        string[] _CreditorID = collection["Creditorsname"].Split('-');
                        string DebitAccount = _CreditorID[0].ToString().Trim();
                        string Debitnames = _CreditorID[1].ToString().Trim();

                        string[] _purchaseID = collection["Purchaseaccount"].Split('-');
                        string CreditAccount = _purchaseID[0].ToString().Trim();
                        string Creditnames = _purchaseID[1].ToString().Trim();
                        DateTime dts = DateTime.Now;
                        int _min = 100;
                        int _max = 100;
                        Random _rdm = new Random();
                        Random random = new Random();
                        Random generator = new Random();

                        string value = generator.Next(0, 100).ToString("D2");
                        //string refs = dat + collection["Loanref"];
                        double amount = Convert.ToDouble(collection["Invoiceamount"]);
                        //string refs = dat + collection["Loanref"];
                        double Vatamount = Convert.ToDouble(collection["Totalvat"]);

                        string dat = "C" + "." + DebitAccount + "." + CreditAccount + "." + dts.Year.ToString() + "." + dts.Month.ToString() + "." + dts.Day.ToString() + "." + value;

                        //string DateString = collection["Datecreated"];
                        //IFormatProvider culture = new CultureInfo("en-US", true);
                        //DateTime dateVal = DateTime.ParseExact(DateString, "yyyy-MM-dd", culture);
                        //string[] _creditorID = collection["Debtorsname"].Split('-');
                        /// data["Accountno"] = _creditorID[0].ToString().Trim();
                        // data["Creditorname"] = _creditorID[1].ToString().Trim();
                        data1["loanid"] = dat;
                        data1["Amount"] = amount.ToString();
                        data1["DebitAccount"] = DebitAccount;
                        data1["CreditAccount"] = CreditAccount;
                        data1["Narration"] = collection["Description"];
                        data1["TransDate"] = collection["Duedate"];//dateVal.ToString();
                        data1["LoanType"] = "Transaction";
                        data1["CommissionAmount"] = "0";
                        data1["mpesaref"] = dat;


                        data2["loanid"] = dat;
                        data2["Amount"] = Vatamount.ToString();
                        data2["DebitAccount"] = DebitAccount;
                        data2["CreditAccount"] = "C010";
                        data2["Narration"] = collection["Description"];
                        data2["TransDate"] = collection["Duedate"]; ;//dateVal.ToString();
                        data2["LoanType"] = "Transaction";
                        data2["CommissionAmount"] = "0";
                        data2["mpesaref"] = dat;


                        data["Creditorsname"] = collection["Creditorsname"];

                        data["approvedby"] = _userssession;

                        //-----------------------------------------------------------
                        data["approved"] = "1";
                        where["id"] = id;
                        sql1 = Logic.DMLogic.InsertString("JournalEntries", data1);

                        flag1 = Blogic.RunNonQuery(sql1);
                        sql2 = Logic.DMLogic.InsertString("JournalEntries", data2);

                        flag2 = Blogic.RunNonQuery(sql2);
                        sql = Logic.DMLogic.UpdateString("tblpurchaseaccount", data, where);
                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            response.Add("Invoice approved successfully.");
                            _successfailview = "ChurchRegistrationSuccess";


                            try
                            {
                                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                            }

                            catch
                            {

                            }
                        }
                    }
                    else if (_action == "Reject")
                    {
                        data["Debtorsname"] = collection["Debtorsname"];

                        data["approvedby"] = _userssession;

                        //-----------------------------------------------------------
                        data["approved"] = "2";
                        where["id"] = id;
                        sql = Logic.DMLogic.UpdateString("tblsalesaccount", data, where);
                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            response.Add("Invoice rejected successfully.");
                            _successfailview = "ChurchRegistrationSuccess";


                            try
                            {
                                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                            }

                            catch
                            {

                            }
                        }
                    }
                }
                else
                {
                    response.Add("You are not authorized to approve");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }

            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        public ActionResult Creditorsapproval(int id)
        {
            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "Creditorsapproval";
                string uniqueID = id.ToString();
                sql = " select * from tblcreditors WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Creditorname"] = rd["Creditorname"];
                            ViewData["Mobileno"] = rd["Mobileno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Address"] = rd["Address"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Contactperson"] = rd["Contactperson"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["CreateBy"] = rd["CreateBy"];
                            ViewData["AccountNo"] = rd["AccountNo"];
                            
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Creditorsapproval(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
          
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
          
            DateTime getdate = DateTime.Now;
            
            if (errors.Count > 0)
            {
                _successfailview = "Creditorsapproval";

                ViewData["AccountNo"] = collection["AccountNo"];


            }
            else
            {
                try
                {
                    string actionedby = collection["CreateBy"];
                    string _userssession = HttpContext.Session["username"].ToString();
                    if (actionedby != _userssession)
                    {
                        data["Creditorname"] = collection["Creditorname"];
                        data["Accountno"] = collection["Accountno"];
                        data["approvedby"] = _userssession;

                        //-----------------------------------------------------------
                        data["approved"] = "1";
                        where["id"] = id;

                        
                        sql = Logic.DMLogic.UpdateString("tblcreditors", data, where);
                        flag = Blogic.RunNonQuery(sql);
                        if (flag)
                        {
                            response.Add("Creditor approved successfully.");
                            _successfailview = "ChurchRegistrationSuccess";


                            try
                            {
                                Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                            }

                            catch
                            {

                            }
                        }
                    }
                    else
                    {
                        response.Add("You are not authorized to approve");
                        _successfailview = "ChurchRegistrationSuccess";
                    }
                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        public ActionResult RejectCreditors(int id)
        {
            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "RejectCreditors";
                string uniqueID = id.ToString();
                sql = " select * from tblcreditors WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Creditorname"] = rd["Creditorname"];
                            ViewData["Mobileno"] = rd["Mobileno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Address"] = rd["Address"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Contactperson"] = rd["Contactperson"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["CreateBy"] = rd["CreateBy"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult RejectCreditors(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            try
            {
                string actionedby = collection["CreateBy"];
                string _userssession = HttpContext.Session["username"].ToString();
                if (actionedby != _userssession)
                {
                    data["Creditorname"] = collection["Creditorname"];

                    data["approvedby"] = _userssession;

                    //-----------------------------------------------------------
                    data["approved"] = "2";
                    where["id"] = id;
                    sql = Logic.DMLogic.UpdateString("tblcreditors", data, where);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Creditor rejected successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }
                }
                else
                {
                    response.Add("You are not authorized to approve");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }

            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }

        public ActionResult ReapproveCreditors(int id)
        {
            string _viewToDisplayData = "Error";
            if (id != 0)
            {
                _viewToDisplayData = "ReapproveCreditors";
                string uniqueID = id.ToString();
                sql = " select * from tblcreditors WHERE id= '" + uniqueID + "'";
                rd = Blogic.RunQueryReturnDataReader(sql);
                using (rd)
                {
                    if (rd != null && rd.HasRows)
                    {
                        while (rd.Read())
                        {
                            ViewData["Creditorname"] = rd["Creditorname"];
                            ViewData["Mobileno"] = rd["Mobileno"];
                            ViewData["Email"] = rd["Email"];
                            ViewData["Address"] = rd["Address"];
                            ViewData["Town"] = rd["Town"];
                            ViewData["Contactperson"] = rd["Contactperson"];
                            ViewData["Datecreated"] = rd["Datecreated"];
                            ViewData["CreateBy"] = rd["CreateBy"];
                        }
                    }
                }
            }
            else
            {
                //report the record selection criteria was not right.
                _viewToDisplayData = "ErrorPage";
            }

            return View(_viewToDisplayData);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult ReapproveCreditors(FormCollection collection, string id)
        {
            string sql = "";
            bool flag = false;
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

            try
            {
                string actionedby = collection["CreateBy"];
                string _userssession = HttpContext.Session["username"].ToString();
                if (actionedby != _userssession)
                {
                    data["Creditorname"] = collection["Creditorname"];

                    data["approvedby"] = _userssession;

                    //-----------------------------------------------------------
                    data["approved"] = "0";
                    where["id"] = id;
                    sql = Logic.DMLogic.UpdateString("tblcreditors", data, where);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Creditor Reapproved successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }
                }
                else
                {
                    response.Add("You are not authorized to approve");
                    _successfailview = "ChurchRegistrationSuccess";
                }
            }
            catch
            {
                _successfailview = "ErrorPage";
            }

            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View(_successfailview);
        }
        public ActionResult AddCreditors()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult AddCreditors(FormCollection collection)
        {
            string sql = "";
            bool flag = false;
            string sql1 = "";
            bool flag1 = false;
            string _viewtoDisplay = "";
            string _action = collection[2];
            string _action_w = collection[3];
            string _SQLSelectionCriteria = "";
            List<string> errors = new List<string>();
            List<string> response = new List<string>();
            Dictionary<string, string> data = new Dictionary<string, string>();
            Dictionary<string, string> data1 = new Dictionary<string, string>();
            DateTime getdate = DateTime.Now;
            string dt = getdate.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);


            ViewData["ModelErrors"] = errors;

            if (collection["AccountNo"].ToString().Length == 0)
            {
                errors.Add("You must ENTER Unique AccountNo.Its Mandatory");
            }
            string codeExist = Blogic.RunStringReturnStringValue("SELECT * FROM ChartOfAccounts where AccountNo='" + collection["AccountNo"] + "'");

            if (codeExist != "" && codeExist != "0")
            {
                errors.Add("Account already exists");

            }
           
            string gquery = "select max(selectcode) from ChartOfAccounts ";
            string accountno = "";
            accountno = Blogic.RunStringReturnStringValue(gquery);

            int accountnos = Convert.ToInt32(accountno);
            int code = accountnos + 1;
            string codeExists = Blogic.RunStringReturnStringValue("SELECT * FROM tblCreditors where Mobileno='" + collection["Mobileno"] + "'");

            if (codeExists != "" && codeExists != "0")
            {
                errors.Add("Creditor already exists");

            }
            if (collection["Mobileno"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Mobileno .Its Mandatory");
            }
            if (collection["Creditorname"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Creditorname.Its Mandatory");
            }
            if (collection["Email"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Email.Its Mandatory");
            }

            if (collection["Town"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Town.Its Mandatory");
            }

            if (collection["Contactperson"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Contactperson.Its Mandatory");
            }

            if (collection["Address"].ToString().Length == 0)
            {
                errors.Add("You must ENTER the Address.Its Mandatory");
            }
            if (errors.Count > 0)
            {
                _successfailview = "AddCreditors";
                ViewData["Address"] = collection["Address"];
                ViewData["Contactperson"] = collection["Contactperson"];
                ViewData["Town"] = collection["Town"];
                ViewData["Email"] = collection["Email"];
                ViewData["Mobileno"] = collection["Mobileno"];
                ViewData["Creditorname"] = collection["Creditorname"];


            }
            else
            {
                try
                {
                    string _userssession = HttpContext.Session["username"].ToString();

                    data1["ItemName"] = collection["Creditorname"];
                    data1["AccountNo"] = collection["AccountNo"];
                    data1["AccountType"] = collection["selectedAccountType"];
                    data1["createby"] = _userssession;
                    data1["createddate"] = dt.ToString();
                    data1["selectcode"] = code.ToString();
                    data1["typecode"] = "3";

                    data["Creditorname"] = collection["Creditorname"];
                    data["Accountno"] = collection["Accountno"];
                    data["Mobileno"] = collection["Mobileno"];
                    data["Email"] = collection["Email"];
                    data["Address"] = collection["Address"];
                    data["Town"] = collection["Town"];
                    data["Contactperson"] = collection["Contactperson"];
                    data["approved"] = "0";
                    data["approvedby"] = _userssession;
                    data["Datecreated"] = dt;
                    //-----------------------------------------------------------
                    data["CreateBy"] = _userssession;
                    sql1 = Logic.DMLogic.InsertString("ChartOfAccounts", data1);
                    flag1 = Blogic.RunNonQuery(sql1);
                    sql = Logic.DMLogic.InsertString("tblcreditors", data);
                    flag = Blogic.RunNonQuery(sql);
                    if (flag)
                    {
                        response.Add("Creditor has been created successfully.");
                        _successfailview = "ChurchRegistrationSuccess";


                        try
                        {
                            Utilities.Users.ManageAuditTrail(MyGlobalVariables.username, (string)HttpContext.Session["mobile1"], "MAIN BRANCH", "created loan type: " + collection["loantype"] + " " + (string)HttpContext.Session["username"], "LOGIN", (string)HttpContext.Session["UserHostAddress"], (string)HttpContext.Session["UserHostName"]);
                        }

                        catch
                        {

                        }
                    }

                }
                catch
                {
                    _successfailview = "ErrorPage";
                }
            }
            ViewData["ModelErrors"] = errors;
            ViewData["Response"] = response;
            return View("AddCreditors");
        }
    }
}

