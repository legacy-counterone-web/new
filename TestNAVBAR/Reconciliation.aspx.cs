﻿using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace TestNAVBAR.Models
{
    public partial class Reconciliation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            string SQL = "select  a.borrowerphone,b.transactioncode,b.paymentdate,B.transamount from loanapplications a,paymenthistory b WHERE a.loanref = b.loanref  and  " + System.Web.HttpContext.Current.Session["SQLSelectionCriteria"].ToString() + " order by b.paymentdate asc ";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("tbl");
                        da.Fill(dt);
                    }
                }
            }

            ReportDocument Reconciliation = new ReportDocument();
            string reportPath = Server.MapPath("~/aspnet_client/Reconciliation.rpt");
            Reconciliation.Load(reportPath);
            Reconciliation.SetDataSource(dt);
            ReconciliationViewer.ReportSource = Reconciliation;
            ReconciliationViewer.HasToggleGroupTreeButton = false;
            ReconciliationViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;

        }
    }
}