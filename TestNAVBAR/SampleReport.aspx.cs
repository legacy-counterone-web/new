﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
namespace TestNAVBAR
{
    public partial class SampleReport : System.Web.UI.Page
    {
        ReportDocument Report = new ReportDocument();
        protected void Page_Load(object sender, EventArgs e)
        {
            //string reportPath = System.Web.HttpContext.Current.Server.MapPath("~/aspnet_client/LoanUsersData.rpt");
            DataTable dt = new DataTable();
            string _currentuser = "Njoro";
            string SQL = "select  top 15 loanref,borrowerphone,'Jordin' as loanamount,'"+_currentuser+"' as approvername,loanbalance,loanamount from  loanapplications";
            string sConstr = ConfigurationSettings.AppSettings["ApplicationServices"];
            // string sConstr = ConfigurationManager.ConnectionStrings["ApplicationServices"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sConstr))
            {
                using (SqlCommand comm = new SqlCommand(SQL, conn))
                {
                    conn.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(comm))
                    {
                        dt = new DataTable("spr");
                        da.Fill(dt);
                    }
                }
            }
            ReportDocument sReport = new ReportDocument();
            string reportPath = System.Web.HttpContext.Current.Server.MapPath("~/aspnet_client/SampleReport.rpt");
            sReport.Load(reportPath);
            sReport.SetDataSource(dt);
            SampleReportViewer.ReportSource = sReport;
            SampleReportViewer.HasToggleGroupTreeButton = false;
            SampleReportViewer.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            SampleReportViewer.HasRefreshButton = true;

            SampleReportViewer.HasPageNavigationButtons = true;
            SampleReportViewer.RefreshReport();
        }

    }
}