﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.InteropServices;

namespace MySecurity
{
    [ClassInterface(ClassInterfaceType.None),
    Guid("CB08030C-3401-4d9b-BD05-77B72F8D8AC3")]
    public class CryptoFactory :ICryptoFactory 
    {
        public CryptoFactory()
        {
        }
        // Methods
        public ICrypto MakeCryptographer()
        {
            return new Rijndael();
        }
    }

    [Guid("D271C496-3AEA-4f48-8544-537FE0AF779E")]
    public interface ICryptoFactory
    {
        // Methods
        [DispId(1)]
        ICrypto MakeCryptographer();
    }


    [Guid("85C12014-D7CF-484c-A623-77D9F69F46CB")]
    public interface ICrypto
    {
        // Methods
        [DispId(1)]
        int BlockSize();
        [DispId(2)]
        string Decrypt(string data);
        [DispId(3)]
        string Encrypt(string data);
        [DispId(4)]
        int KeySize();
    }

    [ClassInterface(ClassInterfaceType.None),
    Guid("5C2E0233-A93F-4651-9A7F-A7CC90D2B426")]
    public class Rijndael : ICrypto
    {
        public Rijndael()
        {
        }
        // Fields
        private byte[] _iv = new byte[] { 0x53, 0x47, 0x1a, 0x3a, 0x36, 0x23, 0x16, 11, 0x53, 0x47, 0x1a, 0x3a, 0x36, 0x23, 0x16, 11 };
        private byte[] _key = new byte[] { 
        0x84, 0x2a, 0x35, 0x7c, 0x4b, 0x38, 0x57, 0x26, 9, 10, 0xa1, 0x84, 0xb7, 0x5b, 0x69, 0x10, 
        0x75, 0xda, 0x95, 230, 0xdd, 0xd4, 0xeb, 0x40
     };

        // Methods
        public int BlockSize()
        {
            RijndaelManaged managed = new RijndaelManaged();
            return managed.BlockSize;
        }

        public string Decrypt(string data)
        {
            string str = "";
            try
            {
                byte[] buffer = Convert.FromBase64String(data);
                MemoryStream stream2 = new MemoryStream(buffer, 0, buffer.Length);
                RijndaelManaged managed = new RijndaelManaged();
                CryptoStream stream = new CryptoStream(stream2, managed.CreateDecryptor(this._key, this._iv), CryptoStreamMode.Read);
                str = new StreamReader(stream).ReadToEnd();
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;

            }
            return str;
        }

        public string Encrypt(string data)
        {
            string str = "";
            try
            {
                byte[] bytes = new UTF8Encoding().GetBytes(data);
                MemoryStream stream2 = new MemoryStream();
                RijndaelManaged managed = new RijndaelManaged();
                CryptoStream stream = new CryptoStream(stream2, managed.CreateEncryptor(this._key, this._iv), CryptoStreamMode.Write);
                stream.Write(bytes, 0, bytes.Length);
                stream.FlushFinalBlock();
                str = Convert.ToBase64String(stream2.GetBuffer(), 0, (int)stream2.Length);
            }
            catch (Exception exception1)
            {
                Exception exception = exception1;
            }
            return str;
        }

        public int KeySize()
        {
            RijndaelManaged managed = new RijndaelManaged();
            return managed.KeySize;
        }
    }


}


